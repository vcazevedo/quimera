//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.
//

#ifndef __CHIMERA_INTERSECTED_LINE_MESH_H_
#define __CHIMERA_INTERSECTED_LINE_MESH_H_
#pragma once

#include "Mesh/LineMesh.h"

namespace Chimera {

	namespace Meshes {

		template <class VectorT>
		class IntersectedLineMesh : public LineMesh<VectorT> {

		public:
			#pragma region Constructors
			/** Active constructor. Remember to pass the location correctly for 2-D (XY face) if needed, since initializing
				regular grid patches may have issues with wrong plane locations. */
			IntersectedLineMesh(const typename IntersectedLineMesh::params_t &lineMeshParams, dimensions_t gridDimensions, Scalar gridDx, cellLocation_t planeOrientation = XYPlane, bool perturbPoints = true)
				: IntersectedLineMesh::LineMesh(), m_regularGridPatches(gridDimensions) {
				m_planeOrientation = planeOrientation;
				this->m_params = lineMeshParams;
				m_gridDx = gridDx;
				this->m_hasUpdated = false;
				m_gridDimensions = gridDimensions;
				m_perturbPoints = perturbPoints;

				if (this->m_params.initialPoints.empty()) {
					throw std::logic_error("IntersectedLineMesh: not able to create line without points specification.");
				}

				if (this->m_params.extrudeAlongNormalWidth != 0.0f) {
					this->extrudePointsAlongNormals(this->m_params.extrudeAlongNormalWidth, this->m_params.initialPoints);
					this->m_isClosedMesh = true;
				}
				else {
					if (!lineMeshParams.initialPoints.empty())
						this->m_isClosedMesh = lineMeshParams.initialPoints.back() == lineMeshParams.initialPoints.front();
				}

				//All vertices initialized here are geometry vertices
				this->initializeVertices();
				m_geometryVertices = this->m_vertices;

				/** Initializing geometry edges */
				if (lineMeshParams.initialEdges.empty()) {
					for (uint i = 0; i + 1< m_geometryVertices.size(); ++i) {
						m_geometryEdges.emplace_back(m_geometryVertices[i], m_geometryVertices[i + 1]);
					}
					if (this->m_isClosedMesh && m_geometryVertices.size() >= 3) {
						m_geometryEdges.emplace_back(m_geometryVertices.back(), m_geometryVertices.front());
					}
				}
				else {
					for (pair<uint, uint> e : lineMeshParams.initialEdges) {
						m_geometryEdges.emplace_back(m_geometryVertices[e.first], m_geometryVertices[e.second]);
					}
				}

				if (perturbPoints) {
					perturbOnGridLineEdges(m_gridDx);
				}

				for (auto edge : m_geometryEdges) {
					vector<pair<DoubleScalar, VectorT>> crossings = computeGridCrossings(edge);
					addEdgeWithCrossings(edge, crossings);
				}

				for (shared_ptr<Vertex<VectorT>> pV : m_geometryVertices) {
					OwnConnectedStructure<Edge<VectorT>>* pConnectedEdges = dynamic_cast<OwnConnectedStructure<Edge<VectorT>>*>(pV.get());
					if (pConnectedEdges->getConnectedStructures().size() == 1) {
						this->addBorderVertex(pV);
					}
				}

				initializeRegularGridPatches();

				this->updateCentroid();
				this->initializeVertexNormals();
			}

			/** Passive constructor. Remember to pass the location correctly for 2-D (XY face) if needed, since initializing
			regular grid patches may have issues with wrong plane locations. */
			IntersectedLineMesh(const vector<shared_ptr<Vertex<VectorT>>> &vertices, const vector<shared_ptr<Edge<VectorT>>> &edges, dimensions_t gridDimensions,
								Scalar gridDx, cellLocation_t planeOrientation = XYPlane, bool perturbPoints = true) :
				IntersectedLineMesh::LineMesh(vertices, edges), m_regularGridPatches(gridDimensions) {
				m_gridDx = gridDx;
				this->m_hasUpdated = false;
				m_planeOrientation = planeOrientation;
				m_gridDimensions = gridDimensions;

				for (shared_ptr<Edge<VectorT>> e : this->m_elements) {
					m_geometryEdges.emplace_back(e->getVertex1(), e->getVertex2());
				}

				if (this->m_elements.back()->getVertices().second->getID() == this->m_elements.front()->getVertices().first->getID()) {
					this->m_isClosedMesh = true;
				}

				initializeRegularGridPatches();
				this->updateCentroid();
				this->initializeVertexNormals();
			}
			#pragma endregion

			#pragma region AccessFunctions
			Scalar getGridDx() const {
				return m_gridDx;
			}

			const dimensions_t & getGridDimensions() const {
				return m_gridDimensions;
			}

			const vector<uint> & getPatchesIndices(int i, int j) const {
				return m_regularGridPatches(i, j);
			}

			const vector<shared_ptr<Vertex<VectorT>>> & getIntersectionVertices() const {
				return m_intersectedVertices;
			}

			const vector<shared_ptr<Vertex<VectorT>>> & getGeometryVertices() const {
				return m_geometryVertices;
			}

			#pragma endregion

			#pragma region Functionalities
			/** Updates internal structures with a newly computed pose */
			virtual void updatePose(const Pose<VectorT> &newPose) override;
			#pragma endregion
		protected:
			#pragma region ClassMembers
			/** Location to compute crossings in 3-D. 2-D meshes will always be on the XY plane.*/
			cellLocation_t m_planeOrientation;

			/** Grid settings */
			Scalar m_gridDx;
			dimensions_t m_gridDimensions;

			/** Quick access vector containing all vertices that are onEdge (or onGrid on degenerate cases). */
			vector<shared_ptr<Vertex<VectorT>>> m_intersectedVertices;

			/** Auxiliary structure used for organizing line-patches inside a regular grid. It stores local elements that are
				inside a regular grid that this line mesh is embedded. This. if it stores a greater than zero vector inside the
				array, it means that regular-grid location contains a part of the line mesh (the specific part is indicated by
				the indices) */
			Array2D<vector<uint>> m_regularGridPatches;

			/** Quick access vector containing all original geometry vertices. */
			vector<shared_ptr<Vertex<VectorT>>> m_geometryVertices;

			/** Information on original geometry edges */
			vector<pair<shared_ptr<Vertex<VectorT>>, shared_ptr<Vertex<VectorT>>>> m_geometryEdges;

			/** Perturb line points to avoid hard to deal cases*/
			bool m_perturbPoints;
			#pragma endregion

			#pragma region InitializationFunctions
			virtual void initializeRegularGridPatches();
			#pragma endregion

			#pragma region PrivateFunctionalities
			/** Goes through sub-elements and removes the connectivity to structures that might be deleted */
			void clearConnectedStructures();

			/** For a given vertex pair that represents an edge, computes the points that it intersected on the grid.
				Returns the ordered list of points, organized by the distance from the interesected points to the
				first edge point. */
			vector<pair<DoubleScalar, VectorT>> computeGridCrossings(const pair<shared_ptr<Vertex<VectorT>>, shared_ptr<Vertex<VectorT>>> &edge);

			/** Creates multiple sub edges from an specified edge according to the crossings computed */
			void addEdgeWithCrossings(const pair<shared_ptr<Vertex<VectorT>>, shared_ptr<Vertex<VectorT>>> &edge, vector<pair<DoubleScalar, VectorT>> &crossings);


			virtual void perturbOnGridLineEdges(Scalar dx);
			#pragma endregion
		};
	}
}

#endif
