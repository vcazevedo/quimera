//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#pragma once
#ifndef __CHIMERA_HALF_VOLUME_H_
#define __CHIMERA_HALF_VOLUME_H_

#pragma once

#include "Mesh/Cell.h"

namespace Chimera {

	namespace Meshes {

		template <class VectorType>
		class Volume;

		template <class VectorType>
		class HalfVolume {
			private:

				#pragma region Constructors
				/** HalfVolume constructor is private! We only want shared_ptr instances of half-volumes since the complicated
					memory management that this class entails. */
				HalfVolume(const vector<shared_ptr<HalfCell<VectorType>>> &halfCells, weak_ptr<Volume<VectorType>> pVolume) : m_halfCells(halfCells) {
					m_pParentVolume = pVolume;
					m_ID = m_currID++;
				}

			public:
				/** True class constructor: creates a shared pointer and redirects it to HalfVolume constructors. */
				template <class ... Args>
				static shared_ptr<HalfVolume<VectorType>> create(Args&& ... args) {
					//TODO: Replace with make_shared to avoid unnecessary copy
					return shared_ptr<HalfVolume<VectorType>>(new HalfVolume(std::forward<Args>(args)...));
				}

				static void resetIDs() {
					m_currID = 0;
				}
				#pragma endregion


				#pragma region Access Functions
				uint getID() const {
					return m_ID;
				}

				const vector<shared_ptr<HalfCell<VectorType>>> & getHalfCells() const {
					return m_halfCells;
				}

				shared_ptr<Volume<VectorType>> getVolume() {
					return m_pParentVolume.lock();
				}
				#pragma endregion

				#pragma region Operators
				FORCE_INLINE bool operator==(const HalfVolume<VectorType> &rhs) const {
					return m_ID == rhs.getID();
				}
				#pragma endregion

			protected:

			#pragma region ClassMembers
			/** Halfcells */
			vector<shared_ptr<HalfCell<VectorType>>> m_halfCells;

			/** Parent volume */
			weak_ptr<Volume<VectorType>> m_pParentVolume;

			/** ID vars */
			uint m_ID;
			static uint m_currID;
			#pragma endregion
		};

		template <class VectorType>
		unsigned int HalfVolume<VectorType>::m_currID = 0;

	}
}

#pragma once
#endif
