//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef _CHIMERA_POSE_H_
#define _CHIMERA_POSE_H_
#pragma once

#include "Utils/Utils.h"
#include "Mesh/Vertex.h"
#include "Math/MathUtilsCore.h"

namespace Chimera {

	namespace Meshes {

		/** Encodes the pose for an object assuming a rigid frame*/
		template <class VectorT, bool isVector2>
		class PoseBase {
		public:

			#pragma region Constructors
			PoseBase() {
			
			}

			PoseBase(const VectorT &centroid) : m_centroid(centroid) {

			}
			#pragma endregion

			#pragma region Functionalities
			/** Updates a pose to a new pose, updating the vertices that this pose is affecting. */
			void update(const PoseBase<VectorT, isVector2> &pose, const vector <shared_ptr<Vertex<VectorT>>> &vertices) {
				updatePositions(pose, vertices);
			}
			#pragma endregion

			#pragma region AccessFunctions
			const VectorT & getCentroid() const {
				return m_centroid;
			}
			/** Only sets centroid for late initialization. Does not updates mesh vertices! */
			void setCentroid(const VectorT &centroid) {
				m_centroid = centroid;
			}
			#pragma endregion

		protected:
			
			#pragma region ClassMembers
			VectorT m_centroid;
			#pragma endregion

			#pragma region PrivateFunctionalities
			/** Updates Vertex Positions */
			virtual void updatePositions(const PoseBase<VectorT, isVector2> &pose, const vector <shared_ptr<Vertex<VectorT>>> &vertices);

			/** Applies the rotation, and it is dependent on the pose being 2-D or 3-D. */
			virtual void applyRotation(const PoseBase<VectorT, isVector2> &pose, const vector<shared_ptr<Vertex<VectorT>>> &vertices) = 0;
			#pragma endregion
		};


		template <class VectorT, bool isVector2>
		class PoseT : public PoseBase <VectorT, isVector2> {
			public:
			
			PoseT() {

			}
		};

		/** Encodes 2-D pose */
		template <class VectorT>
		class PoseT<VectorT, true> : public PoseBase<VectorT, true> {
		public:
			#pragma region Constructors
			/** Default constructor: rotation must be passed here as zero, since its not initially to vertices on the 
				constructor. It only updates vertices states by the update function. */
			PoseT(const VectorT &centroid, Scalar angle = 0) : PoseT::PoseBase(centroid),
				m_rotation(angle) {

			}

			/** Lazy constructor*/
			PoseT() {
				m_rotation = 0;
			}
			#pragma endregion
			#pragma region AccessFunctions
			const DoubleScalar & getRotation() const {
				return m_rotation;
			}
			/** Only sets the rotation for late initialization. Does not updates mesh vertices! */
			void setRotation(DoubleScalar rotation) {
				m_rotation = rotation;
			}
			#pragma endregion
		protected:

			#pragma region ClassMembers
			DoubleScalar m_rotation;
			#pragma endregion

			#pragma region PrivateFunctionalities
			/** Applies a 2-D rotation to target vertices considering the differences between this pose and the 
				argument pose. */
			virtual void applyRotation(const PoseBase<VectorT, true> &pose, const vector<shared_ptr<Vertex<VectorT>>> &vertices);
			#pragma endregion
		};

		/** Encodes 3-D pose */
		template <class VectorT>
		class PoseT<VectorT, false> : public PoseBase<VectorT, false> {
		public:
			#pragma region Constructors
			PoseT(const VectorT &centroid, const VectorT &axis, Scalar angle) : PoseT::PoseBase(centroid),
				m_rotation(axis, angle) {

			}
			/** Lazy constructor*/
			PoseT() {
				
			}

			#pragma endregion
		
			#pragma region AccessFunctions
			//const VectorT & getCentroid() const {
			//	return m_centroid;
			//}

			const Quaternion<VectorT> & getRotation() const {
				return m_rotation;
			}

			/** Only sets the rotation for late initialization. Does not updates mesh vertices! */
			void setRotation(const Quaternion<VectorT> & rotation) {
				m_rotation = rotation;
			}
			#pragma endregion
		
		protected:

			#pragma region ClassMembers
			Quaternion<VectorT> m_rotation;
			#pragma endregion

			#pragma region PrivateFunctionalities
			/** Applies a 3-D rotation to target vertices considering the differences between this pose and the 
				argument pose. */
			virtual void applyRotation(const PoseBase<VectorT, false> &pose, const vector<shared_ptr<Vertex<VectorT>>> &vertices);
			#pragma endregion
		};

		template <typename VectorT>
		using Pose = PoseT<VectorT, Core::isVector2<VectorT>::value>;
	}
}

#endif