//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_HALFEDGE_H_
#define __CHIMERA_HALFEDGE_H_

#pragma once

#include "ChimeraCore.h"
#include "MeshesCoreDefs.h"
#include "Mesh/Vertex.h"

namespace Chimera {
	namespace Meshes {

		/** Class Forward Declarations */
		template <class VectorType>
		class Edge;
		template <class VectorType>
		class HalfCell;
		template <class VectorType>
		class HalfVoxel;

		/** Halfedge structure: does not stores centroids or vertices relative to edges. It only indicates
		* direction by the use of vertices indices. If one wants to access concrete attributes, it should do it
		* through Edge class. */
		template <class VectorType>
		class HalfEdge : public OwnConnectedStructure<HalfCell<VectorType>>, public OwnConnectedStructure<HalfVoxel<VectorType>> {
		public:
			#pragma region Constructors
			private:
			/** HalfEdge constructor is private! We only want shared_ptr instances of halfedges since the complicated 
				memory management that this class entails. */
			HalfEdge(shared_ptr<Vertex<VectorType>> pV1, shared_ptr<Vertex<VectorType>> pV2, weak_ptr<Edge<VectorType>> pEdge) {
				m_vertices.first = pV1;
				m_vertices.second = pV2;
				m_normal = -(pV2->getPosition() - pV1->getPosition()).perpendicular().normalized();
				m_pEdge = pEdge;
				m_ID = m_currID++;
			}

			HalfEdge(const pair<shared_ptr<Vertex<VectorType>>, shared_ptr<Vertex<VectorType>>> &pointIndices, weak_ptr<Edge<VectorType>> pEdge) :
				m_vertices(pointIndices) {
				m_normal = -(m_vertices.second->getPosition() - m_vertices.first->getPosition()).perpendicular().normalized();
				m_pEdge = pEdge;
				m_ID = m_currID++;
			}

			public:
			
			/** True class constructor: creates a shared pointer and redirects it to half-edge constructors.*/
			template <class ... Args>
			static shared_ptr<HalfEdge<VectorType>> create(Args&& ... args) {
				//TODO: Replace with make_shared to avoid unnecessary copy
				return shared_ptr<HalfEdge<VectorType>>(new HalfEdge(std::forward<Args>(args)...));
			}

			#pragma endregion
			static void resetIDs() {
				m_currID = 0;
			}

			#pragma region AccessFunctions
			uint getID() const {
				return m_ID;
			}

			shared_ptr<Edge<VectorType>> getEdge() {
				return m_pEdge.lock();
			}

			const pair<shared_ptr<Vertex<VectorType>>, shared_ptr<Vertex<VectorType>>> & getVertices() const {
				return m_vertices;
			}

			pair<shared_ptr<Vertex<VectorType>>, shared_ptr<Vertex<VectorType>>> & getVertices() {
				return m_vertices;
			}

			void setFirstVertex(shared_ptr<Vertex<VectorType>> pVertex) {
				m_vertices.first = pVertex;
			}

			void setSecondVertex(shared_ptr<Vertex<VectorType>> pVertex) {
				m_vertices.second = pVertex;
			}

			void setLocation(halfEdgeLocation_t halfEdgeLocation) {
				m_location = halfEdgeLocation;
			}

			halfEdgeLocation_t getLocation() const {
				return m_location;
			}

			void setNormal(const VectorType &edgeNormal) {
				m_normal = edgeNormal;
			}

			const VectorType & getNormal() const {
				return m_normal;
			}

			bool isVisited() const {
				return m_visited;
			}

			void setVisited(bool visited) {
				m_visited = visited;
			}
			#pragma endregion

			#pragma region Operators
			FORCE_INLINE bool operator==(const HalfEdge<VectorType> &rhs) const {
				return m_ID == rhs.getID();
			}
			#pragma endregion

		protected:
			/** Half edges own vertices */
			pair<shared_ptr<Vertex<VectorType>>, shared_ptr<Vertex<VectorType>>> m_vertices;
			
			/** Half edges do not own references to upper level structures*/
			weak_ptr<Edge<VectorType>> m_pEdge;

			VectorType m_normal;

			// This is an accelerator structure that facilitates 2-D cut-cells creation
			halfEdgeLocation_t m_location;

			/** ID vars */
			uint m_ID;
			static uint m_currID;

			/** Helps traversing algorithms */
			bool m_visited;
		};


		template <class VectorType>
		unsigned int HalfEdge<VectorType>::m_currID = 0;
	}
}

#endif
