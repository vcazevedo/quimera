//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#pragma once
#ifndef __CHIMERA_HEXA_VOLUME_H_
#define __CHIMERA_HEXA_VOLUME_H_

#pragma once

#include "Mesh/Volume.h"

namespace Chimera {

	namespace Meshes {


		template<class VectorType, template <class> class ElementType>
		const vector<shared_ptr<ElementType<VectorType>>>& getElements(shared_ptr<HexaVolume<VectorType>> pHexaVolume);


		template <class VectorType>
		class HexaVolume : public std::enable_shared_from_this<HexaVolume<VectorType>> {

		private:
			#pragma region Constructors
			/** Volume constructor is private! We only want shared_ptr instances of volumes since the complicated
				memory management that this class entails. */
			HexaVolume(const vector<shared_ptr<Cell<VectorType>>> &cells, dimensions_t cellLocation, DoubleScalar gridDx) : m_cells(cells), m_edgeToCellMap(cells) {
				m_ID = m_currID++;
				m_gridCellLocation = cellLocation;
				m_gridDx = gridDx;

				split();
			}

		public:
			#pragma region StructuresDefinition
			/** Primitive Structure used for templated meshes for returning composed meshes with these primitive structures*/
			typedef Cell<VectorType> PrimitiveStructure;
			#pragma endregion

			/** True class constructor: creates a shared pointer and redirects it to Volume constructors. */
			static shared_ptr<HexaVolume<VectorType>> create(const vector<shared_ptr<Cell<VectorType>>> & cells, dimensions_t cellLocation, DoubleScalar gridDx) {
				//TODO: Replace with make_shared to avoid unnecessary copy
				shared_ptr<HexaVolume<VectorType>> pNewHexaVolume(new HexaVolume(cells, cellLocation, gridDx));

				//Setting up proper parent references on children halfCells, using shared_from_this functionality
				for (auto pVolume : pNewHexaVolume->getVolumes()) {
					pVolume->setHexaVolume(pNewHexaVolume->shared_from_this());
				}

				return pNewHexaVolume;
			}

			static void resetIDs() {
				m_currID = 0;
			}
			#pragma endregion


			#pragma region AccessFunctions
			uint getID() const {
				return m_ID;
			}

			const vector<shared_ptr<Volume<VectorType>>> & getVolumes() const {
				return m_volumes;
			}

			/** Same name so classes can use the same methods for QuadCells and HexaVolumes */
			const vector<shared_ptr<Volume<VectorType>>>& getCells() const {
				return m_volumes;
			}


			void setGridCellLocation(const dimensions_t &gridCellLocation) {
				m_gridCellLocation = gridCellLocation;
			}

			const dimensions_t & getGridCellLocation() const {
				return m_gridCellLocation;
			}

			Scalar getGridSpacing() const {
				return m_gridDx;
			}

			bool intersects(const VectorType& position, DoubleScalar accuracy = 1e-5) {
				return false;
			}


			#pragma endregion

			#pragma region Operators
			FORCE_INLINE bool operator==(const Volume<VectorType> &rhs) const {
				return m_ID == rhs.getID();
			}
			#pragma endregion

			#pragma region Functionalities
			/** Looks for the volume that this point is contained */
			shared_ptr<Volume<VectorType>> getVolume(const VectorType &position);
			#pragma endregion


		protected:
		#pragma region HelperStructures
		/** Given an edge ID, gets all the faces that are connected to this edge */
		template <class T>
		class EdgeToCellMap {

		public:
			//Empty constructor
			EdgeToCellMap() { };
			EdgeToCellMap(const vector<shared_ptr<Cell<VectorType>>> &cells) {
				initializeMap(cells);
			};

			const vector<weak_ptr<Cell<VectorType>>> & getCells(shared_ptr<Edge<VectorType>> pEdge) {
				return m_edgeToCellMap[pEdge->getID()];
			}

			void initializeMap(const vector<shared_ptr<Cell<VectorType>>>& faces) {
				m_edgeToCellMap.clear();
				for (uint i = 0; i < faces.size(); i++) {
					for (uint j = 0; j < faces[i]->getEdges().size(); j++) {
						uint faceID = faces[i]->getEdges()[j]->getID();
						m_edgeToCellMap[faces[i]->getEdges()[j]->getID()].push_back(faces[i]);
					}
				}
			}

		protected:
			map<uint, vector<weak_ptr<Cell<VectorType>>>> m_edgeToCellMap;
		};
		#pragma endregion

		#pragma region ClassMembers
		/**Undirected cells to be split*/
		vector<shared_ptr<Cell<VectorType>>> m_cells;

		/** Interior split half volumes */
		vector<shared_ptr<Volume<VectorType>>> m_volumes;

		/** Regular grid location and scale, if applicable */
		dimensions_t m_gridCellLocation;
		DoubleScalar m_gridDx;

		/** Each volume has a edge to edge cell map that maps the elements of that specific Volume. This is a helper structure
			that selects the cells connected to the edge of this specific volume. This is useful when splitting the
			volume into halfVolumes. */
		EdgeToCellMap<VectorType> m_edgeToCellMap;

		/** ID vars */
		uint m_ID;
		static uint m_currID;

		bool m_invalid = false;
		#pragma endregion

		#pragma region PrivateFunctionalities
		/** Splits this volume into a series of half-volumes (cut-volumes). It uses edges orientations to make correct 3-D turns. *
			Initializes the result inside m_halfVolumes vector structure. Returns m_halfVolumes structure */
		void split();

		/** Search for unvisited cells. CellID will contain the first non-visited element found. */
		shared_ptr<Cell<VectorType>> hasUnvisitedCells() {
			for (uint i = 0; i < m_cells.size(); i++) {
				if (m_cells[i]->getLocation() != geometricCell
					&& !m_cells[i]->getHalfCells().first->isVisited()
					&& !m_cells[i]->getHalfCells().second->isVisited()) {
					return m_cells[i];
				}
			}
			return nullptr;
		}

		/** Searches for half-cells inside a cell, and returns the right one considering the dimension of the cell being evaluated.
			Reverse edge gets back-facing geometry cells if true, otherwise returns front-facing geometry cells. */
		shared_ptr<HalfCell<VectorType>> getHalfCell(shared_ptr<Cell<VectorType>> pCell, dimensions_t cellLocation);

		/** Depth first search: in 3-D the connectivity graph does not branches, but the depth-first principle is
			applied. This recursive function will call itself until it has visited all half-cells on a cycle. The visited
			half-cells will be appended to halfCells vector. The reverse edge parameter tracks if the mesh went from
			the grid cells to geometry ones in a reversed manner, i.e. the common half-edge shared was a reversed one.
			If it was, then on the way back to grid-cells, the geometry edge has to be accessed on a reversed manner. */
		void breadthFirstSearch(shared_ptr<HalfCell<VectorType>> pHalfCell, vector<shared_ptr<HalfCell<VectorType>>>& halfCells, shared_ptr<HalfEdge<VectorType>>pPrevHalfEdge);
		#pragma endregion
		};

		template <class VectorType>
		unsigned int HexaVolume<VectorType>::m_currID = 0;

	}
}

#pragma once
#endif
