//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#pragma once
#ifndef __CHIMERA_HALF_CELL_H_
#define __CHIMERA_HALF_CELL_H_

#pragma once

#include "ChimeraCore.h"
#include "MeshesCoreDefs.h"
#include "Mesh/Edge.h"

namespace Chimera {

	namespace Meshes {

		template <class VectorType>
		class Cell;

		/** This class tracks a single connected vector of half-edges, which gives oriented 
				way to access vertices on a polygon. With oriented access we can also
				define normals and connected structures. */
		template <class VectorType>
		class HalfCell : public std::enable_shared_from_this<HalfCell<VectorType>> {

		private:

			#pragma region Constructors
			/** HalfCell constructor is private! We only want shared_ptr instances of half-cells since the complicated
			memory management that this class entails. */
			HalfCell(const vector<shared_ptr<HalfEdge<VectorType>>> &halfEdges, weak_ptr<Cell<VectorType>> pCell, halfCellLocation_t halfCellLocation = backHalfCell) : m_halfEdges(halfEdges) {
				m_pCell = pCell;
				m_ID = m_currID++;
				m_cellLocation = halfCellLocation;
				m_normal = computeNormal();
			}

		public:
			/** True class constructor: creates a shared pointer and redirects it to HalfCell constructors. */
			static shared_ptr<HalfCell<VectorType>> create(const vector<shared_ptr<HalfEdge<VectorType>>>& halfEdges, weak_ptr<Cell<VectorType>> pCell, halfCellLocation_t halfCellLocation = backHalfCell) {
				shared_ptr<HalfCell<VectorType>> newCell(new HalfCell(halfEdges, pCell, halfCellLocation));
				
				/** Update connectivity */
				for (auto pHalfEdge : halfEdges) {
					OwnConnectedStructure<HalfCell<VectorType>>::get(pHalfEdge)->addConnectedStructure(newCell->getID(), newCell->shared_from_this());
				}
				return newCell;
			}

			static void resetIDs() {
				m_currID = 0;
			}
			#pragma endregion 

			#pragma region AccessFunctions
			const vector<shared_ptr<HalfEdge<VectorType>>> & getHalfEdges() const {
				return m_halfEdges;
			}

			vector<shared_ptr<HalfEdge<VectorType>>> & getHalfEdges() {
				return m_halfEdges;
			}

			uint getID() const {
				return m_ID;
			}

			halfCellLocation_t getLocation() const {
				return m_cellLocation;
			}

			void setLocation(halfCellLocation_t halfCellLocation) {
				m_cellLocation = halfCellLocation;
			}

			shared_ptr<Cell<VectorType>> getCell() {
				return m_pCell.lock();
			}

			const shared_ptr<Cell<VectorType>> getCell() const {
				return m_pCell.lock();
			}

			void setCell(weak_ptr<Cell<VectorType>> pCell) {
				m_pCell = pCell;
			}

			const VectorType & getNormal() const {
				return m_normal;
			}

			void setNormal(const VectorType &normal) {
				m_normal = normal;
			}

			bool isVisited() const {
				return m_visited;
			}

			void setVisited(bool visited) {
				m_visited = visited;
			}
			#pragma endregion

			#pragma region Operators
			FORCE_INLINE bool operator==(const HalfCell<VectorType> &rhs) const {
				return m_ID == rhs.getID();
			}
			#pragma endregion
			
			#pragma region Functionalities
			/** Checks if this half-cell has a certain half-edge */
			bool hasHalfedge(shared_ptr<HalfEdge<VectorType>> pHalfEdge);

			/** Checks if this half-cell has a half-edge, but in a reversed direction*/
			bool hasReverseHalfedge(shared_ptr<HalfEdge<VectorType>> pHalfEdge);

			/** Creates half-cell with a opposite orientation from this one. Useful for routine initialization */
			shared_ptr<HalfCell<VectorType>> reversedCopy();
			#pragma endregion 

		protected:
			#pragma region ClassMembers
			/** Half cells own half-edges*/
			vector<shared_ptr<HalfEdge<VectorType>>> m_halfEdges;

			/** But not the reference to the parent cell*/
			weak_ptr<Cell<VectorType>> m_pCell;
			halfCellLocation_t m_cellLocation;

			/** Cell vector attributes */
			VectorType m_normal;

			/** ID vars */
			uint m_ID;
			static uint m_currID;

			/** Helper for 3-D cut-cells subdivision */
			bool m_visited;
			#pragma endregion 

			#pragma region PrivateFunctionalities
			VectorType computeNormal();
			#pragma endregion 

		};

		template <class VectorType>
		unsigned int HalfCell<VectorType>::m_currID = 0;
		#pragma endregion
	}
}

#endif
