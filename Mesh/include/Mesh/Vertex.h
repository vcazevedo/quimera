//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_VERTEX_H_
#define __CHIMERA_VERTEX_H_

#include "ChimeraCore.h"
#include "MeshesCoreDefs.h"

#include <type_traits>

namespace Chimera {
	using namespace Core;

	namespace Meshes {
		
		/** Forward edge/cells declaration */
		template <class VectorType>
		class Edge;

		template <class VectorType>
		class Cell;

		template <class VectorType>
		class Voxel;

		/** Vertex structure: stores the location of the vertex, which is paramount for ensuring correct cut-cells
			creation. It does owns this vertex, so updates have to be done on the vertex itself. It can have VectorType
			attributes (velocities, etc) and scalar attributes, which are configurable through OwnCustomAttribute interface */
		template <class VectorType>
		class Vertex : public OwnCustomAttribute<VectorType>, public OwnCustomAttribute <Scalar>,
										public OwnConnectedStructure<Edge<VectorType>>, public OwnConnectedStructure<Cell<VectorType>>,
										public OwnConnectedStructure<Voxel<VectorType>> {
			/** Constructor is private, since this can only be initialized as shared_ptr */
			#pragma region Constructors
			private:
			
			/** Vertex constructor is private! We only want shared_ptr instances of vertices since the complicated memory
				management that this class entails. */
			Vertex(const VectorType &vertexPosition, vertexType_t vertexType) :
				m_position(vertexPosition) {

				m_vertexID = m_currID++;
				m_vertexType = vertexType;
				m_hasUpdated = false;
				m_onGridNode = false;
				m_borderVertex = false;
				m_internalBorderVertex = false;
				m_isSolid = false;
			}

			Vertex(Vertex copy, bool nonidenticalCopy) {
				*this = copy;
				if (nonidenticalCopy) {
					m_vertexID = m_currID++;
				}
			}

			public:
			/** True class constructor: creates a shared pointer and redirects it to Vertex constructors.*/
			template <class ... Args>
			static shared_ptr<Vertex<VectorType>> create(Args&& ... args) {
				//TODO: Replace with make_shared to avoid unnecessary copy
				return shared_ptr<Vertex<VectorType>>(new Vertex(std::forward<Args>(args)...));
			}

			/** Resets IDs of all vertices, useful for recreation of structures */
			static void resetIDs() {
				m_currID = 0;
			}
			#pragma endregion

			#pragma region AccessFunctions
			const VectorType & getPosition() const {
				return m_position;
			}

			VectorType & getPosition() {
				return m_position;
			}

			void setPosition(const VectorType &position) {
				m_position = position;
			}

			/** Centroid is also a alias for position so this could be easily accessed 
					as other meshes classes.
			*/
			const VectorType& getCentroid() const {
				return m_position;
			}

			
			const VectorType& getNormal() const {
				return m_normal;
			}
			void setNormal(const VectorType& normal) {
				m_normal = normal;
			}

			uint getID() const {
				return m_vertexID;
			}

			vertexType_t getType() const {
				return m_vertexType;
			}

			void setVertexType(vertexType_t vertexType) {
				m_vertexType = vertexType;
			}
			
			/** Update utility*/
			bool hasUpdated() const {
				return m_hasUpdated;
			}

			void setUpdated(bool updated) {
				m_hasUpdated = updated;
			}

			bool isOnGeometryVertex() const {
				if (m_vertexType == geometryVertex || m_vertexType == edgeVertex || m_vertexType == cellVertex)
					return true;
				return false;
			}

			void setOnGridNode(bool onGridNode) {
				m_onGridNode = onGridNode;
			}

			bool isOnGridNode() const {
				return m_onGridNode;
			}

			bool isBorder() const {
				return m_borderVertex;
			}

			void setBorder(bool border) {
				m_borderVertex = border;
			}
			
			bool isInternalBorder() const {
				return m_internalBorderVertex;
			}

			void setInternalBorder(bool border) {
				m_internalBorderVertex = border;
			}
			
			bool isInternalCorner() const {
				return m_internalCorner;
			}

			void setInternalCorner(bool corner) {
				m_internalCorner = corner;
			}

			void setBoundaryCondition(boundaryCondition_t condition) {
				m_boundaryCondition = condition;
			}

			boundaryCondition_t getBoundaryCondition() {
				return m_boundaryCondition;
			}

			bool isSolid() const {
				return m_isSolid;
			}

			void setSolid(bool solid) {
				m_isSolid = solid;
			}

			shared_ptr<Cell<VectorType>> getDualElement2D() {
				return m_dualElement2D;
			}

			void setDualElement2D(shared_ptr<Cell<VectorType>> dualCell) {
				m_dualElement2D = dualCell;
			}


			FORCE_INLINE bool intersects(const VectorType& position, DoubleScalar accuracy = 1e-5) {
				if ((m_position - position).length() < accuracy) {
					return true;
				}
				return false;
			}

			#pragma endregion 

			#pragma region Operators
			FORCE_INLINE bool operator==(const Vertex<VectorType> &rhs) const {
				return m_vertexID == rhs.getID();
			}
			FORCE_INLINE bool operator!=(const Vertex<VectorType> &rhs) const {
				return m_vertexID != rhs.getID();
			}
			#pragma endregion

			protected:
			#pragma region ClassMembers
			/** Essential vertices attributes. Other attributes will be added through OwnCustomAttributes. */
			VectorType m_position;
			vertexType_t m_vertexType;
			VectorType m_normal;
			
			/** Utility used for mixed node velocity computation */
			bool m_hasUpdated;

			/** Special flag used if a vertex is on top of a grid node */
			bool m_onGridNode;

			bool m_borderVertex; // a vertex is a borderVertex if it lies on the outer boundary of the domain
			bool m_internalBorderVertex; // a vertex is a internalBorderVertex if it lies on the boundary of an obstacle
			boundaryCondition_t m_boundaryCondition;
			bool m_isSolid; // a vertex is solid if it lies in the interior of an obstacle
			bool m_internalCorner;

			/** ID vars */
			uint m_vertexID;
			static uint m_currID;

			/** 2D Dual element*/
			shared_ptr<Cell<VectorType>> m_dualElement2D;
			#pragma endregion
		};

		/** Forward edge/cells declaration */
		template <class VectorType>
		class HalfEdge;

		template <class VectorType>
		class HalfCell;

		template <class VectorType>
		class HalfVoxel;

		/** 
			Geometry vertices custom attributes hold velocities, auxiliary velocities, streamfunctions, weights, etc. These are 
			specially stored in a map because now the velocities are stored considering the half-cells (half-volumes) IDs that 
			they the evaluation is being performed, since evaluations across different sides of the mesh can be discontinuous.
			The OwnCustomAttributes inheritance is done through protected modifiers, since the original interface is not 
			accessible anymore. One can only query information from these vertices by passing cell IDs.*/ 
		template<class VectorType>
		class GeometryVertex : public Vertex<VectorType>, 
													protected OwnCustomAttribute<map<uint, shared_ptr<VectorType>>>, 
													protected OwnCustomAttribute<map<uint, shared_ptr<Scalar>>>,
													public OwnConnectedStructure<HalfEdge<VectorType>>, 
													public OwnConnectedStructure<HalfCell<VectorType>>, 
													public OwnConnectedStructure<HalfVoxel<VectorType>> {

		public:
		
		#pragma region AccessFunctions
			template <class AttributeClassT>
			void addAttributes(const vector<string>& attributeNames) {
				for (auto attributeName : attributeNames)
					m_attributes[attributeName] = AttributeClassT();
			}

			template <class CustomTypeT>
			void registerAttribute(const vector<uint>& cellIDs, const string& attributeName) {
				shared_ptr<CustomTypeT> uniqueShared = make_shared<CustomTypeT>();
				for (auto cellID : cellIDs) {
					OwnCustomAttribute<map<uint, shared_ptr<CustomTypeT>>>::getAttribute(attributeName)[cellID] = uniqueShared;
				}
			}

			/** Returns attribute by name. Does not error treatment, in favor of efficiency */
			template <class AttributeClassT>
			AttributeClassT & getAttribute(uint cellID, const string& attributeName) {
				return *(OwnCustomAttribute<map<uint, shared_ptr<VectorType>>>::getAttribute(attributeName)[cellID]);
			}

			/** Returns attribute by name. Does not error treatment, in favor of efficiency */
			template <class AttributeClassT>
			const AttributeClassT& getAttribute(uint cellID, const string& attributeName) const {
				return *(OwnCustomAttribute<map<uint, shared_ptr<VectorType>>>::getAttribute(attributeName)[cellID]);
			}

			/** Sets attribute by name. Does not error treatment, in favor of efficiency */
			template <class AttributeClassT>
			void setAttribute(uint cellID, const string& attributeName, const AttributeClassT& attribute) {
				*(OwnCustomAttribute<map<uint, shared_ptr<VectorType>>>::getAttribute(attributeName)[cellID]) = attribute;
			}

			virtual bool hasAttribute(const string& attributeName) {
				if (m_attributes.find(attributeName) == m_attributes.end())
					return false;
				return true;
			}

			/** Clears attributes */
			virtual void clearAttributes() {
				m_attributes.clear();
			}
		#pragma endregion
		protected:
		
		};

		template <class VectorType>
		unsigned int Vertex<VectorType>::m_currID = 0;
	}
}

#endif
