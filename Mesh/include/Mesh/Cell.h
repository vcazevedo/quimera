//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#pragma once
#ifndef __CHIMERA_CELL_H_
#define __CHIMERA_CELL_H_

#pragma once

#include "ChimeraCore.h"
#include "MeshesCoreDefs.h"
#include "Mesh/Edge.h"
#include "Mesh/HalfCell.h"

#include <unordered_set>

namespace Chimera {

	namespace Meshes {

		/** Forward Declarations */
		template <class VectorType>
		class QuadCell;

		template <class VectorType>
		class Voxel;

		/** Cell structure: stores centroids and attributes relative to cells. It has the ownership of two half-cells,
			* which are created on the construction of this class. This class has a manifold structure, with no  
			* T-vertices. */
		template <class VectorType>
		class Cell : public std::enable_shared_from_this<Cell<VectorType>>,
									public OwnCustomAttribute<VectorType>, public OwnCustomAttribute <Scalar>,
									public OwnConnectedStructure<Voxel<VectorType>> {
		
			
		private:
			#pragma region Constructors
			/** Cell constructor is private! We only want shared_ptr instances of cells since the complicated memory
				management that this class entails. */
			Cell(const vector<shared_ptr<Edge<VectorType>>> &edges, cellLocation_t cellLocation = XYPlane)
				: m_edges(edges), m_halfCells() {
				m_ID = m_currID++;
				m_cellLocation = cellLocation;
				m_isVisited = false;
				m_isSolid = false;
				initializeCentroid();
				m_area = calculateArea();
			}

			/**Empty cell constructor */
			Cell(cellLocation_t cellLocation = XYPlane) {
				m_ID = m_currID++;
				m_isVisited = false;
				m_isSolid = false;
				m_cellLocation = cellLocation;
			}
			

		public:

			/** True class constructor: creates a shared pointer and redirects it to HalfCell constructors. */
			static shared_ptr<Cell<VectorType>> create(const vector<shared_ptr<HalfEdge<VectorType>>> &halfEdges, cellLocation_t cellLocation) {
				vector<shared_ptr<Edge<VectorType>>> edges;

				for (auto pHalfEdge : halfEdges) {
					edges.push_back(pHalfEdge->getEdge());
				}

				shared_ptr<Cell<VectorType>> newCell(new Cell(edges, cellLocation));
				halfCellLocation_t halfCellLocation = Cell<VectorType>::getHalfCellLocation(cellLocation);
				newCell->getHalfCells().first = HalfCell<VectorType>::create(halfEdges, newCell->shared_from_this(), halfCellLocation);
				
				if constexpr (isVector3<VectorType>::value) //Don't create half-cells reversed copies for 2-D 
					newCell->getHalfCells().second = newCell->getHalfCells().first->reversedCopy();

				/** Update connectivity */
				for (auto pEdge : edges) {
					OwnConnectedStructure<Cell<VectorType>> *pEdgeInterface = dynamic_cast<OwnConnectedStructure<Cell<VectorType>> *>(pEdge.get());
					pEdgeInterface->addConnectedStructure(newCell->getID(), newCell->shared_from_this());
				}
				
				return newCell;
			}

			static void resetIDs() {
				m_currID = 0;
			}
			#pragma endregion 

		

			#pragma region Functionalities
			/** Check if a ray crossed through this cell geometric edges*/
			bool crossedThroughGeometry(const VectorType& v1, const VectorType& v2, VectorType& crossingPoint) const;
			
			/** Gets closest distance to the boundary  */
			Scalar getDistanceToBoundary(const VectorType& position) const;

			/** Checks if a point is inside this cell */
			bool isInside(const VectorType& position, DoubleScalar accuracy = 1e-7) const;

			/** For a geometric half-face (triangle), checks if a ray intersects it */
			bool rayIntersect(const VectorType& point, const VectorType& rayDirection);
			#pragma endregion


			#pragma region AccessFunctions
			uint getID() const {
				return m_ID;
			}

			const VectorType & getCentroid() const {
				return m_centroid;
			}

			void setCentroid(const VectorType &centroid) {
				m_centroid = centroid;
			}

			cellLocation_t getLocation() const {
				return m_cellLocation;
			}

			const vector<shared_ptr<Edge<VectorType>> > & getEdges() const {
				return m_edges;
			}

			/** Useful for setting half-cells that have opposing half-edges on cut-cells */
			void swapHalfCells() {
				swap(m_halfCells.first, m_halfCells.second);
			}
			void setQuadCell(shared_ptr<QuadCell<VectorType>> pQuadCell) {
				m_pQuadCell = pQuadCell;
			}
			shared_ptr<QuadCell<VectorType>> getQuadCell() {
				return m_pQuadCell.lock();
			}

			const pair<shared_ptr<HalfCell<VectorType>>, shared_ptr<HalfCell<VectorType>>>& getHalfCells() const {
				return m_halfCells;
			}

			pair<shared_ptr<HalfCell<VectorType>>, shared_ptr<HalfCell<VectorType>>>& getHalfCells() {
				return m_halfCells;
			}

			/** Checks if this class has disconnected half-edges region */
			bool hasDisconnectedRegion() {
				return !m_disconnectedHalfCells.empty();
			}
			/** Adds a disconnected half-edges vector to this face. This is added to the disconnectedHalfEdges structure,
			but ALSO added to the m_halfEdges. */
			void addDisconnectedRegion(const vector<shared_ptr<HalfEdge<VectorType>>>& disconnectedRegion) {
				halfCellLocation_t halfCellLocation = Cell<VectorType>::getHalfCellLocation(m_cellLocation);
				pair<shared_ptr<HalfCell<VectorType>>, shared_ptr<HalfCell<VectorType>>> disconnectedCellsPair;
				disconnectedCellsPair.first = HalfCell<VectorType>::create(disconnectedRegion, this->shared_from_this(), halfCellLocation);
				disconnectedCellsPair.second = disconnectedCellsPair.first->reversedCopy();

				m_disconnectedHalfCells.push_back(disconnectedCellsPair);
			}

			const vector<pair<shared_ptr<HalfCell<VectorType>>, shared_ptr<HalfCell<VectorType>>>>& getDisconnectedHalfCells() const {
				return m_disconnectedHalfCells;
			}
			
			vector<pair<shared_ptr<HalfCell<VectorType>>, shared_ptr<HalfCell<VectorType>>>> & getDisconnectedHalfCells() {
				return m_disconnectedHalfCells;
			}

			bool isVisited() const {
				return m_isVisited;
			}

			void setVisited(bool visited) {
				m_isVisited = visited;
			}

			bool isSolid() const {
				return m_isSolid;
			}

			void setSolid(bool solid) {
				m_isSolid = solid;
			}

			shared_ptr<Vertex<VectorType>> getDualElement2D() {
				return m_dualElement2D;
			}

			void setDualElement2D(shared_ptr<Vertex<VectorType>> dualVertex) {
				m_dualElement2D = dualVertex;
			}

			double getArea() {
				return calculateArea();
			}

			bool intersects(const VectorType& position, DoubleScalar accuracy = 1e-5) {
				return isInside(position);
			}
			#pragma endregion

			#pragma region Operators
			FORCE_INLINE bool operator==(const Cell<VectorType> &rhs) const {
				return m_ID == rhs.getID();
			}
			#pragma endregion
		protected:

			#pragma region ClassMembers
			/**Undirected edges of this cell */
			vector<shared_ptr<Edge<VectorType>>> m_edges;

			/** The half cells pair that this cell contains */
			pair<shared_ptr<HalfCell<VectorType>>, shared_ptr<HalfCell<VectorType>>> m_halfCells;

			/** Disconnected half cells pairs that this cell might contain */
			vector<pair<shared_ptr<HalfCell<VectorType>>, shared_ptr<HalfCell<VectorType>>>> m_disconnectedHalfCells;


			/** 3-D plane cell location, if applicable */
			cellLocation_t m_cellLocation;

			/** Centroid, if applicable */
			VectorType m_centroid;

			/** Parent QuadCell reference */
			weak_ptr<QuadCell<VectorType>> m_pQuadCell;

			/** Area */
			Scalar m_area;

			/** ID vars */
			uint m_ID;
			static uint m_currID;

			/** Aux */
			bool m_isVisited;
			bool m_isSolid;

			/** 2D Dual element*/
			shared_ptr<Vertex<VectorType>> m_dualElement2D;
			#pragma endregion

			#pragma region PrivateFunctionalities
			/** Only works for 3-D, where this cell half-cells are mirrored */
			DoubleScalar calculateArea();

			void initializeCentroid();

			static halfCellLocation_t getHalfCellLocation(cellLocation_t cellLocation) {
				switch (cellLocation) {
					case XYPlane:
						return backHalfCell;
					break;
					case XZPlane:
						return bottomHalfCell;
					break;
					case YZPlane:
						return leftHalfCell;
					break;
					//Geometry
					default:
						return geometryHalfCell;
					break;
				}
			}
			#pragma endregion

		};

		template <class VectorType>
		unsigned int Cell<VectorType>::m_currID = 0;
	}
}

#endif
