#pragma once
#ifndef __CHIMERA_MESH_MESH_SLICER_H_
#define __CHIMERA_MESH_MESH_SLICER_H_

#include <Mesh/MeshesCoreDefs.h>

#include <Math/DoubleScalar.h>
#include <Math/Scalar.h>

#include <stack>
#include <vector>

namespace Chimera {

	namespace Core {

		class Vector3;
		class Vector3D;

	}

	namespace Meshes {

		template <typename VectorType>
		class MeshSlicer
		{

		public:

			using Scalar = Core::Scalar;
			using DoubleScalar = Core::DoubleScalar;

			enum gridProximityResolution {
				snapToGrid,
				pushAwayFromGrid
			};

			MeshSlicer(Scalar gridDx, gridProximityResolution gridProximityResolution = pushAwayFromGrid);

			void slice(std::vector<VectorType>& vertices, std::vector<std::vector<uint> >& faces);

		private:

			template <typename VectorType2>
			struct vector_traits {};

			template <>
			struct vector_traits<Core::Vector3> {
				typedef Scalar ScalarType;
			};

			template <>
			struct vector_traits<Core::Vector3D> {
				typedef DoubleScalar ScalarType;
			};

			typedef typename vector_traits<VectorType>::ScalarType ScalarType;

			template <cellLocation_t Plane>
			void sliceAxis(std::vector<VectorType>& vertices, std::vector<std::vector<uint>>& faces);

			template <typename FwdIt>
			std::vector<uint>* processEdge(FwdIt first, FwdIt last, std::stack<std::pair<int64_t, std::vector<uint>*>>& facesToFinish, std::vector<std::unique_ptr<std::vector<uint>>>& newFaces, std::vector<uint>* curFace);

			std::vector<uint>* processCutVertex(int64_t slice, uint vertex, std::stack<std::pair<int64_t, std::vector<uint>*>>& facesToFinish, std::vector<std::unique_ptr<std::vector<uint>>>& newFaces, std::vector<uint>* curFace);

			void proximityResolution(ScalarType& position);

			Scalar m_gridDx;
			gridProximityResolution m_gridProximityResolution;
			ScalarType m_proximityTolerance;

		};

	}

}

#endif // __CHIMERA_MESH_MESH_SLICER_H_
