//  Copyright (c) 2017, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef _CHIMERA_POLYGONAL_MESH_
#define _CHIMERA_POLYGONAL_MESH_

#pragma once
#include "Mesh.h"

namespace Chimera {

	namespace Meshes {

		template <class VectorT>
		class PolygonalMeshType {
		public:
			using VectorType = VectorT;
			using ElementPrimitiveType = Meshes::Cell<VectorType>;
		};


		/** A polygonal mesh has faces with two attached half-faces each */
		template <class VectorType>
		class PolygonalMesh : public Mesh<PolygonalMeshType<VectorType>> {

		public:
	
			#pragma region Constructors
				// constructs a polygonalMesh with libigl mesh
				// TODO: re-implement this if needed
				//PolygonalMesh(const tuple<Eigen::MatrixX3d, Eigen::MatrixX3i, Eigen::MatrixX3d> &libiglMesh, const dimensions_t &gridDimensions, Scalar gridDx, bool useCutCells = false);
				
				PolygonalMesh(const string &objFile, const Pose<VectorType> &pose = Pose<VectorType>());
				PolygonalMesh(const tuple<const vector<VectorType> &, const vector<vector<uint>>&> &mesh, const Pose<VectorType> &pose = Pose<VectorType>());

				//Empty mesh constructor
				PolygonalMesh() {
					m_hasUpdated = false;
				}
			#pragma endregion

			#pragma region AccessFunctions
				bool hasUpdated() const {
					return m_hasUpdated;
				}

				void setHasUpdated(bool hasUpdatedVar) {
					m_hasUpdated = hasUpdatedVar;
				}

				vector<shared_ptr<Edge<VectorType>>> & getBorderEdges() {
					return m_borderEdges;
				}

				void addBorderEdge(shared_ptr<Edge<VectorType>> pEdge) {
					m_borderEdges.push_back(pEdge);
				}
			#pragma endregion	

			#pragma region Functionalities
				void updatePoints(vector<VectorType>* points);

				uint getOriginalEdge(shared_ptr<Vertex<VectorType>> vertex);
			#pragma endregion
		protected:

			#pragma region ClassMembers
			//used for reinitializing
			VectorType m_scale;
			string m_objFilename;
			bool m_hasUpdated;

			/** Quick identification of edges at dangling locations */
			vector<shared_ptr<Edge<VectorType>>> m_borderEdges;

			/** Tag border edges from polygons indices list*/
			map <uint, bool> m_borderEdgesTags;
			#pragma endregion

			#pragma region InitializationFunctions
			virtual void initializeFaces(const tuple<const vector<VectorType> &, const vector<vector<uint>>&> &mesh);
			void initializeVertexNormals();
			#pragma endregion

			#pragma region PrivateFunctionalities
			uint64_t edgeHash(uint32_t i, uint32_t j, uint32_t numVertices) const {
				if (i < j)
					return static_cast<uint64_t>(numVertices)*i + j;
				return static_cast<uint64_t>(numVertices)*j + i;
			}

			uint64_t halfEdgeHash(uint32_t i, uint32_t j, uint32_t numVertices) const {
				return static_cast<uint64_t>(numVertices)*j + i;
			}
			#pragma endregion
		};
	}
}
#endif
