//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.
//	

#ifndef __CHIMERA_LINE_MESH_H_
#define __CHIMERA_LINE_MESH_H_

#pragma once

#include "ChimeraCore.h"
#include "Mesh/Vertex.h"
#include "Mesh/Mesh.h"

namespace Chimera {

	namespace Meshes {

		template <class VectorT>
		class LineMeshType {
		public:
			using VectorType = VectorT;
			using ElementPrimitiveType = Meshes::Edge<VectorType>;
		};

		template <class VectorT>
		class LineMesh : public Mesh <LineMeshType<VectorT>> {
		
		public:

			#pragma region ExteriorClasses
			/** Construction parameters */
			struct params_t {
				/** Mesh points */
				vector<VectorT> initialPoints;
				/** Edges */
				vector<pair<uint, uint>> initialEdges;
				/** Position relative to the centroid */
				VectorT position; // Relative to the mesh's centroid
				/** Central relative to the initial points */
				VectorT centroid;
				/** Scalar extrude along normals width */
				Scalar extrudeAlongNormalWidth;

				params_t() {
					extrudeAlongNormalWidth = 0.0f;
				}

				void updateCentroid() {
					centroid = VectorT();
					if (initialPoints.back() == initialPoints.front()) { //Closed mesh
						for (uint i = 0; i + 1 < initialPoints.size(); i++) {
							centroid += initialPoints[i];
						}
						centroid /= initialPoints.size() - 1;
					}
					else {
						for (uint i = 0; i + 1 < initialPoints.size(); i++) {
							centroid += initialPoints[i];
						}
						centroid /= initialPoints.size();
					}
				}
			};
			#pragma endregion

			#pragma region Constructors
			//Empty constructor
			LineMesh() { }
			LineMesh(const params_t &lineMeshParams);

			LineMesh(const vector<shared_ptr<Vertex<VectorT>>> &vertices, const vector<shared_ptr<Edge<VectorT>>> &edges);
			#pragma endregion

			#pragma region AccessFunctions
			params_t * getParams() {
				return &m_params;
			}

			bool isClosedMesh() const {
				return m_isClosedMesh;
			}

			void updateCentroid() {
				VectorT centroid;
				for (uint i = 0; i < this->getVertices().size(); i++) {
					centroid += this->getVertices()[i]->getPosition();
				}
				if (this->getVertices().size() > 0) {
					centroid /= this->getVertices().size();
				}
				this->m_pose.setCentroid(centroid);
			}

			bool hasUpdated() const {
				return m_hasUpdated;
			}

			void setHasUpdated(bool hasUpdatedVar) {
				m_hasUpdated = hasUpdatedVar;
			}

			vector<shared_ptr<Vertex<VectorT>>> & getBorderVertices() {
				return m_borderVertices;
			}

			void addBorderVertex(shared_ptr<Vertex<VectorT>> pVertex) {
				m_borderVertices.push_back(pVertex);
			}
			#pragma endregion

			#pragma region Functionalities
			/** Checks if the LineMesh intersects with given segment */
			bool segmentIntersection(const VectorT &v1, const VectorT &v2);
			/** Checks if the LineMesh intersects with given segment */
			bool segmentIntersection(const VectorT &v1, const VectorT &v2, VectorT &intersectionPoint);
			
			/** Checks the point against all segments, returns minimal distance to this point */
			Scalar getDistanceToPoint(const VectorT &position);

			void removeDuplicatedVertices() {
				for (int j = 0; j < this->getVertices().size() - 1;) {
					int nextJ = roundClamp<int>(j + 1, 0, this->getVertices().size());
					DoubleScalar edgeDistance = (this->getVertices()[j]->getPosition() - this->getVertices()[nextJ]->getPosition()).length();
					if (edgeDistance == 0.0f) {
						this->m_vertices.erase(this->m_vertices.begin() + j);
					}
					else {
						j++;
					}
				}
			}
			#pragma endregion

		protected:

			#pragma region ClassMembers
			bool m_isClosedMesh;
			params_t m_params;

			bool m_hasUpdated;

			/** Quick identification of vertices at dangling locations */
			vector<shared_ptr<Vertex<VectorT>>> m_borderVertices;

			#pragma endregion

			#pragma region InitializationFunctions
			virtual void initializeVertices() {
				uint numPoints = m_params.initialPoints.size();
				if (m_isClosedMesh)
					numPoints--; //avoid duplicates

				for (uint i = 0; i < numPoints; i++) {
					this->constructVertex(m_params.initialPoints[i], geometryVertex);
				}
			}

			virtual void initializeEdges();
			void initializeVertexNormals();
			#pragma endregion

			#pragma region PrivateFunctionalities
			void extrudePointsAlongNormals(Scalar extrudeAlongNormalWidth, vector<VectorT> & points);
			#pragma endregion
		};

	}
}

#endif
