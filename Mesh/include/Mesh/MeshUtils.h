
//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef _CHIMERA_DATA_MESH_UTILS_H_
#define _CHIMERA_DATA_MESH_UTILS_H_

#pragma once

#include "ChimeraCore.h"
#include "Mesh/Cell.h"

//Cross reference
//#include "CutCells/CutCells3D.h"
//#include "Mesh/TriangleMesh3D.h"

namespace Chimera {
	namespace Meshes {
		namespace MeshUtils {

			int findDiscontinuity(const vector<pair<unsigned int, unsigned int>> &edges, int initialIndex = 0);

			bool comparePairs(pair<Vector3, unsigned int *> a, pair<Vector3, unsigned int *> b);

			/** Simplifies redundant vertices and faces representation. This redundant structure is created from the assembly
			/** of different cut-faces of a cut-cell*/
			pair < vector<Vector3>, vector<vector<unsigned int>> > simplifyMesh(const vector<Vector3> &vertices, const vector<vector<unsigned int>> & faces);

			/** Calculates the centroid of a line. Works for non-convex cells. The line must be closed.*/
			//Vector2 calculateCentroid(const vector<Vector2> &points);

			/** Calculates the centroid of a line when its points are lying on the same plane (3-D). Works for non-convex cells.
				The line must be closed. */
			template <class VectorT>
			VectorT calculateCentroid(const vector<VectorT> &points);

			/** Calculates the signed distance function of a line when its points are lying on the same plane (3-D).
			Works for non-convex cells. The line must be closed. */
			template <class VectorT>
			DoubleScalar signedDistanceFunction(const VectorT &position, const vector<VectorT> &points, cellLocation_t faceLocation) {
				DoubleScalar minDistance = FLT_MAX;
				int ithEdge = -1;
				for (int i = 0; i < points.size(); i++) {
					int nextI = roundClamp<int>(i + 1, 0, points.size());
					DoubleScalar tempDistance = distanceToLineSegment(position, points[i], points[nextI]);
					VectorT edgeCentroid = (points[i] + points[nextI])*0.5;

					if (tempDistance < minDistance) {
						minDistance = tempDistance;
						ithEdge = i;
					}
				}
				int j = 0;
				int nextIthEdge = roundClamp<int>(ithEdge + 1, 0, points.size());
				VectorT edgeNormal = -getEdgeNormal(points[ithEdge], points[nextIthEdge], faceLocation);
				VectorT edgeCentroid = (points[nextIthEdge] + points[ithEdge])*0.5;
				if (edgeNormal.dot(position - edgeCentroid) < 0) {
					minDistance = -minDistance;
				}

				return minDistance;
			}

			template <class VectorT>
			VectorT getEdgeNormal(const VectorT &e1, const VectorT &e2, cellLocation_t faceLocation) {
				VectorT edgeVec = e2 - e1;
				switch (faceLocation) {
				case cellLocation_t::YZPlane:
					return VectorT(0, edgeVec.z, -edgeVec.y);
					break;
				case cellLocation_t::XZPlane:
					return VectorT(-edgeVec.z, 0, edgeVec.x);
					break;
				case cellLocation_t::XYPlane:
					return VectorT(-edgeVec.y, edgeVec.x, 0);
					break;

				default:
					throw std::logic_error("Invalid face location");
					return VectorT(0, 0, 0);
				break;
				}
			}

			/**Checks if triangle is cw */

			template <class VectorT>
			bool checkClockwise(const VectorT& v1, VectorT& v2, const VectorT& v3) {
				Matrix3x3 matDet;
				matDet.column[0][0] = v1.x;
				matDet.column[0][1] = v1.y;
				matDet.column[0][2] = 1;

				matDet.column[1][0] = v2.x;
				matDet.column[1][1] = v2.y;
				matDet.column[1][2] = 1;

				matDet.column[2][0] = v3.x;
				matDet.column[2][1] = v3.y;
				matDet.column[2][2] = 1;

				return matDet.determinant() < 0;
			}
		}
	}

	template<class VectorT>
	VectorT Chimera::Meshes::MeshUtils::calculateCentroid(const vector<VectorT>& points) {
		Scalar signedAreaXY = 0, signedAreaXZ = 0, signedAreaYZ = 0;
		Vector2 centroidXY, centroidXZ, centroidYZ;
		VectorT centroid;

		for (int i = 0; i < points.size(); i++) {
			int nextI = roundClamp<int>(i + 1, 0, points.size());
			VectorT initialEdge = points[i];
			VectorT finalEdge = points[nextI];
			VectorT edgeCentroid = (points[i] + points[nextI])*0.5;

			if (initialEdge == finalEdge)
				continue;

			//Projecting onto the XY plane
			Vector2 initialEdgeXY(initialEdge.x, initialEdge.y);
			Vector2 finalEdgeXY(finalEdge.x, finalEdge.y);

			Scalar currAreaXY = initialEdgeXY.cross(finalEdgeXY);
			signedAreaXY += currAreaXY;
			centroidXY.x += (edgeCentroid.x)*currAreaXY;
			centroidXY.y += (edgeCentroid.y)*currAreaXY;

			//Projecting onto the XZ plane
			Vector2 initialEdgeXZ(initialEdge.x, initialEdge.z);
			Vector2 finalEdgeXZ(finalEdge.x, finalEdge.z);

			Scalar currAreaXZ = initialEdgeXZ.cross(finalEdgeXZ);
			signedAreaXZ += currAreaXZ;
			centroidXZ.x += (edgeCentroid.x)*currAreaXZ;
			centroidXZ.y += (edgeCentroid.z)*currAreaXZ;

			//Projecting onto the YZ plane
			Vector2 initialEdgeYZ(initialEdge.y, initialEdge.z);
			Vector2 finalEdgeYZ(finalEdge.y, finalEdge.z);

			Scalar currAreaYZ = initialEdgeYZ.cross(finalEdgeYZ);
			signedAreaYZ += currAreaYZ;
			centroidYZ.x += (edgeCentroid.y)*currAreaYZ;
			centroidYZ.y += (edgeCentroid.z)*currAreaYZ;
		}

		signedAreaXZ *= 0.5f;
		signedAreaXY *= 0.5f;
		signedAreaYZ *= 0.5f;

		if (abs(signedAreaXZ) < 1e-4  && abs(signedAreaXY) < 1e-4) {
			centroid.x = points[0].x;
		}
		else if (abs(signedAreaXZ) > abs(signedAreaXY)) {
			centroid.x = centroidXZ.x / (3.0f*signedAreaXZ);
		}
		else {
			centroid.x = centroidXY.x / (3.0f*signedAreaXY);
		}

		if (abs(signedAreaXY) < 1e-4  && abs(signedAreaYZ) < 1e-4) {
			centroid.y = points[0].y;
		}
		else  if (abs(signedAreaXY) > abs(signedAreaYZ)) {
			centroid.y = centroidXY.y / (3.0f*signedAreaXY);
		}
		else {
			centroid.y = centroidYZ.x / (3.0f*signedAreaYZ);
		}

		if (abs(signedAreaXZ) < 1e-4  && abs(signedAreaYZ) < 1e-4) {
			centroid.z = points[0].z;
		}
		else if (abs(signedAreaXZ) > abs(signedAreaYZ)) {
			centroid.z = centroidXZ.y / (3.0f*signedAreaXZ);
		}
		else {
			centroid.z = centroidYZ.y / (3.0f*signedAreaYZ);
		}

		return centroid;
	}
}

#endif
