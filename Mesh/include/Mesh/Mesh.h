//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_MESH_H___
#define __CHIMERA_MESH_H___

#pragma once

#include "ChimeraCore.h"
#include "Mesh/Edge.h"
#include "Mesh/Cell.h"
#include "Mesh/Volume.h"
#include "Mesh/Pose.h"

namespace Chimera {
	using namespace Core;
	
	namespace Meshes {
    


		typedef bool_type<true> true_type;
		typedef bool_type<false> false_type;

		// trait stuff
		template <typename T>
		struct isElementPrimitiveCell : false_type
		{
			static const bool value = false;
		};

		template <>
		struct isElementPrimitiveCell<Cell<Vector2>> : true_type
		{
			static const bool value = true;
		};

		template <>
		struct isElementPrimitiveCell<Cell<Vector2D>> : true_type
		{
			static const bool value = true;
		};

		// trait stuff
		template <typename T>
		struct isElementPrimitiveQuadCell : false_type
		{
			static const bool value = false;
		};

		template <>
		struct isElementPrimitiveQuadCell<QuadCell<Vector2>> : true_type
		{
			static const bool value = true;
		};


		template <>
		struct isElementPrimitiveQuadCell<QuadCell<Vector2D>> : true_type
		{
			static const bool value = true;
		};

		// trait stuff
		template <typename T>
		struct isElementPrimitiveEdge : false_type
		{
			static const bool value = false;
		};

		template <>
		struct isElementPrimitiveEdge<Edge<Vector2>> : true_type
		{
			static const bool value = true;
		};

		template <>
		struct isElementPrimitiveEdge<Edge<Vector2D>> : true_type
		{
			static const bool value = true;
		};




		class MeshType2D {
		public:
			using VectorType = Core::Vector2;
			using ElementPrimitiveType = Meshes::Edge<VectorType>;
		};

		class MeshType3D {
		public:
			using VectorType = Core::Vector3;
			using ElementPrimitiveType = Meshes::Cell<VectorType>;
		};


		template <typename MeshType>
		class Mesh {

		protected:
			typedef typename MeshType::VectorType VectorType;
			typedef typename MeshType::ElementPrimitiveType ElementPrimitiveType;

		public:

			#pragma region ExternalStructures
			/** Each mesh has attached renderingAttributes_t that can be accessed through it. In this way, controlling
				if a mesh is going to be rendered (and how) is possible by a per-mesh basis */
			enum meshDrawingMode_t {
				drawWireframe,
				drawShaded,
				drawWireframeOnShaded
			};
			struct renderingAttributes_t {
				bool draw;
				bool drawPoints;
				bool drawNormals;
				bool drawElements;
				meshDrawingMode_t meshDrawingMode;

				renderingAttributes_t() {
					drawPoints = false;
					meshDrawingMode = drawWireframeOnShaded;
					drawNormals = false;
					draw = true;
					drawElements = true;
				}
			};

			#pragma endregion
			#pragma region Constructors and Destructors
			/** Constructor with pre-built structures */
			Mesh(const vector<shared_ptr<Vertex<VectorType>>> &vertices, const vector<shared_ptr<ElementPrimitiveType>> &elementsPrimitive, const Pose<VectorType> &pose = Pose<VectorType>());

			/** Empty constructor */
			Mesh() {
				m_ID = m_currID++;
			}

			#pragma endregion

			#pragma region AccessFunctions
			virtual const vector<shared_ptr<Vertex<VectorType>>> & getVertices() const {
				return m_vertices;
			}

			virtual vector<shared_ptr<Vertex<VectorType>>> & getVertices() {
				return m_vertices;
			}

			virtual const Pose<VectorType> & getPose() const {
				return m_pose;
			}

			const pair<VectorType, VectorType> & getBounds() const {
				return m_bounds;
			}

			void setBounds(const pair<VectorType, VectorType> &bounds) {
				m_bounds = bounds;
			}

			bool isInsideBounds(const VectorType &position) {
				if (isVector2<VectorType>().value) {
					if (position[0] > m_bounds.first[0] && position[1] > m_bounds.first[1] &&
							position[0] < m_bounds.second[0] && position[1] < m_bounds.second[1]) {
						return true;
					}
					return false;
				}
				else {
					if (position[0] > m_bounds.first[0] && position[1] > m_bounds.first[1] && position[2] > m_bounds.first[2] &&
							position[0] < m_bounds.second[0] && position[1] < m_bounds.second[1] && position[2] < m_bounds.second[2]) {
						return true;
					}
					return false;
				}
			}

			bool isInsideReducedBounds(const VectorType& position, Scalar width) {
				pair<VectorType, VectorType> reducedBounds(m_bounds);
				
				reducedBounds.first[0] += width;
				reducedBounds.first[1] += width;
				if (isVector3<VectorType>().value) {
					reducedBounds.first[2] += width;
				}

				reducedBounds.second[0] -= width;
				reducedBounds.second[1] -= width;
				if (isVector3<VectorType>().value) {
					reducedBounds.second[2] -= width;
				}

				if (isVector2<VectorType>().value) {
					if (position[0] > reducedBounds.first[0] && position[1] > reducedBounds.first[1] &&
							position[0] < reducedBounds.second[0] && position[1] < reducedBounds.second[1]) {
						return true;
					}
					return false;
				}
				else {
					if (position[0] > reducedBounds.first[0] && position[1] > reducedBounds.first[1] && position[2] > reducedBounds.first[2] &&
							position[0] < reducedBounds.second[0] && position[1] < reducedBounds.second[1] && position[2] < reducedBounds.second[2]) {
						return true;
					}
					return false;
				}
			}

			/** Updates current vertices with the new pose*/
			virtual void updatePose(const Pose<VectorType> &newPose) {
				m_pose.update(newPose, m_vertices);
			}

			/** Updates mesh velocities. Usually extends velocities from vertices to elements. Left to be implemented by
				base class */
			virtual void updateVelocities() {

			}

			template <class ... Args>
			shared_ptr<Vertex<VectorType>> constructVertex(Args&& ... args) {
				m_vertices.push_back(Vertex<VectorType>::create(std::forward<Args>(args)...));
				return m_vertices.back();
			}

			const vector<shared_ptr<ElementPrimitiveType>> & getElements() const {
				return m_elements;
			}

			vector<shared_ptr<ElementPrimitiveType>>& getElements() {
				return m_elements;
			}

			//This class will take ownership and delete any elements added. Similarly, elements removed will not be cleaned up.
			vector<shared_ptr<ElementPrimitiveType>> & getRawElements() {
				return m_elements;
			}

			template <class ... Args>
			shared_ptr<ElementPrimitiveType> constructElement(Args&& ... args) {
				m_elements.push_back(ElementPrimitiveType::create(std::forward<Args>(args)...));
				return m_elements.back();
			}

			const shared_ptr<ElementPrimitiveType> getElement(uint id) const {
				return m_elements[id];
			}

			shared_ptr<ElementPrimitiveType> getElement(uint id) {
				return m_elements[id];
			}

			void setName(const string &gName) {
				m_name = gName;
			}

			const string & getName() const {
				return m_name;
			}

			const renderingAttributes_t & getRenderingAttributes() const {
				return m_renderingAttributes;
			}

			renderingAttributes_t & getRenderingAttributes() {
				return m_renderingAttributes;
			}

			///** Add the attribute to all vertices initializing with the passed value */
			//template<class AttributeClassT>
			//void addVerticesAtribute(const string& attributeName, AttributeClassT initialValue) {
			//	for (auto pVertex : m_vertices) {
			//		OwnCustomAttribute<AttributeClassT>::get(pVertex)->setAttribute(attributeName, initialValue);
			//	}
			//}

			///** Add the attribute to all elements initializing with the passed value.
			//		Doesn't error treat, but expects element class to be derived of OwnCustomAttribute */
			//template<class AttributeClassT>
			//void addElementsAttribute(const string& attributeName, AttributeClassT initialValue) {
			//	for (auto pElement : m_elements) {
			//		OwnCustomAttribute<AttributeClassT>::get(pElement)->setAttribute(attributeName, initialValue);
			//	}
			//}

			virtual DoubleScalar getSignedDistance(const VectorType& position) {
				return 0.0;
			}

			#pragma endregion

			#pragma region Functionalities
			virtual shared_ptr<ElementPrimitiveType> getElement(const VectorType &position, DoubleScalar accuracy = 1e-5) {
				for (auto pElement : m_elements) {
					if (pElement->intersects(position, accuracy)) {
						return pElement;
					}
				}
				return nullptr;
			}


			/** Naive brute force vertex grabber. Computational intensive, derived 
					classes should overwrite this for efficiency! If no vertex is found
					returns a nullptr.*/
			FORCE_INLINE virtual shared_ptr<Vertex<VectorType>> getVertex(const VectorType& position, DoubleScalar accuracy = 1e-5) {
				/** Iterate over all vertices */
				for (auto pVertex : m_vertices) {
					if (pVertex->intersects(position, accuracy)) {
						return pVertex;
					}
				}
				return nullptr;
			}
			#pragma endregion
		protected:

			#pragma region Class Members
			/** Mesh identification */
			uint m_ID;

			/** Vertices: vertices ID's are relative to this vector structure. */
			vector<shared_ptr<Vertex<VectorType>>> m_vertices;

			/** Mesh pose. Useful to encode rigid transformations */
			Pose<VectorType> m_pose;

			/** Mesh Elements:
			In a case of a planar mesh, elements are chosen to be faces;
			In a case of a volumetric mesh, elements are chosen to be volumes;
			In a case of a line mesh, elements are chosen to be edges. */
			vector<shared_ptr<ElementPrimitiveType>> m_elements;

			/** Unique mesh ID increment variable*/
			static uint m_currID;

			/** Mesh identification name*/
			string m_name;

			/** Rendering attributes */
			renderingAttributes_t m_renderingAttributes;

			/** Mesh bounds */
			pair<VectorType, VectorType> m_bounds;
			#pragma endregion
		};

		template <typename MeshType>
		uint Mesh<typename MeshType>::m_currID = 0;
	}

}
#endif
