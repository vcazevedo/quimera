//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#pragma once
#ifndef __CHIMERA_QUAD_CELL_H_
#define __CHIMERA_QUAD_CELL_H_

#pragma once

#include "ChimeraCore.h"
#include "MeshesCoreDefs.h"
#include "Mesh/Cell.h"

#include <unordered_set>

namespace Chimera {

	namespace Meshes {

		template <class VectorType>
		class HalfVolume;


		template<class VectorType, template <class> class ElementType>
		const vector<shared_ptr<ElementType<VectorType>>> & getElements(shared_ptr<QuadCell<VectorType>> pQuadCell);

		/** Cell structure: stores centroids and attributes relative to cells. It has the ownership of two half-cells,
			* which are created on the construction of this class. This might have a non-manifold structure, with
			* T-vertices. */
		template <class VectorType>
		class QuadCell : public std::enable_shared_from_this<QuadCell<VectorType>> {


		private:
			#pragma region Constructors
			/** QuadCell constructor is private! We only want shared_ptr instances of cells since the complicated memory
				management that this class entails. */
			QuadCell(const vector<shared_ptr<Edge<VectorType>>> &edges, dimensions_t cellIndexLocation, DoubleScalar gridDx, cellLocation_t cellLocation = XYPlane)
				: m_edges(edges), m_vertexToEdgeMap(edges) {
				m_ID = m_currID++;
				m_gridCellLocation = cellIndexLocation;
				m_gridDx = gridDx;
				m_cellLocation = cellLocation;
				m_cellType = fluidCell;

				/** Storing edges by location and vertices for easy posterior access */
				map<uint, shared_ptr<Vertex<VectorType>>> verticesMap;
				for (auto pEdge : edges) {
					m_edgesByLocation[Edge<VectorType>::classifyEdge(pEdge, cellIndexLocation, gridDx, cellLocation)].push_back(pEdge);
					verticesMap[pEdge->getVertices().first->getID()] = pEdge->getVertices().first;
					verticesMap[pEdge->getVertices().second->getID()] = pEdge->getVertices().second;
				}

				for (auto verticesMapIter : verticesMap) {
					m_vertices.push_back(verticesMapIter.second);
				}

				split();
			}
			#pragma endregion

			#pragma region InternalExceptions
			/** Thrown for the non-manifold splitting algorithm on non-manifold geometries when the depthFirstSearch
				function does not creates  a valid loop, thus roll back is needed */
			struct nonLoopingCellException : public exception {
				int numberEdges;
				nonLoopingCellException() {
					numberEdges = 0;
				}
			};
			#pragma endregion

		public:

			#pragma region StructuresDefinition
			/** Primitive Structure used for templated meshes for returning composed meshes with these primitive structures*/
			typedef  Edge<VectorType> PrimitiveStructure;
			#pragma endregion

			/** True class constructor: creates a shared pointer and redirects it to Cells constructors. Already initialized and split cells constructor. */
			static shared_ptr<QuadCell<VectorType>> create(const vector<shared_ptr<Edge<VectorType>>> &edges, dimensions_t cellIndexLocation, DoubleScalar gridDx, cellLocation_t cellLocation = XYPlane) {
				//TODO: Replace with make_shared to avoid unnecessary copy
				shared_ptr<QuadCell<VectorType>> pNewQuadCell(new QuadCell(edges, cellIndexLocation, gridDx, cellLocation));

				//Setting up proper parent references on children halfCells, using shared_from_this functionality
				for (auto pCell : pNewQuadCell->getCells()) {
					pCell->setQuadCell(pNewQuadCell->shared_from_this());
				}

				return pNewQuadCell;
			}

			static void resetIDs() {
				m_currID = 0;
			}


			#pragma region Functionalities
			/** Returns the Cell which contains this this point */
			shared_ptr<Cell<VectorType>> getCell(const VectorType &position);
			#pragma endregion


			#pragma region AccessFunctions
			uint getID() const {
				return m_ID;
			}

			cellLocation_t getLocation() const {
				return m_cellLocation;
			}

			const vector<shared_ptr<Edge<VectorType>> > & getEdges() const {
				return m_edges;
			}

			const vector<shared_ptr<Vertex<VectorType>>>& getVertices() const {
				return m_vertices;
			}

			void setCellType(cellType_t cellType) {
				m_cellType = cellType;
			}

			cellType_t getCellType() {
				return m_cellType;
			}

			/** Access edges grouped by location */
			const vector<shared_ptr<Edge<VectorType>>>& getEdges(halfEdgeLocation_t edgeLocation) const {
				return m_edgesByLocation[edgeLocation];
			}

			void setGridCellLocation(const dimensions_t &gridCellLocation) {
				m_gridCellLocation = gridCellLocation;
			}

			const dimensions_t & getGridCellLocation() const {
				return m_gridCellLocation;
			}

			const vector<shared_ptr<Cell<VectorType>>> & getCells() const {
				return m_cells;
			}

			vector<shared_ptr<Cell<VectorType>>>& getCells() {
				return m_cells;
			}
			void setCells(const vector<shared_ptr<Cell<VectorType>>> &cells) {
				m_cells = cells;
			}

			Scalar getGridSpacing() const {
				return m_gridDx;
			}

			bool intersects(const VectorType& position, DoubleScalar accuracy = 1e-5) {
				for (auto cell : m_cells) {
					if (cell->intersects(position, accuracy)) {
						return true;
					}
				}
				return false;
			}
			#pragma endregion

			#pragma region Operators
			FORCE_INLINE bool operator==(const Cell<VectorType> &rhs) const {
				return m_ID == rhs.getID();
			}
			#pragma endregion
		protected:
			#pragma region PrivateClasses

			/** Given a vertex ID, gets all the edges that are connected to this vertex */
			template <class T>
			class VertexToEdgeMap {

			public:
				//Empty constructor
				VertexToEdgeMap() { };
				VertexToEdgeMap(const vector<shared_ptr<Edge<VectorType>>>& edges) {
					initializeMap(edges);
				};

				//Returns a new vector with shared_ptrs for weak references. Does not returns weak_refs since its unpractical
				vector<shared_ptr<Edge<VectorType>>> getEdges(weak_ptr<Vertex<VectorType>> pVertex) {
					vector<shared_ptr<Edge<VectorType>>> edges;
					for (weak_ptr<Edge<VectorType>> e : m_vertexToEdgeMap[pVertex.lock()->getID()]) {
						edges.push_back(e.lock());
					}
					return edges;
				}

				void initializeMap(const vector<shared_ptr<Edge<VectorType>>>& edges) {
					m_vertexToEdgeMap.clear();
					for (int i = 0; i < edges.size(); i++) {
						m_vertexToEdgeMap[edges[i]->getVertex1()->getID()].push_back(edges[i]);
						m_vertexToEdgeMap[edges[i]->getVertex2()->getID()].push_back(edges[i]);
					}
				}

			protected:
				/** Do not have ownership of the stored references */
				map<uint, vector<weak_ptr<Edge<VectorType>>>> m_vertexToEdgeMap;
			};

			#pragma endregion
			#pragma region ClassMembers
			/**Undirected edges of this cell */
			vector<shared_ptr<Edge<VectorType>>> m_edges;

			/**Vertices of this cell */
			vector<shared_ptr<Vertex<VectorType>>> m_vertices;

			/** Edges by location, access by halfEdgeLocation enums */
			vector<shared_ptr<Edge<VectorType>>> m_edgesByLocation[5];

			/** Split cells*/
			vector<shared_ptr<Cell<VectorType>>> m_cells;

			/** Regular grid location and scale, if applicable */
			dimensions_t m_gridCellLocation;
			DoubleScalar m_gridDx;
			cellLocation_t m_cellLocation;

			/** ID vars */
			uint m_ID;
			static uint m_currID;

			/** Cell type */
			cellType_t m_cellType;

			/** Error dealing rollback variable. If set to true, will treat the exception and return a general cell
				containing all edges present on the cell.*/
			static bool m_handleInvalidConfigurationException;

			/** Each quadCell has a vertex to edge map that maps the elements of that specific face. This is a helper
			    structure that selects the edges connected to the vertex of this specific face. This is useful when
					splitting the quadCells into cells. */
			VertexToEdgeMap<VectorType> m_vertexToEdgeMap;
			#pragma endregion

			#pragma region PrivateFunctionalities
			/** Splits this cell into a series of half-cells. It uses simples 2-D heuristics to make correct turns. *
				Initializes the internal cells vector structure. Stores the result in m_halfCells structure */
			void split();

			/** Handles exceptions. For now just creates a regular grid cell, but more complex schemes can be implemented
				in the future.*/
			void handleException(const exception &e, shared_ptr<HalfEdge<VectorType>> pEdge);

			/** Creates an empty regular halfCell for handling exceptions and produce at least an approximate solution.
				It receives a non-geometric edge, which will be traversed to create a regular grid cell. */
			void createRegularHalfCell(shared_ptr<Edge<VectorType>> pEdge);

			/** Search for unvisited grid half-edges. EdgeID will contain the first non-visited element found. */
			shared_ptr<HalfEdge<VectorType>> hasUnvisitedEdges();

			/** Search for unvisited geometry edges. EdgeID will contain the first non-visited element found. */
			shared_ptr<HalfEdge<VectorType>>  hasUnvisitedGeometryEdges() {
				for (uint i = 0; i < m_edges.size(); i++) {
					if (m_edges[i]->getType() == geometricEdge) {
						if (!m_edges[i]->getHalfEdges().first->isVisited())
							return m_edges[i]->getHalfEdges().first;
						if (!m_edges[i]->getHalfEdges().second->isVisited())
							return m_edges[i]->getHalfEdges().second;
					}
				}
				return nullptr;
			}

			/** Depth first search: in 2-D the connectivity graph does not branches, but the depth-first principle is
				applied. This recursive function will call itself until it has visited all edges on a cycle. The visited
				edges will be appended to halfedges vector. A custom exterior may be specified for the purpose of recursion*/
			uint depthFirstSearch(shared_ptr<HalfEdge<VectorType>> pEdge, vector<shared_ptr<HalfEdge<VectorType>>> &halfEdges,
														const unordered_set<shared_ptr<Edge<VectorType>>> &exterior = unordered_set<shared_ptr<Edge<VectorType>>>());

			/** Halfcell location trick for making the correct turn when coming from a geometry edge into a grid edge.
			    Uses the idea that a cell should always be counterclockwise to find the correct half-edge given a location.
				Also considers that the first halfedge of edge will be positive vector relative to the Cartesian space. */
			shared_ptr<HalfEdge<VectorType>> getOrientedHalfEdge(shared_ptr<Edge<VectorType>> pEdge, halfEdgeLocation_t halfEdgeLocation);

			/** used to evaluate if a set of halfedges is ordered counterclockwise*/
			template<class Iterator>
			bool isCounterClockwise(Iterator begin,
															Iterator end);

			void cycleDFS(shared_ptr<HalfEdge<VectorType>> pCurrEdge,
										vector<shared_ptr<HalfEdge<VectorType>>> &traversedEdges,
										unordered_set<shared_ptr<HalfEdge<VectorType>>> &halfEdgeSet,
										unordered_set<shared_ptr<Edge<VectorType>>> &visitedEdges);

			void findGeometryCycles(vector<shared_ptr<HalfEdge<VectorType>>> &geometryInterior);
			#pragma endregion
		};

		template <class VectorType>
		unsigned int QuadCell<VectorType>::m_currID = 0;

	}
}

#endif
