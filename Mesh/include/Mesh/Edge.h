//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_EDGE_H_
#define __CHIMERA_EDGE_H_

#include "ChimeraCore.h"
#include "MeshesCoreDefs.h"
#include "Mesh/Vertex.h"
#include "Mesh/HalfEdge.h"
#include <memory>

#pragma once

namespace Chimera {
	namespace Meshes {

		/** Connected Structures Forward Declaration */
		template <class VectorType>
		class Cell;

		template <class VectorType>
		class Voxel;

		/** Edge structure: stores centroids and attributes relative to edges. It might have the ownership of
		  * two half-edges, which are created on the construction of this class. A edge could be created with
		  * only one side, this is configured by the last parameter. In this way, the edge has only one true side. */
		template <class VectorType>
		class Edge : public std::enable_shared_from_this<Edge<VectorType>>,
					public OwnCustomAttribute<VectorType>, public OwnCustomAttribute <Scalar>,
					public OwnConnectedStructure<Cell<VectorType>>, public OwnConnectedStructure<Voxel<VectorType>> {
		private:

			#pragma region Constructors

			/** Edge constructor is private! We only want shared_ptr instances of edges since the complicated memory
				management that this class entails. */
			Edge(shared_ptr<Vertex<VectorType>> pV1, shared_ptr<Vertex<VectorType>> pV2, edgeType_t edgeType, bool borderEdge = false) {
				m_ID = m_currID++;
				m_vertices.first = pV1;
				m_vertices.second = pV2;

				/*if (pV1->getPosition() == pV2->getPosition()) {
					throw std::logic_error("Attempted to construct Edge with identical vertices");
				}*/

				m_edgeType = edgeType;
				m_borderEdge = borderEdge;
				m_boundaryCondition = none;

				m_centroid = (pV1->getPosition() + pV2->getPosition())*0.5;
			}

		public:
			/** True class constructor: creates a shared pointer and redirects it to edge constructors. Also deals
				with setting proper half-edges weak references.*/
			template <class ... Args>
			static shared_ptr<Edge<VectorType>> create(Args&& ... args) {
				//TODO: Replace with make_shared to avoid unnecessary copy
				shared_ptr<Edge<VectorType>> newEdge(new Edge(std::forward<Args>(args)...));
				newEdge->getHalfEdges().first = HalfEdge<VectorType>::create(newEdge->getVertices().first, newEdge->getVertices().second, newEdge->shared_from_this());

				/** Add connectivity information to vertices*/
				OwnConnectedStructure<Edge<VectorType>>* pConnectedEdge1 = dynamic_cast<OwnConnectedStructure<Edge<VectorType>>*>(newEdge->getVertices().first.get());
				pConnectedEdge1->addConnectedStructure(newEdge->getID(), newEdge);
				OwnConnectedStructure<Edge<VectorType>>* pConnectedEdge2 = dynamic_cast<OwnConnectedStructure<Edge<VectorType>>*>(newEdge->getVertices().second.get());
				pConnectedEdge2->addConnectedStructure(newEdge->getID(), newEdge);

				if (!newEdge->isBorder()) {
					newEdge->getHalfEdges().second = HalfEdge<VectorType>::create(newEdge->getVertices().second, newEdge->getVertices().first, newEdge->shared_from_this());
				}

				newEdge->setVisited(false);

				if (newEdge->getType() == xAlignedEdge) {
					newEdge->getHalfEdges().first->setLocation(bottomHalfEdge);
					newEdge->getHalfEdges().second->setLocation(topHalfEdge);
				}
				else if (newEdge->getType() == yAlignedEdge) {
					newEdge->getHalfEdges().first->setLocation(rightHalfEdge);
					newEdge->getHalfEdges().second->setLocation(leftHalfEdge);
				}

				return newEdge;
			}
			virtual ~Edge(){
			}

			static void resetIDs() {
				m_currID = 0;
			}
			#pragma endregion

			#pragma region AccessFunctions
			const pair<shared_ptr<HalfEdge<VectorType>>, shared_ptr<HalfEdge<VectorType>>> & getHalfEdges() const {
				return m_halfEdges;
			}

			const pair<shared_ptr<Vertex<VectorType>>, shared_ptr<Vertex<VectorType>>> & getVertices() const {
				return m_vertices;
			}

			/** Only to be used by create function, avoiding other classes to set halfEdges or vertices later */
			pair<shared_ptr<HalfEdge<VectorType>>, shared_ptr<HalfEdge<VectorType>>> & getHalfEdges() {
				return m_halfEdges;
			}

			pair<shared_ptr<Vertex<VectorType>>, shared_ptr<Vertex<VectorType>>> & getVertices() {
				return m_vertices;
			}

			uint getID() const {
				return m_ID;
			}

			const VectorType & getCentroid() const {
				return m_centroid;
			}

			/** Returns the first vertex of the first halfedge */
			shared_ptr<Vertex<VectorType>> getVertex1() const {
				return m_halfEdges.first->getVertices().first;
			}

			/** Returns the second vertex of the first halfedge */
			shared_ptr<Vertex<VectorType>> getVertex2() const {
				return m_halfEdges.first->getVertices().second;
			}

			bool isVisited() const {
				return m_halfEdges.first->isVisited() || m_halfEdges.second->isVisited();
			}

			void setVisited(bool visited) {
				m_halfEdges.first->setVisited(visited);
				if (m_halfEdges.second) {
					m_halfEdges.second->setVisited(visited);
				}
			}

			edgeType_t getType() const {
				return m_edgeType;
			}

			DoubleScalar getLength() const {
				return (m_vertices.second->getPosition() - m_vertices.first->getPosition()).length();
			}

			bool isBorder() const {
				return m_borderEdge;
			}

			void setBorder(bool border) {
				m_borderEdge = border;
			}

			void setBoundaryCondition(boundaryCondition_t condition) {
				m_boundaryCondition = condition;
			}

			boundaryCondition_t getBoundaryCondition() {
				return m_boundaryCondition;
			}

			shared_ptr<Edge<VectorType>> getDualElement2D() {
				return m_dualElement2D;
			}

			void setDualElement2D(shared_ptr<Edge<VectorType>> dualEdge) {
				m_dualElement2D = dualEdge;
			}

			bool intersects(const VectorType& position, DoubleScalar accuracy = 1e-5) {
				return isOnEdge(m_vertices.first->getPosition(), m_vertices.second->getPosition(), position, accuracy);
			}
			#pragma endregion

			#pragma region Operators
			FORCE_INLINE bool operator==(const Edge<VectorType> &rhs) const {
				return m_ID == rhs.getID();
			}
			#pragma endregion

			#pragma region Functionalities
			static halfEdgeLocation_t classifyEdge(shared_ptr<Edge<VectorType>> pEdge, const dimensions_t &gridDimensions, DoubleScalar gridDx, cellLocation_t cellLocation);
			static halfEdgeLocation_t classifyEdgeXY(shared_ptr<Edge<VectorType>> pEdge, const dimensions_t &gridDimensions, DoubleScalar gridDx);
			static halfEdgeLocation_t classifyEdgeXZ(shared_ptr<Edge<VectorType>> pEdge, const dimensions_t &gridDimensions, DoubleScalar gridDx);
			static halfEdgeLocation_t classifyEdgeYZ(shared_ptr<Edge<VectorType>> pEdge, const dimensions_t &gridDimensions, DoubleScalar gridDx);
			#pragma endregion


		protected:
			#pragma region ClassMembers
			/** Half edges are owned by this class*/
			pair<shared_ptr<HalfEdge<VectorType>>, shared_ptr<HalfEdge<VectorType>>> m_halfEdges;
			/** Vertices are owned by this class as well, since edges cannot exist without vertices*/
			pair<shared_ptr<Vertex<VectorType>>, shared_ptr<Vertex<VectorType>>> m_vertices;

			/** VectorType properties */
			VectorType m_centroid;
			VectorType m_normal;

			/** Border edge marker and edge type*/
			bool m_borderEdge; // an edge is a borderEdge if it lies on the outer boundary of the domain
			edgeType_t m_edgeType;
			boundaryCondition_t m_boundaryCondition;

			/** ID vars */
			uint m_ID;
			static uint m_currID;

			/** 2D Dual element*/
			shared_ptr<Edge<VectorType>> m_dualElement2D;
			#pragma endregion
		};

		template <class VectorType>
		unsigned int Edge<VectorType>::m_currID = 0;
	}
}

#endif
