//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#pragma once
#ifndef __CHIMERA_VOXEL_H_
#define __CHIMERA_VOXEL_H_

#pragma once

#include "Mesh/Cell.h"
#include "Mesh/HalfVolume.h"

namespace Chimera {

	namespace Meshes {

		/** Forward Declarations */
		template <class VectorType>
		class HexaVolume;

		template <class VectorType>
		class Volume : public std::enable_shared_from_this<Volume<VectorType>>,
									public OwnCustomAttribute<VectorType>, public OwnCustomAttribute <Scalar>
		{
		
		private:
			#pragma region Constructors
			/** Volume constructor is private! We only want shared_ptr instances of volumes since the complicated
				memory management that this class entails. */
			Volume(const vector<shared_ptr<Cell<VectorType>>> &cells) : m_cells(cells) {
				m_ID = m_currID++;
				for (auto pCell : m_cells) {
					for (auto pEdge : pCell->getEdges()) {
						m_verticesMap[pEdge->getVertices().first->getID()] = pEdge->getVertices().first;
						m_verticesMap[pEdge->getVertices().second->getID()] = pEdge->getVertices().second;
					}
				}
			}

		public:
			/** True class constructor: creates a shared pointer and redirects it to Volume constructors. */
			static shared_ptr<Volume<VectorType>> create(const vector<shared_ptr<HalfCell<VectorType>>>& halfCells) {
				vector<shared_ptr<Cell<VectorType>>> cells;

				for (auto pHalfCell : halfCells) {
					cells.push_back(pHalfCell->getCell());
				}

				shared_ptr<Volume<VectorType>> newVolume(new Volume(cells));
				newVolume->getHalfVolumes().first = HalfVolume<VectorType>::create(halfCells, newVolume->shared_from_this());
				//No need for a second mirrored halfVolume for now
				newVolume->getHalfVolumes().second = nullptr;

				/** Update connectivity */
				/*for (auto pEdge : edges) {
					OwnConnectedStructure<Cell<VectorType>>* pEdgeInterface = dynamic_cast<OwnConnectedStructure<Cell<VectorType>>*>(pEdge.get());
					pEdgeInterface->addConnectedStructure(newCell->getID(), newCell->shared_from_this());
				}*/

				return newVolume;
			}

			static void resetIDs() {
				m_currID = 0;
			}
			#pragma endregion 

			#pragma region Functionalities
			/** Checks if a given point is inside this volume */
			bool isInside(const VectorType& position, VectorType direction = VectorType(0, 0, 1)) const;

			/** Checks if the specified line segment crosses a geometry face */
			bool crossedThroughGeometry(const VectorType& v1, const VectorType& v2, VectorType& crossingPoint);
			#pragma endregion


			#pragma region AccessFunctions
			uint getID() const {
				return m_ID;
			}

			const vector<shared_ptr<Cell<VectorType>>> & getCells() const {
				return m_cells;
			}

			const pair<shared_ptr<HalfVolume<VectorType>>, shared_ptr<HalfVolume<VectorType>>> & getHalfVolumes() const {
				return m_halfVolumes;
			}

			pair<shared_ptr<HalfVolume<VectorType>>, shared_ptr<HalfVolume<VectorType>>> & getHalfVolumes() {
				return m_halfVolumes;
			}

			template <class ... Args>
			shared_ptr<HalfVolume<VectorType>> constructHalfVolume(Args&& ... args) {
				m_halfVolumes.push_back(HalfVolume<VectorType>::create(std::forward<Args>(args)...));
				return m_halfVolumes.back();
			}

			void setInvalid() {
				m_invalid = true;
			}

			bool isInvalid() const{
				return m_invalid;
			}

			const VectorType& getCentroid() const {
				return m_centroid;
			}

			void setHexaVolume(shared_ptr<HexaVolume<VectorType>> pHexaVolume) {
				m_pHexaVolume = pHexaVolume;
			}
			shared_ptr<HexaVolume<VectorType>> getHexaVolume() {
				return m_pHexaVolume.lock();
			}

			const map<uint, shared_ptr<Vertex<VectorType>>> & getVerticesMap() const {
				return m_verticesMap;
			}

			#pragma endregion

			#pragma region Operators
			FORCE_INLINE bool operator==(const Volume<VectorType> &rhs) const {
				return m_ID == rhs.getID();
			}
			#pragma endregion

			
		protected:
			
			#pragma region ClassMembers
			/** Interior split half volumes */
			pair<shared_ptr<HalfVolume<VectorType>>, shared_ptr<HalfVolume<VectorType>>> m_halfVolumes;

			/**Undirected cells of this volume */
			vector<shared_ptr<Cell<VectorType>>> m_cells;

			/** Centroid */
			VectorType m_centroid;

			/** ID vars */
			uint m_ID;
			static uint m_currID;

			/** Parent QuadCell reference */
			weak_ptr<HexaVolume<VectorType>> m_pHexaVolume;

			/** Vertices map by ID*/
			map<uint, shared_ptr<Vertex<VectorType>>> m_verticesMap;

			bool m_invalid = false;
			#pragma endregion
		};

		template <class VectorType>
		unsigned int Volume<VectorType>::m_currID = 0;

	}
}

#pragma once
#endif
