#include "Mesh/HalfVolume.h"
//#include "Mesh/Volume.h"

namespace Chimera {
	namespace Meshes {
		
		template class HalfVolume<Vector3D>;
		template class HalfVolume<Vector3>;

	}
}
