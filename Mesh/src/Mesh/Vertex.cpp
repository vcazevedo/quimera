#include "Mesh/Vertex.h"
//#include "Mesh/Edge.h"
//#include "Mesh/Cell.h"
//#include "Mesh/Voxel.h"

namespace Chimera {
	namespace Meshes {
		
		template class Vertex<Vector2>;
		template class Vertex<Vector2D>;
		template class Vertex<Vector3>;
		template class Vertex<Vector3D>;

	}
}
