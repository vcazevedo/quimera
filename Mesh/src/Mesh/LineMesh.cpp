//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.
//	
#include "Mesh/LineMesh.h"

namespace Chimera {
	
	namespace Meshes {

		#pragma region Constructors
		template <class VectorType>
		LineMesh<VectorType>::LineMesh(const params_t &lineMeshParams) {
			m_hasUpdated = false;
			m_params = lineMeshParams;

			if (m_params.extrudeAlongNormalWidth != 0.0f) {
				extrudePointsAlongNormals(m_params.extrudeAlongNormalWidth, m_params.initialPoints);
				m_isClosedMesh = true;
			}

			uint numPoints = m_params.initialPoints.size();
			if (m_isClosedMesh)
				numPoints--; //avoid duplicates

			if (m_params.initialEdges.empty()) {
				for (uint i = 0; i + 1 < numPoints; ++i) {
					m_params.initialEdges.emplace_back(i, i + 1);
				}
				if (m_isClosedMesh && m_params.initialPoints.size() >= 3) {
					m_params.initialEdges.push_back(pair<uint, uint>(m_params.initialEdges.back().second, 0));
				}
			}

			initializeVertices();
			initializeEdges();

			initializeVertexNormals();

			for (shared_ptr<Vertex<VectorType>> pV : this->getVertices()) {
				OwnConnectedStructure<Edge<VectorType>>* pConnectedEdges = dynamic_cast<OwnConnectedStructure<Edge<VectorType>>*>(pV.get());

				if (pConnectedEdges->getConnectedStructures().size() == 1) {
					addBorderVertex(pV);
				}
			}

			if (!lineMeshParams.initialPoints.empty())
				m_isClosedMesh = lineMeshParams.initialPoints.back() == lineMeshParams.initialPoints.front();
			
		}

		template <class VectorType>
		LineMesh<VectorType>::LineMesh(const vector<shared_ptr<Vertex<VectorType>>> &vertices, const vector<shared_ptr<Edge<VectorType>>> &edges) :
			LineMesh::Mesh(vertices, edges),
			m_isClosedMesh(false) {
			m_hasUpdated = false;

			if (!vertices.empty())
				m_isClosedMesh = vertices.back()->getPosition() == vertices.front()->getPosition();
		}

		#pragma endregion
		
		#pragma region Functionalities
		template<class VectorType>
		bool LineMesh<VectorType>::segmentIntersection(const VectorType &v1, const VectorType &v2) {
			for (int i = 0; i < static_cast<int>(this->getElements().size()) - 1; i++) {
				if (DoLinesIntersect(v1, v2, this->getElements()[i]->getVertex1()->getPosition(), this->getElements()[i]->getVertex2()->getPosition()))
					return true;
			}
			return false;
		}

		template<class VectorType>
		bool LineMesh<VectorType>::segmentIntersection(const VectorType &v1, const VectorType &v2, VectorType &outputVec) {
			for (int i = 0; i < static_cast<int>(this->getElements().size()) - 1; i++) {
				if (DoLinesIntersect(v1, v2, this->getElements()[i]->getVertex1()->getPosition(), this->getElements()[i]->getVertex2()->getPosition(), outputVec))
					return true;
			}
			return false;
		}


		template<class VectorType>
		Scalar LineMesh<VectorType>::getDistanceToPoint(const VectorType &position) {
			Scalar minDistance = FLT_MAX;
			for (int i = 0; i < this->m_vertices.size(); i++) {
				int nextI = roundClamp<int>(i + 1, 0, this->m_vertices.size());
				Scalar currDistance = distanceToLineSegment(position, this->getVertices()[i]->getPosition(), this->getVertices()[nextI]->getPosition());
				if (currDistance < minDistance) {
					minDistance = currDistance;
				}
			}
			return minDistance;
		}

		#pragma endregion

		#pragma region InitializationFunctions
		template<class VectorType>
		void LineMesh<VectorType>::initializeEdges() {
			for (pair<uint, uint> edge : m_params.initialEdges) {
				this->m_elements.push_back(Edge<VectorType>::create(this->getVertices()[edge.first], this->getVertices()[edge.second], geometricEdge));
			}
		}
	
	
		template<class VectorType>
		void LineMesh<VectorType>::initializeVertexNormals() {
			int start;
			int end;
			if (m_isClosedMesh) {
				start = 0;
				end = this->getVertices().size();
			}
			else {
				start = 1;
				end = this->getVertices().size() - 1;
			}
			for (int i = start; i < end; i++) {
				int nextI = roundClamp<int>(i + 1, 0, this->getVertices().size());
				int prevI = roundClamp<int>(i - 1, 0, this->getVertices().size());

				VectorType prevNormal = this->getVertices()[i]->getPosition() - this->getVertices()[prevI]->getPosition();
				VectorType nextNormal = this->getVertices()[nextI]->getPosition() - this->getVertices()[i]->getPosition();
				prevNormal = prevNormal.perpendicular();
				nextNormal = nextNormal.perpendicular();
				this->getVertices()[i]->setNormal((prevNormal + nextNormal).normalized());
			}
			if (!m_isClosedMesh) {
				this->getVertices().front()->setNormal((this->getVertices()[1]->getPosition() - this->getVertices().front()->getPosition()).perpendicular().normalized());
				this->getVertices().back()->setNormal((this->getVertices().back()->getPosition() - this->getVertices()[this->getVertices().size() - 2]->getPosition()).perpendicular().normalized());
			}
		}
		#pragma endregion

		#pragma region PrivateFunctionalities
		template<class VectorType>
		void LineMesh<VectorType>::extrudePointsAlongNormals(Scalar extrudeAlongNormalWidth, vector<VectorType> &points) {
			//TODO: make this work for non-manifold meshes
			vector<VectorType> tempPoints(points);
			tempPoints.resize(points.size() * 2);

			for (int i = 0; i < points.size(); i++) {
				VectorType normal;
				if (i == points.size() - 1) {
					normal = (points.back() - points[points.size() - 2]);
				}
				else if (i == 0) {
					normal = points[i + 1] - points[i];
				}
				else {
					normal = (points[i + 1] - points[i])*0.5 +
						(points[i] - points[i - 1])*0.5;
				}
				normal = normal.perpendicular().normalized();

				VectorType tempNode = points[i] + normal*extrudeAlongNormalWidth;
				tempPoints[(tempPoints.size() - 1) - i] = points[i] + normal*extrudeAlongNormalWidth;
			}

			points.resize(tempPoints.size());
			for (uint i = 0; i < tempPoints.size(); i++) {
				points[i] = tempPoints[i];
			}

			points.push_back(points.front());
		}
		#pragma endregion

		template class LineMesh<Vector2>;
		template class LineMesh<Vector2D>;
		template class LineMesh<Vector3>;
		template class LineMesh<Vector3D>;
	}
}
