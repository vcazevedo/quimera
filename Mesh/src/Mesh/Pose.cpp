#include "Mesh/Pose.h"

namespace Chimera {
	namespace Meshes {

		#pragma region Functionalities
		template <class VectorT, bool isVector2>
		void PoseBase<VectorT, isVector2>::updatePositions(const PoseBase<VectorT, isVector2> &pose, const vector <shared_ptr<Vertex<VectorT>>> &vertices) {

			//Rotation is always applied at the center of mass
			for (auto pVertex : vertices) {
				pVertex->setPosition(pVertex->getPosition() - m_centroid);
			}

			/** Applies the rotation, handled by child classes that consider 2-D or 3-D spaces */
			applyRotation(pose, vertices);

			//Reversing initial translation + adding the translation due the difference of centroids
			VectorT translation = pose.getCentroid() - m_centroid;
			for (auto pVertex : vertices) {
				pVertex->setPosition(pVertex->getPosition() + m_centroid + translation);
			}

			m_centroid = pose.getCentroid();
		}
		#pragma endregion

		#pragma region PrivateFunctionalities2D
		template<class VectorT>
		void PoseT<VectorT, true>::applyRotation(const PoseBase<VectorT, true> &pose, const vector<shared_ptr<Vertex<VectorT>>> &vertices) {
			const PoseT<VectorT, true> *pPose = dynamic_cast<const PoseT<VectorT, true> *>(&pose);
			if (pPose) {
				DoubleScalar angleDifference = pPose->getRotation() - m_rotation;
				for (auto pVertex : vertices) {
					pVertex->getPosition().rotate(angleDifference);
				}
			}
			else {
				throw(std::logic_error("Pose: error in applying a 3-D pose to a 2-D pose"));
			}

			m_rotation = pPose->getRotation();
		}

		#pragma endregion

		#pragma region PrivateFunctionalities3D
		template<class VectorT>
		void PoseT<VectorT, false>::applyRotation(const PoseBase<VectorT, false> &pose, const vector<shared_ptr<Vertex<VectorT>>> &vertices) {
			const PoseT<VectorT, false> *pPose = dynamic_cast<const PoseT<VectorT, false> *>(&pose);
			if (pPose) {
				/** TODO: apply quaternion trickery to find the difference between rotations */
				/*Quaternion<VectorT> slerpQuat = Quaternion<VectorT>::slerp(pose.getRotation();
				for (auto pVertex : vertices) {
					pVertex->getPosition().rotate(angleDifference);
				}*/
			}
			else {
				throw(std::logic_error("Pose: error in applying a 2-D pose to a D-D pose"));
			}
			m_rotation = pPose->getRotation();
		}

		#pragma endregion

		template class PoseBase<Vector2, isVector2<Vector2>::value>;
		template class PoseBase<Vector2D, isVector2<Vector2D>::value>;
		template class PoseBase<Vector3, isVector2<Vector3>::value>;
		template class PoseBase<Vector3D, isVector2<Vector3D>::value>;

		template class PoseT<Vector2, isVector2<Vector2>::value>;
		template class PoseT<Vector2D, isVector2<Vector2D>::value>;
		template class PoseT<Vector3, isVector2<Vector3>::value>;
		template class PoseT<Vector3D, isVector2<Vector3D>::value>;
	}
}
