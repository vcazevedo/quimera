#include "Mesh/Volume.h"

namespace Chimera {
	namespace Meshes {

		#pragma region Functionalities
		template<class VectorType>
		bool Volume<VectorType>::isInside(const VectorType& position, VectorType direction) const {
			bool insidePolygon = false;
			for (int i = 0; i < m_cells.size(); i++) {
				if (m_cells[i]->getLocation() == geometricCell) {
					if (m_cells[i]->rayIntersect(position, direction)) {
						insidePolygon = !insidePolygon;
					}
				}
				else {
					if (direction == VectorType(0, 0, 1) && m_cells[i]->getLocation() == XYPlane) { //XY Plane
						VectorType projectedPoint(position.x, position.y, 0);
						if (m_cells[i]->isInside(projectedPoint)) {
							insidePolygon = !insidePolygon;
						}
					}
					else if (direction == VectorType(0, 1, 0) && m_cells[i]->getLocation() == XZPlane) { //XZ Plane
						VectorType projectedPoint(position.x, 0, position.z);
						if (m_cells[i]->isInside(projectedPoint)) {
							insidePolygon = !insidePolygon;
						}
					}
					else if (direction == VectorType(1, 0, 0) && m_cells[i]->getLocation() == YZPlane) { //YZ Plane
						VectorType projectedPoint(0, position.y, position.z);
						if (m_cells[i]->isInside(projectedPoint)) {
							insidePolygon = !insidePolygon;
						}
					}
				}
			}
			return insidePolygon;
		}

		template<class VectorType>
		bool Volume<VectorType>::crossedThroughGeometry(const VectorType& v1, const VectorType& v2, VectorType& crossingPoint) {
			for (int i = 0; i < m_cells.size(); i++) {
				if (m_cells[i]->getLocation() == geometricCell) {
					VectorType triPoints[3];
					triPoints[0] = m_cells[i]->getHalfCells().first->getHalfEdges()[0]->getVertices().first->getPosition();
					triPoints[1] = m_cells[i]->getHalfCells().first->getHalfEdges()[1]->getVertices().first->getPosition();
					triPoints[2] = m_cells[i]->getHalfCells().first->getHalfEdges()[2]->getVertices().first->getPosition();
					if (segmentTriangleIntersect(v1, v2, triPoints)) {
						return true;
					}
				}
			}
			return false;
		}
		#pragma endregion

		template class Volume<Vector3D>;
		template class Volume<Vector3>;
	}
}
