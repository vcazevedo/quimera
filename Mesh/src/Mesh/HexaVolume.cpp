#include "Mesh/HexaVolume.h"

namespace Chimera {
	namespace Meshes {

		#pragma region Functionalities
		template<class VectorType>
		shared_ptr<Volume<VectorType>> HexaVolume<VectorType>::getVolume(const VectorType& position) {
			for (int i = 0; i < m_volumes.size(); i++) {
				if (m_volumes[i]->isInside(position)) {
					return m_volumes[i];
				}
			}
			//Didn't found cells using the standard orientations, try other ones
			Logger::getInstance()->get() << "Volume not found with standard direction, try another ones" << endl;
			for (int i = 0; i < m_volumes.size(); i++) {
				if (m_volumes[i]->isInside(position, VectorType(0, 1, 0))) {
					return m_volumes[i];
				}
			}

			Logger::getInstance()->get() << "Volume not found with standard direction, try last one" << endl;
			for (int i = 0; i < m_volumes.size(); i++) {
				if (m_volumes[i]->isInside(position, VectorType(1, 0, 0))) {
					return m_volumes[i];
				}
			}

			Logger::getInstance()->get() << "Volume not found with all standard direction, returning first one" << endl;
			return m_volumes[0];
			//Didn't find any, return null

			return nullptr;
		}


		#pragma endregion

		#pragma region PrivateFunctionalities
		template<class VectorType>
		void HexaVolume<VectorType>::split() {
			vector<shared_ptr<HalfCell<VectorType>>> halfCells;
			shared_ptr<Cell<VectorType>> pCell = nullptr;


			for (int i = 0; i < m_cells.size(); i++) {
				m_cells[i]->getHalfCells().first->setVisited(false);
				m_cells[i]->getHalfCells().second->setVisited(false);

				for (auto halfcellPair : m_cells[i]->getDisconnectedHalfCells()) {
					/*if (halfcell->isInvalid()) {
						setInvalid();
					}*/
					halfcellPair.first->setVisited(false);
					halfcellPair.second->setVisited(false);
				}
			}

			while ((pCell = hasUnvisitedCells()) != nullptr) {
				halfCells.clear();

				/** After closing a loop with breadthFirstSearch, halfEdgesVector will have a closed half-cell */
				breadthFirstSearch(getHalfCell(pCell, m_gridCellLocation), halfCells, nullptr);

				m_volumes.push_back(Volume<VectorType>::create(halfCells));

				for (shared_ptr<HalfCell<VectorType>> cell : halfCells) {
					/*if (cell->isInvalid()) {

					}*/
				}

				/** Adding connectivity information for half-volumes */
				//for (int i = 0; i < m_volumes().back()->getHalfCells().size(); i++) {
				//	shared_ptr<Cell<VectorType>> pCurrCell = m_volumes().back()->getHalfCells()[i]->getCell();
				//	bool repeat = false;
				//	if (pCurrCell->getLocation() != geometricCell) {
				//		for (weak_ptr<HalfVolume<VectorType>> v : pCurrCell->getConnectedHalfVolumes()) {
				//			if (*v.lock() == *m_volumes().back()) {
				//				throw std::logic_error("grid cell added twice");
				//				repeat = true;
				//				break;
				//			}
				//		}
				//	}
				//	/*if (!repeat) {
				//		pCurrCell->addConnectedHalfVolume(m_volumes().back());
				//	}*/
				//}
			}

			//Leave all edges Unvisited
			for (int i = 0; i < m_cells.size(); i++) {
				m_cells[i]->getHalfCells().first->setVisited(false);
				m_cells[i]->getHalfCells().second->setVisited(false);

				for (auto halfcellPair : m_cells[i]->getDisconnectedHalfCells()) {
					/*if (halfcell->isInvalid()) {
						setInvalid();
					}*/
					halfcellPair.first->setVisited(false);
					halfcellPair.second->setVisited(false);
				}
			}

		}

		template<class VectorType>
		shared_ptr<HalfCell<VectorType>> HexaVolume<VectorType>::getHalfCell(shared_ptr<Cell<VectorType>> pCell, dimensions_t cellLocation) {

			//Any point will work for checking where the cell is
			const VectorType cellPoint = pCell->getEdges().front()->getCentroid();

			switch (pCell->getLocation()) {
			case geometricCell:
				return nullptr;

			case XZPlane: //Bottom Cell
				if (floor(cellPoint.y / m_gridDx) == cellLocation.y) {
					return pCell->getHalfCells().first;
				}
				break;
			case YZPlane: //Left Cell
				if (floor(cellPoint.x / m_gridDx) == cellLocation.x) {
					return pCell->getHalfCells().first;
				}
				break;
			case XYPlane: //Back Cell
				if (floor(cellPoint.z / m_gridDx) == cellLocation.z) {
					return pCell->getHalfCells().first;
				}
				break;
			}
			return pCell->getHalfCells().second;

		}

		template<class VectorType>
		void HexaVolume<VectorType>::breadthFirstSearch(shared_ptr<HalfCell<VectorType>> pHalfCell, vector<shared_ptr<HalfCell<VectorType>>> &halfCells, shared_ptr<HalfEdge<VectorType>> pPrevHalfEdge) {
			if (pHalfCell->isVisited())
				return;

			//Tag this cell as visited
			pHalfCell->setVisited(true);
			halfCells.push_back(pHalfCell);

			for (int i = 0; i < pHalfCell->getHalfEdges().size(); i++) {
				shared_ptr<HalfEdge<VectorType>> pCurrHalfEdge = pHalfCell->getHalfEdges()[i];
				shared_ptr<Edge<VectorType>> pCurrEdge = pCurrHalfEdge->getEdge();
				shared_ptr<HalfEdge<VectorType>> otherHalfEdge = pCurrEdge->getHalfEdges().first == pCurrHalfEdge
					? pCurrEdge->getHalfEdges().second
					: pCurrEdge->getHalfEdges().first;
				//Choosing next cells
				const vector<weak_ptr<Cell<VectorType>>> &wNextCells = m_edgeToCellMap.getCells(pCurrEdge);
				vector<shared_ptr<Cell<VectorType>>> nextCells;
				for (weak_ptr<Cell<VectorType>> wCell : wNextCells) {
					nextCells.push_back(wCell.lock());
				}

				if (nextCells.size() > 3) { //Case that is currently not supported, abort
					throw(std::logic_error("Volume split: high frequency feature found on top of mesh."));
				}
				else if (nextCells.size() == 3) { //Going from gridEdges to geometryEdges or vice-versa
					if (pHalfCell->getLocation() == geometryHalfCell) {
						bool followThrough = false;
						for (int j = 0; j < 3; j++) {
							if (nextCells[j]->getLocation() != geometricCell) {
								shared_ptr<HalfCell<VectorType>> pNextHalfCell = getHalfCell(nextCells[j], m_gridCellLocation);
								if (!pNextHalfCell->hasHalfedge(pCurrHalfEdge)) {
									breadthFirstSearch(pNextHalfCell, halfCells, pCurrHalfEdge);
									followThrough = true;
									break;
								}
							}
						}
						if (!followThrough) {
							for (int j = 0; j < 3; j++) {
								if (nextCells[j]->getLocation() != geometricCell) {
									shared_ptr<HalfCell<VectorType>> pNextHalfCell = getHalfCell(nextCells[j], m_gridCellLocation);
									//if it contains both half edges, both sides of that cell are the same (due to it being dangling)
									//and we can choose that as the next one.
									if (pNextHalfCell->hasHalfedge(otherHalfEdge)) {
										breadthFirstSearch(pNextHalfCell, halfCells, pCurrHalfEdge);
										followThrough = true;
										break;
									}
								}
							}
							if (!followThrough) {
								throw std::logic_error("Volume bfs: no neighboring cell candidates in geometry to grid cell case");
							}
						}
					} else {
						if (nextCells[0]->getLocation() == geometricCell) {
							shared_ptr<HalfCell<VectorType>> pNextHalfCell = nextCells[0]->getHalfCells().first;
							if (!pNextHalfCell->hasHalfedge(pCurrHalfEdge)) {
								breadthFirstSearch(pNextHalfCell, halfCells, pCurrHalfEdge);
							}
							else {
								breadthFirstSearch(nextCells[0]->getHalfCells().second, halfCells, pCurrHalfEdge);
							}

						}
						else if (nextCells[1]->getLocation() == geometricCell) {
							shared_ptr<HalfCell<VectorType>> pNextHalfCell = nextCells[1]->getHalfCells().first;
							if (!pNextHalfCell->hasHalfedge(pCurrHalfEdge)) {
								breadthFirstSearch(pNextHalfCell, halfCells, pCurrHalfEdge);
							}
							else {
								breadthFirstSearch(nextCells[1]->getHalfCells().second, halfCells, pCurrHalfEdge);
							}
						}
						else if (nextCells[2]->getLocation() == geometricCell) {
							shared_ptr<HalfCell<VectorType>> pNextHalfCell = nextCells[2]->getHalfCells().first;
							if (!pNextHalfCell->hasHalfedge(pCurrHalfEdge)) {
								breadthFirstSearch(pNextHalfCell, halfCells, pCurrHalfEdge);
							}
							else {
								breadthFirstSearch(nextCells[2]->getHalfCells().second, halfCells, pCurrHalfEdge);
							}
						}
						else {
							throw(std::logic_error("Error on vertex path choosing algorithm: not connected to geometric cell."));
						}
					}
				}
				else if (nextCells.size() == 2) {
					if (nextCells[0]->getID() == pHalfCell->getCell()->getID()) {
						if (!nextCells[1]->getHalfCells().first->hasHalfedge(pCurrHalfEdge)) {
							breadthFirstSearch(nextCells[1]->getHalfCells().first, halfCells, pCurrHalfEdge);
						} else if(!nextCells[1]->getHalfCells().second->hasHalfedge(pCurrHalfEdge)){
							breadthFirstSearch(nextCells[1]->getHalfCells().second, halfCells, pCurrHalfEdge);
						} else if (nextCells[1]->getLocation() != geometricCell) {
							breadthFirstSearch(getHalfCell(nextCells[1], m_gridCellLocation), halfCells, pCurrHalfEdge);
						}
						else {
							throw std::logic_error("Volume bfs: both geometric cells contain the same half edge");
						}
					}
					else {
						if (!nextCells[0]->getHalfCells().first->hasHalfedge(pCurrHalfEdge)) {
							breadthFirstSearch(nextCells[0]->getHalfCells().first, halfCells, pCurrHalfEdge);
						} else if(!nextCells[0]->getHalfCells().second->hasHalfedge(pCurrHalfEdge)){
							breadthFirstSearch(nextCells[0]->getHalfCells().second, halfCells, pCurrHalfEdge);
						} else if (nextCells[0]->getLocation() != geometricCell) {
							breadthFirstSearch(getHalfCell(nextCells[0], m_gridCellLocation), halfCells, pCurrHalfEdge);
						}
						else {
							throw std::logic_error("Volume bfs: both geometric cells contain the same half edge");
						}
					}
				}
				else { //This is a open ended point, go back through same geometric edges
					if (nextCells[0]->getLocation() != geometricCell) {
						cout << "Volume bfs: both geometric cells contain the same half edge" << endl;
						//TODO: prevent this from happening; this shouldn't happen at all!
						return;
					}
					if (nextCells[0]->getHalfCells().second == pHalfCell) {
						breadthFirstSearch(nextCells[0]->getHalfCells().first, halfCells, pCurrHalfEdge);
					}
					else {
						breadthFirstSearch(nextCells[0]->getHalfCells().second, halfCells, pCurrHalfEdge);
					}
				}

			}
		}
		#pragma endregion


		template<class VectorType>
		const vector<shared_ptr<Cell<VectorType>>>& getElements(shared_ptr<HexaVolume<VectorType>> pHexaVolume) {
			return pHexaVolume->getCells();
		}

		template<class VectorType>
		const vector<shared_ptr<Edge<VectorType>>>& getElements(shared_ptr<HexaVolume<VectorType>> pHexaVolume) {
			return pHexaVolume->getEdges();
		}

		template<class VectorType>
		const vector<shared_ptr<Vertex<VectorType>>>& getElements(shared_ptr<HexaVolume<VectorType>> pHexaVolume) {
			return pHexaVolume->getVertices();
		}

		// template const vector<shared_ptr<Vertex<Vector3>>>& getElements<Vector3, Vertex>(shared_ptr<HexaVolume<Vector3>> pHexaVolume);
		// template const vector<shared_ptr<Edge<Vector3>>>& getElements<Vector3, Edge>(shared_ptr<HexaVolume<Vector3>> pHexaVolume);
		// template const vector<shared_ptr<Cell<Vector3>>>& getElements<Vector3, Cell>(shared_ptr<HexaVolume<Vector3>> pHexaVolume);
		// template const vector<shared_ptr<Vertex<Vector3D>>>& getElements<Vector3D, Vertex>(shared_ptr<HexaVolume<Vector3D>> pHexaVolume);
		// template const vector<shared_ptr<Edge<Vector3D>>>& getElements<Vector3D, Edge>(shared_ptr<HexaVolume<Vector3D>> pHexaVolume);
		// template const vector<shared_ptr<Cell<Vector3D>>>& getElements<Vector3D, Cell>(shared_ptr<HexaVolume<Vector3D>> pHexaVolume);


		template class HexaVolume<Vector3D>;
		template class HexaVolume<Vector3>;
	}
}
