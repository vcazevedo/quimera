#include "Mesh/MeshSlicer.h"

#include "Mesh/Vertex.h"

#include "Math/Vector3.h"
#include "Math/Vector3d.h"

#include <map>
#include <numeric>

namespace Chimera {

	namespace Meshes {

		template <typename VectorType>
		MeshSlicer<VectorType>::MeshSlicer(Scalar gridDx, gridProximityResolution gridProximityResolution) :
			m_gridDx(gridDx),
			m_gridProximityResolution(gridProximityResolution),
			m_proximityTolerance(gridDx / ScalarType(1e5))
		{}

		template <typename VectorType>
		void MeshSlicer<VectorType>::slice(std::vector<VectorType>& vertices, std::vector<std::vector<uint> >& faces)
		{
			// snap close vertices onto the grid or push them away from the grid
			// this avoids tiny triangles at grid nodes and lets us use exact equality comparisons of
			// vertex and grid node positions in the remainder of the algorithm
			size_t numVertices = vertices.size();
			for (size_t i = 0; i < numVertices; i++) {
				for (uint j = 0; j < 3; j++) {
					proximityResolution(vertices[i][j]);
				}
			}

			// slice along each axis
			sliceAxis<YZPlane>(vertices, faces);
			sliceAxis<XZPlane>(vertices, faces);
			sliceAxis<XYPlane>(vertices, faces);

			// triangulate (simple fan triangulation)
			size_t numFaces = faces.size();
			for (size_t i = 0; i < numFaces; i++) {
				size_t numVertices = faces[i].size();
				if (numVertices == 3) {
					continue;
				}
				for (size_t j = 2; j < numVertices - 1; j++) {
					std::vector<uint>& face = faces[i];
					faces.push_back({ face[0], face[j], face[j + 1] });
				}
				faces[i].resize(3);
			}
		}

		template <typename VectorType>
		template <cellLocation_t Plane>
		void MeshSlicer<VectorType>::sliceAxis(std::vector<VectorType>& vertices, std::vector<std::vector<uint>>& faces) {

			auto edgeIndex = [&](const uint v1, const uint v2) {
				return std::make_tuple(vertices[v1][Plane], vertices[v1][(Plane + 1) % 3], vertices[v1][(Plane + 2) % 3]) <
					std::make_tuple(vertices[v2][Plane], vertices[v2][(Plane + 1) % 3], vertices[v2][(Plane + 2) % 3]) ?
					std::make_pair(v1, v2) : std::make_pair(v2, v1);
			};

			// build edge map
			std::map<std::pair<uint, uint>, std::vector<std::pair<int64_t, uint>>> edges;
			for (const std::vector<uint>& face : faces) {
				size_t numVertices = face.size();
				for (size_t v = 0; v < numVertices; v++) {
					std::pair<uint, uint> edge = edgeIndex(face[v], face[(v + 1) % numVertices]);
					edges.insert(std::make_pair(edge, std::vector<std::pair<int64_t, uint>>()));
				}
			}

			// slice edges
			for (auto& edge : edges) {
				VectorType v1 = vertices[edge.first.first];
				VectorType v2 = vertices[edge.first.second];
				ScalarType p1 = v1[Plane];
				ScalarType p2 = v2[Plane];
				int64_t slice = static_cast<int64_t>(std::ceil(p1 / m_gridDx));
				ScalarType pos = slice * m_gridDx;
				if (pos == p1) {
					slice++;
				}
				while ((pos = slice * m_gridDx) < p2) {
					VectorType v = v1 + ((pos - p1) / (p2 - p1)) * (v2 - v1);
					edge.second.push_back(std::make_pair(slice, static_cast<uint>(vertices.size())));
					for (uint j = 1; j < 3; j++) {
						proximityResolution(v[(Plane + j) % 3]);
					}

					vertices.push_back(v);
					slice++;
				}
				if (edge.second.size() > 0 && edge.second.back().first * m_gridDx == p2) {
					edge.second.pop_back();
				}
			}

			auto dir = [&](uint v1, uint v2) {
				return vertices[v1][Plane] == vertices[v2][Plane] ? 0 :
					(edgeIndex(v1, v2).first == v1 ? 1 : -1);
			};

			// build new topology
			size_t numFaces = faces.size();
			for (size_t i = 0; i < numFaces; i++) {
				const std::vector<uint>& face = faces[i];
				std::vector<std::unique_ptr<std::vector<uint>>> newFaces;
				newFaces.push_back(std::make_unique<std::vector<uint>>());
				std::vector<uint>* curFace = newFaces.back().get();
				std::stack<std::pair<int64_t, std::vector<uint>*>> facesToFinish;
				facesToFinish.push(std::make_pair(std::numeric_limits<int64_t>::max(), curFace));
				size_t numVertices = face.size();
				int8_t lastDir = dir(face[numVertices - 1], face[0]);
				for (size_t j = 0; j < numVertices; j++) {
					uint v = face[j];
					uint vNext = face[(j + 1) % numVertices];
					curFace->push_back(v);
					std::pair<uint, uint> edgeIdx = edgeIndex(v, vNext);
					int8_t curDir = dir(v, vNext);
					if (std::abs(lastDir + curDir) > 1) {
						ScalarType pos = vertices[v][Plane];
						int64_t slice = static_cast<int64_t>(std::round(pos / m_gridDx));
						if (pos == slice * m_gridDx) {
							curFace = processCutVertex(slice, v, facesToFinish, newFaces, curFace);
						}
					}
					lastDir = curDir;
					const std::vector<std::pair<int64_t, uint> >& edge = edges[edgeIdx];
					if (edge.empty()) {
						// the edge is not cut
						continue;
					}
					assert(curDir != 0);
					if (curDir > 0) {
						curFace = processEdge(edge.begin(), edge.end(), facesToFinish, newFaces, curFace);
					}
					else {
						curFace = processEdge(edge.rbegin(), edge.rend(), facesToFinish, newFaces, curFace);
					}
				}
				facesToFinish.pop();
				assert(facesToFinish.size() == 0);
				auto newFacesIt = newFaces.begin();
				faces[i] = **newFacesIt;
				newFacesIt++;
				for (; newFacesIt != newFaces.end(); newFacesIt++) {
					faces.push_back(**newFacesIt);
				}
			}
		}

		template <typename VectorType>
		template <typename FwdIt>
		std::vector<uint>* MeshSlicer<VectorType>::processEdge(FwdIt first, FwdIt last, std::stack<std::pair<int64_t, std::vector<uint>*>>& facesToFinish, std::vector<std::unique_ptr<std::vector<uint>>>& newFaces, std::vector<uint>* curFace) {
			for (; first != last; first++) {
				curFace->push_back(first->second);
				curFace = processCutVertex(first->first, first->second, facesToFinish, newFaces, curFace);
			}
			return curFace;
		}

		template <typename VectorType>
		std::vector<uint>* MeshSlicer<VectorType>::processCutVertex(int64_t slice, uint vertex, std::stack<std::pair<int64_t, std::vector<uint>*>>& facesToFinish, std::vector<std::unique_ptr<std::vector<uint>>>& newFaces, std::vector<uint>* curFace) {
			if (facesToFinish.top().first == slice) {
				// finish face
				facesToFinish.pop();
				curFace = facesToFinish.top().second;
			}
			else {
				// start a new face
				newFaces.push_back(std::make_unique<std::vector<uint>>());
				curFace = newFaces.back().get();
				facesToFinish.push(std::make_pair(slice, curFace));
			}
			curFace->push_back(vertex);
			return curFace;
		}

		template <typename VectorType>
		void MeshSlicer<VectorType>::proximityResolution(ScalarType& position) {
			int64_t axisSlice = static_cast<int64_t>(std::round(position / m_gridDx));
			ScalarType gridPos = axisSlice * m_gridDx;
			ScalarType diff = std::abs(gridPos - position);
			if (0 < diff && diff < m_proximityTolerance) {
				switch (m_gridProximityResolution) {
				case snapToGrid:
					position = gridPos;
					break;
				case pushAwayFromGrid:
					position = gridPos + (gridPos < position ? m_proximityTolerance : -m_proximityTolerance);
					break;
				}
			}
		}

		template class MeshSlicer<Vector3>;
		template class MeshSlicer<Vector3D>;

	}

}
