#include "Mesh/PolygonalMesh.h"

//#include "igl/copyleft/cgal/mesh_to_polyhedron.h"

namespace Chimera {

	namespace Meshes {

		#pragma region Constructors
		/* TODO: re-implement this if needed
		template <>
		PolygonalMesh<Vector3>::PolygonalMesh(const tuple<Eigen::MatrixX3d, Eigen::MatrixX3i, Eigen::MatrixX3d> &libiglMesh, const dimensions_t &gridDimensions, Scalar gridDx, bool useCutCells)
			: m_pCgalPolyhedron(nullptr), m_pMeshSlicer(nullptr), m_regularGridPatches(gridDimensions) {
			const Eigen::MatrixX3d &V = get<0>(libiglMesh); // vertices
			const Eigen::MatrixX3i &F = get<1>(libiglMesh); // faces (triangles)
			const Eigen::MatrixX3d &N = get<2>(libiglMesh); // vertex normals
			Eigen::RowVector3d center = V.colwise().sum() / V.rows();
			m_centroid = Vector3(center.x(), center.y(), center.z());
			m_pCgalPolyhedron = new CGALWrapper::CgalPolyhedron();
			igl::copyleft::cgal::mesh_to_polyhedron(V, F, *m_pCgalPolyhedron);
			if (useCutCells) {
				m_pMeshSlicer = new CGALWrapper::MeshSlicer<Vector3>(m_pCgalPolyhedron, gridDimensions, gridDx);
				m_pMeshSlicer->sliceMesh();
				CGALWrapper::triangulatePolyhedron(m_pCgalPolyhedron);
			}
			CGALWrapper::Conversion::polyhedron3ToFacesAndVertices(m_pCgalPolyhedron, m_vertices, m_elements);
			if (useCutCells) {
				classifyVertices(gridDx);
				m_pMeshSlicer->initializeLineMeshes(m_vertices);
				createHalfFaces();
			}
			computeVerticesNormals();
			if (useCutCells) {
				initializeRegularGridPatches(gridDx);
			}
		}
		*/

		template <class VectorType>
		PolygonalMesh<VectorType>::PolygonalMesh(const tuple<const vector<VectorType> &, const vector<vector<uint>> &> &mesh, const Pose<VectorType> &pose /*= Pose<VectorType>()*/)  {
			m_hasUpdated = false;
			initializeFaces(mesh);
			initializeVertexNormals();
		}

		template <class VectorType>
		PolygonalMesh<VectorType>::PolygonalMesh(const string &objFilename, const Pose<VectorType> &pose /*= Pose<VectorType>()*/) {
			m_objFilename = objFilename;
		}

		#pragma endregion

		#pragma region InitializationFunctions
		template <class VectorType>
		void PolygonalMesh<VectorType>::initializeFaces(const tuple<const vector<VectorType> &, const vector<vector<uint>>&> &mesh) {
			vector<VectorType> vertices;
			vector<vector<uint>> polygons;
			std::tie(vertices, polygons) = mesh;

			/** Creating vertices */
			for (const VectorType & point : vertices) {
				this->m_vertices.push_back(Vertex<VectorType>::create(point, geometryVertex));
			}

			/** Tagging border edges */
			map <uint64_t, bool> m_temp;

			for (const vector<uint> &polygon : polygons) {
				for (int i = 0; i < polygon.size(); i++) {
					uint nextI = roundClamp<int>(i + 1, 0, polygon.size());
					m_temp[halfEdgeHash(polygon[i], polygon[nextI], vertices.size())] = true;
				}
			}

			for (const vector<uint> &polygon : polygons) {
				for (int i = 0; i < polygon.size(); i++) {
					uint nextI = roundClamp<int>(i + 1, 0, polygon.size());
					//Looks for the inverse direction of a certain edge, if not found, it is a border edge
					if (m_temp.find(halfEdgeHash(polygon[nextI], polygon[i], vertices.size())) == m_temp.end()) {
						m_borderEdgesTags[edgeHash(polygon[i], polygon[nextI], vertices.size())] = true;
					}
					else {
						m_borderEdgesTags[edgeHash(polygon[i], polygon[nextI], vertices.size())] = false;
					}
				}
			}

			map<uint64_t, shared_ptr<Edge<VectorType>>> edgesMap;
			vector<vector<pair<uint, uint>>> polygonHalfEdges;
			/** Constructing all edges first */
			for (const vector<uint> &polygon : polygons) {
				for (int i = 0; i < polygon.size(); i++) {
					uint nextI = roundClamp<int>(i + 1, 0, polygon.size());
					uint edgeHashID = edgeHash(polygon[i], polygon[nextI], vertices.size());
					if (edgesMap.find(edgeHashID) == edgesMap.end()) {
						edgesMap[edgeHashID] = Edge<VectorType>::create(this->m_vertices[polygon[i]], this->m_vertices[polygon[nextI]], geometricEdge, m_borderEdgesTags[edgeHashID]);
						if (m_borderEdgesTags[edgeHashID]) {
							m_borderEdges.push_back(edgesMap[edgeHashID]);
						}
					}
				}
			}

			/** Then building all faces */
			for (const vector<uint> &polygon : polygons){
				vector<shared_ptr<Edge<VectorType>>> polygonEdges;
				vector<shared_ptr<HalfEdge<VectorType>>> polygonHalfEdges;
				for (int i = 0; i < polygon.size(); i++) {
					uint nextI = roundClamp<int>(i + 1, 0, polygon.size());
					polygonEdges.push_back(edgesMap[edgeHash(polygon[i], polygon[nextI], vertices.size())]);

					/** Initializing half edges */
					uint vertexID = this->m_vertices[polygon[i]]->getID();
					uint nextVertexID = this->m_vertices[polygon[nextI]]->getID();
					if(	vertexID == polygonEdges.back()->getHalfEdges().first->getVertices().first->getID() &&
							nextVertexID == polygonEdges.back()->getHalfEdges().first->getVertices().second->getID()) {
						polygonHalfEdges.push_back(polygonEdges.back()->getHalfEdges().first);
					}
					else if(vertexID == polygonEdges.back()->getHalfEdges().second->getVertices().first->getID() &&
									nextVertexID == polygonEdges.back()->getHalfEdges().second->getVertices().second->getID()) {
						polygonHalfEdges.push_back(polygonEdges.back()->getHalfEdges().second);
					}
					else {
						throw std::logic_error("PolygonalMesh: invalid halfedges on faces construction");
					}
				}
				shared_ptr<Cell<VectorType>> face = this->constructElement(polygonHalfEdges, geometricCell);
				for (int i = 0; i < polygon.size(); i++) {
					OwnConnectedStructure<Cell<VectorType>>* pConnectedCells = dynamic_cast<OwnConnectedStructure<Cell<VectorType>>*>(this->m_vertices[polygon[i]].get());
					pConnectedCells->addConnectedStructure(face->getID(), face);
				}
			}

		}

		template <class VectorType>
		void PolygonalMesh<VectorType>::initializeVertexNormals() {
			for (int i = 0; i < this->getVertices().size(); i++) {
				VectorType accumulatedNormal;
				OwnConnectedStructure<Cell<VectorType>>* pConnectedCells = dynamic_cast<OwnConnectedStructure<Cell<VectorType>>*>(this->m_vertices[i].get());
				for (int j = 0; j < pConnectedCells->getConnectedStructures().size(); j++) {
					accumulatedNormal += pConnectedCells->getConnectedStructures()[j]->getHalfCells().first->getNormal();
				}
				accumulatedNormal /= pConnectedCells->getConnectedStructures().size();
				accumulatedNormal.normalize();
				this->getVertices()[i]->setNormal(-accumulatedNormal);
			}
		}
		#pragma endregion

		#pragma region Functionalities
		template <class VectorType>
		uint PolygonalMesh<VectorType>::getOriginalEdge(shared_ptr<Vertex<VectorType>> vertex) {
			DoubleScalar epsilon = std::numeric_limits<Scalar>::epsilon() * 16;
			uint minIndex = 0;
			//Simon TODO: add this back if necessary
			/*for (uint i = 0; i < m_geometryEdges.size(); ++i) {
				if (isOnEdge(m_geometryEdges[i]->getVertex1()->getPosition(), m_geometryEdges[i]->getVertex2()->getPosition(), vertex->getPosition(), epsilon)) {
					return i;
				}
			}*/
			return std::numeric_limits<uint>::max();
		}

		template <class VectorType>
		void PolygonalMesh<VectorType>::updatePoints(vector<VectorType>* points) {

		}
		#pragma endregion
		template class PolygonalMesh<Vector3>;
		template class PolygonalMesh<Vector3D>;
	}
}
