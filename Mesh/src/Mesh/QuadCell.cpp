#include "Mesh/QuadCell.h"
#include "Grids/GridUtils.h"

namespace Chimera {

	namespace Meshes {

		template<class VectorType>
		bool QuadCell<VectorType>::m_handleInvalidConfigurationException = false;

		#pragma region Functionalities
		template<class VectorType>
		shared_ptr<Cell<VectorType>> QuadCell<VectorType>::getCell(const VectorType& position) {
			for (auto pCell : m_cells) {
				if (pCell->isInside(position)) {
					return pCell;
				}
			}
			throw(std::logic_error("QuadCell::getCell(): cell not found at given position"));
			//return nullptr;
		}
		#pragma endregion

		#pragma region PrivateFunctionalities
		template<class VectorType>
		void QuadCell<VectorType>::split() {

			if (!m_cells.empty()) {
				throw std::logic_error("Splitting non empty cell!");
			}

			/** Initially keep track of a vector of oriented vertices through halfedges structures */
			vector<shared_ptr<HalfEdge<VectorType>>> halfEdges;
			shared_ptr<HalfEdge<VectorType>> pHalfEdge = nullptr;

			/** Ensure that vertices that will be processed for splitting are not visited*/
			for (int i = 0; i < m_edges.size(); i++) {
				m_edges[i]->setVisited(false);
				m_edges[i]->getHalfEdges().first->getVertices().second->setUpdated(false);
				m_edges[i]->getHalfEdges().second->getVertices().second->setUpdated(false);
			}

			vector <vector<shared_ptr<HalfEdge<VectorType>>>> floatingRegions;
			vector<shared_ptr<HalfEdge<VectorType>>> geometryInterior;
			unordered_set<shared_ptr<Edge<VectorType>>> geometryExterior;

			while ((pHalfEdge = hasUnvisitedEdges()) != nullptr || (pHalfEdge = hasUnvisitedGeometryEdges()) != nullptr) {
				//First, find interior geometry cycles, and mark them all as visited, so we evaluate the "outside" first.
				findGeometryCycles(geometryInterior);

				for (shared_ptr<HalfEdge<VectorType>> pInteriorEdge : geometryInterior) {
					pInteriorEdge->setVisited(true);
				}

				floatingRegions.clear();

				//Then, find all cut cells in the now tree like geometry structures.
				while ((pHalfEdge = hasUnvisitedEdges()) != nullptr || (pHalfEdge = hasUnvisitedGeometryEdges()) != nullptr) {
					halfEdges.clear();

					/** Setting all geometry edges to be non-visited for proper looping */
					/** After closing a loop with breadthFirstSearch, halfEdgesVector will have a closed half-cell */
					try {
						uint gridEdgeSegments = depthFirstSearch(pHalfEdge, halfEdges, geometryExterior);
						if (gridEdgeSegments == numeric_limits<uint>::max()) {
							throw std::logic_error("Invalid cell found");
						}

						//If a counterclockwise geometric structure is found, that structure must be outwards facing, and added separately as a disconnected structure.
						if (gridEdgeSegments == 0 && (!isCounterClockwise(halfEdges.begin(), halfEdges.end()))) {
							floatingRegions.push_back(halfEdges);

							//Make sure to mark all vertices as unvisited after finding each cell.
							for (shared_ptr<HalfEdge<VectorType>> halfEdge : halfEdges) {
								halfEdge->getVertices().second->setUpdated(false);
							}
							continue;
						}
					}
					catch (exception e) {
						if (m_handleInvalidConfigurationException) { //This variables sets if we should handle invalid cell configurations
							handleException(e, pHalfEdge);

							//Returns after this
							return;
						}
						else { //Or we just forward the throw for other classses to deal with
							throw (e);
						}
					}

					//Make sure to mark all vertices as unvisited after finding each cell.
					for (shared_ptr<HalfEdge<VectorType>> halfEdge : halfEdges) {
						halfEdge->getVertices().second->setUpdated(false);
					}

					m_cells.push_back(Cell<VectorType>::create(halfEdges, m_cellLocation));
					//getHalfCells().back()->setDangling(m_isDanglingCell);

				}

				/** Lastly, the floating regions are added as disconnected half-edges to the cells that they are inside.
					Interpolation algorithms must be aware of that, so the half-cell is tagged as having a disconnected
					region*/
				for (auto floatingRegion : floatingRegions) {
					for (auto pCell : m_cells) {
						//Check if the first vertex is inside the cell, assuming that theres no intersections and overlaps
						//between disconnected regions inside a half-cell
						if (pCell->isInside(floatingRegion.front()->getVertices().first->getPosition())) {
							pCell->addDisconnectedRegion(floatingRegion);
							break;
						}
					}
				}

				// Then, mark all the geometry cycles as unvisited.
				for (shared_ptr<HalfEdge<VectorType>> pInteriorEdge : geometryInterior) {
					pInteriorEdge->setVisited(false);
				}

				// As a final step, mark which edges now belong to the exterior of the new pseudo-regular grid cell we wish to treat.
				geometryExterior.clear();

				for (shared_ptr<HalfEdge<VectorType>> pInteriorEdge : geometryInterior) {
					if (pInteriorEdge->getEdge()->isVisited()) {
						geometryExterior.insert(pInteriorEdge->getEdge());
					}
				}

				geometryInterior.clear();

				// We can now do the same for the interior cycles, which are guaranteed to have exactly one cell around the outside.
			}

			//Leave all edges Unvisited
			for (uint i = 0; i < m_edges.size(); i++) {
				m_edges[i]->setVisited(false);
			}
		}

		template <class VectorType>
		template <class Iterator>
		bool QuadCell<VectorType>::isCounterClockwise(Iterator begin,
																							Iterator end) {
			vector<VectorType> points;
			points.reserve(distance(begin,end));
			for (auto edgeIt = begin; edgeIt != end; ++edgeIt) {
				points.push_back((*edgeIt)->getVertices().first->getPosition());
			}

			VectorType normal(0);
			if (!isVector2<VectorType>::value) {
				//use unified constructor to allow code generation for both 2d and 3d vectors.
				switch (getLocation()) {
				case XZPlane:
					normal.y = 1;
					break;
				case YZPlane:
					normal.x = 1;
					break;
				case XYPlane:
					normal = VectorType(1);
					normal.x = 0;
					normal.y = 0;
					break;
				default:;
				}
			}

			DoubleScalar area = calculateNumStablePolygonArea(points, normal);

			//TODO: merge zero areas. Those specific degenerate cases can be fixed by merging edges/vertices to become a curve rather than a polygon.
			if (abs(area) <= Core::singlePrecisionThreshold * points.front().length()) {
				throw std::logic_error("Cell isCounterClockwise: Degenerate geometric area, cannot resolve");
			}

			return area > 0;
		}

		template <class VectorType>
		void QuadCell<VectorType>::cycleDFS(shared_ptr<HalfEdge<VectorType>> pCurrEdge,
									vector<shared_ptr<HalfEdge<VectorType>>> &traversedEdges,
									unordered_set<shared_ptr<HalfEdge<VectorType>>> &halfEdgeSet,
									unordered_set<shared_ptr<Edge<VectorType>>> &visitedEdges) {
			visitedEdges.insert(pCurrEdge->getEdge());
			//If we find a cycle:
			if (pCurrEdge->getVertices().second->hasUpdated()) {
				traversedEdges.push_back(pCurrEdge);
				for (auto pEdgeIt = traversedEdges.begin(); pEdgeIt != traversedEdges.end(); ++pEdgeIt) {

					//check where the cycle connects to
					if ((*pEdgeIt)->getVertices().first == pCurrEdge->getVertices().second) {

						//Mark halfedges based on orientation
						//We do not mark them as visited, as we want to find ALL cycles, which may reuse edges.
						if (isCounterClockwise(pEdgeIt, traversedEdges.end())) {
							for (; pEdgeIt != traversedEdges.end(); ++pEdgeIt) {
								halfEdgeSet.insert(*pEdgeIt);
							}
						}
						else {
							for (; pEdgeIt != traversedEdges.end(); ++pEdgeIt) {
								pair<shared_ptr<HalfEdge<VectorType>>, shared_ptr<HalfEdge<VectorType>>> halfEdges = (*pEdgeIt)->getEdge()->getHalfEdges();
								shared_ptr<HalfEdge<VectorType>> oppEdge = halfEdges.first != (*pEdgeIt) ? halfEdges.first : halfEdges.second;
								halfEdgeSet.insert(oppEdge);
							}
						}

						return;
					}
				}
				throw std::logic_error("Cell cycleDFS: Could not find cycle at visited vertex");
			}
			else {
				pCurrEdge->getVertices().second->setUpdated(true);

				traversedEdges.push_back(pCurrEdge);

				uint curDepth = traversedEdges.size();

				for (shared_ptr<Edge<VectorType>> pNextEdge : m_vertexToEdgeMap.getEdges(pCurrEdge->getVertices().second)) {
					shared_ptr<HalfEdge<VectorType>> pNextHalfEdge = pNextEdge->getHalfEdges().first->getVertices().first == pCurrEdge->getVertices().second
																														? pNextEdge->getHalfEdges().first
																														: pNextEdge->getHalfEdges().second;

					//recursively search each possible next edge as a depth first search.
					if (pNextEdge != pCurrEdge->getEdge() && !pNextHalfEdge->isVisited()
							&& pNextEdge->getType() == geometricEdge) {

						cycleDFS(pNextHalfEdge, traversedEdges, halfEdgeSet, visitedEdges);

						traversedEdges.resize(curDepth);
					}

				}
				pCurrEdge->getVertices().second->setUpdated(false);
			}
		}

		template <class VectorType>
		void QuadCell<VectorType>::findGeometryCycles(vector<shared_ptr<HalfEdge<VectorType>>>& geometryInterior) {
			unordered_set<shared_ptr<HalfEdge<VectorType>>> halfEdgeSet;
			unordered_set<shared_ptr<Edge<VectorType>>> visitedEdges;

			//Mark all considered vertices as not visited.
			for (shared_ptr<Edge<VectorType>> pEdge : getEdges()) {
				pEdge->getHalfEdges().first->getVertices().first->setUpdated(false);
				pEdge->getHalfEdges().second->getVertices().first->setUpdated(false);
			}

			for (shared_ptr<Edge<VectorType>> pEdge : getEdges()) {

				if (pEdge->getType() == geometricEdge && visitedEdges.count(pEdge) == 0 && !pEdge->isVisited()) {

					shared_ptr<Vertex<VectorType>> startingVertex = pEdge->getHalfEdges().first->getVertices().first;
					startingVertex->setUpdated(true);
					vector<shared_ptr<HalfEdge<VectorType>>> traversedEdges{};
					cycleDFS(pEdge->getHalfEdges().first, traversedEdges, halfEdgeSet, visitedEdges);
					startingVertex->setUpdated(false);

					startingVertex = pEdge->getHalfEdges().second->getVertices().first;
					startingVertex->setUpdated(true);
					traversedEdges.clear();
					cycleDFS(pEdge->getHalfEdges().second, traversedEdges, halfEdgeSet, visitedEdges);
					startingVertex->setUpdated(false);

				}
			}


			for (shared_ptr<HalfEdge<VectorType>> pInteriorEdge : halfEdgeSet) {
				geometryInterior.push_back(pInteriorEdge);
			}
		}

		template<class VectorType>
		void QuadCell<VectorType>::handleException(const exception &e, shared_ptr<HalfEdge<VectorType>> pEdge) {
			Logger::getInstance()->get() << "Cell: invalid edge configuration at " << m_gridCellLocation.x << "," << m_gridCellLocation.y << "," << m_gridCellLocation.z << std::endl;
			Logger::getInstance()->get() << "Creating a regular cell as approximate solution" << std::endl;

			/** Clearing previous half-cells computed, only one full grid half-cell is left */
			m_cells.clear();

			if (pEdge->getEdge()->getType() == geometricEdge) {
				for (int i = 0; i < m_edges.size(); i++) {
					if (m_edges[i]->getType() != geometricEdge) {
						pEdge = m_edges[i]->getHalfEdges().first;
						break;
					}
				}
			}
			if (pEdge->getEdge()->getType() == geometricEdge) {
				throw std::logic_error("Cell: unable to find no geometric edge for handling incorrect cell configuration.");
			}

			//Create regular cell already pushes back into m_halfCells
			createRegularHalfCell(pEdge->getEdge());

		}

		template<class VectorType>
		void QuadCell<VectorType>::createRegularHalfCell(shared_ptr<Edge<VectorType>> pEdge) {
			vector<shared_ptr<HalfEdge<VectorType>>> halfEdges;
			halfEdgeLocation_t location = Edge<VectorType>::classifyEdge(pEdge, m_gridCellLocation, m_gridDx, m_cellLocation);
			shared_ptr<HalfEdge<VectorType>> pNextHalfEdge = getOrientedHalfEdge(pEdge, location);
			halfEdges.push_back(pNextHalfEdge);
			shared_ptr<Vertex<VectorType>> nextVertex = halfEdges.back()->getVertices().second;

			halfCellLocation_t halfCellLocation;
			if (m_cellLocation == XYPlane)
				halfCellLocation = backHalfCell;
			else if (m_cellLocation == XZPlane)
				halfCellLocation = bottomHalfCell;
			else if (m_cellLocation == YZPlane)
				halfCellLocation = leftHalfCell;

			m_cells.push_back(Cell<VectorType>::create(halfEdges, m_cellLocation));
		}

		template <class VectorType>
		shared_ptr<HalfEdge<VectorType>> QuadCell<VectorType>::hasUnvisitedEdges()
		{
			for (uint i = 0; i < m_edges.size(); i++)
			{
				if (m_edges[i]->getType() != geometricEdge)
				{
					if (!m_edges[i]->isVisited())
					{
						halfEdgeLocation_t currLocation = Edge<VectorType>::classifyEdge(m_edges[i], m_gridCellLocation, m_gridDx, m_cellLocation);
						return getOrientedHalfEdge(m_edges[i], currLocation);
					}
				}
			}
			return nullptr;
		}

		template<class VectorType>
		uint QuadCell<VectorType>::depthFirstSearch(shared_ptr<HalfEdge<VectorType>> pEdge,
																						vector<shared_ptr<HalfEdge<VectorType>>> &halfEdges,
																						const unordered_set<shared_ptr<Edge<VectorType>>> &exterior) {

			const bool extEdge = (pEdge->getEdge()->getType() != geometricEdge || exterior.count(pEdge->getEdge()) > 0);

			uint extEdgeMod = extEdge ? 1 : 0;

			if (pEdge->isVisited()) {
				if (*pEdge == *halfEdges.front()) {
					return extEdge ? 1 : 0;
				}
				else {
					return numeric_limits<uint>::max();
				}
			}

			if (pEdge->getVertices().second->hasUpdated()) {
				return numeric_limits<uint>::max();
			}

			halfEdgeLocation_t currLocation = Edge<VectorType>::classifyEdge(pEdge->getEdge(), m_gridCellLocation, m_gridDx, m_cellLocation);
			//Tag this edge as visited
			pEdge->setVisited(true);

			pEdge->getVertices().second->setUpdated(true);

			halfEdges.push_back(pEdge);

			pEdge->setLocation(currLocation);

			//Choosing next Edge
			const vector<shared_ptr<Edge<VectorType>>> &nextEdges = m_vertexToEdgeMap.getEdges(pEdge->getVertices().second);
			if (nextEdges.size() >= 3 && pEdge->getEdge()->getType() == geometricEdge && pEdge->getVertices().second->getType() == cellVertex) {

				//Mesh is non-manifold

				uint bestCand = numeric_limits<uint>::max();
				uint curDepth = halfEdges.size();

				vector<shared_ptr<HalfEdge<VectorType>>> halfEdgesCopy(halfEdges);

				/** list of dangling curves that may or may not get added to the final set.*/
				vector<vector<shared_ptr<HalfEdge<VectorType>>>> danglingCurves;

				for (auto nextEdge : nextEdges) {
					/** Selecting the proper next edge, and performing a depth first search with the HalfEdgesCopy */
					shared_ptr<HalfEdge<VectorType>> nextHalfEdge = *nextEdge->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
						? nextEdge->getHalfEdges().first
						: nextEdge->getHalfEdges().second;
					if (nextHalfEdge->isVisited()) {
						continue;
					}

					uint curCand = depthFirstSearch(nextHalfEdge, halfEdgesCopy);

					if (curCand <= bestCand) {
						//if this branch leads to a dangling curve,
						if (halfEdgesCopy.front() != halfEdges.front()) {
							//Add it to the list, and reassign halfEdgesCopy.
							danglingCurves.emplace_back();
							halfEdgesCopy.swap(danglingCurves.back());
							halfEdgesCopy = halfEdges;
							continue;
						}

						//Otherwise, consider it the best considered branch.
						bestCand = curCand;
						halfEdges = halfEdgesCopy;
					}

					for (uint i = curDepth; i < halfEdgesCopy.size(); ++i) {
						halfEdgesCopy[i]->setVisited(false);
						halfEdgesCopy[i]->getVertices().second->setUpdated(false);
					}
					halfEdgesCopy.resize(curDepth);
				}

				if (!danglingCurves.empty()) {

					uint plane;
					switch (m_cellLocation) {
					case YZPlane:
						plane = 0;
						break;
					case XZPlane:
						plane = 1;
						break;
					case XYPlane:
						plane = 2;
						break;
					default:
						throw std::logic_error("Cell: calling split on geometry cell!");
					}

					Vector2D p1 = get2DVector(halfEdges[curDepth - 1]->getVertices().first->getPosition(), plane);
					Vector2D p2 = get2DVector(halfEdges[curDepth - 1]->getVertices().second->getPosition(), plane);
					Vector2D p3 = get2DVector(halfEdges[curDepth]->getVertices().second->getPosition(), plane);

					bool reflexAngle = (p3 - p2).cross(p1 - p2) < 0;

					//use simple pointer as we will not reuse these pointers outside of this if branch.
					vector<vector<shared_ptr<HalfEdge<VectorType>>>*> addedCurves;

					//first, check which dangling curves are in the correct position.
					for (vector<shared_ptr<HalfEdge<VectorType>>> curve : danglingCurves) {
						Vector2D curvePos = get2DVector(curve.front()->getVertices().second->getPosition(), plane);
						bool isLeft = reflexAngle
							? (p2 - p1).cross(curvePos - p2) > 0 || (p3 - p2).cross(curvePos - p2) > 0
							: (p2 - p1).cross(curvePos - p2) > 0 && (p3 - p2).cross(curvePos - p2) > 0;
						if (isLeft) {
							addedCurves.push_back(&curve);
						}
					}

					if (addedCurves.size() > 1) {
						sort(addedCurves.begin(), addedCurves.end(),
							[&, plane](vector<shared_ptr<HalfEdge<VectorType>>>* c1, vector<shared_ptr<HalfEdge<VectorType>>>* c2) {
								Vector2D c1Pos = get2DVector(c1->front()->getVertices().second->getPosition(), plane);
								Vector2D c2Pos = get2DVector(c2->front()->getVertices().second->getPosition(), plane);
								bool reflex = (c2Pos - p1).cross(p1 - p2) < 0;
								return reflex
									? (p2 - p1).cross(c1Pos - p2) > 0 || (c2Pos - p2).cross(c1Pos - p2) > 0
									: (p2 - p1).cross(c1Pos - p2) > 0 && (c2Pos - p2).cross(c1Pos - p2) > 0;
							}
						);
					}

					auto iterator = halfEdges.begin() + curDepth;

					for (vector<shared_ptr<HalfEdge<VectorType>>>* curve : addedCurves) {
						iterator = halfEdges.insert(iterator, curve->begin(), curve->end());
						iterator += curve->size();
					}
				}

				for (uint i = curDepth; i < halfEdges.size(); ++i) {
					halfEdges[i]->setVisited(true);
				}

				return max(extEdgeMod + bestCand, bestCand);

			}
			else if (nextEdges.size() == 3) { //Going from gridEdges to geometryEdges or vice-versa
				if (currLocation == geometryHalfEdge) {
					bool followThroughOneEdge = false;
					if (nextEdges[0]->getType() != geometricEdge) {
						halfEdgeLocation_t nextLocation = Edge<VectorType>::classifyEdge(nextEdges[0], m_gridCellLocation, m_gridDx, m_cellLocation);
						shared_ptr<HalfEdge<VectorType>> pHalfEdge = getOrientedHalfEdge(nextEdges[0], nextLocation);
						if (pHalfEdge->getVertices().first->getID() == halfEdges.back()->getVertices().second->getID()) { //This is the right halfedge
							followThroughOneEdge = true;
							const uint ret = *nextEdges[0]->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
								? depthFirstSearch(nextEdges[0]->getHalfEdges().first, halfEdges)
								: depthFirstSearch(nextEdges[0]->getHalfEdges().second, halfEdges);
							return max(extEdgeMod + ret, ret);
						}
					}
					if (!followThroughOneEdge && nextEdges[1]->getType() != geometricEdge) {
						halfEdgeLocation_t nextLocation = Edge<VectorType>::classifyEdge(nextEdges[1], m_gridCellLocation, m_gridDx, m_cellLocation);
						shared_ptr<HalfEdge<VectorType>> pHalfEdge = getOrientedHalfEdge(nextEdges[1], nextLocation);
						if (pHalfEdge->getVertices().first->getID() == halfEdges.back()->getVertices().second->getID()) { //This is the right halfedge
							followThroughOneEdge = true;
							const uint ret = *nextEdges[1]->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
								? depthFirstSearch(nextEdges[1]->getHalfEdges().first, halfEdges)
								: depthFirstSearch(nextEdges[1]->getHalfEdges().second, halfEdges);
							return max(extEdgeMod + ret, ret);
						}
					}
					if (!followThroughOneEdge && nextEdges[2]->getType() != geometricEdge) {
						halfEdgeLocation_t nextLocation = Edge<VectorType>::classifyEdge(nextEdges[2], m_gridCellLocation, m_gridDx, m_cellLocation);
						shared_ptr<HalfEdge<VectorType>> pHalfEdge = getOrientedHalfEdge(nextEdges[2], nextLocation);
						if (pHalfEdge->getVertices().first->getID() == halfEdges.back()->getVertices().second->getID()) { //This is the right halfedge
							followThroughOneEdge = true;
							const uint ret = *nextEdges[2]->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
								? depthFirstSearch(nextEdges[2]->getHalfEdges().first, halfEdges)
								: depthFirstSearch(nextEdges[2]->getHalfEdges().second, halfEdges);
							return max(extEdgeMod + ret, ret);
						}
					}
					throw(std::logic_error("Cell DFS: Error on vertex path choosing algorithm: unexpected geometry to grid edge case."));
				}
				else {
					if (nextEdges[0]->getType() == geometricEdge) {
						const uint ret = *nextEdges[0]->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
							? depthFirstSearch(nextEdges[0]->getHalfEdges().first, halfEdges)
							: depthFirstSearch(nextEdges[0]->getHalfEdges().second, halfEdges);
						return max(extEdgeMod + ret, ret);
					}
					else if (nextEdges[1]->getType() == geometricEdge) {
						const uint ret = *nextEdges[1]->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
							? depthFirstSearch(nextEdges[1]->getHalfEdges().first, halfEdges)
							: depthFirstSearch(nextEdges[1]->getHalfEdges().second, halfEdges);
						return max(extEdgeMod + ret, ret);
					}
					else if (nextEdges[2]->getType() == geometricEdge) {
						const uint ret = *nextEdges[2]->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
							? depthFirstSearch(nextEdges[2]->getHalfEdges().first, halfEdges)
							: depthFirstSearch(nextEdges[2]->getHalfEdges().second, halfEdges);
						return max(extEdgeMod + ret, ret);
					}
					else {
						throw(std::logic_error("Error on vertex path choosing algorithm: not connected to geometric edge."));
					}
				}
			}
			else if (nextEdges.size() == 2) {
				shared_ptr<Edge<VectorType>> nextEdge = *nextEdges[0] == *pEdge->getEdge() ? nextEdges[1] : nextEdges[0];
				const uint ret = *nextEdge->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
					? depthFirstSearch(nextEdge->getHalfEdges().first, halfEdges)
					: depthFirstSearch(nextEdge->getHalfEdges().second, halfEdges);
				return max(extEdgeMod + ret, ret);
			}
			else if (nextEdges.size() == 1) { //This is a open ended point, go back through same geometric edges
				if (halfEdges.back()->getEdge()->getType() != geometricEdge) {
					throw(std::logic_error("Open ended vertex found on top of a grid-edge. Not supported"));
				}

				uint curDepth = halfEdges.size();

				//Roll back all visited geometry edges until a non geometry edge
				for (int i = halfEdges.size() - 1; i >= 0; i--) {
					shared_ptr<HalfEdge<VectorType>> otherEdge = *halfEdges[i]->getEdge()->getHalfEdges().first == *halfEdges[i]
						? halfEdges[i]->getEdge()->getHalfEdges().second
						: halfEdges[i]->getEdge()->getHalfEdges().first;
					halfEdges.push_back(otherEdge);

					otherEdge->setVisited(true);

					if (m_vertexToEdgeMap.getEdges(halfEdges[i]->getVertices().first).size() > 2) {

						if (otherEdge->getVertices().second->getType() == edgeVertex) {
							halfEdges.pop_back();
							otherEdge->getVertices().second->setUpdated(false);
							otherEdge->setVisited(false);
							return depthFirstSearch(otherEdge, halfEdges);
						}

						halfEdges.erase(halfEdges.begin(), halfEdges.begin() + curDepth);
						return 0;
					}
				}

				//If the loop does not exit, we have started on a dangling edge.
				//Thus, first non-recursively add all necessary edges up to the next branch
				shared_ptr<HalfEdge<VectorType>> curEdge = halfEdges.back();
				vector<shared_ptr<Edge<VectorType>>> edges = m_vertexToEdgeMap.getEdges(curEdge->getVertices().second);

				while (edges.size() == 2) {
					shared_ptr<Edge<VectorType>> nextEdge = *edges[0] == *curEdge->getEdge() ? edges[1] : edges[0];
					curEdge = *nextEdge->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
						? nextEdge->getHalfEdges().first
						: nextEdge->getHalfEdges().second;
					if (curEdge->isVisited()) {
						throw std::logic_error("Cell Split: Visited edge halfway through tracing a non branching path");
					}
					halfEdges.push_back(curEdge);
					curEdge->setVisited(true);
					curEdge->getVertices().second->setUpdated(true);
					edges = m_vertexToEdgeMap.getEdges(curEdge->getVertices().second);
				}

				uint plane;
				switch (m_cellLocation) {
				case YZPlane:
					plane = 0;
					break;
				case XZPlane:
					plane = 1;
					break;
				case XYPlane:
					plane = 2;
					break;
				default:
					throw std::logic_error("Cell: calling split on geometry cell!");
				}

				Vector2D p1 = get2DVector(curEdge->getVertices().first->getPosition(), plane);
				Vector2D p2 = get2DVector(curEdge->getVertices().second->getPosition(), plane);
				Vector2D p3 = p1;

				shared_ptr<HalfEdge<VectorType>> nextHalfEdge;

				//Then, find the correct edge based on angle
				for (auto nextEdge : edges) {

					if (nextEdge != curEdge->getEdge()) {
						shared_ptr<HalfEdge<VectorType>> candHalfEdge = *nextEdge->getHalfEdges().first->getVertices().first == *curEdge->getVertices().second
							? nextEdge->getHalfEdges().first
							: nextEdge->getHalfEdges().second;

						bool reflexAngle = (p3 - p2).cross(p1 - p2) < 0;

						Vector2D candPos = get2DVector(candHalfEdge->getVertices().second->getPosition(), plane);

						bool isLeft = reflexAngle
							? (p2 - p1).cross(candPos - p2) > 0 || (p3 - p2).cross(candPos - p2) > 0
							: (p2 - p1).cross(candPos - p2) > 0 && (p3 - p2).cross(candPos - p2) > 0;

						if (isLeft) {
							p3 = candPos;
							nextHalfEdge = candHalfEdge;
						}
					}
				}
				vector<shared_ptr<HalfEdge<VectorType>>> halfEdgeNonDangling;

				uint ret = depthFirstSearch(nextHalfEdge, halfEdgeNonDangling);

				curEdge = *halfEdges.back()->getEdge()->getHalfEdges().first == *halfEdges.back()
					? halfEdges.back()->getEdge()->getHalfEdges().second
					: halfEdges.back()->getEdge()->getHalfEdges().first;

				halfEdges.insert(halfEdges.end(), halfEdgeNonDangling.begin(), halfEdgeNonDangling.end());

				edges = m_vertexToEdgeMap.getEdges(curEdge->getVertices().second);

				while (edges.size() == 2) {
					shared_ptr<Edge<VectorType>> nextEdge = *edges[0] == *curEdge->getEdge() ? edges[1] : edges[0];
					curEdge = *nextEdge->getHalfEdges().first->getVertices().first == *pEdge->getVertices().second
						? nextEdge->getHalfEdges().first
						: nextEdge->getHalfEdges().second;
					if (curEdge->isVisited()) {
						return ret;
					}
					halfEdges.push_back(curEdge);
					curEdge->setVisited(true);
					curEdge->getVertices().second->setUpdated(true);
					edges = m_vertexToEdgeMap.getEdges(curEdge->getVertices().second);
				}

				throw std::logic_error("Cell split: invalid dangling edge connectivity");
			}

			throw std::logic_error("Cell Split: Invalid case");
		}

		template<class VectorType>
		shared_ptr<HalfEdge<VectorType>> QuadCell<VectorType>::getOrientedHalfEdge(shared_ptr<Edge<VectorType>> pEdge, halfEdgeLocation_t halfEdgeLocation) {
			/** Previous function just returned first or second, we actually have to check x and y*/

			/*if (halfEdgeLocation == topHalfEdge || halfEdgeLocation == leftHalfEdge)
				return pEdge->getHalfEdges().second;
			else if (halfEdgeLocation == bottomHalfEdge || halfEdgeLocation == rightHalfEdge)
				return pEdge->getHalfEdges().first;
			else {
				throw exception("Invalid half-edge location for getOrientedHalfEdge function. ");
			}*/

			/** Wrangling for making it work for arbitrary planes */
			uint pos1, pos2;
			if (m_cellLocation == XYPlane) {
				pos1 = 0; pos2 = 1;
			} else if (m_cellLocation == XZPlane) {
				pos1 = 0; pos2 = 2;
			}
			else if (m_cellLocation == YZPlane) {
				pos1 = 2; pos2 = 1;
			}

			//Assume counter clock-wise orientation and check positions of vertices individually
			if (halfEdgeLocation == topHalfEdge) {
				if (pEdge->getHalfEdges().second->getVertices().second->getPosition()[pos1] < pEdge->getHalfEdges().second->getVertices().first->getPosition()[pos1]) {
					return pEdge->getHalfEdges().second;
				}
				else {
					return pEdge->getHalfEdges().first;
				}
			}

			if (halfEdgeLocation == bottomHalfEdge) {
				if (pEdge->getHalfEdges().first->getVertices().second->getPosition()[pos1] > pEdge->getHalfEdges().first->getVertices().first->getPosition()[pos1]) {
					return pEdge->getHalfEdges().first;
				}
				else {
					return pEdge->getHalfEdges().second;
				}
			}

			if (halfEdgeLocation == leftHalfEdge) {
				if (pEdge->getHalfEdges().second->getVertices().second->getPosition()[pos2] < pEdge->getHalfEdges().second->getVertices().first->getPosition()[pos2]) {
					return pEdge->getHalfEdges().second;
				}
				else {
					return pEdge->getHalfEdges().first;
				}
			}

			if (halfEdgeLocation == rightHalfEdge) {
				if (pEdge->getHalfEdges().first->getVertices().second->getPosition()[pos2] > pEdge->getHalfEdges().first->getVertices().first->getPosition()[pos2]) {
					return pEdge->getHalfEdges().first;
				}
				else {
					return pEdge->getHalfEdges().second;
				}
			}

			return nullptr;
		}

		#pragma endregion


		template<class VectorType>
		const vector<shared_ptr<Edge<VectorType>>> & getElements(shared_ptr<QuadCell<VectorType>> pQuadCell) {
			return pQuadCell->getEdges();
		}

		template<class VectorType>
		const vector<shared_ptr<Vertex<VectorType>>>& getElements(shared_ptr<QuadCell<VectorType>> pQuadCell) {
			return pQuadCell->getVertices();
		}


		template const vector<shared_ptr<Vertex<Vector2>>>& getElements(shared_ptr<QuadCell<Vector2>> pQuadCell);
		template const vector<shared_ptr<Edge<Vector2>>>& getElements(shared_ptr<QuadCell<Vector2>> pQuadCell);
		template const vector<shared_ptr<Vertex<Vector3>>>& getElements(shared_ptr<QuadCell<Vector3>> pQuadCell);
		template const vector<shared_ptr<Edge<Vector3>>>& getElements(shared_ptr<QuadCell<Vector3>> pQuadCell);

		template class QuadCell<Vector2>;
		template class QuadCell<Vector2D>;
		template class QuadCell<Vector3>;
		template class QuadCell<Vector3D>;
	}
}
