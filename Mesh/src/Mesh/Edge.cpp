#include "Mesh/Edge.h"

namespace Chimera {
	namespace Meshes {

		#pragma region Functionalities
		template <class VectorType>
		halfEdgeLocation_t Edge<VectorType>::classifyEdge(shared_ptr<Edge<VectorType>> pEdge,
																	const dimensions_t &gridDimensions,
																	DoubleScalar gridDx, cellLocation_t cellLocation) {
			if (pEdge->getType() == geometricEdge) {
				return geometryHalfEdge;
			}

			switch (cellLocation) {
				case XYPlane:
					return classifyEdgeXY(pEdge, gridDimensions, gridDx);
				break;
				case XZPlane:
					return classifyEdgeXZ(pEdge, gridDimensions, gridDx);
				break;
				case YZPlane:
					return classifyEdgeYZ(pEdge, gridDimensions, gridDx);
				break;
				default:
					throw(std::logic_error("unexpected Edge case"));
			}

		}

		template <class VectorType>
		halfEdgeLocation_t Edge<VectorType>::classifyEdgeXY(		shared_ptr<Edge<VectorType>> pEdge,
																	const dimensions_t &gridDimensions,
																	DoubleScalar gridDx) {
			if (pEdge->getType() == xAlignedEdge) {
				if (floor(pEdge->getVertex1()->getPosition()[1] / gridDx) == gridDimensions.y) {
					return bottomHalfEdge;
				}
				else {
					return topHalfEdge;
				}
			}
			else {
				if (floor(pEdge->getVertex1()->getPosition()[0] / gridDx) == gridDimensions.x) {
					return leftHalfEdge;
				}
				else {
					return rightHalfEdge;
				}
			}
		}

		template <class VectorType>
		halfEdgeLocation_t Edge<VectorType>::classifyEdgeXZ(	shared_ptr<Edge<VectorType>> pEdge,
																	const dimensions_t &gridDimensions,
																	DoubleScalar gridDx) {
			if (pEdge->getType() == zAlignedEdge) {
				if (floor(pEdge->getVertex1()->getPosition()[0] / gridDx) == gridDimensions.x) {
					return leftHalfEdge;
				}
				else {
					return rightHalfEdge;
				}
			}
			else {
				if (floor(pEdge->getVertex1()->getPosition()[2] / gridDx) == gridDimensions.y) {
					return bottomHalfEdge;
				}
				else {
					return topHalfEdge;
				}
			}
		}

		template <class VectorType>
		halfEdgeLocation_t Edge<VectorType>::classifyEdgeYZ(		shared_ptr<Edge<VectorType>> pEdge,
																	const dimensions_t &gridDimensions,
																	DoubleScalar gridDx) {
			if (pEdge->getType() == zAlignedEdge) {
				if (floor(pEdge->getVertex1()->getPosition()[1] / gridDx) == gridDimensions.y) {
					return bottomHalfEdge;
				}
				else {
					return topHalfEdge;
				}
			}
			else {
				if (floor(pEdge->getVertex1()->getPosition()[2] / gridDx) == gridDimensions.x) {
					return leftHalfEdge;
				}
				else {
					return rightHalfEdge;
				}
			}
		}
		#pragma endregion


		template class Edge<Vector2>;
		template class Edge<Vector2D>;
		template class Edge<Vector3>;
		template class Edge<Vector3D>;
	}
}
