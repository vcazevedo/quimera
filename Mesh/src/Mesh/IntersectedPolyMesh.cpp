#include "Mesh/IntersectedPolyMesh.h"

#include "Mesh/MeshSlicer.h"

namespace Chimera {

	namespace Meshes {

		#pragma region Constructors
		template <class VectorType>
		IntersectedPolyMesh<VectorType>::IntersectedPolyMesh(const string &objFilename, dimensions_t gridDimensions,
			Scalar gridDx, bool perturbPoints, const Pose<VectorType> &pose) : IntersectedPolyMesh::PolygonalMesh(),
			m_regularGridPatches(gridDimensions) {
			this->m_pose = pose;
			m_gridDimensions = gridDimensions;
			m_gridDx = gridDx;
			this->m_objFilename = objFilename;

			std::vector<VectorType> vertices;
			/*std::tie(vertices, m_geometryFaces) = OBJMeshImporter<VectorType>::getInstance()->loadMesh(objFilename);*/
			VectorType centroid = pose.getCentroid();
			for (size_t i = 0; i < vertices.size(); i++) {
				VectorType& v = vertices[i];
				v = v + centroid;
				m_geometryVertices.push_back(this->constructVertex(vertices[i], geometryVertex));
			}

			slice();
		}

		#pragma endregion

		#pragma region PrivateFunctionalities

		template <class VectorType>
		void IntersectedPolyMesh<VectorType>::classifyVertices() {
			for (const std::shared_ptr<Vertex<VectorType>>& vertex : this->getVertices()) {
				const VectorType& position = vertex->getPosition();
				size_t alignedAxes = 0;
				for (size_t i = 0; i < 3; i++) {
					alignedAxes += std::round(position[i] / m_gridDx) * m_gridDx == position[i];
				}
				switch (alignedAxes) {
				case 0:
					vertex->setVertexType(geometryVertex);
					break;
				case 1:
					vertex->setVertexType(cellVertex);
					break;
				case 2:
					vertex->setVertexType(edgeVertex);
					break;
				case 3:
					vertex->setVertexType(gridVertex);
					break;
				default:
					// should never happen
					assert(false);
					break;
				}
			}
		}

		template<class VectorType>
		void IntersectedPolyMesh<VectorType>::initializeRegularGridPatches() {
			const std::vector<std::shared_ptr<Cell<VectorType>>>& cells = this->getElements();
			for (size_t l = 0; l < cells.size(); l++) {
				VectorType gridSpacePosition = cells[l]->getCentroid() / m_gridDx;
				int i = static_cast<int>(std::floor(gridSpacePosition.x));
				int j = static_cast<int>(std::floor(gridSpacePosition.y));
				int k = static_cast<int>(std::floor(gridSpacePosition.z));
				m_regularGridPatches(i, j, k).push_back(l);
			}
		}

		template <class VectorType>
		void IntersectedPolyMesh<VectorType>::slice() {
			std::vector<VectorType> vertices;
			for (auto& v : m_geometryVertices) {
				vertices.push_back(v->getPosition());
			}
			std::vector<std::vector<uint>> faces(m_geometryFaces);

			MeshSlicer<VectorType> mesh_slicer(m_gridDx);
			mesh_slicer.slice(vertices, faces);

			this->m_vertices.clear();
			this->m_elements.clear();
			m_regularGridPatches.assign(vector<uint>());

			// create polygonal mesh
			this->initializeFaces(std::make_tuple(vertices, faces));

			classifyVertices();

			initializeLineMeshes();
			this->initializeVertexNormals();
			initializeRegularGridPatches();
		}

		template <class VectorType>
		void IntersectedPolyMesh<VectorType>::initializeLineMeshes() {
			const std::vector<std::shared_ptr<Vertex<VectorType>>>& vertices = this->getVertices();
			size_t numVertices = vertices.size();
			std::stack<std::shared_ptr<Vertex<VectorType>>> verticesToVisit;
			for (uint i = 0; i < 3; i++) {
				m_lineMeshes[i].clear();
				for (size_t j = 0; j < numVertices; j++) {
					std::shared_ptr<Vertex<VectorType>> vertex = vertices[j];
					double pos = vertex->getPosition()[i];
					int64_t slice = static_cast<int64_t>(std::round(pos / m_gridDx));
					if (vertex->hasUpdated() || slice * m_gridDx != pos) {
						continue;
					}
					std::vector<std::shared_ptr<Vertex<VectorType>>> lineMeshVertices;
					std::vector<std::shared_ptr<Edge<VectorType>>> lineMeshEdges;
					verticesToVisit.push(vertex);
					while (!verticesToVisit.empty()) {
						std::shared_ptr<Vertex<VectorType>> curVertex = verticesToVisit.top();
						verticesToVisit.pop();
						if (curVertex->hasUpdated()) {
							continue;
						}
						lineMeshVertices.push_back(curVertex);
						curVertex->setUpdated(true);
						OwnConnectedStructure<Edge<VectorType>>* pConnectedEdges = dynamic_cast<OwnConnectedStructure<Edge<VectorType>>*>(curVertex.get());
						std::vector<std::shared_ptr<Edge<VectorType>>> edges = pConnectedEdges->getConnectedStructures();
						for (size_t k = 0; k < edges.size(); k++) {
							if (edges[k]->isVisited()) {
								// edge already considered
								continue;
							}
							std::shared_ptr<Vertex<VectorType>> nextVertex = *edges[k]->getVertices().first == *curVertex ? edges[k]->getVertices().second : edges[k]->getVertices().first;
							double nextPos = nextVertex->getPosition()[i];
							int64_t nextVertexSlice = static_cast<int64_t>(std::round(nextVertex->getPosition()[i] / m_gridDx));
							if (slice != nextVertexSlice || slice * m_gridDx != nextPos) {
								// neighboring vertex is not on the same plane
								continue;
							}
							lineMeshEdges.push_back(edges[k]);
							edges[k]->setVisited(true);
							if (!nextVertex->hasUpdated()) {
								verticesToVisit.push(nextVertex);
							}

							// closes the mesh, remove this to make the algorithm work for non-manifold meshes!
							if (nextVertex->getID() == vertex->getID()) {
								lineMeshVertices.push_back(nextVertex);
								break;
							}
						}
					}
					std::for_each(lineMeshEdges.begin(), lineMeshEdges.end(), [](const std::shared_ptr<Edge<VectorType>>& edge) { edge->setVisited(false); });
					assert(slice >= 0);
					m_lineMeshes[i][static_cast<uint>(slice)].push_back(std::make_shared<IntersectedLineMesh<VectorType>>(lineMeshVertices, lineMeshEdges, m_gridDimensions, m_gridDx, cellLocation_t(i), true));
				}
				std::for_each(vertices.begin(), vertices.end(), [](const std::shared_ptr<Vertex<VectorType>>& vertex) { vertex->setUpdated(false); });
			}
		}

		#pragma endregion

		template class IntersectedPolyMesh<Vector3>;
		template class IntersectedPolyMesh<Vector3D>;
	}
}
