#include "Mesh/MeshUtils.h"

namespace Chimera {
	namespace Data{

		namespace MeshUtils {

			bool comparePairs(pair<Vector3, unsigned int *> a, pair<Vector3, unsigned int *> b) {
				if (a.first != b.first) {
					return a.first < b.first;
				} else {
					return *a.second < *b.second;
				}
			}

			pair < vector<Vector3>, vector<vector<unsigned int>> > simplifyMesh(const vector<Vector3> &vertices, const vector<vector<unsigned int>> & faces) {
				pair < vector<Vector3>, vector<vector<unsigned int>> > simplifiedMesh;
				vector<vector<unsigned int>> localFaces(faces);
				vector<pair<Vector3, unsigned int *>> allPairs;
				for (int i = 0; i < faces.size(); i++) {
					for (int j = 0; j < faces[i].size(); j++) {
						unsigned int *pIndex = &localFaces[i][j];
						allPairs.push_back(pair<Vector3, unsigned int *>(vertices[*pIndex], pIndex));
					}
				}
				sort(allPairs.begin(), allPairs.end(), MeshUtils::comparePairs);

				for (int i = 0; i < allPairs.size(); i++) {
					int nextIndex = roundClamp<int>(i + 1, 0, allPairs.size());
					while (allPairs[i].first == allPairs[nextIndex].first) {
						if (*allPairs[nextIndex].second > *allPairs[i].second) {
							*allPairs[nextIndex].second = *allPairs[i].second;
						}
						else if (*allPairs[nextIndex].second < *allPairs[i].second){
							*allPairs[i].second = *allPairs[nextIndex].second;
						}
						i++;
						if (i >= allPairs.size())
							break;
						nextIndex = roundClamp<int>(i + 1, 0, allPairs.size());
					}
				}

				for (int i = 0; i < allPairs.size();) {
					int nextIndex = roundClamp<int>(i + 1, 0, allPairs.size());
					if (simplifiedMesh.first.size() > 0 && simplifiedMesh.first.back() != vertices[*allPairs[i].second]) {
						simplifiedMesh.first.push_back(vertices[*allPairs[i].second]);
					}
					else {
						simplifiedMesh.first.push_back(vertices[*allPairs[i].second]);
					}

					if (*allPairs[i].second == *allPairs[nextIndex].second) {
						while (allPairs[i].first == allPairs[nextIndex].first) {
							*allPairs[i].second = simplifiedMesh.first.size() - 1;
							i++;
							nextIndex = roundClamp<int>(i + 1, 0, allPairs.size());
						}
						*allPairs[i].second = simplifiedMesh.first.size() - 1;
						i++;
					}
					else {
						i++;
					}
				}

				for (int i = 0; i < localFaces.size(); i++) {
					simplifiedMesh.second.push_back(localFaces[i]);
				}

				//Removing points duplicates
				for (int i = 0; i < simplifiedMesh.second.size(); i++) {
					for (int j = 1; j < simplifiedMesh.second[i].size();) {
						if (simplifiedMesh.second[i][j] == simplifiedMesh.second[i][j - 1]) {
							simplifiedMesh.second[i].erase(simplifiedMesh.second[i].begin() + j);
						}
						else {
							j++;
						}
					}
				}

				Vector3 centroid;
				for (int i = 0; i < simplifiedMesh.first.size(); i++) {
					centroid += simplifiedMesh.first[i];
				}
				centroid /= simplifiedMesh.first.size();

				// Align face normals
				//map<int, bool> edgeMap;

				//for (int i = 0; i < simplifiedMesh.second.size(); i++) {
				//	for (int j = 0; j < simplifiedMesh.second[i].size(); j++) {
				//		int nextJ = roundClamp<int>(j + 1, 0, simplifiedMesh.second[i].size());
				//		int currPoint = simplifiedMesh.second[i][j], nextPoint = simplifiedMesh.second[i][nextJ];
				//		int hashKey = nextPoint*simplifiedMesh.first.size() + currPoint;
				//		if (edgeMap.find(hashKey) != edgeMap.end()) {

				//			//Removing all old entries of this face on the edgeMap
				//			for (int k = 0; k < j; k++) {
				//				int nextK = roundClamp<int>(k + 1, 0, simplifiedMesh.second[i].size());
				//				currPoint = simplifiedMesh.second[i][k], nextPoint = simplifiedMesh.second[i][nextK];
				//				hashKey = nextPoint*simplifiedMesh.first.size() + currPoint;
				//				auto edgeMapIt = edgeMap.find(hashKey);
				//				if (edgeMapIt != edgeMap.end()) {
				//					edgeMap.erase(edgeMapIt);
				//				}
				//			}
				//			//Flip all edges of current face current vertex
				//			reverse(simplifiedMesh.second[i].begin(), simplifiedMesh.second[i].end());

				//			//Adding back all the entries on the edgeMap
				//			for (int k = 0; k < simplifiedMesh.second[i].size(); k++) {
				//				int nextK = roundClamp<int>(k + 1, 0, simplifiedMesh.second[i].size());
				//				currPoint = simplifiedMesh.second[i][k], nextPoint = simplifiedMesh.second[i][nextK];
				//				hashKey = nextPoint*simplifiedMesh.first.size() + currPoint;
				//				if (edgeMap.find(hashKey) != edgeMap.end()) {
				//					cout << "chola " << endl;
				//				}
				//				edgeMap[hashKey] = true;
				//			}

				//			//Break from outer edge map
				//			break;
				//		}
				//		else {
				//			edgeMap[hashKey] = true;
				//		}
				//	} 
				//}

				return simplifiedMesh;
			}

			int findDiscontinuity(const vector<pair<unsigned int, unsigned int>> &edges, int initialIndex) {
				 for (int i = initialIndex; i < edges.size() - 1; i++) {
					 if (edges[i].second != edges[i + 1].first) {
						 return i + 1; //Returns the first edge after discontinuity 
					 }
				 }
				 return -1; //No discontinuity
			 }

			
			/*Vector2 calculateCentroid(const vector<Vector2> &points) {
				Scalar signedArea = 0;
				Vector2 centroid;
				for (int i = 0; i < points.size(); i++) {
					int nextI = roundClamp<int>(i + 1, 0, points.size());
					Scalar currA = points[i].cross(points[nextI]);
					signedArea += currA;
					centroid += ((points[i] + points[nextI])*0.5)*currA;
				}
				signedArea *= 0.5f;
				centroid /= (6.0f*signedArea);
				return centroid;
			}*/

		


		}
	}
}
