//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.
//

#include "Mesh/IntersectedLineMesh.h"
#include "Grids/GridUtils.h"

namespace Chimera {

	namespace Meshes {

		#pragma region Functionalities


		template <class VectorType>
		void IntersectedLineMesh<VectorType>::updatePose(const Pose<VectorType> &newPose) {
			clearConnectedStructures();
			for (uint i = 0; i < m_regularGridPatches.getDimensions().x; i++) {
				for (uint j = 0; j < m_regularGridPatches.getDimensions().y; j++) {
					m_regularGridPatches(i, j).clear();
				}
			}
			//Emptying edges
			this->m_elements.clear();

			/** Updates pose only for geometry vertices */
			this->m_pose.update(newPose, m_geometryVertices);

			//Delete all vertices, except the geometry ones, since modifying a mesh position will make the crossings to
			//be recomputed.
			this->m_vertices.clear();
			m_intersectedVertices.clear();
			this->m_vertices = m_geometryVertices;

			if (m_perturbPoints) {
				perturbOnGridLineEdges(m_gridDx);
			}

			for (auto edge : m_geometryEdges) {
				vector<pair<DoubleScalar, VectorType>> crossings = computeGridCrossings(edge);
				addEdgeWithCrossings(edge, crossings);
			}

			initializeRegularGridPatches();

			this->initializeVertexNormals();

		}
		#pragma endregion

		#pragma region InitializationFunctions
		template<class VectorType>
		void IntersectedLineMesh<VectorType>::initializeRegularGridPatches() {
			switch (m_planeOrientation) {
			case XYPlane: { // x = i, y = j
				m_regularGridPatches.resize(dimensions_t(m_gridDimensions.x, m_gridDimensions.y, 0));
				for (uint j = 0; j < this->getElements().size(); j++) {
					shared_ptr<Edge<VectorType>> pCurrEdge = this->getElements()[j];
					VectorType gridSpacePosition = pCurrEdge->getCentroid() / m_gridDx;
					dimensions_t dimTemp(floor(gridSpacePosition.x), floor(gridSpacePosition.y));
					m_regularGridPatches(floor(gridSpacePosition.x), floor(gridSpacePosition.y)).push_back(j);
				}
				break;
			}
			case YZPlane: { //z = i, y = j
				m_regularGridPatches.resize(dimensions_t(m_gridDimensions.z, m_gridDimensions.y, 0));
				for (uint j = 0; j < this->getElements().size(); j++) {
					shared_ptr<Edge<VectorType>> pCurrEdge = this->getElements()[j];
					VectorType gridSpacePosition = pCurrEdge->getCentroid() / m_gridDx;
					dimensions_t dimTemp(floor(gridSpacePosition[2]), floor(gridSpacePosition.y));
					m_regularGridPatches(floor(gridSpacePosition[2]), floor(gridSpacePosition.y)).push_back(j);
				}
				break;
			}
			case XZPlane: { //x = j, z = j
				m_regularGridPatches.resize(dimensions_t(m_gridDimensions.x, m_gridDimensions.z, 0));
				for (uint j = 0; j < this->getElements().size(); j++) {
					shared_ptr<Edge<VectorType>> pCurrEdge = this->getElements()[j];
					VectorType gridSpacePosition = pCurrEdge->getCentroid() / m_gridDx;
					dimensions_t dimTemp(floor(gridSpacePosition.x), floor(gridSpacePosition[2]));
					m_regularGridPatches(floor(gridSpacePosition.x), floor(gridSpacePosition[2])).push_back(j);
				}
				break;
			}
			}
		}
		#pragma endregion


		#pragma region PrivateFunctionalities
		template <class VectorType>
		void IntersectedLineMesh<VectorType>::clearConnectedStructures() {
			for (auto pEdge : this->getElements()) {
				OwnConnectedStructure<Cell<VectorType>>* pConnectedCells = dynamic_cast<OwnConnectedStructure<Cell<VectorType>>*>(pEdge.get());
				pConnectedCells->clearConnectedStructures();
			}
			for (auto pVertex : this->getVertices()) {
				OwnConnectedStructure<Edge<VectorType>>* pConnectedEdges = dynamic_cast<OwnConnectedStructure<Edge<VectorType>>*>(pVertex.get());
				pConnectedEdges->clearConnectedStructures();

				OwnConnectedStructure<Cell<VectorType>>* pConnectedCells = dynamic_cast<OwnConnectedStructure<Cell<VectorType>>*>(pVertex.get());
				pConnectedCells->clearConnectedStructures();

				OwnConnectedStructure<Volume<VectorType>>* pConnectedVolumes = dynamic_cast<OwnConnectedStructure<Volume<VectorType>>*>(pVertex.get());
				pConnectedVolumes->clearConnectedStructures();
			}
		}

		template<class VectorType>
		bool crossingsSortFunction(pair<DoubleScalar, VectorType> c1, pair<DoubleScalar, VectorType> c2) {
			return c1.first < c2.first;
		}

		template<class VectorType>
		vector<pair<DoubleScalar, VectorType>> IntersectedLineMesh<VectorType>::computeGridCrossings(const pair<shared_ptr<Vertex<VectorType>>, shared_ptr<Vertex<VectorType>>> &edge) {
			if (m_gridDimensions.z != 0) {
				throw std::logic_error("IntersectedLineMesh: computeGridCrossings not supported in 3-D!");
			}

			VectorType iniHorizPoint;
			iniHorizPoint.x = floor(edge.first->getPosition().x / m_gridDx)*m_gridDx;
			iniHorizPoint.y = floor(edge.first->getPosition().y / m_gridDx)*m_gridDx;

			VectorType initVerticalPoint;
			initVerticalPoint.y = floor(edge.first->getPosition().y / m_gridDx)*m_gridDx;
			initVerticalPoint.x = floor(edge.first->getPosition().x / m_gridDx)*m_gridDx;

			vector<pair<DoubleScalar, VectorType>> allCrossings;
			DoubleScalar currLength = (edge.second->getPosition() - edge.first->getPosition()).length();

			// Compute horizontal crossings
			int numCells = floor(edge.second->getPosition().y / m_gridDx) - floor(iniHorizPoint.y / m_gridDx);
			int numCellsSign = numCells == 0 ? 1 : numCells / abs(numCells);
			for (int currIndex = 0; currIndex <= abs(numCells); currIndex++) {
				VectorType verticalVec;
				verticalVec.y = numCellsSign*currIndex*m_gridDx;
				VectorType horizontalLine = iniHorizPoint + verticalVec;
				VectorType horizontalCrossing;
				VectorType horizontalVec;
				horizontalVec.x = m_gridDx;
				if (segmentLineIntersection(edge.first->getPosition(), edge.second->getPosition(), horizontalLine, horizontalLine + horizontalVec, horizontalCrossing)) {
					DoubleScalar alpha = (horizontalCrossing - edge.first->getPosition()).length() / currLength;
					allCrossings.push_back(pair<DoubleScalar, VectorType>(alpha, horizontalCrossing));
				}
			}

			// Compute vertical crossings
			numCells = floor(edge.second->getPosition().x / m_gridDx) - floor(initVerticalPoint.x / m_gridDx);
			numCellsSign = numCells == 0 ? 1 : numCells / abs(numCells);
			for (int currIndex = 0; currIndex <= abs(numCells); currIndex++) {
				VectorType horizontalVec;
				horizontalVec.x = numCellsSign*currIndex*m_gridDx, 0;
				VectorType verticalLine = initVerticalPoint + horizontalVec;
				VectorType verticalCrossing;
				VectorType verticalVec;
				verticalVec.y = m_gridDx;
				if (segmentLineIntersection(edge.first->getPosition(), edge.second->getPosition(), verticalLine, verticalLine + verticalVec, verticalCrossing)) {
					DoubleScalar alpha = (verticalCrossing - edge.first->getPosition()).length() / currLength;
					allCrossings.push_back(pair<DoubleScalar, VectorType>(alpha, verticalCrossing));
				}
			}

			std::sort(allCrossings.begin(), allCrossings.end(), crossingsSortFunction<VectorType>);

			return allCrossings;
		}

		template<class VectorType>
		void IntersectedLineMesh<VectorType>::addEdgeWithCrossings(const pair<shared_ptr<Vertex<VectorType>>, shared_ptr<Vertex<VectorType>>> &edge, vector<pair<DoubleScalar, VectorType>> &crossings) {
			shared_ptr<Vertex<VectorType>> prevVertex = edge.first;

			if (m_gridDimensions.z != 0) {
				throw std::logic_error("IntersectedLineMesh: addEdgeWithCrossings not supported in 3-D!");
			}

			Scalar alphaPrecisionThreshold = Core::singlePrecisionThreshold*PI * 10;
			for (int j = 0; j < crossings.size(); j++) {
				if (j > 0 && (crossings[j].first - crossings[j - 1].first) < alphaPrecisionThreshold) {
					//Dont push this vertex and mark previous vertex as hybrid
					//Warning
					Logger::getInstance()->get() << "IntersectedLineMesh: Merging vertical and horizontal crossings due proximity." << endl;

					//Snapping the vertex position to be exaclty on the top of a grid node
					dimensions_t snappedDim(round(m_intersectedVertices.back()->getPosition().x / m_gridDx), round(m_intersectedVertices.back()->getPosition().y / m_gridDx));
					VectorType snappedPosition;
					snappedPosition.x = snappedDim.x*m_gridDx;
					snappedPosition.y = snappedDim.y*m_gridDx;
					m_intersectedVertices.back()->setPosition(snappedPosition);
					m_intersectedVertices.back()->setOnGridNode(true);
				}
				else {
					//m_intersectedVertices vector size stores how many geometry vertices were added, so we could
					//pad the insertion of newly created intersection vertices.
					shared_ptr<Vertex<VectorType>> intersectedVertex = Vertex<VectorType>::create(crossings[j].second, edgeVertex);
					this->m_vertices.push_back(intersectedVertex);
					m_intersectedVertices.push_back(intersectedVertex);
					this->m_elements.push_back(Edge<VectorType>::create(prevVertex, intersectedVertex, geometricEdge));
					prevVertex = intersectedVertex;
				}
			}
			this->m_elements.push_back(Edge<VectorType>::create(prevVertex, edge.second, geometricEdge));
		}

		template<class VectorType>
		void IntersectedLineMesh<VectorType>::perturbOnGridLineEdges(Scalar dx) {
			for (int i = 0; i < this->m_vertices.size(); i++) {
				while (Grids::isOnGridEdge(this->m_vertices[i]->getPosition(), dx)) {
					VectorType perturb;
					perturb.x = ((double)rand()) / RAND_MAX;
					perturb.y = ((double)rand()) / RAND_MAX;
					perturb *= 1e-5;
					this->m_vertices[i]->setPosition(this->m_vertices[i]->getPosition() + perturb);
					Logger::getInstance()->get() << "IntersectedLineMesh: Perturbing points to avoid on gridEdge errors. " << endl;
				}
			}
		}
		#pragma endregion

		template class IntersectedLineMesh<Vector2>;
		template class IntersectedLineMesh<Vector2D>;
		template class IntersectedLineMesh<Vector3>;
		template class IntersectedLineMesh<Vector3D>;
	}
}
