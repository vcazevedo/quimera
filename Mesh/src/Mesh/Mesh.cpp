#include "Mesh/Mesh.h"
#include "Mesh/LineMesh.h"
#include "Grids/QuadGridMesh.h"
#include "Grids/HexaGridMesh.h"

namespace Chimera {

	namespace Meshes {

		/*template <typename ElementPrimitive>
		unsigned int Mesh<ElementPrimitive>::m_currID = 0;*/

		#pragma region MeshAuxiliaryStructures

		#pragma region Constructors and Destructors
		template <typename ElementPrimitiveType>
		Mesh<ElementPrimitiveType>::Mesh(const vector<shared_ptr<Vertex<VectorType>>> &vertices, const vector<shared_ptr<ElementPrimitiveType>> &elementsPrimitive, const Pose<VectorType> &pose) 
			: m_vertices(vertices), m_elements(elementsPrimitive), m_pose(pose) {
			m_ID = m_currID++;
			VectorType centroid;
			for (int i = 0; i < m_vertices.size(); i++) {
				centroid += m_vertices[i]->getPosition();
			}
			centroid /= static_cast<DoubleScalar>(m_vertices.size());
			//Constructs a pose with the vertices centroid
			//m_pose = Pose<VectorType>(m_centroid);
			m_pose.setCentroid(centroid);
			//Updates the pose to match the given pose
			//m_pose.update(pose, m_vertices);
		}
		#pragma endregion

		#pragma region LinkingDeclarations
		template class Mesh<MeshType2D>;
		template class Mesh<MeshType3D>;

		template class Mesh<LineMeshType<Vector2>>;
		template class Mesh<LineMeshType<Vector2D>>;
		template class Mesh<LineMeshType<Vector3>>;
		template class Mesh<LineMeshType<Vector3D>>;

		template class Mesh<Grids::QuadGridType>;
		template class Mesh<Grids::QuadGridTypeDouble>;

		template class Mesh<Grids::HexaGridType>;
		template class Mesh<Grids::HexaGridTypeDouble>;
		#pragma endregion
	}
}
