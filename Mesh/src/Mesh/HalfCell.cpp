#include "Mesh/HalfCell.h"

namespace Chimera {

	namespace Meshes {

		#pragma region Functionalities
		template<class VectorType>
		bool HalfCell<VectorType>::hasHalfedge(shared_ptr<HalfEdge<VectorType>> pHalfEdge) {
			for (int i = 0; i < m_halfEdges.size(); i++) {
				if (pHalfEdge->getID() == m_halfEdges[i]->getID()) {
					return true;
				}
			}
			return false;
		}

		template<class VectorType>
		bool HalfCell<VectorType>::hasReverseHalfedge(shared_ptr<HalfEdge<VectorType>> pHalfEdge) {
			for (int i = 0; i < m_halfEdges.size(); i++) {
				if (pHalfEdge->getVertices().first->getID() == m_halfEdges[i]->getVertices().second->getID() &&
					pHalfEdge->getVertices().second->getID() == m_halfEdges[i]->getVertices().first->getID()) {
					return true;
				}
			}
			return false;
		}

		template<class VectorType>
		shared_ptr<HalfCell<VectorType>> HalfCell<VectorType>::reversedCopy() {
			vector<shared_ptr<HalfEdge<VectorType>>> inverseHalfEdges;
			for(int i = 0; i < m_halfEdges.size(); i++) {
				uint rI = m_halfEdges.size() - (i + 1);

				if (*m_halfEdges[rI]->getEdge()->getHalfEdges().first == *m_halfEdges[rI]) {
					inverseHalfEdges.push_back(m_halfEdges[rI]->getEdge()->getHalfEdges().second);
				}
				else if (*m_halfEdges[rI]->getEdge()->getHalfEdges().second == *m_halfEdges[rI]) {
					inverseHalfEdges.push_back(m_halfEdges[rI]->getEdge()->getHalfEdges().first);
				}
				else {
					throw(std::logic_error("HalfFace reversed copy error: halfedges inside edge do not match"));
				}
			}
			halfCellLocation_t halfCellLocation = backHalfCell;
			if (m_cellLocation == backHalfCell)
				halfCellLocation = frontHalfCell;
			if (m_cellLocation == leftHalfCell)
				halfCellLocation = rightHalfCell;
			if (m_cellLocation == bottomHalfCell)
				halfCellLocation = topHalfCell;

			return HalfCell<VectorType>::create(inverseHalfEdges, m_pCell, halfCellLocation);
		}

		#pragma endregion

		#pragma region PrivateFunctionalities
		template<>
		Vector2 HalfCell<Vector2>::computeNormal() {
			return Vector2(0, 0);
		}

		template<>
		Vector2D HalfCell<Vector2D>::computeNormal() {
			return Vector2D(0, 0);
		}

		template<>
		Vector3 HalfCell<Vector3>::computeNormal() {
			Vector3 v1, v2;
			v1 = m_halfEdges[0]->getVertices().second->getPosition() - m_halfEdges[0]->getVertices().first->getPosition();
			v2 = m_halfEdges[1]->getVertices().second->getPosition() - m_halfEdges[1]->getVertices().first->getPosition();
			Vector3 normal = v1.cross(v2);
			normal.normalize();
			return normal;
		}

		template<>
		Vector3D HalfCell<Vector3D>::computeNormal() {
			Vector3D v1, v2;
			v1 = m_halfEdges[0]->getVertices().second->getPosition() - m_halfEdges[0]->getVertices().first->getPosition();
			v2 = m_halfEdges[1]->getVertices().second->getPosition() - m_halfEdges[1]->getVertices().first->getPosition();
			Vector3D normal = v1.cross(v2);
			normal.normalize();
			return normal;
		}
		#pragma endregion

		template class HalfCell<Vector2>;
		template class HalfCell<Vector2D>;
		template class HalfCell<Vector3>;
		template class HalfCell<Vector3D>;
	}
}
