#include "Mesh/Cell.h"
#include "Grids/GridUtils.h"

namespace Chimera {

	namespace Meshes {
		
		
		#pragma region Functionalities

		template <class VectorType>
		bool Cell<VectorType>::crossedThroughGeometry(const VectorType& v1, const VectorType& v2, VectorType& crossingPoint) const {
			bool crossedThroughGeometry = false;
			for (auto pEdge : m_edges) {
				if (pEdge->getType() == geometricEdge) {
					if (DoLinesIntersect(v1, v1, pEdge->getVertices().first->getPosition(), pEdge->getVertices().second->getPosition(), crossingPoint)) {
						return true;
					}
				}
			}
			return false;
		}

		template <class VectorType>
		Scalar Cell<VectorType>::getDistanceToBoundary(const VectorType& position) const {
			Scalar minDistance = FLT_MAX;
			for (auto pEdge : m_edges) {
				if (pEdge->getType() == geometricEdge) {
					Scalar currDistance = distanceToLineSegment(position, pEdge->getVertices().first->getPosition(),
																																pEdge->getVertices().second->getPosition());
					if (currDistance < minDistance) {
						minDistance = currDistance;
					}
				}
			}
			return minDistance;
		}

		template<class VectorType>
		bool Cell<VectorType>::isInside(const VectorType& position, DoubleScalar accuracy) const {
			const vector<shared_ptr<HalfEdge<VectorType>>> & halfEdges = m_halfCells.first->getHalfEdges();

			/** Checking if points are vertices */
			for (int i = 0; i < halfEdges.size(); i++) {
				if (halfEdges[i]->getVertices().first->intersects(position, accuracy)) {
					return true;
				}
			}

			/** Checking if points are livin on the edge */
			for (unsigned i = 0; i < halfEdges.size(); i++) {
				if (halfEdges[i]->getEdge()->intersects(position, accuracy))
					return true;
			}

			//http://alienryderflex.com/polygon/ polygon function
			int i = 0;
			bool insidePolygon = false;

			if (m_cellLocation == XYPlane) { //XY plane
				for (unsigned i = 0; i < halfEdges.size(); i++) {
					const VectorType& pointPositionI = halfEdges[i]->getVertices().first->getPosition();
					const VectorType& pointPositionJ = halfEdges[i]->getVertices().second->getPosition();
					if ((pointPositionI.y < position.y && pointPositionJ.y >= position.y)
						|| (pointPositionJ.y < position.y && pointPositionI.y >= position.y)) {
						if (pointPositionI.x + (position.y - pointPositionI.y) / (pointPositionJ.y - pointPositionI.y) * (pointPositionJ.x - pointPositionI.x) < position.x) {
							insidePolygon = !insidePolygon;
						}
					}
				}
			}
			else if (m_cellLocation == YZPlane) { //YZ plane 
				for (unsigned i = 0; i < halfEdges.size(); i++) {
					const VectorType& pointPositionI = halfEdges[i]->getVertices().first->getPosition();
					const VectorType& pointPositionJ = halfEdges[i]->getVertices().second->getPosition();
					if ((pointPositionI.y < position.y && pointPositionJ.y >= position.y)
						|| (pointPositionJ.y < position.y && pointPositionI.y >= position.y)) {
						if (pointPositionI[2] + (position.y - pointPositionI.y) / (pointPositionJ.y - pointPositionI.y) * (pointPositionJ[2] - pointPositionI[2]) < position[2]) {
							insidePolygon = !insidePolygon;
						}
					}
				}
			}
			else if (m_cellLocation == XZPlane) { //XZ plane 
				for (unsigned i = 0; i < halfEdges.size(); i++) {
					const VectorType& pointPositionI = halfEdges[i]->getVertices().first->getPosition();
					const VectorType& pointPositionJ = halfEdges[i]->getVertices().second->getPosition();
					if ((pointPositionI[2] < position[2] && pointPositionJ[2] >= position[2])
						|| (pointPositionJ[2] < position[2] && pointPositionI[2] >= position[2])) {
						if (pointPositionI.x + (position[2] - pointPositionI[2]) / (pointPositionJ[2] - pointPositionI[2]) * (pointPositionJ.x - pointPositionI.x) < position.x) {
							insidePolygon = !insidePolygon;
						}
					}
				}
			}
			else { //Geometric half-face
				return false; //In 3-d we wont deal with this 
			}

			return insidePolygon;
		}

		template<class VectorType>
		bool Cell<VectorType>::rayIntersect(const VectorType& point, const VectorType& rayDirection) {
			if constexpr (isVector3<VectorType>::value) {
				if (m_cellLocation != geometricCell) {
					return false;
				}
				VectorType* trianglePoints = new VectorType[3];

				trianglePoints[0] = m_halfCells.first->getHalfEdges()[0]->getVertices().first->getPosition();
				trianglePoints[1] = m_halfCells.first->getHalfEdges()[1]->getVertices().first->getPosition();
				trianglePoints[2] = m_halfCells.first->getHalfEdges()[2]->getVertices().first->getPosition();

				VectorType intersectionPoint;
				return triangleRayIntersection(trianglePoints, point, rayDirection, intersectionPoint);
			}
			//Only implemented for vector3 operations
			return false;
		}

		#pragma region PrivateFunctionalities
		
		template <class VectorType>
		DoubleScalar Cell<VectorType>::calculateArea() {
			if constexpr (isVector3<VectorType>::value){
				DoubleScalar areaSum = 0.0f;
				auto halfFace = m_halfCells.first;
				auto halfEdges = halfFace->getHalfEdges();
				for (int i = 0; i < halfEdges.size(); i++) {
					areaSum += halfFace->getNormal().dot(halfEdges[i]->getVertices().first->getPosition().cross(halfEdges[i]->getVertices().second->getPosition()));
				}
				return abs(areaSum * 0.5);
			}
			return 0;
		}

		template <class VectorType>
		void Cell<VectorType>::initializeCentroid() {
			if constexpr (isVector2<VectorType>::value) {
				map<uint, uint> m_verticesMap;
				VectorType centroid;
				uint numVertices = 0;
				DoubleScalar area = 0.0;
				auto vlast = m_edges[0]->getVertex1();
				for (auto& edge : m_edges) {
					//Not efficient but works --> a better solution would be using also the area of the cell
					centroid += edge->getVertex1()->getPosition();
					centroid += edge->getVertex2()->getPosition();

				}
				m_centroid = centroid/(2*m_edges.size());
			}
			else if constexpr (isVector3<VectorType>::value) {
				VectorType centroid(0);

				DoubleScalar area = 0.0;
				VectorType v = m_edges[0]->getVertex1()->getPosition();
				auto vlast = m_edges[0]->getVertex1();
				for (auto& edge : m_edges) {
					auto& halfEdge = (edge->getHalfEdges().first->getVertices().first->getID() == vlast->getID()) ?
						edge->getHalfEdges().first :
						edge->getHalfEdges().second;
					VectorType v1 = halfEdge->getVertices().first->getPosition() - v;
					VectorType v2 = halfEdge->getVertices().second->getPosition() - v;
					VectorType c = v1.cross(v2);
					DoubleScalar tempArea = 0.5 * v1.cross(v2).length();
					area += tempArea;
					centroid += (3 * v + v1 + v2) / 3 * tempArea;

					vlast = halfEdge->getVertices().second;
				}
				//cout << area << endl;
				//m_areaFraction = abs(area/m_gridDx/m_gridDx);
				if (area > 0) {
					m_centroid = centroid / area;
				}
				else {
					m_centroid = m_edges[0]->getVertex1()->getPosition();
				}
				switch (m_cellLocation) {
				case YZPlane:
					m_centroid.x = m_edges[0]->getVertex1()->getPosition().x;
					break;
				case XZPlane:
					m_centroid.y = m_edges[0]->getVertex1()->getPosition().y;
					break;
				case XYPlane:
					m_centroid.z = m_edges[0]->getVertex1()->getPosition().z;
					break;
				default:
					break;
				}
			}
			
		}
		#pragma endregion

		template class Cell<Vector2>;
		template class Cell<Vector2D>;
		template class Cell<Vector3>;
		template class Cell<Vector3D>;

	}
}
