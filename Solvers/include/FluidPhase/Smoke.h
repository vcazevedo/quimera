//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.


#ifndef __CHIMERA_SMOKE_H_
#define __CHIMERA_SMOKE_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "ChimeraAdvection.h"

namespace Chimera {

	
	using namespace Grids;
	using namespace Advection;
	
	/** Smoke phase class */
	
	namespace Solvers {
		
		template <typename SolverType>
		class Smoke : public UpdateInterface {

		protected:
			using VectorType = typename SolverType::VectorType;

			//MeshType used for representing meshes for obstacles representation
			using EmbeddedMeshType = typename SolverType::EmbeddedMeshType;

			//GridMeshType represents the Grid Type, which also defines the voxel
			using GridMeshType = typename SolverType::GridMeshType;

			#pragma region ClassStructures
			class Shape {
				VectorType m_position;
				bool m_continuousSource;
				string m_name;

				static uint m_ID;

			public:
				Shape(const VectorType& position, bool continuousSource = true) : m_position(position) {
					m_continuousSource = continuousSource;
					m_ID++;
				}

				virtual bool isInside(const VectorType& position) = 0;

				virtual Scalar computeSignedDistance(const VectorType& position) = 0;

				virtual void updateLevelSet(shared_ptr<LevelSetGridMesh<EmbeddedMeshType, GridMeshType>> pLevelSet) {
					for (auto pVertex : pLevelSet->getVertices()) {
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("levelSet", computeSignedDistance(pVertex->getPosition()));
					}
				}

				bool isContinuousSource() {
					return m_continuousSource;
				}
			};

			class Sphere : public Shape {
				Scalar m_radius;

			public:
				Sphere(const VectorType& position, Scalar radius, bool continuousSource = true) :  Shape(position, continuousSource) {
					m_radius = radius;
					Shape::m_name = "sphere_" + string(Shape::m_ID);
				}

				virtual bool isInside(const VectorType& position) {
					Scalar distance = (position - Shape::m_position).length();
					if (distance < m_radius) {
						return true;
					}
					return false;
				}

				virtual Scalar computeSignedDistance(const VectorType& position) {
					Scalar distance = (position - Shape::m_position).length();
					if (isInside(position)) {
						return -distance;
					}
					return distance;
				}
			};

			#pragma endregion
			

		public:

			Smoke(shared_ptr<LevelSetGridMesh<EmbeddedMeshType, GridMeshType>> pLevelSet) : m_pLevelSet(pLevelSet) {

			}


			#pragma region InitializeShape
			void addSphereSource(const VectorType &position, Scalar radius, bool initializeSource = true, bool continuousSorce = true) {
				m_smokeShapes.push_back(make_shared<Sphere>(position, radius, continuousSorce));
				
				if (initializeSource) {
					m_smokeShapes.back()->updateLevelSet(m_pLevelSet);
					applySources();
				}
			}
			#pragma endregion

			#pragma region Functionalities
			virtual void update(Scalar dt) {
				m_pLevelSet->reset();

				for (auto pShape : m_smokeShapes) {
					if(pShape->isContinuousSource()) {
						pShape->updateLevelSet(m_pLevelSet);
					}
				}

				applySources();
			};

			void applySources() {
				/** Apply sources*/
				for (auto pVertex : m_pLevelSet->getVertices()) {
					if (OwnCustomAttribute<Scalar>::get(pVertex)->getAttribute("levelSet") <= 0) {
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("smoke", m_smokeValue);
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("temperature", m_temperatureValue);
					}
				}
			}
			#pragma endregion

		protected:
			#pragma region ClassMembers
			vector<shared_ptr<Shape>> m_smokeShapes;

			shared_ptr<LevelSetGridMesh<EmbeddedMeshType, GridMeshType>> m_pLevelSet;

			/** Smoke and temperature values*/
			Scalar m_smokeValue;
			Scalar m_smokeVariance;

			Scalar m_temperatureValue;
			Scalar m_temperatureVariance;
			#pragma endregion
		};

	}
}

#endif
