
#ifndef __GHOST_LIQUID_SOLVER_3D_H__
#define __GHOST_LIQUID_SOLVER_3D_H__
#pragma once

#include "ChimeraCore.h"
#include "Solvers/3D/VoxelizedGridSolver3D.h"

namespace Chimera {
	namespace Solvers {

		class GhostLiquidSolver3D : public VoxelizedGridSolver3D {
		public:
			#pragma region Constructors
			GhostLiquidSolver3D(const params_t &params, StructuredGrid<Vector3, Array3D> *pGrid,
				const vector<BoundaryCondition<Vector3, Array3D> *> &boundaryConditions,
				LiquidRepresentation<Vector3, Array3D> *pLiquidRepresentation);
			#pragma endregion

			#pragma region Update Functions
			// Saves Liquid Mesh into a wavefront (.obj) file
			void saveLiquidMesh(Scalar dt) const;
			// Updates ghost liquid solver
			void update(Scalar dt) override;
			#pragma endregion

			#pragma region Simulation Functions
			// Divergence free pressure projection.
			// This function is implementation specific
			void divergenceFree(Scalar dt) override;
			// Calculates flux divergent
			Scalar calculateFluxDivergent(int i, int j, int k) override;
			// Apply Gravity Force
			void applyForces(Scalar dt) override;
			#pragma endregion

		protected:
			#pragma region Ghost Fluid Method Helper Methods
			Scalar phiHelper(dimensions_t cellIndex) const;
			// method that computes the linear interpolation between adjacent cells of partial cell
			Scalar thetaHelper(Scalar inside, Scalar outside) const;
			virtual Scalar ghostFluidHelper(const dimensions_t &liquidCellIndex, const dimensions_t &otherCellIndex) const;
			void applyGhostFluidDiagonal();
			void correctVelocityGhostFluid(Scalar dt);
			#pragma endregion

			#pragma region Internal Functionalities
			// Updates Viscosity Poisson Matrix
			void updateViscosityMatrix(Scalar dt);
			// Makes Viscosity Update on Velocities
			void solveViscosity();
			// Updates thin objects Poisson Matrix. This function is implementation-specific.
			// Returns if the method needs additional entries on the Poisson matrix.
			bool updatePoissonMatrix();
			// Extrapolates Velocities at air-fluid boundary
			void extrapolateVelocities();
			#pragma endregion

			#pragma region Helper Methods
			// returns the indices of the requested cell, seen from the provided cell indices
			static dimensions_t getNeighborIndex(const dimensions_t &cell, halfCellLocation_t location) {
				dimensions_t displacement(0, 0, 0);
				switch (location) {
					case rightHalfCell:  displacement.x =  1; break;
					case leftHalfCell:   displacement.x = -1; break;
					case topHalfCell:    displacement.y =  1; break;
					case bottomHalfCell: displacement.y = -1; break;
					case frontHalfCell:  displacement.z =  1; break;
					case backHalfCell:   displacement.z = -1; break;
					default: throw std::logic_error("unrecognized halfFaceLocation_t");
				}
				return cell + displacement;
			}
			// inserts the cell's neighbor's indices in the provided vector neighbors (vector entries present will be wiped)
			static void getNeighborIndices(const dimensions_t &cell, vector<dimensions_t> &neighbors) {
				const int i = cell.x, j = cell.y, k = cell.z;
				neighbors = vector<dimensions_t>(6);
				neighbors[rightHalfCell]  = dimensions_t(i + 1, j, k); // right
				neighbors[leftHalfCell]   = dimensions_t(i - 1, j, k); // left
				neighbors[topHalfCell]    = dimensions_t(i, j + 1, k); // top
				neighbors[bottomHalfCell] = dimensions_t(i, j - 1, k); // bottom
				neighbors[frontHalfCell]  = dimensions_t(i, j, k + 1); // front
				neighbors[backHalfCell]   = dimensions_t(i, j, k - 1); // back
			}
			// inserts the cell's neighbor's indices in the provided vector neighbors (vector entries present will be wiped)
			// excludes entries that are on outer boundary
			void getNonBoundaryNeighborIndices(const dimensions_t &cell, vector<dimensions_t> &neighbors) const {
				const int i = cell.x, j = cell.y, k = cell.z;
				neighbors.clear(); neighbors.reserve(6);
				if (i != 1) neighbors.emplace_back(i - 1, j, k); // left
				if (j != 1) neighbors.emplace_back(i, j - 1, k); // bottom
				if (k != 1) neighbors.emplace_back(i, j, k - 1); // back
				if (i + 2 != m_dimensions.x) neighbors.emplace_back(i + 1, j, k); // right
				if (j + 2 != m_dimensions.y) neighbors.emplace_back(i, j + 1, k); // top
				if (k + 2 != m_dimensions.z) neighbors.emplace_back(i, j, k + 1); // front
			}
			#pragma endregion

			#pragma region Class Members
			const Scalar m_airDensityCoeff;
			const Scalar m_liquidDensityCoeff;
			const Scalar m_liquidViscosityCoeff;
			LiquidRepresentation<Vector3, Array3D> *m_pLiquidRepresentation;
			// Matrices for Eigen CG Computations
			vector<Eigen::Triplet<Scalar>> m_viscosityMatrixEntries;
			// cell types reference to Liquid Representation
			Array3D<cellType_t> &m_cellTypes;
			#pragma endregion
		};



	}
}

#endif
