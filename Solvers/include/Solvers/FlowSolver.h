//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.


#ifndef __CHIMERA_FLOW_SOLVER__
#define __CHIMERA_FLOW_SOLVER__
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "ChimeraMesh.h"
#include "ChimeraAdvection.h"
#include "Eigen/Sparse"

namespace Chimera {

	
	using namespace Grids;
	using namespace Advection;
	
	/** Flow Solver class. Used as a base class for all types of structured flow solvers. 
	 ** It defines a solving routine (update) which is the base building block function for all types of flow solvers
			It also implies that all quantities added to the solver will undergo advection and mass conservation. */
	
	namespace Solvers {
		class FlowSolverType2D {
		public:
			using VectorType = Core::Vector2;
			using EmbeddedMeshType = Meshes::LineMeshType<VectorType>;
			using EmbeddedMeshClass = Meshes::IntersectedLineMesh<VectorType>;
			using ElementPrimitiveType = Meshes::QuadCell<VectorType>;
			using GridMeshType = Grids::QuadGridType;
		};

		class FlowSolverTypeDouble2D {
		public:
			using VectorType = Core::Vector2D;
			using EmbeddedMeshType = Meshes::LineMesh<VectorType>;
			using ElementPrimitiveType = Meshes::QuadCell<VectorType>;
			using GridMeshType = Grids::QuadGridTypeDouble;
		};

		template <typename SolverType>
		class FlowSolver : public UpdateInterface {
		protected:
			using VectorType = typename SolverType::VectorType;
			
			//MeshType used for representing meshes for obstacles representation
			using EmbeddedMeshClass = typename SolverType::EmbeddedMeshClass;

			//GridMeshType represents the Grid Type, which also defines the voxel
			using GridMeshType = typename SolverType::GridMeshType;
			
		public:
			#pragma region Constructors
			FlowSolver(shared_ptr<GridMesh<GridMeshType>> pSimulationGrid,
									const map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>>> &levelSets) {
				
				m_pSimulationGrid = pSimulationGrid;
				m_levelSets = levelSets;

				m_pAdvectionSolver = nullptr;
				m_numSteps = 0;
			}

			virtual ~FlowSolver() = default;
			#pragma endregion

			#pragma region AccessFunctions
			FORCE_INLINE shared_ptr<Advection::AdvectionBase> getAdvectionSolver() const {
				return m_pAdvectionSolver;
			}

			/** Grid */
			FORCE_INLINE shared_ptr<GridMesh<GridMeshType>> getSimulationGrid() const {
				return m_pSimulationGrid;
			}

			FORCE_INLINE shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>> getLevelSet(const string &levelSetName) const {
				return m_levelSets.at(levelSetName);
			}

			FORCE_INLINE const map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>>> getLevelSets() const {
				return m_levelSets;
			}

			#pragma endregion
		protected:
			#pragma region ClassMembers
			/** Number of iterations performed by the simulation so far */
			uint m_numSteps;

			/** Simulation Mesh Grid */
			shared_ptr<GridMesh<GridMeshType>> m_pSimulationGrid;

			/** Levelset grid map for geometries, objects and liquids. Each entry is separated from simulation grid since 
					it can be sampled at higher resolution. */
			map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>>> m_levelSets;

			/** Advection algorithm */
			shared_ptr<Advection::AdvectionBase> m_pAdvectionSolver;

			/** Poisson Matrix */
			shared_ptr<Eigen::SparseMatrix<Scalar>> m_pPoissonMatrix;

			/** Timers for tracking the time of specific fluid phases */
			map<string, Core::Timer> m_timers;
			#pragma endregion

			#pragma region InitializationFunctions
			/** Each solver should add its necessary variables in the gridMesh structure*/
			virtual void initializeGridVariables() = 0;
			#pragma endregion
		};
	}
	

}

#endif
