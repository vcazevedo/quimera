//  Copyright (c) 2013, Vinicius Costa zevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_VOXELIZED_SOLVER_2D__
#define __CHIMERA_VOXELIZED_SOLVER_2D__
#pragma once

#include "ChimeraCore.h"
#include "ChimeraAdvection.h"
#include "ChimeraInterpolation.h"
#include "Solvers/FlowSolver.h"
#include "ChimeraMesh.h"

namespace Chimera {

	using namespace Core;
	using namespace Advection;
	using namespace Solvers;
	using namespace Meshes;

	/** Implementation of the classic Navier-Stokes solver, for unsteady incompressible flows.
			Initializes the grid mesh with staggered Cartesian velocities stored at grid edges centers,
			and pressures stored at regular grid cells centroids.
			This class is responsible for transferring solid objects representations to a voxelized grid,
			representing the object without fractional boundaries. Its main responsibilities are:
				- update level set representations and solid cells
				- update poisson matrix to solid objects
				- voxelized velocities boundary conditions
				- transfer fluid velocities forces to the immersed bodies
	*/

	namespace Solvers {



		template <typename SolverType>
		class VoxelizedGridSolver2D : public FlowSolver<SolverType> {
		protected:
			using VectorType = typename SolverType::VectorType;
			using EmbeddedMeshType = typename SolverType::EmbeddedMeshType;
			using EmbeddedMeshClass = typename SolverType::EmbeddedMeshClass;
			using GridMeshType = typename SolverType::GridMeshType;
			#if defined _WIN32
				using ParentClass = typename FlowSolver<SolverType>;
			#else
				using ParentClass = class FlowSolver<SolverType>;
			#endif


		public:

			#pragma region Constructors
			//Default constructor for derived classes
			VoxelizedGridSolver2D(shared_ptr<GridMesh<GridMeshType>> pSimulationGrid, const map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>>>& levelSets);
			#pragma endregion

			#pragma region Functionalities
			virtual void update(Scalar dt) override;
			#pragma endregion

		protected:
			using VelocityInterpolant = Interpolant <VectorType, GridMeshType>;
			using ScalarInterpolant = Interpolant <Scalar, GridMeshType>;

			#pragma region ClassMembers
			shared_ptr<Eigen::VectorXf> m_pDivergents;
			shared_ptr<Eigen::VectorXf> m_pPressures;
			Array2D<unsigned char> m_voxelizedCells;

			/** Store primal (m_pQuadGridMesh) mesh */
			shared_ptr<QuadGridMesh<GridMeshType>> m_pQuadGridMesh;

			shared_ptr<VelocityInterpolant> m_pVelocityInterpolant;
			shared_ptr<VelocityInterpolant> m_pAuxVelocityInterpolant;
			#pragma endregion

			#pragma region InitializationFunctions
			virtual void initializeGridVariables() override {
				if (!m_pQuadGridMesh) {
					throw std::logic_error("VoxelizedGridSolver2D::verifyGridVariables() not a valid quadGridMesh!");
				}
				m_pVelocityInterpolant = m_pQuadGridMesh->template addEdgesAttribute<Scalar, BilinearStaggeredInterpolant2D<VectorType, GridMeshType>>("velocity", 0.0f);
				m_pAuxVelocityInterpolant = m_pQuadGridMesh->template addEdgesAttribute<Scalar, BilinearStaggeredInterpolant2D<VectorType, GridMeshType>>("auxVelocity", 0.0f);
				
				m_pVelocityInterpolant->setSiblingInterpolant(m_pAuxVelocityInterpolant);
				m_pAuxVelocityInterpolant->setSiblingInterpolant(m_pVelocityInterpolant);

				m_pQuadGridMesh->template addCellsAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, GridMeshType>>("pressure", 0.0f);
				m_pQuadGridMesh->template addCellsAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, GridMeshType>>("divergence", 0.0f);
				m_pQuadGridMesh->template addVerticesAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, GridMeshType>>("vorticity", 0.0f);

				if (ParentClass::m_levelSets.find("smoke") != ParentClass::m_levelSets.end()) {
					auto pSmokeGrid = ParentClass::m_levelSets["smoke"]->getGrid();

					SemiLagrangianAdvection<GridMeshType>* pAdvection = dynamic_cast<SemiLagrangianAdvection<GridMeshType>*>(this->m_pAdvectionSolver.get());
					pAdvection->addScalarFieldInterpolant(pSmokeGrid->getScalarFieldInterpolantVertices("temperature"));
					pAdvection->addScalarFieldInterpolant(pSmokeGrid->getScalarFieldInterpolantVertices("smoke"));
				}
			}

			virtual void initializeAdvection() {
				//SemiLagrangianAdvection<GridMeshType> *pAdvection = dynamic_cast<SemiLagrangianAdvection<GridMeshType>*>(this->m_pAdvectionSolver.get());
			}
			#pragma endregion

			
			#pragma region BoundaryConditions
			/** Enforces solid walls boundary conditions for both velocities and pressure samples */
			virtual void enforceSolidWallsConditions(const VectorType &solidVelocity);

			virtual void updateVoxelizedCells();
			#pragma endregion

			#pragma region PressureProjection
			/** Initializes the Poisson matrix accordingly with Finite Differences formulation */
			virtual void updatePoissonMatrix();

			/** These function assume that QuadGridMesh is strictly regular, so each edge vector has only one edge
					and the quadCells have only one Cell.*/
			virtual void updateDivergents();
			virtual void solvePressure(Scalar dt);
			virtual void applyPressureGradients(Scalar dt);
			
			/** Given the pressure solved by the Linear system, projects the velocity in its divergence-free part. */
			virtual void project(Scalar dt, bool firstCall = true);

			uint getPressureRow(uint i, uint j) {
				return (j - 1) * (m_pQuadGridMesh->getGridDimensions().x - 2) + (i - 1);
			}
			#pragma endregion

			#pragma region Misc
			void computeVorticity();
			#pragma endregion
		};
	}
}

#endif
