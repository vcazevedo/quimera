#pragma once
//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_GHOST_LIQUIDS_SOLVER_2D__
#define __CHIMERA_GHOST_LIQUIDS_SOLVER_2D__
#pragma once


#include "ChimeraCore.h"
#include "ChimeraAdvection.h"
#include "ChimeraParticles.h"
#include "ChimeraInterpolation.h"

#include "Solvers/2D/VoxelizedGridSolver2D.h"

#include "ChimeraLevelSets.h"

namespace Chimera {

	using namespace LevelSets;
	/** Implementation of the ghost-pressures method for liquids */
	class GhostLiquidSolver : public VoxelizedGridSolver2D {
	public:

		#pragma region ConstructorsDestructors
		/** Standard constructor. Receives params that will configure solver's several characteristics, the underlying
		** structured grid *pGrid, boundary conditions and the liquids original mesh representation. */
		GhostLiquidSolver(const params_t &params, StructuredGrid<Vector2, Array2D> *pGrid, const vector<BoundaryCondition<Vector2, Array2D> *> &boundaryConditions,
							LiquidRepresentation<Vector2, Array2D> *pLiquidRepresentation);
		#pragma endregion ConstructorsDestructors

		#pragma region Ghost Fluid Method Helper Methods
		Scalar phiHelper(int i, int j) const;
		Scalar thetaHelper(Scalar inside, Scalar outside) const;
		virtual Scalar ghostFluidHelper(int i1, int j1, int i2, int j2) const;
		void applyGhostFluidDiagonal();
		void correctVelocityGhostFluid(Scalar dt);
		#pragma endregion

		#pragma region UpdateFunctions
		/** Updates thin objects Poisson Matrix. This function is implementation-specific. Returns if the method needs
		* additional entries on the Poisson matrix. */
		bool updatePoissonMatrix();
		#pragma endregion UpdateFunctions

		#pragma region SimulationFunctions
		/** Divergence free pressure projection. This function is implementation specific */
		void divergenceFree(Scalar dt);

		/** Calculates flux divergent */
		Scalar calculateFluxDivergent(int i, int j);

		/** Force applied */
		void applyForces(Scalar dt) override;

		/** Updates ghost liquid solver */
		void update(Scalar dt) override;
		#pragma endregion SimulationFunctions

		#pragma region AccessFunctions
		//bool isBoundaryCell(int i, int j) {
		//	return m_boundaryCells(i, j);
		//}

		LiquidRepresentation<Vector2, Array2D> * getLiquidRepresentation() {
			return m_pLiquidRepresentation;
		}
		#pragma endregion AccessFunctions

	protected:

		#pragma region InternalFunctionalities
		/** Given a ray traced from cellIndex to nextCell, calculates the distance between the intersection of this ray
		  * with the liquids geometry */
		Scalar distanceToLiquid(const dimensions_t &cellIndex, const dimensions_t &nextcellIndex) const;

		void extrapolateVelocities();
		#pragma endregion InternalFunctionalities


		#pragma region 
		Scalar m_liquidDensityCoeff;
		Scalar m_airDensityCoeff;
		Scalar m_surfaceTensionCoeff;
		LiquidRepresentation<Vector2, Array2D> *m_pLiquidRepresentation;

		//Cell types reference to Liquid representation
		Array2D<cellType_t> &m_cellTypes;
		#pragma endregion
	};

}

#endif
