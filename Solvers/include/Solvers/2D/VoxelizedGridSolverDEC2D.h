#ifndef __CHIMERA_VOXELIZED_SOLVER_DEC_2D__
#define __CHIMERA_VOXELIZED_SOLVER_DEC_2D__
#pragma once

#include "Solvers/2D/VoxelizedGridSolver2D.h"

namespace Chimera {
	using namespace Core;
	using namespace Advection;
	using namespace Solvers;
	using namespace Particles;
	using namespace Solids;
	using namespace Meshes;
	namespace Solvers {
		template <typename SolverType>
		class VoxelizedGridSolverDEC2D : public VoxelizedGridSolver2D<SolverType> {
		protected: 
			using VectorType = typename SolverType::VectorType;
			using EmbeddedMeshClass = typename SolverType::EmbeddedMeshClass;
			using GridMeshType = typename SolverType::GridMeshType;
			#if defined _WIN32
				using ParentClass = typename VoxelizedGridSolver2D<SolverType>;
			#else
				using ParentClass = class VoxelizedGridSolver2D<SolverType>;
			#endif
		public:
			#pragma region Constructors
			//Default constructor for derived classes
			VoxelizedGridSolverDEC2D(shared_ptr<GridMesh<GridMeshType>> pSimulationGrid, 
														const map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>>>& levelSets,
														shared_ptr<AdvectionBase> pAdvectionSolver, 
														shared_ptr<PoissonSolver> pPoissonSolver);
			#pragma endregion

			#pragma region DECOperators
			Eigen::SparseMatrix<Scalar> getDualDelta0Operator();
			Eigen::SparseMatrix<Scalar> getPrimalDelta1Operator();
			Eigen::SparseMatrix<Scalar> getPrimalHodge0Operator();
			Eigen::SparseMatrix<Scalar> getDualHodge1Operator();
			Eigen::SparseMatrix<Scalar> getPrimalHodge1Operator();
			Eigen::SparseMatrix<Scalar> getDualHodge2Operator();
			#pragma endregion


			#pragma region SolverFunctions
			void updatePoissonMatrix() override;
			void project(Scalar dt, bool firstCall = true) override;
			void updateDivergents() override;
			void solvePressure(Scalar dt) override;
			void applyPressureGradients(Scalar dt) override;
			void applyExtraBoundaryConditions();
			#pragma endregion

			#pragma region UtilityFunctions
			void processPrimalGrid();
			void processDualGrid();
			int paddingEdges(vector<shared_ptr<Edge<VectorType>>> pEdges) {
				int edgesIdPadding = INT_MAX;

				for (auto pEdge : pEdges) {
					if (pEdge->getID() < edgesIdPadding)
						edgesIdPadding = pEdge->getID();
				}

				return edgesIdPadding;
			}

			int paddingVertices(vector<shared_ptr<Vertex<VectorType>>> pVertices) {
				int verticesIdPadding = INT_MAX;

				for (auto pVertex : pVertices) {
					if (pVertex->getID() < verticesIdPadding)
						verticesIdPadding = pVertex->getID();
				}

				return verticesIdPadding;
			}

			FORCE_INLINE shared_ptr<DualMesh<GridMeshType>> getSimulationDualMesh() const override {
				return m_pDualMesh;
			}
			#pragma endregion

		private:
			#pragma region ClassMembers
			//Primal and Dual grids
			shared_ptr<QuadGridMesh<GridMeshType>> m_pQuadGridMesh;
			shared_ptr<DualMesh<GridMeshType>> m_pDualMesh;

			//Information about the primal grid
			int m_nonSolidCells;

			//Information about the dual grid
			int m_dualPaddingVertices;
			int m_dualPaddingEdges;

			//Boundary Conditions
			boundaryCondition_t m_southBoundary;
			boundaryCondition_t m_northBoundary;
			boundaryCondition_t m_eastBoundary;
			boundaryCondition_t m_westBoundary;
			Scalar m_inflowVelocity;

			//DEC Operators
			Eigen::SparseMatrix<Scalar> m_dualDelta0Operator;
			Eigen::SparseMatrix<Scalar> m_primalDelta1Operator;
			Eigen::SparseMatrix<Scalar> m_primalHodge0Operator;
			Eigen::SparseMatrix<Scalar> m_dualHodge1Operator;
			Eigen::SparseMatrix<Scalar> m_primalHodge1Operator;
			Eigen::SparseMatrix<Scalar> m_dualHodge2Operator;
			#pragma endregion
		};
	}

}

#endif //__CHIMERA_VOXELIZED_SOLVER_DEC_2D__
