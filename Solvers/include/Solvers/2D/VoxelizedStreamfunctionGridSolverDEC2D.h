//  Copyright (c) 2013, Vinicius Costa zevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_VOXELIZED_STREAMFUNCTION_DEC_SOLVER_2D__
#define __CHIMERA_VOXELIZED_STREAMFUNCTION_DEC_SOLVER_2D__
#pragma once

#include "ChimeraCore.h"
#include "ChimeraAdvection.h"
#include "ChimeraParticles.h"
#include "ChimeraInterpolation.h"
#include "ChimeraLevelSets.h"
#include "ChimeraSolids.h"
#include "ChimeraMesh.h"

#include "Solvers/2D/VoxelizedGridSolver2D.h"

namespace Chimera {

	using namespace Core;
	using namespace Advection;
	using namespace Solvers;
	using namespace Particles;
	using namespace Solids;
	using namespace Meshes;

	//To be modified!
	/** Implementation of the classic Navier-Stokes solver, for unsteady incompressible flows.
			Initializes the grid mesh with staggered Cartesian velocities stored at grid edges centers,
			and pressures stored at regular grid cells centroids.
			This class is responsible for transferring solid objects representations to a voxelized grid,
			representing the object without fractional boundaries. Its main responsibilities are:
				- update level set representations and solid cells
				- update poisson matrix to solid objects
				- voxelized velocities boundary conditions
				- transfer fluid velocities forces to the immersed bodies
	*/

	namespace Solvers {

		template <typename SolverType>
		class VoxelizedStreamfunctionGridSolverDEC2D : public VoxelizedGridSolver2D<SolverType> {
		protected:
			using VectorType = typename SolverType::VectorType;
			using EmbeddedMeshType = typename SolverType::EmbeddedMeshType;
			using EmbeddedMeshClass = typename SolverType::EmbeddedMeshClass;
			using GridMeshType = typename SolverType::GridMeshType;
			#if defined _WIN32
				using ParentClass = typename VoxelizedGridSolver2D<SolverType>;
			#else
				using ParentClass = class VoxelizedGridSolver2D<SolverType>;
			#endif


		public:
			#pragma region Constructors
			//Default constructor for derived classes
			VoxelizedStreamfunctionGridSolverDEC2D(shared_ptr<GridMesh<GridMeshType>> pSimulationGrid, 
																						 const map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>>>& levelSets,
																						 shared_ptr<AdvectionBase> pAdvectionSolver, 
																						 shared_ptr<PoissonSolver> pPoissonSolver);
			#pragma endregion

			#pragma region InitializationFunctions
			virtual void initializeGridVariables() override {
				//Add only extras that we need
				ParentClass::m_pQuadGridMesh->template addVerticesAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, GridMeshType>>("streamfunction", 0.0f);
			}
			#pragma endregion

			#pragma region DECOperators
			Eigen::SparseMatrix<Scalar> getPrimalDelta0Operator();
			Eigen::SparseMatrix<Scalar> getDualDelta1Operator();
			Eigen::SparseMatrix<Scalar> getDualDelta1Operator2();
			Eigen::SparseMatrix<Scalar> getPrimalHodge0Operator();
			Eigen::SparseMatrix<Scalar> getPrimalHodge1Operator();
			Eigen::SparseMatrix<Scalar> getDualHodge1Operator();
			Eigen::SparseMatrix<Scalar> getDualHodge2Operator();
			Eigen::SparseMatrix<Scalar> getDualHodge2Operator2();
			Eigen::SparseMatrix<Scalar> getDualHodge2Operator3();
			Eigen::SparseMatrix<Scalar> getViscosityMatrix(bool boundaryViscosity);
			#pragma endregion

			#pragma region SolverFunctions
			void updatePoissonMatrix() override;
			void update(Scalar dt) override;
			void project(Scalar dt, bool firstCall = true) override;
			void computeVorticity(Scalar dt, bool firstCall = true);
			void solveStreamfunction(Scalar dt);
			void applyStreamfunction(Scalar dt);
			void auxVorticityBC();
			void vorticityBC();
			void applyStreamfunctionBC(shared_ptr<Vertex<VectorType>> pVertex, Scalar& previousStreamfunction);
			void streamfunctionBC();
			#pragma endregion

			#pragma region UtilityFunctions
			void processPrimalGrid();
			void processDualGrid();
			int paddingEdges(vector<shared_ptr<Edge<VectorType>>> pEdges) {
				int edgesIdPadding = INT_MAX;

				#pragma omp parallel for
				for (auto pEdge : pEdges) {
					if (pEdge->getID() < edgesIdPadding)
						edgesIdPadding = pEdge->getID();
				}

				return edgesIdPadding;
			}

			int paddingVertices(vector<shared_ptr<Vertex<VectorType>>> pVertices) {
				int verticesIdPadding = INT_MAX;

				#pragma omp parallel for
				for (auto pVertex : pVertices) {
					if (pVertex->getID() < verticesIdPadding)
						verticesIdPadding = pVertex->getID();
				}

				return verticesIdPadding;
			}

			int paddingCells(vector<shared_ptr<Cell<VectorType>>> pCells) {
				int cellsIdPadding = INT_MAX;

				#pragma omp parallel for
				for (auto pCell : pCells) {
					if (pCell->getID() < cellsIdPadding)
						cellsIdPadding = pCell->getID();
				}

				return cellsIdPadding;
			}

			Scalar measureAuxEnergy(shared_ptr<QuadGridMesh<GridMeshType>> pQuadGridMesh) {
				auto pEdges = pQuadGridMesh->getEdges();
				Scalar energy = 0.f;
				for (auto pEdge : pEdges) {
					//if (!(pEdge->getVertex1()->isInternalBorder() && pEdge->getVertex2()->isInternalBorder()) && !pEdge->getVertex1()->isSolid() && !pEdge->getVertex2()->isSolid()) {
						auto auxVelocity = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("auxVelocity");
						energy += auxVelocity * auxVelocity;
					//}
				}
				return energy;
			}
			
			Scalar measureEnergy(shared_ptr<QuadGridMesh<GridMeshType>> pQuadGridMesh) {
				auto pEdges = pQuadGridMesh->getEdges();
				Scalar energy = 0.f;
				for (auto pEdge : pEdges) {
					auto velocity = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity");
					energy += velocity * velocity;
				}
				return energy;
			}
			#pragma endregion

			FORCE_INLINE shared_ptr<DualMesh<GridMeshType>> getSimulationDualMesh() const override {
				return m_pDualMesh;
			}

		protected:
			using VelocityInterpolant = Interpolant <VectorType, GridMeshType>;
			using ScalarInterpolant = Interpolant <Scalar, GridMeshType>;

			#pragma region ClassMembers
			//Primal and  Dual grids
			shared_ptr<DualMesh<GridMeshType>> m_pDualMesh;
			shared_ptr<QuadGridMesh<GridMeshType>> m_pQuadGridMesh;

			//Information about the primal grid
			int m_primalInternalVertices;
			int m_primalBorderVertices;
			int* m_primalVertexIDToNewVertexID;

			//Information about the dual grid
			int m_dualPaddingCells;
			int m_dualPaddingEdges;
			int m_dualPaddingVertices;

			//Boundary Conditions
			boundaryCondition_t m_southBoundary;
			boundaryCondition_t m_northBoundary;
			boundaryCondition_t m_eastBoundary;
			boundaryCondition_t m_westBoundary;
			Scalar m_inflowVelocity;
			boundaryCondition_t m_internalBoundary;
			bool m_TaylorVortices;
			Scalar m_viscosity;

			//DEC operators
			Eigen::SparseMatrix<Scalar> m_primalDelta0Operator;
			Eigen::SparseMatrix<Scalar> m_dualDelta1Operator;
			Eigen::SparseMatrix<Scalar> m_dualDelta1Operator2;
			Eigen::SparseMatrix<Scalar> m_primalHodge0Operator;
			Eigen::SparseMatrix<Scalar> m_primalHodge1Operator;
			Eigen::SparseMatrix<Scalar> m_dualHodge1Operator;
			Eigen::SparseMatrix<Scalar> m_dualHodge2Operator;
			Eigen::SparseMatrix<Scalar> m_dualHodge2Operator2;
			Eigen::SparseMatrix<Scalar> m_dualHodge2Operator3;
			Eigen::SparseMatrix<Scalar> m_difference;
			Eigen::SparseMatrix<Scalar> m_viscosityMatrix;
			#pragma endregion
		};
	}
}

#endif
