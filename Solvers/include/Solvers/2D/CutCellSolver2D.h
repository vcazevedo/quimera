//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_VARIATIONAL_SOLVER_2D__
#define __CHIMERA_VARIATIONAL_SOLVER_2D__
#pragma once

#include "ChimeraCore.h"
#include "ChimeraAdvection.h"
#include "ChimeraParticles.h"
#include "ChimeraInterpolation.h"
#include "ChimeraGrids.h"
#include "ChimeraMesh.h"
#include "ChimeraSolids.h"
#include "Solvers/2D/VoxelizedGridSolver2D.h"

namespace Chimera {

	using namespace Core;
	using namespace Advection;
	using namespace Meshes;
	using namespace Solids;
	using namespace Grids;

	/** Implementation of the classic Navier-Stokes solver, for unsteady incompressible flows It uses a CutCell
	  * formulation for rigid body objects and a new approach for treatment of thin objects.
			Following configurations:
			Dependent variables: Pressure, Cartesian velocity components;
			Variable arrangement: Nodal (only one supported);
			Pressure Coupling: Fractional Step; */

	namespace Solvers {

		class CutCellSolverType2D {
		public:
			using VectorType = Core::Vector2;
			using EmbeddedMeshType = Meshes::IntersectedLineMesh<VectorType>;
			using VoxelType = Meshes::QuadCell<VectorType>;
			using GridMeshType = Grids::QuadGridType;
		};

		class CutCellSolverTypeDouble2D {
		public:
			using VectorType = Core::Vector2D;
			using EmbeddedMeshType = Meshes::IntersectedLineMesh<VectorType>;
			using VoxelType = Meshes::QuadCell<VectorType>;
			using GridMeshType = Grids::QuadGridTypeDouble;
		};

		template <typename SolverType>
			class CutCellSolver2D : public VoxelizedGridSolver2D<SolverType> {
			protected:
				using VectorType = typename SolverType::VectorType;
				using EmbeddedMeshType = typename SolverType::EmbeddedMeshType;
				using GridMeshType = typename SolverType::GridMeshType;
				#if defined _WIN32
					using ParentClass = typename FlowSolver<SolverType>;
				#else
					using ParentClass = class FlowSolver<SolverType>;
				#endif

			public:

			#pragma region Constructors
			CutCellSolver2D(shared_ptr<GridMesh<GridMeshType>> pSimulationMesh,
												const map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshType, GridMeshType>>>& levelSets,
												shared_ptr<AdvectionBase> pAdvectionSolver, shared_ptr<PoissonSolver> pPoissonSolver, 
												const vector<shared_ptr<IntersectedLineMesh<VectorType>>>& lineMeshes = vector<shared_ptr<IntersectedLineMesh<VectorType>>>());
			#pragma endregion


			protected:
			using VelocityInterpolant = Interpolant <VectorType, GridMeshType>;
			using ScalarInterpolant = Interpolant <Scalar, GridMeshType>;
			
			#pragma region ClassMembers
			shared_ptr<VelocityInterpolant> m_pVelocityInterpolant;
			shared_ptr<VelocityInterpolant> m_pAuxVelocityInterpolant;
			#pragma endregion
			#pragma region InitializationFunctions
			/** Initializes the Poisson matrix accordingly with Finite Differences formulation */
			virtual void updatePoissonMatrix() override;

			/** Initializes several interpolants used for the simulation and debugging */
			virtual void initializeInterpolants() override;
			#pragma endregion

			#pragma region BoundaryConditions
			virtual void updateImmersedObjects(Scalar dt);
			/** Enforces solid walls boundary conditions for both velocities and pressure samples */
			virtual void enforceSolidWallsConditions(const VectorType &solidVelocity) override;
			#pragma endregion


			#pragma region PressureProjection
			virtual void updateDivergents() override;
			virtual void solvePressure() override;
			virtual void applyPressureGradients(Scalar dt) override;
			#pragma endregion

			#pragma region AuxiliaryFunctions
			virtual void initializeGridVariables() override {
				shared_ptr<QuadGridMesh<GridMeshType>> pQuadGridMesh = QuadGridMesh<GridMeshType>::get(FlowSolver<SolverType>::m_pSimulationGrid);
				if (!pQuadGridMesh) {
					throw std::logic_error("CutCellSolver2D::verifyGridVariables() not a valid quadGridMesh!");
				}
				/** Grid arrangement is node-based for advection, and staggered for projection, so we add
						both edge (from VoxelizedSolver init) and nodal variables (here)*/
				pQuadGridMesh->template addEdgesAttribute<VectorType, BilinearStaggeredInterpolant2D<VectorType, GridMeshType>>("velocity", 0.0f);
				pQuadGridMesh->template addEdgesAttribute<VectorType, BilinearStaggeredInterpolant2D<VectorType, GridMeshType>>("auxVelocity", 0.0f);

				m_pVelocityInterpolant = pQuadGridMesh->template addVerticesAttribute<VectorType, BilinearNodalInterpolant2D<VectorType, GridMeshType>>("velocity", VectorType());
				m_pAuxVelocityInterpolant = pQuadGridMesh->template addVerticesAttribute<VectorType, BilinearNodalInterpolant2D<VectorType, GridMeshType>>("auxVelocity", VectorType());

				m_pVelocityInterpolant->setSiblingInterpolant(m_pAuxVelocityInterpolant);
				m_pAuxVelocityInterpolant->setSiblingInterpolant(m_pVelocityInterpolant);

			}
			#pragma endregion
		};
	}


}

#endif
