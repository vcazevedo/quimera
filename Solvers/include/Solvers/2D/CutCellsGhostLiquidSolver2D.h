#pragma once
//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_CUTCELLS_GHOST_LIQUIDS_SOLVER_2D__
#define __CHIMERA_CUTCELLS_GHOST_LIQUIDS_SOLVER_2D__
#pragma once

/************************************************************************/
/* Chimera Core															*/
/************************************************************************/
#include "ChimeraCore.h"
#include "ChimeraAdvection.h"

/************************************************************************/
/* Chimera Data			                                                */
/************************************************************************/
#include "ChimeraLevelSets.h"
#include "Solvers/2D/GhostLiquidSolver2D.h"

namespace Chimera {

	using namespace Core;
	using namespace LevelSets;

	namespace Solvers {

	// Implementation of the ghost-pressures method for liquids with cut-cells
	class CutCellsGhostLiquidSolver2D : public GhostLiquidSolver {
	public:

		#pragma region ConstructorsDestructors
		/** Standard constructor. Receives params that will configure solver's several characteristics, the underlying
		  * structured grid *pGrid, boundary conditions and the liquids original mesh representation. */
		CutCellsGhostLiquidSolver2D(const params_t &params, StructuredGrid<Vector2, Array2D> *pGrid,
			const vector<BoundaryCondition<Vector2, Array2D> *> &boundaryConditions,
			LiquidRepresentation<Vector2, Array2D> *pLiquidRepresentation);
		#pragma endregion ConstructorsDestructors

		#pragma region UpdateFunctions
		// Updates Cut-Cell Ghost Liquid Solver
		void update(Scalar dt) override;
		#pragma endregion UpdateFunctions

		#pragma region PressureProjection
		void advectionFixForRightBoundary();
		void enforceBoundaryConditions() override;
		Scalar calculateFluxDivergent(int i, int j) override;
		#pragma endregion PressureProjection
		
		#pragma region SimulationFunctions
		#pragma endregion SimulationFunctions		
		
		#pragma region AccessFunctions
		CutCells2D<Vector2> * getCutCells() {
			return m_pCutCells;
		}
		const Array2D<Vector2> & getNodalVelocityField() const {
			return m_nodalBasedVelocities;
		}
		#pragma endregion AccessFunctions

	protected:

		#pragma region Ghost Fluid Method Helper Methods
		Scalar phiHelper(Vector2 pos) const;
		Scalar thetaHelper(uint cutCellIndex, dimensions_t otherCell) const;
		Scalar ghostFluidHelper(int i1, int j1, int i2, int j2) const override;
		#pragma endregion

		#pragma region InternalFunctionalities
		// Helper function: gets the 2-D offset given a cell location, e.g the offset for a left location is (-1, 0)
		static dimensions_t getEdgeOffset(halfEdgeLocation_t edge) {
			switch (edge) {
				case topHalfEdge:
					return dimensions_t(0, 1);
					break;
				case leftHalfEdge:
					return dimensions_t(-1, 0);
					break;
				case rightHalfEdge:
					return dimensions_t(1, 0);
					break;
				case bottomHalfEdge:
					return dimensions_t(0, -1);
					break;
				default:
					return dimensions_t(0, 0);
					break;
			}
		}
		#pragma endregion

		#pragma region Initialization Methods
		// Initializes Advection
		AdvectionBase * initializeAdvectionClass() override;
		// Initializes Cut-Cell Structures
		CutCells2D<Vector2>* initializeCutCells();
		// Reinitializes Cut-Cell Structures after the update
		void reinitializeCutCells();
		// Initializes Nodal velocity Interpolants
		void initializeInterpolants() override;
		#pragma endregion

		#pragma region Cut-Cell Class Members
		CutCells2D<Vector2> *m_pCutCells;
		vector<Scalar> m_cutCellsPressures;
		vector<Scalar> m_cutCellsDivergents;
		// Velocity data for cut-cells
		CutCellsVelocities2D<Vector2> *m_pCutCellsVelocities2D;
		// Storing intermediary velocity data for cut-cells
		CutCellsVelocities2D<Vector2> *m_pAuxCutCellsVelocities2D;
		// Nodal based velocities
		Array2D<Vector2> m_nodalBasedVelocities;
		Array2D<Vector2> m_auxNodalBasedVelocities;
		#pragma endregion

		#pragma region DebuggingFunctionality
		void printPoissonMatrix(const string &fileName = "values.dat", const string &folderPath = R"(T:\private\desktop-dinfk-xp\poissonMatrix\)") const {
			ofstream pMatrixFile(folderPath + fileName);
			m_pPoissonMatrix->cuspPrint(pMatrixFile);
			pMatrixFile.close();
		}
		#pragma endregion DebuggingFunctionality
	};
	}
}

#endif