#include "Solvers/2D/VoxelizedGridSolverDEC2D.h"

namespace Chimera {
	namespace Solvers {
		#pragma region Constructors
		//Default constructor for derived classes
		template<typename SolverType>
		VoxelizedGridSolverDEC2D<SolverType>::VoxelizedGridSolverDEC2D(shared_ptr<GridMesh<GridMeshType>> pSimulationGrid,
																																	 const map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>>>& levelSets,
																																	 shared_ptr<AdvectionBase> pAdvectionSolver,
																																	 shared_ptr<PoissonSolver> pPoissonSolver) 
		: ParentClass(pSimulationGrid, levelSets, pAdvectionSolver, pPoissonSolver)
		{
			//Primal grid
			m_pQuadGridMesh = ParentClass::m_pQuadGridMesh;

			//Boundary Conditions --> should be read from the xml file!
			//For the velocity-pressure formulation, the BCs are stored on the edges!
			m_southBoundary = inflow;
			m_northBoundary = outflow;
			m_eastBoundary = freeSlip;
			m_westBoundary = freeSlip;
			m_inflowVelocity = 0.1;
			processPrimalGrid(); //Store the BC on the edges of the primal grid --> needed for the construction of the Dual

			//Dual grid
			m_pDualMesh = make_shared<DualMesh<GridMeshType>>(m_pQuadGridMesh, voxelizedDECSolver); //Must be constructed else also the primal grid doesn't have the dual elements!
			processDualGrid();

			//DEC Operators
			m_dualDelta0Operator = getDualDelta0Operator();
			m_primalDelta1Operator = getPrimalDelta1Operator();
			m_primalHodge0Operator = getPrimalHodge0Operator();
			m_dualHodge1Operator = getDualHodge1Operator();
			m_primalHodge1Operator = getPrimalHodge1Operator();
			m_dualHodge2Operator = getDualHodge2Operator();

			//Solver initialization
			updatePoissonMatrix();
		}
		#pragma endregion

		#pragma region DECOperators
		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedGridSolverDEC2D<SolverType>::getDualDelta0Operator() {
			auto pVertices = m_pDualMesh->getVertices();
			auto pEdges = m_pDualMesh->getElements();

			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(2 * pEdges.size());

			//Construction of the matrix
			for (auto pEdge : pEdges) {
				shared_ptr<Vertex<VectorType>> pVertex1, pVertex2;
				if (pEdge->getType() == yAlignedEdge) {
					pVertex1 = pEdge->getVertex1();
					pVertex2 = pEdge->getVertex2();
				}
				else if (pEdge->getType() == xAlignedEdge) {
					pVertex1 = pEdge->getVertex2();
					pVertex2 = pEdge->getVertex1();
				}

				int v1Id = pVertex1->getID() - m_dualPaddingVertices;
				int v2Id = pVertex2->getID() - m_dualPaddingVertices;
				int eId = pEdge->getID() - m_dualPaddingEdges;

				if (!pVertex1->isBorder())
					triplets.push_back(Eigen::Triplet<Scalar>(eId, v1Id, -1.));
				if (!pVertex2->isBorder())
					triplets.push_back(Eigen::Triplet<Scalar>(eId, v2Id, 1.));
			}

			Eigen::SparseMatrix<Scalar> delta0Operator(pEdges.size(), pVertices.size());
			delta0Operator.setFromTriplets(triplets.begin(), triplets.end());
			return delta0Operator;
		}

		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedGridSolverDEC2D<SolverType>::getPrimalDelta1Operator() {
			std::vector<Eigen::Triplet<Scalar>> triplets;
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();
			triplets.reserve(4 * gridDimensions.x * gridDimensions.y);

			#pragma omp parallel for
			for (int i = 0; i < gridDimensions.x; ++i) {
				#pragma omp parallel for private(j)
				for (int j = 0; j < gridDimensions.y; ++j) {
					auto pCell = m_pQuadGridMesh->getCells(i, j).front(); //Works only for voxelized approach
					if (!pCell->isSolid()) {
						auto pHalfCell = pCell->getHalfCells().first;
						auto pHalfEdges = pHalfCell->getHalfEdges();
						for (auto pHalfEdge : pHalfEdges) {
							auto pVertex1 = pHalfEdge->getVertices().first;
							auto pVertex2 = pHalfEdge->getVertices().second;

							if (pHalfEdge->getEdge()->getType() == xAlignedEdge) {
								if (pVertex1->getPosition().x < pVertex2->getPosition().x)
									triplets.push_back(Eigen::Triplet<Scalar>(pCell->getID(), pHalfEdge->getEdge()->getID(), -1.));
								else
									triplets.push_back(Eigen::Triplet<Scalar>(pCell->getID(), pHalfEdge->getEdge()->getID(), 1.));
							}
							else if (pHalfEdge->getEdge()->getType() == yAlignedEdge) {
								if (pVertex1->getPosition().y < pVertex2->getPosition().y)
									triplets.push_back(Eigen::Triplet<Scalar>(pCell->getID(), pHalfEdge->getEdge()->getID(), 1.));
								else
									triplets.push_back(Eigen::Triplet<Scalar>(pCell->getID(), pHalfEdge->getEdge()->getID(), -1.));
							}
						}
					}
				}
			}

			Eigen::SparseMatrix<Scalar> delta1Operator(gridDimensions.x * gridDimensions.y, m_pQuadGridMesh->getEdges().size());
			delta1Operator.setFromTriplets(triplets.begin(), triplets.end());
			return delta1Operator;
		}

		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedGridSolverDEC2D<SolverType>::getPrimalHodge0Operator() {
			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();
			auto gridSpacing = m_pQuadGridMesh->getGridSpacing();
			triplets.reserve(gridDimensions.x * gridDimensions.y);

			#pragma omp parallel for
			for (int i = 0; i < gridDimensions.x; ++i) {
				#pragma omp parallel for private(j)
				for (int j = 0; j < gridDimensions.y; ++j) {
					auto pCell = m_pQuadGridMesh->getCells(i, j).front(); //Works only for voxelized approach
					triplets.push_back(Eigen::Triplet<Scalar>(pCell->getID(), pCell->getID(), gridSpacing * gridSpacing)); //works only for voxelized approach
				}
			}

			Eigen::SparseMatrix<Scalar> Hodge0Operator(gridDimensions.x * gridDimensions.y, gridDimensions.x * gridDimensions.y);
			Hodge0Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge0Operator;
		}

		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedGridSolverDEC2D<SolverType>::getDualHodge1Operator() {
			auto pEdges = m_pDualMesh->getElements();

			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pEdges.size());

			//Construction of the matrix
			for (auto pEdge : pEdges) {
				int eId = pEdge->getID() - m_dualPaddingEdges;
				double entry = pEdge->getDualElement2D()->getLength() / pEdge->getLength();
				triplets.push_back(Eigen::Triplet<Scalar>(eId, eId, entry));
			}

			Eigen::SparseMatrix<Scalar> Hodge1Operator(pEdges.size(), pEdges.size());
			Hodge1Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge1Operator;
		}
		
		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedGridSolverDEC2D<SolverType>::getPrimalHodge1Operator() {
			auto pEdges = m_pQuadGridMesh->getEdges();

			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pEdges.size());

			//Construction of the matrix
			for (auto pEdge : pEdges) {
				//Original ID
				int eId = pEdge->getID();

				if (!(pEdge->getDualElement2D() == nullptr)) {
					double entry = pEdge->getDualElement2D()->getLength() / pEdge->getLength(); //double check
					triplets.push_back(Eigen::Triplet<Scalar>(eId, eId, entry));
				}
			}

			Eigen::SparseMatrix<Scalar> Hodge1Operator(pEdges.size(), pEdges.size());
			Hodge1Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge1Operator;
		}

		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedGridSolverDEC2D<SolverType>::getDualHodge2Operator() {
			auto pVertices = m_pDualMesh->getVertices();
			auto gridSpacing = m_pQuadGridMesh->getGridSpacing();

			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pVertices.size());

			//Construction of the matrix
			for (auto pVertex : pVertices) {
				int vId = pVertex->getID() - m_dualPaddingVertices;
				double entry = 1. / (gridSpacing * gridSpacing); //This doesn't work for cut-cells!
				triplets.push_back(Eigen::Triplet<Scalar>(vId, vId, entry));
			}

			Eigen::SparseMatrix<Scalar> Hodge2Operator(pVertices.size(), pVertices.size());
			Hodge2Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge2Operator;
		}
		#pragma endregion

		#pragma region SolverFunctions
		template<typename SolverType>
		void VoxelizedGridSolverDEC2D<SolverType>::updatePoissonMatrix() {
			auto pressureMatrix = m_dualHodge2Operator.cwiseInverse() * m_dualDelta0Operator.transpose() * m_dualHodge1Operator * m_dualDelta0Operator;
			
			int internalDualVertices = 0;
			for (auto pVertex : m_pDualMesh->getVertices()) {
				if (!pVertex->isBorder())
					++internalDualVertices;
			}

			auto pressureMatrixReduced = pressureMatrix.block(0, 0, internalDualVertices, internalDualVertices);
			
			typename PoissonSolver::params_t params;
			ParentClass::ParentClass::m_pPoissonSolver = make_shared<Poisson::PoissonSolver>(params, make_shared<Eigen::SparseMatrix<Scalar>>(pressureMatrixReduced));
		}

		template <typename SolverType>
		void VoxelizedGridSolverDEC2D<SolverType>::project(Scalar dt, bool firstCall) {
			ParentClass::computeAuxVorticity();
			applyExtraBoundaryConditions(); //To correct the auxVelocities after advection
			updateDivergents();
			solvePressure(dt);
			applyPressureGradients(dt);
			ParentClass::computeAuxVorticity();
		}

		template <typename SolverType>
		void VoxelizedGridSolverDEC2D<SolverType>::updateDivergents() {
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();
			auto edges = m_pQuadGridMesh->getEdges();
			Eigen::VectorXf auxVelocities(edges.size());
			for (auto edge : edges) {
				auxVelocities(edge->getID()) = OwnCustomAttribute<Scalar>::get(edge)->getAttribute("auxVelocity");
			}

			auto divergence = m_primalHodge0Operator * m_primalDelta1Operator * auxVelocities;

			#pragma omp parallel for
			for (int i = 0; i < gridDimensions.x; ++i) {
				#pragma omp parallel for private(j)
				for (int j = 0; j < gridDimensions.y; ++j) {
					auto cell = m_pQuadGridMesh->getCells(i, j).front();
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getQuadCell(i, j)->getCells().front())->setAttribute("divergence", divergence(cell->getID()));
				}
			}
		}

		template<typename SolverType>
		void VoxelizedGridSolverDEC2D<SolverType>::solvePressure(Scalar dt) {
			uint i, j;
			dimensions_t gridDimensions = m_pQuadGridMesh->getGridDimensions();
			auto gridSpacing = m_pQuadGridMesh->getGridSpacing();

			shared_ptr<Eigen::VectorXf> divergence = make_shared<Eigen::VectorXf>(m_nonSolidCells);
			shared_ptr<Eigen::VectorXf> pressure = make_shared<Eigen::VectorXf>(m_nonSolidCells);

			/** Updating divergents from cells to array */
			#pragma omp parallel for
			for (i = 0; i < gridDimensions.x; i++) {
				#pragma omp parallel for private(j)
				for (j = 0; j < gridDimensions.y; j++) {
					auto cell = m_pQuadGridMesh->getCells(i, j).front(); //works only for voxelized approach
					if (!cell->isSolid()) {
						int vId = cell->getDualElement2D()->getID() - m_dualPaddingVertices;
						(*divergence)(vId) = -1 / (dt)*OwnCustomAttribute<Scalar>::get(cell)->getAttribute("divergence");
					}
				}
			}

			/** Solve the system */
			ParentClass::ParentClass::m_pPoissonSolver->solve(divergence, pressure);

			/** Setting pressures from array to cells*/
			#pragma omp parallel for
			for (i = 0; i < gridDimensions.x; i++) {
				#pragma omp parallel for private(j)
				for (j = 0; j < gridDimensions.y; j++) {
					auto cell = ParentClass::m_pQuadGridMesh->getCells(i, j).front(); //works only for voxelized approach
					if (!cell->isSolid()) {
						int vId = cell->getDualElement2D()->getID() - m_dualPaddingVertices;
						OwnCustomAttribute<Scalar>::get(cell)->setAttribute("pressure", (*pressure)(vId));
					}
				}
			}
		}

		template <typename SolverType>
		void VoxelizedGridSolverDEC2D<SolverType>::applyPressureGradients(Scalar dt) {
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();
			Eigen::VectorXf pressures(gridDimensions.x * gridDimensions.y);
			#pragma omp parallel for
			for (int i = 0; i < gridDimensions.x; i++) {
				#pragma omp parallel for private(j)
				for (int j = 0; j < gridDimensions.y; j++) {
					auto cell = m_pQuadGridMesh->getCells(i, j).front(); //works only for voxelized approach
					pressures(cell->getID()) = OwnCustomAttribute<Scalar>::get(cell)->getAttribute("pressure");
				}
			}

			auto pressureGradient = m_primalHodge1Operator.cwiseInverse() * m_primalDelta1Operator.transpose() * pressures;
			auto pEdges = m_pQuadGridMesh->getEdges();
			for (auto edge : pEdges) {
				auto auxVelocity = OwnCustomAttribute<Scalar>::get(edge)->getAttribute("auxVelocity");
				OwnCustomAttribute<Scalar>::get(edge)->setAttribute("velocity", pressureGradient(edge->getID()) * dt + auxVelocity); //pressureGradient will be 0 for edges that have no-slip/free-slip/inflow BC!
			}
		}

		template <typename SolverType>
		void VoxelizedGridSolverDEC2D<SolverType>::applyExtraBoundaryConditions() {
			//Velocity inside the solids is 0
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();
			#pragma omp parallel for
			for (int i = 0; i < gridDimensions.x; i++) {
				#pragma omp parallel for private(j)
				for (int j = 0; j < gridDimensions.y; j++) {
					auto cell = m_pQuadGridMesh->getCells(i, j).front(); //works only for voxelized approach
					if (cell->isSolid()) {
						auto edges = cell->getEdges();
						for (auto edge : edges) {
							OwnCustomAttribute<Scalar>::get(edge)->setAttribute("auxVelocity", 0);
						}
					}
				}
			}

			//Inflow velocity
			auto pEdges = m_pQuadGridMesh->getEdges();
			for (auto pEdge : pEdges) {
				if(pEdge->getBoundaryCondition() == inflow)
					OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute("auxVelocity", m_inflowVelocity);
			}
		}
		#pragma endregion

		#pragma region UtilityFunctions
		template <typename SolverType>
		void VoxelizedGridSolverDEC2D<SolverType>::processPrimalGrid() {
			//For velocity-pressure formulation, the BC information is stored on edges --> the dual grid is constructed based on edges!
			vector<shared_ptr<Edge<VectorType>>> pEdges = m_pQuadGridMesh->getEdges();
			for (shared_ptr<Edge<VectorType>> pEdge : pEdges) {
				if (pEdge->isBorder()) {
					if (pEdge->getType() == xAlignedEdge) {
						if (pEdge->getCentroid().y == m_pQuadGridMesh->getBounds().first[1]) //bottom
							pEdge->setBoundaryCondition(m_southBoundary);
						else if (pEdge->getCentroid().y == m_pQuadGridMesh->getBounds().second[1]) //top
							pEdge->setBoundaryCondition(m_northBoundary);
					}
					else if (pEdge->getType() == yAlignedEdge) {
						if (pEdge->getCentroid().x == m_pQuadGridMesh->getBounds().first[0]) //left
							pEdge->setBoundaryCondition(m_westBoundary);
						else if (pEdge->getCentroid().x == m_pQuadGridMesh->getBounds().second[0]) //right
							pEdge->setBoundaryCondition(m_eastBoundary);
					}
				}
			}

			//Count the cells that are not solid
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();
			int nonSolidCells = 0;
			#pragma omp parallel for
			for (uint i = 0; i < gridDimensions.x; i++) {
			#pragma omp parallel for private(j)
				for (uint j = 0; j < gridDimensions.y; j++) {
					auto pCell = m_pQuadGridMesh->getCells(i, j).front(); //works only for voxelized approach
					if (!pCell->isSolid())
						++nonSolidCells;
				}
			}
			m_nonSolidCells = nonSolidCells;
		}

		template <typename SolverType>
		void VoxelizedGridSolverDEC2D<SolverType>::processDualGrid() {
			//Vertices
			auto pDualVertices = m_pDualMesh->getVertices();
			m_dualPaddingVertices = paddingVertices(pDualVertices);

			//Edges
			auto pDualEdges = m_pDualMesh->getElements();
			m_dualPaddingEdges = paddingEdges(pDualEdges);
		}
		#pragma endregion

		template class VoxelizedGridSolverDEC2D<FlowSolverType2D>;
	}
}
