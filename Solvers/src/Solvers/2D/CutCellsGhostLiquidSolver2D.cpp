//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#include "Solvers/2D/CutCellsGhostLiquidSolver2D.h"
#include <omp.h>

namespace Chimera {

	namespace Solvers {

		CutCellsGhostLiquidSolver2D::CutCellsGhostLiquidSolver2D(const params_t &params, StructuredGrid<Vector2, Array2D> *pGrid,
			const vector<BoundaryCondition<Vector2, Array2D> *> &boundaryConditions, LiquidRepresentation<Vector2, Array2D> *pLiquidRepresentation)
			: GhostLiquidSolver(params, pGrid, boundaryConditions, pLiquidRepresentation), m_nodalBasedVelocities(pGrid->getDimensions()), m_auxNodalBasedVelocities(pGrid->getDimensions()) {
			// initialize cut-cells
			m_pCutCells = initializeCutCells();

			m_pCutCellsVelocities2D = new CutCellsVelocities2D<Vector2>(m_pCutCells, m_params.solidBoundaryType);
			m_pAuxCutCellsVelocities2D = new CutCellsVelocities2D<Vector2>(m_pCutCells, m_params.solidBoundaryType);

			// re-initializes interpolants
			initializeInterpolants();

			// uptade advection to incorporate cut-cells
			ParticleBasedAdvection<Vector2, Array2D> *pParticleBasedAdvection = dynamic_cast<ParticleBasedAdvection<Vector2, Array2D>*>(m_pAdvection);
			//ParticlesToNodalGrid2D *pParticlesToNodal = dynamic_cast<ParticlesToNodalGrid2D *>(pParticleBasedAdvection->getParticlesToGrid());
			//pParticlesToNodal->setCutCellsVelocities(m_pCutCellsVelocities2D);
			//pParticleBasedAdvection->getParticleBasedIntegrator()->setCutCells(m_pCutCells);
			//pParticleBasedAdvection->getParticlesSampler()->setCutCells(m_pCutCells);

			dynamic_cast<ConjugateGradient *>(m_pPoissonSolver)->reinitializePreconditioners();
		};


		/************************************************************************/
		/* Public Functions                                                     */
		/************************************************************************/
		#pragma region UpdateFunctions
		void CutCellsGhostLiquidSolver2D::update(Scalar dt) {
			ParticleBasedAdvection<Vector2, Array2D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector2, Array2D> *>(m_pAdvection);
			CubicStreamfunctionInterpolant2D<Vector2> *pCInterpolant = dynamic_cast<CubicStreamfunctionInterpolant2D<Vector2> *>(pParticleBasedAdv->getParticleBasedIntegrator()->getInterpolant());
			BilinearStreamfunctionInterpolant2D *pSInterpolant = dynamic_cast<BilinearStreamfunctionInterpolant2D*>(pParticleBasedAdv->getParticleBasedIntegrator()->getInterpolant());

			m_numIterations++;
			m_totalSimulationTimer.start();

			if (PhysicsCore<Vector2>::getInstance()->getElapsedTime() < dt) {
				applyForces(dt);
				pParticleBasedAdv->updateGridAttributes();
				enforceBoundaryConditions();
				updateDivergents(dt);
				enforceBoundaryConditions();
				solvePressure();
				enforceBoundaryConditions();
				project(dt);
				enforceBoundaryConditions();
				return;
			}

			applyForces(dt);
			enforceBoundaryConditions();

			m_advectionTimer.start();
			enforceBoundaryConditions();

			if (pCInterpolant) {
				pCInterpolant->computeStreamfunctions();
			}
			else if (pSInterpolant) {
				pSInterpolant->computeStreamfunctions();
			}

			enforceBoundaryConditions();
			extrapolateVelocities();
			pParticleBasedAdv->updatePositions(dt);
			m_pLiquidRepresentation->updateMeshes();
			reinitializeCutCells();

			updatePoissonMatrix();
			pParticleBasedAdv->updateGridAttributes();

			m_advectionTimer.stop();
			m_advectionTime = m_advectionTimer.secondsElapsed();
			enforceBoundaryConditions();

			// Solve pressure
			m_solvePressureTimer.start();
			updateDivergents(dt);
			solvePressure();

			// Ghost Fluid Velocity Update
			correctVelocityGhostFluid(dt);

			enforceBoundaryConditions();
			m_solvePressureTimer.stop();
			m_solvePressureTime = m_solvePressureTimer.secondsElapsed();

			// Project velocity
			m_projectionTimer.start();
			project(dt);
			m_projectionTimer.stop();
			m_projectionTime = m_projectionTimer.secondsElapsed();
			enforceBoundaryConditions();

			enforceBoundaryConditions();
			pParticleBasedAdv->updateParticleAttributes();

			enforceBoundaryConditions();

			updateKineticEnergy();

			m_totalSimulationTimer.stop();
			m_totalSimulationTime = m_totalSimulationTimer.secondsElapsed();
		}
		#pragma endregion

		#pragma region PressureProjection
		void CutCellsGhostLiquidSolver2D::advectionFixForRightBoundary() {
			ParticleBasedAdvection<Vector2, Array2D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector2, Array2D> *>(m_pAdvection);
			vector<Vector2> &positions = pParticleBasedAdv->getParticlesData()->getPositions();
			vector<Vector2> &velocities = pParticleBasedAdv->getParticlesData()->getVelocities();
			#pragma omp parallel for
			for (uint i = 0; i < positions.size(); i++) {
				Vector2 &pos = positions[i], &vel = velocities[i];
				Scalar relativeX = (pos.x - m_pGridData->getPoint(0, 0).x) / m_pGridData->getGridSpacing();
				int xIndex = static_cast<int>(ceil(relativeX));
				if (xIndex + 1 >= m_pGridData->getDimensions().x) {
					Scalar xFrac = 1.0f + relativeX - xIndex;
					pos.x -= xFrac * m_pGridData->getGridSpacing();
					vel.x *= -1;
				}
			}
		}
		void CutCellsGhostLiquidSolver2D::enforceBoundaryConditions() {
			GhostLiquidSolver::enforceBoundaryConditions();
			// fix for right border
			Vector2 tempVelocity;
			const dimensions_t gridDimensions = m_pGridData->getDimensions();
			GridData2D *pGridData2D = dynamic_cast<GridData2D *>(m_pGridData);
			for (int j = 1; j < gridDimensions.y - 1; j++) {
				tempVelocity = pGridData2D->getVelocity(gridDimensions.x - 3, j);
				tempVelocity.x = 0;
				pGridData2D->setVelocity(tempVelocity, gridDimensions.x - 2, j);

				tempVelocity = pGridData2D->getAuxiliaryVelocity(gridDimensions.x - 3, j);
				tempVelocity.x = 0;
				pGridData2D->setAuxiliaryVelocity(tempVelocity, gridDimensions.x - 2, j);

				pGridData2D->setPressure(pGridData2D->getPressure(gridDimensions.x - 3, j), gridDimensions.x - 2, j);
			}
		}
		Scalar CutCellsGhostLiquidSolver2D::calculateFluxDivergent(int i, int j) {
			return GhostLiquidSolver::calculateFluxDivergent(i, j);

			Scalar divergent = 0;
			const Scalar dx = m_pGridData->getGridSpacing();
			if (m_pCutCells->isCutCellAt(i, j)) {
				const dimensions_t gridCellIndex(i, j);
				std::shared_ptr<Face<Vector2>> pFace = m_pCutCells->getFace(i, j);
				for (std::shared_ptr<HalfFace<Vector2>> pCutCell : pFace->getHalfFaces()) {
					uint idx = pCutCell->getID();
					Vector2 center = pCutCell->getCentroid();
					if (phiHelper(center) <= 0) { // water cut cell
						for (std::shared_ptr<HalfEdge<Vector2>> pHalfEdge : pCutCell->getHalfEdges()) {
							std::shared_ptr<Edge<Vector2>> pEdge = pHalfEdge->getEdge();
							if (pEdge->getType() == xAlignedEdge || pEdge->getType() == yAlignedEdge) {
								const Scalar halfRelativeFract = 0.5f * pEdge->getRelativeFraction();
								const auto &vertices = pHalfEdge->getVertices();
								const Vector2 &normal1 = vertices.first->getNormal();
								const Vector2 &normal2 = vertices.second->getNormal();
								const Vector2 &auxVel1 = vertices.first->getAuxiliaryVelocity(idx);
								const Vector2 &auxVel2 = vertices.second->getAuxiliaryVelocity(idx);
								pEdge->setAuxiliaryVelocity((auxVel1 + auxVel2) * 0.5f);
								divergent += (normal1.dot(auxVel1) + normal2.dot(auxVel2)) * halfRelativeFract;
							}
						}
					}
				}
				divergent /= dx;
			}
			else {
				Scalar leftAuxVelocity = 0.0f;
				Scalar bottomAuxVelocity = 0.0f;
				Scalar rightAuxVelocity = 0.0f;
				Scalar topAuxVelocity = 0.0f;
				// left border
				if (m_pCutCells->isCutCellAt(i - 1, j)) {
					for (std::shared_ptr<Edge<Vector2>> pEdge : m_pCutCells->getEdgeVector(dimensions_t(i - 1, j), yAlignedEdge)) {
						if (pEdge->getHalfEdges().first->getLocation() == rightHalfEdge) {
							leftAuxVelocity += pEdge->getAuxiliaryVelocity().x;
						}
					}
				}
				else {
					leftAuxVelocity = m_pGridData->getAuxiliaryVelocity(i, j).x;
				}
				// right border
				if (m_pCutCells->isCutCellAt(i + 1, j)) {
					for (std::shared_ptr<Edge<Vector2>> pEdge : m_pCutCells->getEdgeVector(dimensions_t(i + 1, j), yAlignedEdge)) {
						if (pEdge->getHalfEdges().first->getLocation() == leftHalfEdge) {
							rightAuxVelocity += pEdge->getAuxiliaryVelocity().x;
						}
					}
				}
				else {
					rightAuxVelocity = m_pGridData->getAuxiliaryVelocity(i + 1, j).x;
				}
				// bottom border
				if (m_pCutCells->isCutCellAt(i, j - 1)) {
					for (std::shared_ptr<Edge<Vector2>> pEdge : m_pCutCells->getEdgeVector(dimensions_t(i, j - 1), xAlignedEdge)) {
						if (pEdge->getHalfEdges().first->getLocation() == topHalfEdge) {
							bottomAuxVelocity += pEdge->getAuxiliaryVelocity().y;
						}
					}
				} else {
					bottomAuxVelocity = m_pGridData->getAuxiliaryVelocity(i, j).y;
				}
				// top border
				if (m_pCutCells->isCutCellAt(i, j + 1)) {
					for (std::shared_ptr<Edge<Vector2>> pEdge : m_pCutCells->getEdgeVector(dimensions_t(i, j + 1), xAlignedEdge)) {
						if (pEdge->getHalfEdges().first->getLocation() == bottomHalfEdge) {
							topAuxVelocity += pEdge->getAuxiliaryVelocity().y;
						}
					}
				}
				else {
					topAuxVelocity = m_pGridData->getAuxiliaryVelocity(i, j + 1).y;
				}
				// compute flux
				const Scalar Dx = rightAuxVelocity - leftAuxVelocity;
				const Scalar Dy = topAuxVelocity - bottomAuxVelocity;
				divergent = (Dx + Dy) / dx;
			}
			return divergent;
		}
		#pragma endregion PressureProjection

		#pragma region SimulationFunctions
		#pragma endregion SimulationFunctions

		/************************************************************************/
		/* Protected Functions                                                  */
		/************************************************************************/

		#pragma region Ghost Fluid Method Helper Methods
		Scalar CutCellsGhostLiquidSolver2D::phiHelper(Vector2 pos) const {
			Interpolant<Scalar, Array2D, Vector2> *pLevelSetInterpolant = m_pLiquidRepresentation->getLevelSetInterpolantPtr();
			const Scalar phi = pLevelSetInterpolant->interpolate(pos);
			return phi;
		}
		Scalar CutCellsGhostLiquidSolver2D::thetaHelper(uint cutCellIndex, dimensions_t otherCell) const {
			Vector2 intersectionPoint;
			const Scalar dx = m_pGridData->getGridSpacing();
			HalfFace<Vector2> &cutCell = m_pCutCells->getCutCell(cutCellIndex);
			const Vector2 p1 = cutCell.getFace()->getCentroid();
			const Vector2 p2 = m_pGridData->getCenterPoint(otherCell.x, otherCell.y);
			for (std::shared_ptr<HalfEdge<Vector2>> pHalfEdge : cutCell.getHalfEdges()) {
				const Vector2 &p3 = pHalfEdge->getVertices().first->getPosition();
				const Vector2 &p4 = pHalfEdge->getVertices().second->getPosition();
				if (DoLinesIntersect(p1, p2, p3, p4, intersectionPoint)) {
					const Scalar theta = (p2 - intersectionPoint).length() / dx;
					return theta;
				}
			}
			return static_cast<Scalar>(0.5f);
		}
		Scalar CutCellsGhostLiquidSolver2D::ghostFluidHelper(int i1, int j1, int i2, int j2) const {
			const dimensions_t minMaxX(1, m_pGridData->getDimensions().x - 2);
			const dimensions_t minMaxY(1, m_pGridData->getDimensions().y - 2);
			Scalar result;
			if (m_pCutCells->isCutCellAt(i1, j1)) {
				const uint idx = m_pCutCells->getCutCellIndex(Vector2(i1, j1) + 0.5f);
				const Scalar alpha = thetaHelper(idx, dimensions_t(i2, j2));
				result = 1 - (1 / alpha);
			}
			else {
				result = GhostLiquidSolver::ghostFluidHelper(i1, j1, i2, j2);
			}
			return result;
		}
		#pragma endregion

		#pragma region InternalFunctionalities
		#pragma endregion InternalFunctionalities

		#pragma region Initialization Methods
		AdvectionBase * CutCellsGhostLiquidSolver2D::initializeAdvectionClass() {
			// Advection Scheme Initialization
			ParticleBasedAdvection<Vector2, Array2D>::params_t *pPbaParams = dynamic_cast<ParticleBasedAdvection<Vector2, Array2D>::params_t *>(m_params.pAdvectionParams);
			if (pPbaParams == nullptr) {
				throw std::logic_error("Invalid custom parameters configuration for initializing advection class.");
			}

			pPbaParams->samplingMethod = m_pLiquidRepresentation->getParams().samplingMethod;
			pPbaParams->resampleParticles = m_pLiquidRepresentation->getParams().resampleParticles;
			pPbaParams->particlesPerCell = m_pLiquidRepresentation->getParams().particlesPerCell;

			ParticleBasedAdvection<Vector2, Array2D> *pParticleBasedAdvection;
			m_pAdvection = pParticleBasedAdvection = new ParticleBasedAdvection<Vector2, Array2D>(
				pPbaParams, m_pVelocityInterpolant, m_pGrid->getGridData(), m_pLiquidRepresentation->getParticlesSampler()
			);
			pParticleBasedAdvection->getParticlesSampler()->setBoundaryConditions(m_boundaryConditions);

			// Advection properties for Cut Cells Initialization
			ParticlesToNodalGrid2D *pParticlesToNodal = dynamic_cast<ParticlesToNodalGrid2D *>(pParticleBasedAdvection->getParticlesToGrid());
			if (pParticlesToNodal == nullptr) {
				throw(std::logic_error("CutCellSolver2D: only particlesToNodalGrid is supported by cut-cells particle-based advection"));
			}
			pParticlesToNodal->setCutCellsVelocities(m_pCutCellsVelocities2D);
			//pParticleBasedAdvection->getParticleBasedIntegrator()->setCutVoxels(/* TODO: check if needed */);
			pParticleBasedAdvection->getParticlesSampler()->setCutCells(m_pCutCells);

			return pParticleBasedAdvection;
		}
		void CutCellsGhostLiquidSolver2D::initializeInterpolants() {
			const Scalar dx = m_pGridData->getGridSpacing();
			m_pVelocityInterpolant = new MeanValueInterpolant2D<Vector2>(m_nodalBasedVelocities, m_pCutCellsVelocities2D, dx);
			m_pAuxVelocityInterpolant = new MeanValueInterpolant2D<Vector2>(m_auxNodalBasedVelocities, m_pAuxCutCellsVelocities2D, dx, true);
			m_pVelocityInterpolant->setSiblingInterpolant(m_pAuxVelocityInterpolant);
		}
		CutCells2D<Vector2>* CutCellsGhostLiquidSolver2D::initializeCutCells() {
			m_cutCellGenerationTimer.start();

			// m_lineMeshes = m_pLiquidRepresentation->getLineMeshes(); TODO: find a way to assign this
			CutCells2D<Vector2> *pPlanarMesh = new CutCells2D<Vector2>(
				m_lineMeshes, m_pGridData->getGridSpacing(), m_pGrid->getDimensions()
			);
			pPlanarMesh->initialize();

			const uint numCutCells = pPlanarMesh->getNumberCutCells();
			m_cutCellsPressures.resize(numCutCells);
			m_cutCellsDivergents.resize(numCutCells);

			m_cutCellGenerationTimer.stop();
			m_cutCellGenerationTime = m_cutCellGenerationTimer.secondsElapsed();
			return pPlanarMesh;
		}
		void CutCellsGhostLiquidSolver2D::reinitializeCutCells() {
			m_cutCellGenerationTimer.start();

			// m_lineMeshes = m_pLiquidRepresentation->getLineMeshes(); TODO: find a way to assign this
			m_pCutCells->reinitialize(m_lineMeshes);

			const uint numCutCells = m_pCutCells->getNumberCutCells();
			m_cutCellsPressures.resize(numCutCells, 0.0f);
			m_cutCellsDivergents.resize(numCutCells, 0.0f);

			m_cutCellGenerationTimer.stop();
			m_cutCellGenerationTime = m_cutCellGenerationTimer.secondsElapsed();
		}
		#pragma endregion

		#pragma region DebuggingFunctionality
		#pragma endregion DebuggingFunctionality
	}
}
