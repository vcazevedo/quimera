//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#include "Solvers/2D/GhostLiquidSolver2D.h"
#include <omp.h>

namespace Chimera {

	/************************************************************************/
	/* Public Functions                                                     */
	/************************************************************************/

	#pragma region Constructors
	GhostLiquidSolver::GhostLiquidSolver(const params_t &params, StructuredGrid<Vector2, Array2D> *pGrid, const vector<BoundaryCondition<Vector2, Array2D> *> &boundaryConditions,
											LiquidRepresentation<Vector2, Array2D> *pLiquidRepresentation): VoxelizedGridSolver2D(params, pGrid),
											m_cellTypes(pLiquidRepresentation->getGridCellTypes()) {
		m_pLiquidRepresentation = pLiquidRepresentation;
		m_pGrid = pGrid;
		m_pGridData = m_pGrid->getGridData2D();
		m_dimensions = m_pGrid->getDimensions();
		m_boundaryConditions = boundaryConditions;
		m_pAdvection = nullptr;

		/** Initializing Poisson Matrix and Solver */
		m_pPoissonMatrix = createPoissonMatrix();
		initializePoissonSolver();
		updatePoissonMatrix();

		/** Boundary conditions for initialization */
		enforceBoundaryConditions();

		/** Velocity and intermediary velocities interpolants */
		initializeInterpolants();

		/** Initializing Advection */
		if (m_params.pAdvectionParams->advectionCategory != LagrangianAdvection) {
			throw(std::logic_error("GhostLiquidSolver: only particle based advection methods are supported now"));
		}
		ParticleBasedAdvection<Vector2, Array2D>::params_t *pPbaParams = dynamic_cast<ParticleBasedAdvection<Vector2, Array2D>::params_t *>(m_params.pAdvectionParams);
		if (pPbaParams == nullptr) {
			throw std::logic_error("Invalid custom parameters configuration for initializing advection class.");
		}

		pPbaParams->samplingMethod = m_pLiquidRepresentation->getParams().samplingMethod;
		pPbaParams->resampleParticles = m_pLiquidRepresentation->getParams().resampleParticles;
		pPbaParams->particlesPerCell = m_pLiquidRepresentation->getParams().particlesPerCell;

		ParticleBasedAdvection<Vector2, Array2D> *pParticleBasedAdvection;
		m_pAdvection = pParticleBasedAdvection = new ParticleBasedAdvection<Vector2, Array2D>(pPbaParams, m_pVelocityInterpolant, m_pGrid->getGridData(), m_pLiquidRepresentation->getParticlesSampler());
		pParticleBasedAdvection->getParticlesSampler()->setBoundaryConditions(m_boundaryConditions);
		m_liquidDensityCoeff = 10;
		m_liquidDensityCoeff = 1;
		m_airDensityCoeff = 1.201;
		m_surfaceTensionCoeff = 0.073;
	};
	#pragma endregion

	#pragma region Ghost Fluid Method Helper Methods
	Scalar GhostLiquidSolver::phiHelper(int i, int j) const {
		const uint numSubDiv = std::pow(2, m_pLiquidRepresentation->getParams().levelSetGridSubdivisions);
		const int idx_x = (numSubDiv * (2 * i + 1)) / 2;
		const int idx_y = (numSubDiv * (2 * j + 1)) / 2;
		return m_pGridData->getFineGridScalarFieldArray().getValue(idx_x, idx_y);
	}
	// method that computes the linear interpolation between adjacent cells of partial cell
	Scalar GhostLiquidSolver::thetaHelper(Scalar inside, Scalar outside) const {
		const Scalar denom = inside - outside;
		if (denom > -1e-4) return 0.5f; // should always be neg, and large enough...
		else return std::max(static_cast<Scalar>(0), std::min(static_cast<Scalar>(1), inside / denom));
	}
	Scalar GhostLiquidSolver::ghostFluidHelper(int i1, int j1, int i2, int j2) const {
		// compute indices of centers of cell
		const Scalar phiCell = phiHelper(i1, j1);
		const Scalar phiOther = phiHelper(i2, j2);
		const Scalar alpha = thetaHelper(phiCell, phiOther);
		return 1 - (1 / alpha);
	}
	void GhostLiquidSolver::applyGhostFluidDiagonal() {
		#pragma omp parallel for
		for (int i = 1; i < m_pGridData->getDimensions().x - 1; i++) {
			for (int j = 1; j < m_pGridData->getDimensions().y - 1; j++) {
				const cellType_t typeCurr = m_cellTypes(i, j);
				// check if boundary cell
				if (typeCurr == boundaryFluidCell) {
					// neighborhoood cell types
					const cellType_t typeTop = m_cellTypes(i, j + 1);
					const cellType_t typeLeft = m_cellTypes(i - 1, j);
					const cellType_t typeRight = m_cellTypes(i + 1, j);
					const cellType_t typeBottom = m_cellTypes(i, j - 1);
					// poisson matrix center value
					Scalar center = 4;
					// check if neighborhood is a (partial) air cell
					// x-direction
					if (typeLeft == boundaryAirCell) {
						center -= ghostFluidHelper(i, j, i - 1, j);
					}
					if (typeRight == boundaryAirCell) {
						center -= ghostFluidHelper(i, j, i + 1, j);
					}
					// y-direction
					if (typeTop == boundaryAirCell) {
						center -= ghostFluidHelper(i, j, i, j + 1);
					}
					if (typeBottom == boundaryAirCell) {
						center -= ghostFluidHelper(i, j, i, j - 1);
					}
					// update poisson matrix
					m_pPoissonMatrix->setRow(
						m_pPoissonMatrix->getRowIndex(i - 1, j - 1), // row index
						-1, -1, center, -1, -1 // matrix values
					);
				}
			}
		}
	}
	void GhostLiquidSolver::correctVelocityGhostFluid(Scalar dt) {
		const Vector2 d = m_pGridData->getScaleFactor(0, 0);
		#pragma omp parallel for
		for (int i = 1; i < m_pGridData->getDimensions().x - 1; i++) {
			for (int j = 1; j < m_pGridData->getDimensions().y - 1; j++) {
				Vector2 vel = m_pGridData->getVelocity(i, j);
				const Scalar pressure = m_pGridData->getPressure(i, j);
				if (m_cellTypes(i, j) == boundaryFluidCell) {
					const Scalar c_pressure = dt * pressure / m_liquidDensityCoeff;
					if (m_cellTypes(i - 1, j) == boundaryAirCell) { // left
						vel.x += c_pressure * ghostFluidHelper(i, j, i - 1, j) / d.x;
					}
					if (m_cellTypes(i + 1, j) == boundaryAirCell) { // right
						vel.x += c_pressure * ghostFluidHelper(i, j, i + 1, j) / d.x;
					}
					if (m_cellTypes(i, j + 1) == boundaryAirCell) { // top
						vel.y += c_pressure * ghostFluidHelper(i, j, i, j + 1) / d.y;
					}
					if (m_cellTypes(i, j - 1) == boundaryAirCell) { // bottom
						vel.y += c_pressure * ghostFluidHelper(i, j, i, j - 1) / d.y;
					}
				}
				else if (m_cellTypes(i, j) == boundaryAirCell) {
					const bool is_top = m_cellTypes(i, j + 1) == boundaryFluidCell;
					const bool is_left = m_cellTypes(i - 1, j) == boundaryFluidCell;
					const bool is_right = m_cellTypes(i + 1, j) == boundaryFluidCell;
					const bool is_bottom = m_cellTypes(i, j - 1) == boundaryFluidCell;
					const Scalar c_pressure = dt * pressure / m_airDensityCoeff;
					// x-direction
					if (is_left || is_right) {
						if (is_left) {
							vel.x -= c_pressure * ghostFluidHelper(i - 1, j, i, j) / d.x;
						}
						if (is_right) {
							vel.x -= c_pressure * ghostFluidHelper(i + 1, j, i, j) / d.x;
						}
					} else {
						vel.x = 0.0f;
					}
					// y-direction
					if (is_top || is_bottom) {
						if (is_top) {
							vel.y -= c_pressure * ghostFluidHelper(i, j + 1, i, j) / d.y;
						}
						if (is_bottom) {
							vel.y -= c_pressure * ghostFluidHelper(i, j - 1, i, j) / d.y;
						}
					} else {
						vel.y = 0.0f;
					}
				}
				m_pGridData->setVelocity(vel, i, j);
			}
		}
	}
	#pragma endregion

	#pragma region UpdateFunctions

	bool GhostLiquidSolver::updatePoissonMatrix() {
		Scalar dx = m_pGridData->getScaleFactor(0, 0).x;

		/** First pass: setup GFM */
		#pragma omp parallel for
		for (int i = 1; i < m_pGridData->getDimensions().x - 1; i++) {
			for (int j = 1; j < m_pGridData->getDimensions().y - 1; j++) {
				/* If the cell is air our fluid, just initially set all its coefficients to the standard value.
				 * In a second pass the algorithm will fix up for boundary cells */
				switch(m_cellTypes(i, j))
				{
					case fluidCell:
					case boundaryFluidCell:
						m_pPoissonMatrix->setRow(m_pPoissonMatrix->getRowIndex(i - 1, j - 1), -1, -1, 4, -1, -1);
						break;
					case airCell:
					case boundaryAirCell:
						m_pPoissonMatrix->setRow(m_pPoissonMatrix->getRowIndex(i - 1, j - 1), 0, 0, 1, 0, 0);
				}
			}
		}

		/** Second pass: updates boundary cells */
		applyGhostFluidDiagonal();

		for (unsigned int i = 0; i < m_boundaryConditions.size(); i++) {
			m_boundaryConditions[i]->updatePoissonMatrix(m_pPoissonMatrix);
		}

		if (m_pPoissonMatrix->isSingular() && m_params.pPoissonSolverParams->solverCategory == Krylov)
			m_pPoissonMatrix->applyCorrection(1e-3);

		m_pPoissonMatrix->updateCudaData();

		return false;

	}
	#pragma endregion UpdateFunctions

	#pragma region SimulationFunctions
	void GhostLiquidSolver::divergenceFree(Scalar dt) {
		const Scalar dx = m_pGridData->getScaleFactor(0, 0).x;
		const Scalar dy = m_pGridData->getScaleFactor(0, 0).y;

		#pragma omp parallel for
		for (int i = 1; i < m_dimensions.x - 1; i++) {
			for (int j = 1; j < m_dimensions.y - 1; j++) {
				Vector2 velocity;
				const cellType_t currCellType = m_cellTypes(i, j);
				// whole cells
				if (currCellType == airCell) {
					velocity.x = m_pGridData->getAuxiliaryVelocity(i, j).x - (dt / m_airDensityCoeff)*((m_pGridData->getPressure(i, j) - m_pGridData->getPressure(i - 1, j)) / dx);
					velocity.y = m_pGridData->getAuxiliaryVelocity(i, j).y - (dt / m_airDensityCoeff)*((m_pGridData->getPressure(i, j) - m_pGridData->getPressure(i, j - 1)) / dy);
				}
				else if (currCellType == fluidCell) {
					velocity.x = m_pGridData->getAuxiliaryVelocity(i, j).x - (dt / m_liquidDensityCoeff)*((m_pGridData->getPressure(i, j) - m_pGridData->getPressure(i - 1, j)) / dx);
					velocity.y = m_pGridData->getAuxiliaryVelocity(i, j).y - (dt / m_liquidDensityCoeff)*((m_pGridData->getPressure(i, j) - m_pGridData->getPressure(i, j - 1)) / dy);
				}
				// boundary cells
				else {
					// helper method to check if same types of cells (boundaryAir treated as AIR, boundaryFluid as FLUID)
					const function<bool(cellType_t, cellType_t)> isSame = [](cellType_t type1, cellType_t type2) {
						if (type1 == boundaryAirCell)
							return type2 == airCell || type2 == boundaryAirCell;
						else
							return type2 == fluidCell || type2 == boundaryFluidCell;
					};

					// X-Velocity Component
					const Scalar gradientCoeff1 = dt / ((currCellType == boundaryAirCell) ? m_airDensityCoeff : m_liquidDensityCoeff);
					if (isSame(currCellType, m_cellTypes(i - 1, j))) {
						const Scalar gradientCoeff = gradientCoeff1;
						velocity.x = m_pGridData->getAuxiliaryVelocity(i, j).x - (gradientCoeff)*((m_pGridData->getPressure(i, j) - m_pGridData->getPressure(i - 1, j)) / dx);
					} else { //Face types do not match, use GFM to properly set pressure gradient
						Scalar distanceToMesh = distanceToLiquid(dimensions_t(i, j), dimensions_t(i - 1, j));
						//Normalize distanceToMesh relative to the grid spacing
						distanceToMesh /= dx;

						Scalar densityHat = 0;

						if (currCellType == boundaryAirCell) //Other cell is fluid
							densityHat = m_airDensityCoeff*distanceToMesh + (1 - distanceToMesh)*m_liquidDensityCoeff;
						else //This cell is fluid, other cell is air
							densityHat = m_liquidDensityCoeff*distanceToMesh + (1 - distanceToMesh)*m_airDensityCoeff;

						const Scalar gradientCoeff = dt / densityHat;
						velocity.x = m_pGridData->getAuxiliaryVelocity(i, j).x - (gradientCoeff)*((m_pGridData->getPressure(i, j) - m_pGridData->getPressure(i - 1, j)) / dx);
					}

					// Y-Velocity Component
					if (isSame(currCellType, m_cellTypes(i, j - 1))) {
						const Scalar gradientCoeff = gradientCoeff1;
						velocity.y = m_pGridData->getAuxiliaryVelocity(i, j).y - (gradientCoeff)*((m_pGridData->getPressure(i, j) - m_pGridData->getPressure(i, j - 1)) / dy);
					}
					else { //Face types do not match, use GFM to properly set pressure gradient
						Scalar distanceToMesh = distanceToLiquid(dimensions_t(i, j), dimensions_t(i, j - 1));
						//Normalize distanceToMesh relative to the grid spacing
						distanceToMesh /= dx;

						Scalar densityHat = 0;

						if (currCellType == boundaryAirCell) //Other cell is fluid
							densityHat = m_airDensityCoeff*distanceToMesh + (1 - distanceToMesh)*m_liquidDensityCoeff;
						else //This cell is fluid, other cell is air
							densityHat = m_liquidDensityCoeff*distanceToMesh + (1 - distanceToMesh)*m_airDensityCoeff;

						const Scalar gradientCoeff = dt / densityHat;
						velocity.y = m_pGridData->getAuxiliaryVelocity(i, j).y - (gradientCoeff)*((m_pGridData->getPressure(i, j) - m_pGridData->getPressure(i, j - 1)) / dy);
					}

				}
				m_pGridData->setVelocity(velocity, i, j);
			}
		}
	}

	Scalar GhostLiquidSolver::calculateFluxDivergent(int i, int j) {
		Scalar divergent = 0;
		const cellType_t cellType = m_cellTypes(i, j);
		if (cellType == fluidCell || cellType == boundaryFluidCell) {
			int row = 0;
			if (m_params.pPoissonSolverParams->solverCategory == Krylov) {
				row = m_pPoissonMatrix->getRowIndex(i - 1, j - 1);
			}
			else {
				row = m_pPoissonMatrix->getRowIndex(i, j);
			}

			const Scalar dx = (m_pGridData->getAuxiliaryVelocity(i + 1, j).x - m_pGridData->getAuxiliaryVelocity(i, j).x) / m_pGridData->getScaleFactor(i, j).x;
			const Scalar dy = (m_pGridData->getAuxiliaryVelocity(i, j + 1).y - m_pGridData->getAuxiliaryVelocity(i, j).y) / m_pGridData->getScaleFactor(i, j).y;

			divergent = dx + dy;
		}
		return divergent;
	}
	void GhostLiquidSolver::applyForces(Scalar dt) {
		ParticleBasedAdvection<Vector2, Array2D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector2, Array2D> *>(m_pAdvection);
		auto particlesTags = pParticleBasedAdv->getParticlesData()->getIntegerBasedAttribute("liquid");
		auto particlesPositions = pParticleBasedAdv->getParticlesData()->getPositions();
		for (int i = 0; i < particlesPositions.size(); i++) {
			//if (particlesTags[i] == 1) {
				pParticleBasedAdv->getParticlesData()->getVelocities()[i].y -= 0.981*dt/m_pGridData->getGridSpacing();
			//}
		}
	}
	void GhostLiquidSolver::update(Scalar dt) {
		m_numIterations++;
		m_totalSimulationTimer.start();


		ParticleBasedAdvection<Vector2, Array2D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector2, Array2D> *>(m_pAdvection);
		if (pParticleBasedAdv == nullptr) {
			throw(std::logic_error("CutCellSolver2D: only particle based advection methods are supported now"));
		}

		if (PhysicsCore<Vector2>::getInstance()->getElapsedTime() < dt) {
			applyForces(dt);
			pParticleBasedAdv->updateGridAttributes();
			enforceBoundaryConditions();
			updateDivergents(dt);
			enforceBoundaryConditions();
			solvePressure();
			enforceBoundaryConditions();
			project(dt);
			enforceBoundaryConditions();

			/*if (m_pParticleBasedAdvection) {
			m_pParticleBasedAdvection->getParticlesSampler()->interpolateVelocities(m_pVelocityInterpolant, m_pParticleBasedAdvection->getParticlesData());
			}*/
		}

		applyForces(dt);
		enforceBoundaryConditions();

		m_advectionTimer.start();

		enforceBoundaryConditions();

		//printInterpolationDivergence(10);

		enforceBoundaryConditions();
		extrapolateVelocities();
		pParticleBasedAdv->updatePositions(dt);
		if (m_pLiquidRepresentation) {
			m_pLiquidRepresentation->updateMeshes();
		}
		updatePoissonMatrix();
		pParticleBasedAdv->updateGridAttributes();


		m_advectionTimer.stop();
		m_advectionTime = m_advectionTimer.secondsElapsed();
		enforceBoundaryConditions();

		/** Solve pressure */
		m_solvePressureTimer.start();
		updateDivergents(dt);
		solvePressure();

		/** Ghost Fluid Velocity Update */
		correctVelocityGhostFluid(dt);

		enforceBoundaryConditions();
		m_solvePressureTimer.stop();
		m_solvePressureTime = m_solvePressureTimer.secondsElapsed();

		/** Project velocity */
		m_projectionTimer.start();
		project(dt);
		m_projectionTimer.stop();
		m_projectionTime = m_projectionTimer.secondsElapsed();
		enforceBoundaryConditions();

		enforceBoundaryConditions();

		// for debugging purposes,
		// compute the vorticity
		updateVorticity();

		if (pParticleBasedAdv) {
			pParticleBasedAdv->updateParticleAttributes();
			m_pGridData->getDensityBuffer().swapBuffers();
			m_pGridData->getTemperatureBuffer().swapBuffers();
		}


		enforceBoundaryConditions();

		updateKineticEnergy();

		m_totalSimulationTimer.stop();
		m_totalSimulationTime = m_totalSimulationTimer.secondsElapsed();

	}
	#pragma endregion SimulationFunctions

	#pragma region InternalFunctionalities
	Scalar GhostLiquidSolver::distanceToLiquid(const dimensions_t &cellIndex, const dimensions_t &nextCellIndex) const {
		Scalar dx = m_pGridData->getScaleFactor(0, 0).x;
		Vector2 cellCentroid = Vector2(cellIndex.x + 0.5, cellIndex.y + 0.5)*dx;
		Vector2 nextCellCentroid = Vector2(nextCellIndex.x + 0.5, nextCellIndex.y + 0.5)*dx;

		/* The algorithm has to check all lines segments to check if the ray from one cell centroid crosses the liquid
		 * interface */
		for (int k = 0; k < m_pLiquidRepresentation->getLineMeshes().size(); k++) {
			Vector2 intersectionPoint;
			const vector<Vector2> &points = m_pLiquidRepresentation->getLineMeshes()[k]->getPoints();
			for (int i = 0; i < points.size(); i++) {
				int nextI = roundClamp<int>(i + 1, 0, points.size());
				if (DoLinesIntersect(cellCentroid, nextCellCentroid, points[i], points[nextI], intersectionPoint)) {
					return (cellCentroid - intersectionPoint).length();
				}
			}
		}
		/* Error, no line intersects the geometry, user defined incorrect function arguments */
		return -1;
	}

	void GhostLiquidSolver::extrapolateVelocities() {
		for (int i = 0; i < m_pGrid->getDimensions().x; ++i) {
			for (int j = 0; j < m_pGrid->getDimensions().y; ++j) {
				const cellType_t typeCurr = m_cellTypes(i, j);

				if (typeCurr == cellType_t::boundaryAirCell) {

					int n = 0;
					Vector2 newVelocity(0, 0);
					Vector2 newAuxiliaryVelocity(0, 0);

					// left
					if (i > 0 && (m_cellTypes(i - 1, j) == cellType_t::fluidCell || m_cellTypes(i - 1, j) == cellType_t::boundaryFluidCell)) {
						newVelocity += m_pGridData->getVelocity(i - 1, j);
						newAuxiliaryVelocity += m_pGridData->getAuxiliaryVelocity(i - 1, j);
						n++;
					}

					// right
					if (i < m_pGridData->getDimensions().x - 1 && (m_cellTypes(i + 1, j) == cellType_t::fluidCell || m_cellTypes(i + 1, j) == cellType_t::boundaryFluidCell)) {
						newVelocity += m_pGridData->getVelocity(i + 1, j);
						newAuxiliaryVelocity += m_pGridData->getAuxiliaryVelocity(i + 1, j);
						n++;
					}

					// bottom
					if (j > 0 && (m_cellTypes(i, j - 1) == cellType_t::fluidCell || m_cellTypes(i, j - 1) == cellType_t::boundaryFluidCell)) {
						newVelocity += m_pGridData->getVelocity(i, j - 1);
						newAuxiliaryVelocity += m_pGridData->getAuxiliaryVelocity(i, j - 1);
						n++;
					}

					// top
					if (j < m_pGridData->getDimensions().y - 1 && (m_cellTypes(i, j + 1) == cellType_t::fluidCell || m_cellTypes(i, j + 1) == cellType_t::boundaryFluidCell)) {
						newVelocity += m_pGridData->getVelocity(i, j + 1);
						newAuxiliaryVelocity += m_pGridData->getAuxiliaryVelocity(i, j + 1);
						n++;
					}

					if (n > 0) {
						newVelocity *= (1.0f / n);
						newAuxiliaryVelocity *= (1.0f / n);
						m_pGridData->setVelocity(newVelocity, i, j);
						m_pGridData->setAuxiliaryVelocity(newAuxiliaryVelocity, i, j);
					}


				}

			}
		}

	}
	#pragma endregion InternalFunctionalities
}
