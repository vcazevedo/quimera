//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#include "Solvers/2D/VoxelizedGridSolver2D.h"


namespace Chimera {

	namespace Solvers {

		#pragma region Constructors
		template <typename SolverType>
		VoxelizedGridSolver2D<SolverType>::VoxelizedGridSolver2D(	shared_ptr<GridMesh<GridMeshType>> pSimulationMesh,
																															const map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>>> &levelSets)
			: ParentClass(pSimulationMesh, levelSets), m_voxelizedCells(pSimulationMesh->getGridDimensions()) {

			m_pQuadGridMesh = QuadGridMesh<typename SolverType::GridMeshType>::get(FlowSolver<SolverType>::m_pSimulationGrid);
			/** Necessary for adding variables used for the simulation phase */
			initializeGridVariables();
			initializeAdvection();

			/** Immersed objects initialization */
			{
				updateVoxelizedCells();
				updatePoissonMatrix();
			}

			/* Pressure variables initialization */
			m_pDivergents = make_shared<Eigen::VectorXf>((m_pQuadGridMesh->getGridDimensions().x - 2)*(m_pQuadGridMesh->getGridDimensions().y - 2));
			m_pPressures = make_shared<Eigen::VectorXf>((m_pQuadGridMesh->getGridDimensions().x - 2)*(m_pQuadGridMesh->getGridDimensions().y - 2));
		}
		#pragma endregion

		#pragma region Functionalities
		template <typename SolverType>
		void VoxelizedGridSolver2D<SolverType>::update(Scalar dt) {
			/** First time-step: Do an additional solve to get div-free velocities from boundary conditions everywhere */
			if (ParentClass::m_numSteps == 0) {
				project(dt);
			}

			/** Advection */
			ParentClass::m_pAdvectionSolver->advect(dt);

			/** Pressure Projection */
			project(dt);

			/** Update of smoke */
			ParentClass::m_pAdvectionSolver->postProjectionUpdate(dt);

		}
		#pragma endregion

		#pragma region InitializationFunctions
		template <typename SolverType>
		void VoxelizedGridSolver2D<SolverType>::updatePoissonMatrix() {
			const dimensions_t gridDim = ParentClass::m_pSimulationGrid->getGridDimensions();
			const auto totalCells = size_t((gridDim.x - 2) * (gridDim.y - 2));

			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(totalCells * 5);

			for (uint i = 1; i < gridDim.x - 1; i++) {
				for (uint j = 1; j < gridDim.y - 1; j++) {
					if (!m_voxelizedCells(i, j)) {
						uint centralValue = 0;
						//Left
						if (!m_voxelizedCells(i - 1, j)) {
							centralValue += 1;
							triplets.emplace_back(getPressureRow(i, j), getPressureRow(i - 1, j), -1);
						}

						//Right
						if (!m_voxelizedCells(i + 1, j)) {
							centralValue += 1;
							triplets.emplace_back(getPressureRow(i, j), getPressureRow(i + 1, j), -1);
						}

						//Bottom
						if (!m_voxelizedCells(i, j - 1)) {
							centralValue += 1;
							triplets.emplace_back(getPressureRow(i, j), getPressureRow(i, j - 1), -1);
						}

						//Top
						if (!m_voxelizedCells(i, j + 1)) {
							centralValue += 1;
							triplets.emplace_back(getPressureRow(i, j), getPressureRow(i, j + 1), -1);
						}

						//Central value
						triplets.emplace_back(getPressureRow(i, j), getPressureRow(i, j), centralValue);
					}
				}
			}

			shared_ptr<Eigen::SparseMatrix<Scalar>> pSparseMatrix = make_shared<Eigen::SparseMatrix<Scalar>>(totalCells, totalCells);
			pSparseMatrix->setFromTriplets(triplets.begin(), triplets.end());
		}

		#pragma endregion

		#pragma region BoundaryConditions
		template <typename SolverType>
		void VoxelizedGridSolver2D<SolverType>::enforceSolidWallsConditions(const VectorType& solidVelocity) {
			const dimensions_t gridDim = m_pQuadGridMesh->getGridDimensions();

			for(int i = 0; i < gridDim.x; ++i) {
				int targetI = (i == 0)? 1 : (i == gridDim.x - 1)? gridDim.x - 2: i;
				auto cell = m_pQuadGridMesh->getQuadCell(i, 0)->getCells().front();
				auto targetCell = m_pQuadGridMesh->getQuadCell(targetI, 1)->getCells().front();
				cell->OwnCustomAttribute<Scalar>::setAttribute("pressure", targetCell->OwnCustomAttribute<Scalar>::getAttribute("pressure"));
				auto edge = m_pQuadGridMesh->getEdges(dimensions_t(i, 0), yAlignedEdge).front();
				auto targetEdge = m_pQuadGridMesh->getEdges(dimensions_t(targetI, 1), yAlignedEdge).front();
				edge->OwnCustomAttribute<Scalar>::setAttribute("velocity", targetEdge->OwnCustomAttribute<Scalar>::getAttribute("velocity"));

				cell = m_pQuadGridMesh->getQuadCell(i, gridDim.y - 1)->getCells().front();
				targetCell = m_pQuadGridMesh->getQuadCell(targetI, gridDim.y - 2)->getCells().front();
				cell->OwnCustomAttribute<Scalar>::setAttribute("pressure", targetCell->OwnCustomAttribute<Scalar>::getAttribute("pressure"));
				edge = m_pQuadGridMesh->getEdges(dimensions_t(i, gridDim.y - 1), yAlignedEdge).front();
				targetEdge = m_pQuadGridMesh->getEdges(dimensions_t(targetI, gridDim.y - 2), yAlignedEdge).front();
				edge->OwnCustomAttribute<Scalar>::setAttribute("velocity", targetEdge->OwnCustomAttribute<Scalar>::getAttribute("velocity"));
			}
			for(int i = 1; i < gridDim.y - 1; ++i) {
				auto cell = m_pQuadGridMesh->getQuadCell(0, i)->getCells().front();
				auto targetCell = m_pQuadGridMesh->getQuadCell(1, i)->getCells().front();
				cell->OwnCustomAttribute<Scalar>::setAttribute("pressure", targetCell->OwnCustomAttribute<Scalar>::getAttribute("pressure"));
				auto edge = m_pQuadGridMesh->getEdges(dimensions_t(0, i), xAlignedEdge).front();
				auto targetEdge = m_pQuadGridMesh->getEdges(dimensions_t(1, i), xAlignedEdge).front();
				edge->OwnCustomAttribute<Scalar>::setAttribute("velocity", targetEdge->OwnCustomAttribute<Scalar>::getAttribute("velocity"));

				cell = m_pQuadGridMesh->getQuadCell(gridDim.x - 1, i)->getCells().front();
				targetCell = m_pQuadGridMesh->getQuadCell(gridDim.x - 2, i)->getCells().front();
				cell->OwnCustomAttribute<Scalar>::setAttribute("pressure", targetCell->OwnCustomAttribute<Scalar>::getAttribute("pressure"));
				edge = m_pQuadGridMesh->getEdges(dimensions_t(gridDim.x - 1, i), xAlignedEdge).front();
				targetEdge = m_pQuadGridMesh->getEdges(dimensions_t(gridDim.x - 2, i), xAlignedEdge).front();
				edge->OwnCustomAttribute<Scalar>::setAttribute("velocity", targetEdge->OwnCustomAttribute<Scalar>::getAttribute("velocity"));
			}
		}

		template <typename SolverType>
		void VoxelizedGridSolver2D<SolverType>::updateVoxelizedCells() {
			m_voxelizedCells.assign(false);
			
			for (auto const &pQuadCell : ParentClass::m_pSimulationGrid->getElements()) {
				dimensions_t cellLocation = pQuadCell->getGridCellLocation();
				if (cellLocation.x == 0 || cellLocation.y == 0
					|| cellLocation.x >= m_pQuadGridMesh->getGridDimensions().x - 1
					|| cellLocation.y >= m_pQuadGridMesh->getGridDimensions().y - 1) {
					m_voxelizedCells(cellLocation) = true;
				}
			}

			if (ParentClass::m_levelSets.find("obstacle") != ParentClass::m_levelSets.end()) {
				auto pObstacleGrid = ParentClass::m_levelSets["obstacle"]->getGrid();
				auto pLevelSetInterpolant = pObstacleGrid->getScalarFieldInterpolantVertices("levelset");
				for (auto const &pQuadCell : ParentClass::m_pSimulationGrid->getElements()) {
					//Our voxelized approach: if at least one corner of the QuadCell has a levelset < 0, 
					// then the cell is considered as a solid.
					for (auto const &vertex : pQuadCell->getVertices()) {
						if (vertex->getPosition().x < m_pQuadGridMesh->getGridDimensions().x * m_pQuadGridMesh->getGridSpacing()
							&& vertex->getPosition().y < m_pQuadGridMesh->getGridDimensions().y * m_pQuadGridMesh->getGridSpacing()) {
							Scalar levelSetValue = pLevelSetInterpolant->interpolate(vertex->getPosition());
							if (levelSetValue <= 0) {
								m_voxelizedCells(pQuadCell->getGridCellLocation()) = true;
								pQuadCell->getCells().front()->setSolid(true); //Works only for voxelized approach
							}
						}
					}
				}
			}
		}
		#pragma endregion

		#pragma region PressureProjection
		template <typename SolverType>
		void VoxelizedGridSolver2D<SolverType>::updateDivergents() {
			int i, j;
			dimensions_t gridDimensions = ParentClass::m_pSimulationGrid->getGridDimensions();

			#pragma omp parallel for
			for (i = 1; i < gridDimensions.x - 1; i++) {
				#pragma omp parallel for private(j)
				for (j = 1; j < gridDimensions.y - 1; j++) {
					if (m_voxelizedCells(i, j)) {
						OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getQuadCell(i, j)->getCells().front())->setAttribute("divergence", 0);
						continue;
					}

					auto pVerticalEdge = m_pQuadGridMesh->getEdges(dimensions_t(i, j), edgeType_t::yAlignedEdge).front();
					auto pHorizontalEdge = m_pQuadGridMesh->getEdges(dimensions_t(i, j), edgeType_t::xAlignedEdge).front();
					auto pVerticalEdgePlus = m_pQuadGridMesh->getEdges(dimensions_t(i + 1, j), edgeType_t::yAlignedEdge).front();
					auto pHorizontalEdgePlus = m_pQuadGridMesh->getEdges(dimensions_t(i, j + 1), edgeType_t::xAlignedEdge).front();
					Scalar xMinus = 0, xPlus = 0, yMinus = 0, yPlus = 0;
					if(!m_voxelizedCells(i - 1, j)) {
						xMinus = OwnCustomAttribute<Scalar>::get(pVerticalEdge)->getAttribute("auxVelocity");
					}
					if(!m_voxelizedCells(i + 1, j)) {
						xPlus = OwnCustomAttribute<Scalar>::get(pVerticalEdgePlus)->getAttribute("auxVelocity");
					}
					if(!m_voxelizedCells(i, j - 1)) {
						yMinus = OwnCustomAttribute<Scalar>::get(pHorizontalEdge)->getAttribute("auxVelocity");
					}
					if(!m_voxelizedCells(i, j + 1)) {
						yPlus = OwnCustomAttribute<Scalar>::get(pHorizontalEdgePlus)->getAttribute("auxVelocity");
					}

					Scalar gradX = xPlus - xMinus;
					gradX *= m_pQuadGridMesh->getGridSpacing();

					Scalar gradY = yPlus - yMinus;
					gradY *= m_pQuadGridMesh->getGridSpacing();

					Scalar divergent = gradX + gradY;
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getQuadCell(i, j)->getCells().front())->setAttribute("divergence", divergent);
				}
			}
		}

		template <typename SolverType>
		void VoxelizedGridSolver2D<SolverType>::solvePressure(Scalar dt) {
			uint i, j;
			dimensions_t gridDimensions = ParentClass::m_pSimulationGrid->getGridDimensions();
			/** Updating divergents from cells to array */
			#pragma omp parallel for
			for (i = 1; i < gridDimensions.x - 1; i++) {
				#pragma omp parallel for private(j)
				for (j = 1; j < gridDimensions.y - 1; j++) {
					(*m_pDivergents)(getPressureRow(i, j)) = -1/dt*OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getQuadCell(i, j)->getCells().front())->getAttribute("divergence");
				}
			}

			/** Solve the system */
			//ParentClass::m_pPoissonSolver->solve(m_pDivergents, m_pPressures);

			/** Setting pressures from array to cells*/
			#pragma omp parallel for
			for (i = 1; i < gridDimensions.x - 1; i++) {
				#pragma omp parallel for private(j)
				for (j = 1; j < gridDimensions.y - 1; j++) {
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getQuadCell(i, j)->getCells().front())->setAttribute("pressure", (*m_pPressures)(getPressureRow(i, j)));
				}
			}
		}

		template <typename SolverType>
		void VoxelizedGridSolver2D<SolverType>::applyPressureGradients(Scalar dt) {
			int i, j;
			dimensions_t gridDimensions = ParentClass::m_pSimulationGrid->getGridDimensions();
			#pragma omp parallel for
			for (i = 1; i < gridDimensions.x - 1; i++) {
				#pragma omp parallel for private(j)
				for (j = 1; j < gridDimensions.y - 1; j++) {
					if (m_voxelizedCells(i, j)) {
						auto pVerticalEdge = m_pQuadGridMesh->getEdges(dimensions_t(i, j), edgeType_t::yAlignedEdge).front();
						auto pHorizontalEdge = m_pQuadGridMesh->getEdges(dimensions_t(i, j), edgeType_t::xAlignedEdge).front();
						OwnCustomAttribute<Scalar>::get(pVerticalEdge)->setAttribute("velocity", 0);
						OwnCustomAttribute<Scalar>::get(pHorizontalEdge)->setAttribute("velocity", 0);
						continue;
					}

					Scalar pressureXMinus = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getQuadCell(i - 1, j)->getCells().front())->getAttribute("pressure");
					Scalar pressureYMinus = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getQuadCell(i, j - 1)->getCells().front())->getAttribute("pressure");
					Scalar pressure = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getQuadCell(i, j)->getCells().front())->getAttribute("pressure");

					Scalar pressureGradX = (pressure - pressureXMinus) / m_pQuadGridMesh->getGridSpacing();
					Scalar pressureGradY = (pressure - pressureYMinus) / m_pQuadGridMesh->getGridSpacing();

					auto pVerticalEdge = m_pQuadGridMesh->getEdges(dimensions_t(i, j), edgeType_t::yAlignedEdge).front();
					auto pHorizontalEdge = m_pQuadGridMesh->getEdges(dimensions_t(i, j), edgeType_t::xAlignedEdge).front();

					if (!m_voxelizedCells(i - 1, j)) {
						Scalar auxVerticalVel = OwnCustomAttribute<Scalar>::get(pVerticalEdge)->getAttribute("auxVelocity");
						auxVerticalVel -= pressureGradX*dt;
						OwnCustomAttribute<Scalar>::get(pVerticalEdge)->setAttribute("velocity", auxVerticalVel);
					} else {
						OwnCustomAttribute<Scalar>::get(pVerticalEdge)->setAttribute("velocity", 0);
					}

					if (!m_voxelizedCells(i, j - 1)) {
						Scalar auxHorizontalVel = OwnCustomAttribute<Scalar>::get(pHorizontalEdge)->getAttribute("auxVelocity");
						auxHorizontalVel -= pressureGradY*dt;
						OwnCustomAttribute<Scalar>::get(pHorizontalEdge)->setAttribute("velocity", auxHorizontalVel);
					}
					else {
						OwnCustomAttribute<Scalar>::get(pHorizontalEdge)->setAttribute("velocity", 0);
					}
				}
			}
		}

		template <typename SolverType>
		void VoxelizedGridSolver2D<SolverType>::project(Scalar dt, bool firstCall) {
			enforceSolidWallsConditions(Vector2(0, 0));

			updateDivergents();
			solvePressure(dt);
			applyPressureGradients(dt);

			enforceSolidWallsConditions(Vector2(0, 0));
		}

		template <typename SolverType>
		void VoxelizedGridSolver2D<SolverType>::computeVorticity() {
			#pragma omp parallel for
			for (int i = 1; i < m_pQuadGridMesh->getGridDimensions().x; i++) {
				#pragma omp parallel for private(j)
				for (int j = 1; j < m_pQuadGridMesh->getGridDimensions().y; j++) {
					dimensions_t ij(i, j);
					dimensions_t iMinusj(i - 1, j);
					dimensions_t ijMinus(i, j - 1);
					auto horizontalEdgeMinus = m_pQuadGridMesh->getEdges(iMinusj, xAlignedEdge).front();
					auto horizontalEdgePlus = m_pQuadGridMesh->getEdges(ij, xAlignedEdge).front();
					auto verticalEdgeMinus = m_pQuadGridMesh->getEdges(ijMinus, yAlignedEdge).front();
					auto verticalEdgePlus = m_pQuadGridMesh->getEdges(ij, yAlignedEdge).front();

					auto uPlus = OwnCustomAttribute<Scalar>::get(verticalEdgePlus)->getAttribute("velocity");
					auto uMinus = OwnCustomAttribute<Scalar>::get(verticalEdgeMinus)->getAttribute("velocity");
					auto vPlus = OwnCustomAttribute<Scalar>::get(horizontalEdgePlus)->getAttribute("velocity");
					auto vMinus = OwnCustomAttribute<Scalar>::get(horizontalEdgeMinus)->getAttribute("velocity");

					auto vorticity = (vPlus - vMinus) / m_pQuadGridMesh->getGridSpacing() - (uPlus - uMinus) / m_pQuadGridMesh->getGridSpacing();
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(i, j))->setAttribute("vorticity", vorticity);
				}
			}
		}

		#pragma endregion

		template class VoxelizedGridSolver2D<FlowSolverType2D>;
	}
}
