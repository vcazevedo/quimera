//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#include "Solvers/2D/VoxelizedStreamfunctionGridSolverDEC2D.h"


namespace Chimera {

	namespace Solvers {

		#pragma region Constructors
		template <typename SolverType>
		VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::VoxelizedStreamfunctionGridSolverDEC2D(shared_ptr<GridMesh<GridMeshType>> pSimulationMesh,
			const map<string, shared_ptr<LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>>>& levelSets,
			shared_ptr<AdvectionBase> pAdvectionSolver, shared_ptr<PoissonSolver> pPoissonSolver)
			: ParentClass(pSimulationMesh, levelSets, pAdvectionSolver, pPoissonSolver) {

			//Primal grid
			m_pQuadGridMesh = ParentClass::m_pQuadGridMesh;

			//Boundary Conditions --> should be read from the xml file!
			//For the vorticity-streamfunction formulation, the BCs are stored on the vertices!
			m_southBoundary = inflow2;
			m_northBoundary = outflow;
			m_eastBoundary = noSlip;
			m_westBoundary = noSlip;
			m_inflowVelocity = 3.0f;
			m_internalBoundary = noSlip;
			m_TaylorVortices = false;

			processPrimalGrid();

			//Dual grid
			m_pDualMesh = make_shared<DualMesh<GridMeshType>>(m_pQuadGridMesh, streamfunctionDECSolver); //Must be constructed, else the dual elements don't exist.
			processDualGrid();

			//Viscosity
			m_viscosity = 0.2f;
			m_viscosityMatrix = getViscosityMatrix(true);

			//DEC operators
			m_primalDelta0Operator = getPrimalDelta0Operator();
			m_dualDelta1Operator = getDualDelta1Operator();
			m_dualDelta1Operator2 = getDualDelta1Operator2();
			m_primalHodge0Operator = getPrimalHodge0Operator();
			m_primalHodge1Operator = getPrimalHodge1Operator();
			m_dualHodge1Operator = getDualHodge1Operator();
			m_dualHodge2Operator = getDualHodge2Operator();
			m_dualHodge2Operator2 = getDualHodge2Operator2();
			m_dualHodge2Operator3 = getDualHodge2Operator3();

			//RHS correction
			Scalar dt = 0.008;
			Eigen::SparseMatrix<Scalar> primalLaplacian = m_primalHodge0Operator.cwiseInverse() * m_primalDelta0Operator.transpose() * m_primalHodge1Operator * m_primalDelta0Operator;
			Eigen::SparseMatrix<Scalar> primalBiharmonicOperator = dt * m_viscosityMatrix * primalLaplacian * primalLaplacian + primalLaplacian;
			/*cout << 0.25 * primalLaplacian << endl;
			cout << 0.0625 * primalLaplacian * primalLaplacian << endl;*/
			auto primalLaplacianReduced = primalBiharmonicOperator.block(0, 0, m_primalInternalVertices + m_primalBorderVertices, m_primalInternalVertices + m_primalBorderVertices);

			Eigen::SparseMatrix<Scalar> dualLaplacian = m_dualHodge2Operator2 * m_dualDelta1Operator2 * m_dualHodge1Operator.cwiseInverse() * m_dualDelta1Operator2.transpose();
			Eigen::SparseMatrix<Scalar> dualLaplacianCorrected = m_dualHodge2Operator3 * m_dualDelta1Operator2 * m_dualHodge1Operator.cwiseInverse() * m_dualDelta1Operator2.transpose();
			Eigen::SparseMatrix<Scalar> dualBiharmonicOperator = dt * m_viscosityMatrix * dualLaplacianCorrected * dualLaplacianCorrected + dualLaplacian;
			/*cout << 0.25 * dualLaplacianCorrected << endl;
			cout << 0.0625 * dualLaplacianCorrected * dualLaplacianCorrected << endl;*/
			auto dualLaplacianReduced = dualBiharmonicOperator.block(0, 0, m_primalInternalVertices + m_primalBorderVertices, m_primalInternalVertices + m_primalBorderVertices);

			auto difference = dualLaplacianReduced - primalLaplacianReduced;
			//cout << 0.0625 * (dualLaplacianCorrected * dualLaplacianCorrected - primalLaplacian * primalLaplacian) << endl;
			m_difference = difference.block(0, 0, m_primalInternalVertices, m_primalInternalVertices + m_primalBorderVertices);

			//Solver initialization
			initializeGridVariables();
			updatePoissonMatrix();
		}
		#pragma endregion
		
		#pragma region DECOperators
		template <typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getPrimalDelta0Operator() {
			auto pVertices = m_pQuadGridMesh->getVertices();
			auto pEdges = m_pQuadGridMesh->getEdges();

			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(2 * pEdges.size());

			//Construction of the matrix
			#pragma omp parallel for
			for (auto pEdge : pEdges) {
				auto pV1 = pEdge->getVertex1();
				auto pV2 = pEdge->getVertex2();

				//For xAligned edges the order of first and second vertex should be switched to respect the right orientation of the edges.
				if (pEdge->getType() == xAlignedEdge) {
					pV1 = pEdge->getVertex2();
					pV2 = pEdge->getVertex1();
				}

				int v1Id = m_primalVertexIDToNewVertexID[pV1->getID()];
				int v2Id = m_primalVertexIDToNewVertexID[pV2->getID()];
				int eId = pEdge->getID();

				if (!pV1->isSolid() && !pV2->isSolid()) {
					if (!pV1->isBorder() || pV1->getBoundaryCondition() == outflow)
						triplets.push_back(Eigen::Triplet<Scalar>(eId, v1Id, -1.f));

					if (!pV2->isBorder() || pV2->getBoundaryCondition() == outflow)
						triplets.push_back(Eigen::Triplet<Scalar>(eId, v2Id, 1.f));
				}
			}

			Eigen::SparseMatrix<Scalar> delta0Operator(pEdges.size(), pVertices.size());
			delta0Operator.setFromTriplets(triplets.begin(), triplets.end());
			return delta0Operator;
		}

		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getDualDelta1Operator() {
			auto pVertices = m_pQuadGridMesh->getVertices();

			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(4 * pVertices.size()); //Reserves 4 entries for every dual cell

			#pragma omp parallel for
			for (auto pVertex : pVertices) {
				if (pVertex->getDualElement2D() != nullptr) {
					auto pDualCell = pVertex->getDualElement2D();
					auto halfCell = pDualCell->getHalfCells().first;
					auto halfEdges = halfCell->getHalfEdges();
					for (auto halfEdge : halfEdges) {
						auto pVertex1 = halfEdge->getVertices().first;
						auto pVertex2 = halfEdge->getVertices().second;

						if (pVertex1->getDualElement2D() != nullptr && pVertex2->getDualElement2D() != nullptr) {
							if (!pVertex1->getDualElement2D()->isSolid() && !pVertex1->getDualElement2D()->isSolid()) {
								//This is a bad solution: half edges should be used instead
								auto cellPosition = pDualCell->getCentroid();
								if (pVertex1->getPosition().y < cellPosition.y && pVertex2->getPosition().y < cellPosition.y) {//bottom
									triplets.push_back(Eigen::Triplet<Scalar>(pDualCell->getID() - m_dualPaddingCells, halfEdge->getEdge()->getID() - m_dualPaddingEdges, 1.f));
								}
								else if (pVertex1->getPosition().y > cellPosition.y && pVertex2->getPosition().y > cellPosition.y) {//top
									triplets.push_back(Eigen::Triplet<Scalar>(pDualCell->getID() - m_dualPaddingCells, halfEdge->getEdge()->getID() - m_dualPaddingEdges, -1.f));
								}
								else if (pVertex1->getPosition().x < cellPosition.x && pVertex2->getPosition().x < cellPosition.x) {//left
									triplets.push_back(Eigen::Triplet<Scalar>(pDualCell->getID() - m_dualPaddingCells, halfEdge->getEdge()->getID() - m_dualPaddingEdges, -1.f));
								}
								else if (pVertex1->getPosition().x > cellPosition.x && pVertex2->getPosition().x > cellPosition.x) {//right
									triplets.push_back(Eigen::Triplet<Scalar>(pDualCell->getID() - m_dualPaddingCells, halfEdge->getEdge()->getID() - m_dualPaddingEdges, 1.f));
								}
							}
						}
						else {//Border edges
							//This is a bad solution: half edges should be used instead
							auto cellPosition = pDualCell->getCentroid();
							if (pVertex1->getPosition().y < cellPosition.y && pVertex2->getPosition().y < cellPosition.y) {//bottom
								triplets.push_back(Eigen::Triplet<Scalar>(pDualCell->getID() - m_dualPaddingCells, halfEdge->getEdge()->getID() - m_dualPaddingEdges, 1.f));
							}
							else if (pVertex1->getPosition().y > cellPosition.y && pVertex2->getPosition().y > cellPosition.y) {//top
								triplets.push_back(Eigen::Triplet<Scalar>(pDualCell->getID() - m_dualPaddingCells, halfEdge->getEdge()->getID() - m_dualPaddingEdges, -1.f));
							}
							else if (pVertex1->getPosition().x < cellPosition.x && pVertex2->getPosition().x < cellPosition.x) {//left
								triplets.push_back(Eigen::Triplet<Scalar>(pDualCell->getID() - m_dualPaddingCells, halfEdge->getEdge()->getID() - m_dualPaddingEdges, -1.f));
							}
							else if (pVertex1->getPosition().x > cellPosition.x && pVertex2->getPosition().x > cellPosition.x) {//right
								triplets.push_back(Eigen::Triplet<Scalar>(pDualCell->getID() - m_dualPaddingCells, halfEdge->getEdge()->getID() - m_dualPaddingEdges, 1.f));
							}
						}
					}
				}
			}

			Eigen::SparseMatrix<Scalar> delta1Operator(m_pDualMesh->getCells().size(), m_pDualMesh->getElements().size());
			delta1Operator.setFromTriplets(triplets.begin(), triplets.end());
			return delta1Operator;
		}
		
		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getDualDelta1Operator2() {
			auto pVertices = m_pQuadGridMesh->getVertices();

			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(4 * pVertices.size()); //Reserves 4 entries for every dual cell

			#pragma omp parallel for
			for (auto pVertex : pVertices) {
				if (pVertex->getDualElement2D() != nullptr) {
					auto pDualCell = pVertex->getDualElement2D();
					auto halfCell = pDualCell->getHalfCells().first;
					auto halfEdges = halfCell->getHalfEdges();
					for (auto halfEdge : halfEdges) {
						auto pVertex1 = halfEdge->getVertices().first;
						auto pVertex2 = halfEdge->getVertices().second;

						if (pVertex1->getDualElement2D() != nullptr && pVertex2->getDualElement2D() != nullptr) {
							if (!pVertex1->getDualElement2D()->isSolid() && !pVertex1->getDualElement2D()->isSolid()) {
								//This is a bad solution: half edges should be used instead
								auto cellPosition = pDualCell->getCentroid();
								if (pVertex1->getPosition().y < cellPosition.y && pVertex2->getPosition().y < cellPosition.y) {//bottom
									triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], halfEdge->getEdge()->getID() - m_dualPaddingEdges, 1.f));
								}
								else if (pVertex1->getPosition().y > cellPosition.y && pVertex2->getPosition().y > cellPosition.y) {//top
									triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], halfEdge->getEdge()->getID() - m_dualPaddingEdges, -1.f));
								}
								else if (pVertex1->getPosition().x < cellPosition.x && pVertex2->getPosition().x < cellPosition.x) {//left
									triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], halfEdge->getEdge()->getID() - m_dualPaddingEdges, -1.f));
								}
								else if (pVertex1->getPosition().x > cellPosition.x && pVertex2->getPosition().x > cellPosition.x) {//right
									triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], halfEdge->getEdge()->getID() - m_dualPaddingEdges, 1.f));
								}
							}
						}
						else {//border edges
							//This is a bad solution: half edges should be used instead
							auto cellPosition = pDualCell->getCentroid();
							if (pVertex1->getPosition().y < cellPosition.y && pVertex2->getPosition().y < cellPosition.y) {//bottom
								triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], halfEdge->getEdge()->getID() - m_dualPaddingEdges, 1.f));
							}
							else if (pVertex1->getPosition().y > cellPosition.y && pVertex2->getPosition().y > cellPosition.y) {//top
								triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], halfEdge->getEdge()->getID() - m_dualPaddingEdges, -1.f));
							}
							else if (pVertex1->getPosition().x < cellPosition.x && pVertex2->getPosition().x < cellPosition.x) {//left
								triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], halfEdge->getEdge()->getID() - m_dualPaddingEdges, -1.f));
							}
							else if (pVertex1->getPosition().x > cellPosition.x && pVertex2->getPosition().x > cellPosition.x) {//right
								triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], halfEdge->getEdge()->getID() - m_dualPaddingEdges, 1.f));
							}
						}
					}
				}
			}

			Eigen::SparseMatrix<Scalar> delta1Operator(m_pDualMesh->getCells().size(), m_pDualMesh->getElements().size());
			delta1Operator.setFromTriplets(triplets.begin(), triplets.end());
			return delta1Operator;
		}

		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getPrimalHodge1Operator() {
			auto pEdges = m_pQuadGridMesh->getEdges();

			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pEdges.size());

			//Construction of the matrix
			#pragma omp parallel for
			for (auto pEdge : pEdges) {
				int eId = pEdge->getID();

				if (!(pEdge->getDualElement2D() == nullptr)) {
					Scalar entry = pEdge->getDualElement2D()->getLength() / pEdge->getLength();
					triplets.push_back(Eigen::Triplet<Scalar>(eId, eId, entry));
				}
			}

			Eigen::SparseMatrix<Scalar> Hodge1Operator(pEdges.size(), pEdges.size());
			Hodge1Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge1Operator;
		}

		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getDualHodge1Operator() {
			auto pEdges = m_pDualMesh->getElements();

			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pEdges.size());

			//Construction of the matrix
			#pragma omp parallel for
			for (auto pEdge : pEdges) {
				//Original ID
				int eId = pEdge->getID() - m_dualPaddingEdges;

				if (!(pEdge->getDualElement2D() == nullptr)) {
					Scalar entry = pEdge->getDualElement2D()->getLength() / pEdge->getLength();
					triplets.push_back(Eigen::Triplet<Scalar>(eId, eId, entry));
				}
			}

			Eigen::SparseMatrix<Scalar> Hodge1Operator(pEdges.size(), pEdges.size());
			Hodge1Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge1Operator;
		}

		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getPrimalHodge0Operator() {
			auto pVertices = m_pQuadGridMesh->getVertices();

			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pVertices.size());

			bool internalObstacle = false;
			//Construction of the matrix
			#pragma omp parallel for
			for (auto pVertex : pVertices) {
				if (pVertex->getDualElement2D() != nullptr) {
					if (!pVertex->isInternalBorder()) {
						Scalar entry = m_pQuadGridMesh->getGridSpacing() * m_pQuadGridMesh->getGridSpacing(); //Works only for voxelized approach
						triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], m_primalVertexIDToNewVertexID[pVertex->getID()], entry));
					}
					else {
						if (!internalObstacle) {
							Scalar entry = m_pQuadGridMesh->getGridSpacing() * m_pQuadGridMesh->getGridSpacing(); //Works only for voxelized approach
							triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], m_primalVertexIDToNewVertexID[pVertex->getID()], entry));
							internalObstacle = true;
						}
					}
				}
			}

			Eigen::SparseMatrix<Scalar> Hodge0Operator(pVertices.size(), pVertices.size());
			Hodge0Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge0Operator;
		}
		
		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getDualHodge2Operator() {
			auto pDualCells = m_pDualMesh->getCells();
			auto gridSpacing = m_pQuadGridMesh->getGridSpacing();

			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pDualCells.size());

			#pragma omp parallel for
			for (auto pDualCell : pDualCells) {
				if (pDualCell->getDualElement2D() != nullptr) {
					triplets.push_back(Eigen::Triplet<Scalar>(pDualCell->getID() - m_dualPaddingCells, pDualCell->getID() - m_dualPaddingCells, 1.f / (gridSpacing * gridSpacing)));
				}
			}
			Eigen::SparseMatrix<Scalar> Hodge2Operator(pDualCells.size(), pDualCells.size());
			Hodge2Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge2Operator;
		}
		
		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getDualHodge2Operator2() {
			auto pDualCells = m_pDualMesh->getCells();
			auto gridSpacing = m_pQuadGridMesh->getGridSpacing();

			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pDualCells.size());
			
			bool internalObstacle = false;
			#pragma omp parallel for
			for (auto pDualCell : pDualCells) {
				if (pDualCell->getDualElement2D() != nullptr) {
					if(!pDualCell->getDualElement2D()->isInternalBorder())
						triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], 1.f / (gridSpacing * gridSpacing)));
					else {
						if (!internalObstacle) {
							triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], 1.f / (gridSpacing * gridSpacing)));
							internalObstacle = true;
						}
					}
				}
			}
			Eigen::SparseMatrix<Scalar> Hodge2Operator(pDualCells.size(), pDualCells.size());
			Hodge2Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge2Operator;
		}
		
		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getDualHodge2Operator3() {
			auto pDualCells = m_pDualMesh->getCells();
			auto gridSpacing = m_pQuadGridMesh->getGridSpacing();

			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pDualCells.size());
			
			bool internalObstacle = false;
			#pragma omp parallel for
			for (auto pDualCell : pDualCells) {
				if (pDualCell->getDualElement2D() != nullptr) {
					if (!pDualCell->getDualElement2D()->isInternalBorder()) {
						if(pDualCell->getDualElement2D()->getBoundaryCondition() != noSlip)
							triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], 1.f / (gridSpacing * gridSpacing)));
						else
							triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], 2.f / (gridSpacing * gridSpacing)));
					}
					else {
						if (!internalObstacle) {
							triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], m_primalVertexIDToNewVertexID[pDualCell->getDualElement2D()->getID()], 1.f / (gridSpacing * gridSpacing)));
							internalObstacle = true;
						}
					}
				}
			}
			Eigen::SparseMatrix<Scalar> Hodge2Operator(pDualCells.size(), pDualCells.size());
			Hodge2Operator.setFromTriplets(triplets.begin(), triplets.end());
			return Hodge2Operator;
		}

		template<typename SolverType>
		Eigen::SparseMatrix<Scalar> VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::getViscosityMatrix(bool boundaryViscosity) {
			auto pVertices = m_pQuadGridMesh->getVertices();

			//Triplets for the construction of the sparse matrix
			std::vector<Eigen::Triplet<Scalar>> triplets;
			triplets.reserve(pVertices.size());

			bool internalObstacle = false;
			//Construction of the matrix
			#pragma omp parallel for
			for (auto pVertex : pVertices) {
				if (pVertex->getDualElement2D() != nullptr) {
					if (!pVertex->isInternalBorder()) {
						triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], m_primalVertexIDToNewVertexID[pVertex->getID()], m_viscosity));
					}
					else {
						if (!internalObstacle && boundaryViscosity) {
							triplets.push_back(Eigen::Triplet<Scalar>(m_primalVertexIDToNewVertexID[pVertex->getID()], m_primalVertexIDToNewVertexID[pVertex->getID()], m_viscosity));
							internalObstacle = true;
						}
					}
				}
			}

			Eigen::SparseMatrix<Scalar> viscosityMatrix(pVertices.size(), pVertices.size());
			viscosityMatrix.setFromTriplets(triplets.begin(), triplets.end());
			return viscosityMatrix;
		}
		#pragma endregion

		#pragma region SolverFonctions
		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::updatePoissonMatrix() {
			Eigen::SparseMatrix<Scalar> poissonMatrix = m_primalHodge0Operator.cwiseInverse() * m_primalDelta0Operator.transpose() * m_primalHodge1Operator * m_primalDelta0Operator;
			Scalar dt = 0.008;
			Eigen::SparseMatrix<Scalar> biharmonicOperator = dt * m_viscosityMatrix * poissonMatrix * poissonMatrix + poissonMatrix;
			/*cout << "Original Poisson Matrix" << endl;
			cout << poissonMatrix << std::endl << endl;*/

			Eigen::SparseMatrix<Scalar> poissonMatrixReduced = biharmonicOperator.block(0, 0, m_primalInternalVertices, m_primalInternalVertices);
			/*cout << "Reduced Poisson Matrix" << endl;
			cout << poissonMatrixReduced << endl << endl;*/

			poissonMatrixReduced = poissonMatrixReduced + m_difference.block(0, 0, m_primalInternalVertices, m_primalInternalVertices);
			//cout << poissonMatrixReduced << endl;

			typename PoissonSolver::params_t params;
			ParentClass::m_pPoissonSolver = make_shared<Poisson::PoissonSolver>(params, make_shared<Eigen::SparseMatrix<Scalar>>(poissonMatrixReduced));
		}

		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::update(Scalar dt) {
			ParentClass::ParentClass::m_totalSimulationTimer.start();

			/** First time-step: Do an additional solve to get div-free velocities from boundary conditions everywhere */
			if (PhysicsCore<Vector2>::getInstance()->getElapsedTime() < dt) {
				if (m_TaylorVortices) {
					//Taylor vortices
					auto center = (m_pQuadGridMesh->getBounds().first + m_pQuadGridMesh->getBounds().second) / 2.;
					auto pEdges = m_pQuadGridMesh->getEdges();
					Scalar U = 1.;
					Scalar a = 0.3;
					for (auto pEdge : pEdges) {
						if (!(pEdge->getVertex1()->isBorder() && pEdge->getVertex2()->isBorder())) {
							auto position = pEdge->getCentroid();
							Scalar temp1 = U * exp(0.5 * (1 - ((position.x - (center.x + 0.4)) * (position.x - (center.x + 0.4)) + (position.y - center.y) * (position.y - center.y)) / (a * a)));
							Scalar temp2 = U * exp(0.5 * (1 - ((position.x - (center.x - 0.4)) * (position.x - (center.x - 0.4)) + (position.y - center.y) * (position.y - center.y)) / (a * a)));
							if (pEdge->getType() == xAlignedEdge) {
								Scalar& velocity = pEdge->OwnCustomAttribute<Scalar>::getAttribute("auxVelocity");
								velocity += temp1 * (position.x - (center.x + 0.4)) + temp2 * (position.x - (center.x - 0.4));
							}
							else if (pEdge->getType() == yAlignedEdge) {
								Scalar& velocity = pEdge->OwnCustomAttribute<Scalar>::getAttribute("auxVelocity");
								velocity += -temp1 * (position.y - center.y) - temp2 * (position.y - center.y);
							}
						}
					}
				}
				ParentClass::doFirstTimestep(dt);
				return;
			}
			else {
				/** If not initial time-step, apply forcing functions into the grid */
				if (ParentClass::ParentClass::m_pAdvectionSolver->getParams().advectionCategory == LagrangianAdvection) {
					//Update particles velocities just before the time-step, after buoyancy
					ParentClass::ParentClass::m_pAdvectionSolver->postProjectionUpdate(dt);
				}
			}

			/** Advection */
			ParentClass::ParentClass::m_advectionTimer.start();
			cout << "Energy 1: " << measureAuxEnergy(m_pQuadGridMesh) << "     " << measureEnergy(m_pQuadGridMesh) << endl;
			ParentClass::ParentClass::m_pAdvectionSolver->advect(dt);
			cout << "Energy 2: " << measureAuxEnergy(m_pQuadGridMesh) << "     " << measureEnergy(m_pQuadGridMesh) << endl;

			ParentClass::ParentClass::m_advectionTimer.stop();
			ParentClass::ParentClass::m_advectionTime = ParentClass::m_advectionTimer.secondsElapsed();
			cout << "Energy 3: " << measureAuxEnergy(m_pQuadGridMesh) << "     " << measureEnergy(m_pQuadGridMesh) << endl;
			ParentClass::applyForces(dt);
			cout << "Energy 4: " << measureAuxEnergy(m_pQuadGridMesh) << "     " << measureEnergy(m_pQuadGridMesh) << endl;

			/** If particle-based velocities, update their velocities at the beginning of the time-step*/

			/** Updates objects and grid representations */
			ParentClass::updateImmersedObjects(dt);

			/** Solve pressure */
			ParentClass::ParentClass::m_solvePressureTimer.start();
			cout << "Energy 5: " << measureAuxEnergy(m_pQuadGridMesh) << "     " << measureEnergy(m_pQuadGridMesh) << endl;
			project(dt, false);
			cout << "Energy 6: " << measureAuxEnergy(m_pQuadGridMesh) << "     " << measureEnergy(m_pQuadGridMesh) << endl;
			ParentClass::ParentClass::m_solvePressureTimer.stop();
			ParentClass::ParentClass::m_solvePressureTime = ParentClass::m_solvePressureTimer.secondsElapsed();

			/** Post projection update: Usually means advection of density fields and update particles attributes */
			cout << "Energy 7: " << measureAuxEnergy(m_pQuadGridMesh) << "     " << measureEnergy(m_pQuadGridMesh) << endl;
			ParentClass::ParentClass::m_pAdvectionSolver->postProjectionUpdate(dt);
			cout << "Energy 8: " << measureAuxEnergy(m_pQuadGridMesh) << "     " << measureEnergy(m_pQuadGridMesh) << endl;
			cout << "-----------------------------------------" << endl;

			ParentClass::ParentClass::m_totalSimulationTimer.stop();
			ParentClass::ParentClass::m_totalSimulationTime = ParentClass::m_totalSimulationTimer.secondsElapsed();
		}

		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::project(Scalar dt, bool firstCall) {
			computeVorticity(dt, firstCall);
			solveStreamfunction(dt);
			applyStreamfunction(dt);
		}

		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::computeVorticity(Scalar dt, bool firstCall) {
			if (PhysicsCore<Vector2>::getInstance()->getElapsedTime() < dt && firstCall) {
				//Store the velocitites in an Eigen vector
				auto pDualEdges = m_pDualMesh->getElements();
				Eigen::VectorXf velocities(pDualEdges.size());
				#pragma omp parallel for
				for (auto pDualEdge : pDualEdges) {
					if (pDualEdge->getDualElement2D() != nullptr)
						velocities(pDualEdge->getID() - m_dualPaddingEdges) = OwnCustomAttribute<Scalar>::get(pDualEdge->getDualElement2D())->getAttribute("auxVelocity") * pDualEdge->getDualElement2D()->getLength(); //This is the velocity after interpolation
					else
						velocities(pDualEdge->getID() - m_dualPaddingEdges) = 0.f; //Won't be considered anyway because of the Hodge1Operator
				}

				//Compute the vorticities
				auto pDualCells = m_pDualMesh->getCells();
				Eigen::VectorXf auxVorticities(pDualCells.size());
				auxVorticities = m_dualHodge2Operator * m_dualDelta1Operator * m_dualHodge1Operator.cwiseInverse() * velocities;

				//Store the auxVorticities back into the grid (and correct them by applying BC)
				auto pVertices = m_pQuadGridMesh->getVertices();
				#pragma omp parallel for
				for (auto pVertex : pVertices) {
					if (!pVertex->isBorder() && !pVertex->isSolid() && !pVertex->isInternalBorder())
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("auxVorticity", auxVorticities(pVertex->getDualElement2D()->getID() - m_dualPaddingCells));
					else if (pVertex->getBoundaryCondition() == outflow || pVertex->getBoundaryCondition() == inflow)
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("auxVorticity", auxVorticities(pVertex->getDualElement2D()->getID() - m_dualPaddingCells));
				}
			}
			//Apply BC
			auxVorticityBC();
		}

		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::solveStreamfunction(Scalar dt) {
			//Get the auxVorticities from the Grid
			auto pVertices = m_pQuadGridMesh->getVertices();
			shared_ptr<Eigen::VectorXf> pAuxVorticities = make_shared<Eigen::VectorXf>(m_primalInternalVertices);
			#pragma omp parallel for
			for (auto pVertex : pVertices) {
				if(!pVertex->isSolid() && !pVertex->isInternalBorder() && (!pVertex->isBorder() || pVertex->getBoundaryCondition() == outflow))
					(*pAuxVorticities)(m_primalVertexIDToNewVertexID[pVertex->getID()]) = OwnCustomAttribute<Scalar>::get(pVertex)->getAttribute("auxVorticity"); //Should be corrected with the contributions of the streamfunctions of the boundaries!
				if (pVertex->isInternalBorder())
					(*pAuxVorticities)(m_primalVertexIDToNewVertexID[pVertex->getID()]) = 0.f;
			}
		
			//Compute the streamfunctions for boundary vertices
			streamfunctionBC();

			//Correction of the RHS
			{
				//External boundaries
				auto gridSpacing = m_pQuadGridMesh->getGridSpacing();
				auto edges = m_pQuadGridMesh->getEdges();

				////----------------------------- Correction of the RHS with DEC ------------------------------------------------------
				Eigen::VectorXf streamfunctions(m_primalInternalVertices + m_primalBorderVertices);
				for (auto pVertex : pVertices) {
					if (pVertex->isBorder() && pVertex->getBoundaryCondition() != outflow) {
						streamfunctions(m_primalVertexIDToNewVertexID[pVertex->getID()]) = OwnCustomAttribute<Scalar>::get(pVertex)->getAttribute("streamfunction");
					}
					else {
						if (!pVertex->isSolid())
							streamfunctions(m_primalVertexIDToNewVertexID[pVertex->getID()]) = 0.f;
					}
				}
				auto rhsCorrection = m_difference * streamfunctions;
				(*pAuxVorticities) -= rhsCorrection;

				//Internal boundaries
				{
					for (auto pVertex : pVertices) {
						if (pVertex->isInternalBorder())
							(*pAuxVorticities)(m_primalVertexIDToNewVertexID[pVertex->getID()]) = 0.f;
					}


					if (m_internalBoundary == noSlip) {
						for (auto pEdge : edges) {
							auto pV1 = pEdge->getVertex1();
							auto pV2 = pEdge->getVertex2();
							if (!pV1->isInternalBorder() && !pV1->isSolid() && pV2->isInternalBorder()) {
								(*pAuxVorticities)(m_primalVertexIDToNewVertexID[pV2->getID()]) += 0.5f * OwnCustomAttribute<Scalar>::get(pV1)->getAttribute("auxVorticity");
							}

							if (pV1->isInternalBorder() && !pV2->isInternalBorder() && !pV2->isSolid()) {
								(*pAuxVorticities)(m_primalVertexIDToNewVertexID[pV1->getID()]) += 0.5f * OwnCustomAttribute<Scalar>::get(pV2)->getAttribute("auxVorticity");
							}
						}
					}
					else if (m_internalBoundary == freeSlip) {
						auto pVertices = m_pQuadGridMesh->getVertices();
						bool internalBorder = false;
						for (auto pVertex : pVertices) {
							if (!internalBorder && pVertex->isInternalBorder()) {
								(*pAuxVorticities)(m_primalVertexIDToNewVertexID[pVertex->getID()]) = 0.f; //Free slip
								internalBorder = true;
							}
						}
					}
				}
			}

			//Compute the streamfunctions
			shared_ptr<Eigen::VectorXf> pStreamfunctions = make_shared<Eigen::VectorXf>(m_primalInternalVertices);
			ParentClass::ParentClass::m_pPoissonSolver->solve(pAuxVorticities, pStreamfunctions);

			//Store back the streamfunctions into the Grid
			#pragma omp parallel for
			for (auto pVertex : pVertices) {
				if (!pVertex->isSolid() && (!pVertex->isBorder() || pVertex->getBoundaryCondition() == outflow))
					OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", (*pStreamfunctions)(m_primalVertexIDToNewVertexID[pVertex->getID()]));
			}
		}

		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::applyStreamfunction(Scalar dt) {
			//Copy the streamfunction values
			auto pDualCells = m_pDualMesh->getCells();
			Eigen::VectorXf streamfunctions(pDualCells.size());

			#pragma omp parallel for
			for (auto pDualCell : pDualCells) {
				streamfunctions(pDualCell->getID() - m_dualPaddingCells) = OwnCustomAttribute<Scalar>::get(pDualCell->getDualElement2D())->getAttribute("streamfunction");
			}

			//Computing the new velocities
			auto velocities = m_dualDelta1Operator.transpose() * streamfunctions;
			auto pDualEdges = m_pDualMesh->getElements();
			#pragma omp parallel for
			for (auto pDualEdge : pDualEdges) {
				if (pDualEdge->getDualElement2D() != nullptr) {
					OwnCustomAttribute<Scalar>::get(pDualEdge->getDualElement2D())->setAttribute("velocity", velocities(pDualEdge->getID() - m_dualPaddingEdges) / pDualEdge->getDualElement2D()->getLength());
				}
			}

			//Computing the new vorticities
			auto vorticities = m_dualHodge2Operator * m_dualDelta1Operator * m_dualHodge1Operator.cwiseInverse() * m_dualDelta1Operator.transpose() * streamfunctions;
			#pragma omp parallel for
			for (auto pDualCell : pDualCells) {
				OwnCustomAttribute<Scalar>::get(pDualCell->getDualElement2D())->setAttribute("vorticity", vorticities(pDualCell->getID() - m_dualPaddingCells));
			}
			//Applying BC (also internal)
			vorticityBC();

			//Check the integral
			Scalar gridSpacing = m_pQuadGridMesh->getGridSpacing();
			auto pEdges = m_pQuadGridMesh->getEdges();
			Scalar integralSum = 0;
			for (auto pEdge : pEdges) {
				auto pV1 = pEdge->getVertex1();
				auto pV2 = pEdge->getVertex2();
				if (pV1->isInternalBorder() && !pV2->isInternalBorder() && !pV2->isSolid()) {
					integralSum += OwnCustomAttribute<Scalar>::get(pV1)->getAttribute("vorticity") - OwnCustomAttribute<Scalar>::get(pV2)->getAttribute("vorticity");
				}
				if (pV2->isInternalBorder() && !pV1->isInternalBorder() && !pV1->isSolid()) {
					integralSum += OwnCustomAttribute<Scalar>::get(pV2)->getAttribute("vorticity") - OwnCustomAttribute<Scalar>::get(pV1)->getAttribute("vorticity");
				}
			}
			cout << integralSum << endl;
		}

		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::auxVorticityBC() {
			auto gridSpacing = m_pQuadGridMesh->getGridSpacing();
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();
			
			//South boundary
			if (m_southBoundary == freeSlip) {
				for (int i = 1; i < gridDimensions.x; ++i) {
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(i, 0))->setAttribute("auxVorticity", 0.f);
				}
			}
			else if (m_southBoundary == noSlip) {
				for (int i = 1; i < gridDimensions.x; ++i) {
					auto velocity = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getEdges(dimensions_t(i, 0), yAlignedEdge).front())->getAttribute("velocity");
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(i, 0))->setAttribute("auxVorticity", -2.f * velocity / gridSpacing);
				}
			}
			
			//North boundary
			if (m_northBoundary == freeSlip) {
				for (int i = 1; i < gridDimensions.x; ++i) {
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(i, gridDimensions.y))->setAttribute("auxVorticity", 0.f);
				}
			}
			else if (m_northBoundary == noSlip) {
				for (int i = 1; i < gridDimensions.x; ++i) {
					auto velocity = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getEdges(dimensions_t(i, gridDimensions.y - 1), yAlignedEdge).front())->getAttribute("velocity");
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(i, gridDimensions.y))->setAttribute("auxVorticity", 2.f * velocity / gridSpacing);
				}
			}
		
			//West boundary
			if (m_westBoundary == freeSlip) {
				for (int j = 0; j <= gridDimensions.y; ++j) {
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(0, j))->setAttribute("auxVorticity", 0.f);
				}
			}
			else if (m_westBoundary == noSlip) {
				for (int j = 0; j <= gridDimensions.y; ++j) {
					auto velocity = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getEdges(dimensions_t(0, j), xAlignedEdge).front())->getAttribute("velocity");
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(0, j))->setAttribute("auxVorticity", 2.f * velocity / gridSpacing);
				}
			}

			//East boundary
			if (m_eastBoundary == freeSlip) {
				for (int j = 0; j <= gridDimensions.y; ++j) {
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(gridDimensions.x, j))->setAttribute("auxVorticity", 0.f);
				}
			}
			else if (m_eastBoundary == noSlip) {
				for (int j = 0; j <= gridDimensions.y; ++j) {
					auto velocity = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getEdges(dimensions_t(gridDimensions.x - 1, j), xAlignedEdge).front())->getAttribute("velocity");
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(gridDimensions.x, j))->setAttribute("auxVorticity", -2.f * velocity / gridSpacing);
				}
			}

			//Internal boundary
			if (m_internalBoundary == freeSlip) {
				auto pVertices = m_pQuadGridMesh->getVertices();
				for (auto pVertex : pVertices) {
					if (pVertex->isInternalBorder()) {
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("auxVorticity", 0.f);
					}
				}
			}
			else if (m_internalBoundary == noSlip) {
				//Not needed
			}
		}
		
		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::vorticityBC() {
			//External BC
			auto gridSpacing = m_pQuadGridMesh->getGridSpacing();
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();

			//South boundary
			if (m_southBoundary == freeSlip) {
				for (int i = 1; i < gridDimensions.x; ++i) {
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(i, 0))->setAttribute("vorticity", 0.f);
				}
			}
			else if (m_southBoundary == noSlip) {
				for (int i = 1; i < gridDimensions.x; ++i) {
					auto velocity = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getEdges(dimensions_t(i, 0), yAlignedEdge).front())->getAttribute("velocity");
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(i, 0))->setAttribute("vorticity", -2.f * velocity / gridSpacing);
				}
			}
			
			//North boundary
			if (m_northBoundary == freeSlip) {
				for (int i = 1; i < gridDimensions.x; ++i) {
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(i, gridDimensions.y))->setAttribute("vorticity", 0.f);
				}
			}
			else if (m_northBoundary == noSlip) {
				for (int i = 1; i < gridDimensions.x; ++i) {
					auto velocity = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getEdges(dimensions_t(i, gridDimensions.y - 1), yAlignedEdge).front())->getAttribute("velocity");
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(i, gridDimensions.y))->setAttribute("vorticity", 2.f * velocity / gridSpacing);
				}
			}
		
			//West boundary
			if (m_westBoundary == freeSlip) {
				for (int j = 0; j <= gridDimensions.y; ++j) {
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(0, j))->setAttribute("vorticity", 0.f);
				}
			}
			else if (m_westBoundary == noSlip) {
				for (int j = 0; j <= gridDimensions.y; ++j) {
					auto velocity = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getEdges(dimensions_t(0, j), xAlignedEdge).front())->getAttribute("velocity");
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(0, j))->setAttribute("vorticity", 2.f * velocity / gridSpacing);
				}
			}

			//East boundary
			if (m_eastBoundary == freeSlip) {
				for (int j = 0; j <= gridDimensions.y; ++j) {
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(gridDimensions.x, j))->setAttribute("vorticity", 0.f);
				}
			}
			else if (m_eastBoundary == noSlip) {
				for (int j = 0; j <= gridDimensions.y; ++j) {
					auto velocity = OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getEdges(dimensions_t(gridDimensions.x - 1, j), xAlignedEdge).front())->getAttribute("velocity");
					OwnCustomAttribute<Scalar>::get(m_pQuadGridMesh->getVertex(gridDimensions.x, j))->setAttribute("vorticity", -2.f * velocity / gridSpacing);
				}
			}

			//Internal BC
			if (m_internalBoundary == noSlip) {
				//Set the vorticities for internalBoundaryVertices to 0
				auto pVertices = m_pQuadGridMesh->getVertices();
				for (auto pVertex : pVertices) {
					if(pVertex->isInternalBorder())
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("vorticity", 0.f);
				}

				//Add contributions from neighbors
				auto pEdges = m_pQuadGridMesh->getEdges();
				for (auto pEdge : pEdges) {
					auto pV1 = pEdge->getVertex1();
					auto pV2 = pEdge->getVertex2();

					if (pV1->isInternalBorder() && !pV2->isInternalBorder() && !pV2->isSolid()) {
						if (pV1->isInternalCorner()) {
							auto streamfunction1 = OwnCustomAttribute<Scalar>::get(pV1)->getAttribute("streamfunction");
							auto streamfunction2 = OwnCustomAttribute<Scalar>::get(pV2)->getAttribute("streamfunction");
							Scalar vorticity = OwnCustomAttribute<Scalar>::get(pV1)->getAttribute("vorticity");
							vorticity += (streamfunction1 - streamfunction2) / (gridSpacing * gridSpacing);
							OwnCustomAttribute<Scalar>::get(pV1)->setAttribute("vorticity", vorticity);
						}
						else {
							auto streamfunction1 = OwnCustomAttribute<Scalar>::get(pV1)->getAttribute("streamfunction");
							auto streamfunction2 = OwnCustomAttribute<Scalar>::get(pV2)->getAttribute("streamfunction");
							Scalar vorticity = OwnCustomAttribute<Scalar>::get(pV1)->getAttribute("vorticity");
							vorticity += 2. * (streamfunction1 - streamfunction2) / (gridSpacing * gridSpacing);
							OwnCustomAttribute<Scalar>::get(pV1)->setAttribute("vorticity", vorticity);
						}
					}
					if (!pV1->isInternalBorder() && !pV1->isSolid() && pV2->isInternalBorder()) {
						if (pV2->isInternalCorner()) {
							auto streamfunction1 = OwnCustomAttribute<Scalar>::get(pV1)->getAttribute("streamfunction");
							auto streamfunction2 = OwnCustomAttribute<Scalar>::get(pV2)->getAttribute("streamfunction");
							Scalar vorticity = OwnCustomAttribute<Scalar>::get(pV2)->getAttribute("vorticity");
							vorticity += (streamfunction2 - streamfunction1) / (gridSpacing * gridSpacing);
							OwnCustomAttribute<Scalar>::get(pV2)->setAttribute("vorticity", vorticity);
						}
						else {
							auto streamfunction1 = OwnCustomAttribute<Scalar>::get(pV1)->getAttribute("streamfunction");
							auto streamfunction2 = OwnCustomAttribute<Scalar>::get(pV2)->getAttribute("streamfunction");
							Scalar vorticity = OwnCustomAttribute<Scalar>::get(pV2)->getAttribute("vorticity");
							vorticity += 2. * (streamfunction2 - streamfunction1) / (gridSpacing * gridSpacing);
							OwnCustomAttribute<Scalar>::get(pV2)->setAttribute("vorticity", vorticity);
						}
					}
				}
			}
			else if (m_internalBoundary == freeSlip) {
				auto pVertices = m_pQuadGridMesh->getVertices();
				for (auto pVertex : pVertices) {
					if (pVertex->isInternalBorder()) {
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("vorticity", 0.f);
					}
				}
			}
		}

		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::applyStreamfunctionBC(shared_ptr<Vertex<VectorType>> pVertex, Scalar& previousStreamfunction) {
			auto gridSpacing = m_pQuadGridMesh->getGridSpacing();
			if (pVertex->getBoundaryCondition() == freeSlip || pVertex->getBoundaryCondition() == noSlip) {
				bool condition1 = (m_southBoundary == inflow && pVertex->getPosition().x == m_pQuadGridMesh->getBounds().second[0] && pVertex->getPosition().y == m_pQuadGridMesh->getBounds().first[1]);
				bool condition2 = (m_northBoundary == inflow && pVertex->getPosition().x == m_pQuadGridMesh->getBounds().first[0] && pVertex->getPosition().y == m_pQuadGridMesh->getBounds().second[1]);
				bool condition3 = (m_southBoundary == inflow2 && pVertex->getPosition().x == m_pQuadGridMesh->getBounds().second[0] && pVertex->getPosition().y == m_pQuadGridMesh->getBounds().first[1]);
				bool condition4 = (m_northBoundary == inflow2 && pVertex->getPosition().x == m_pQuadGridMesh->getBounds().first[0] && pVertex->getPosition().y == m_pQuadGridMesh->getBounds().second[1]);
				if (condition1 || condition2) {
					Scalar newStreamfunction = -m_inflowVelocity * gridSpacing + previousStreamfunction;
					OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", newStreamfunction);
					previousStreamfunction = newStreamfunction;
				}
				else if (condition3) {
					Scalar L = m_pQuadGridMesh->getBounds().second[0] - m_pQuadGridMesh->getBounds().first[0];
					Scalar x = pVertex->getPosition().x - m_pQuadGridMesh->getGridSpacing() * 0.5f;
					Scalar velocity = 4.f * m_inflowVelocity * x * (L - x) / (L * L);
					Scalar newStreamfunction = -velocity * gridSpacing + previousStreamfunction;
					OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", newStreamfunction);
					previousStreamfunction = newStreamfunction;
				}
				else if (condition4) {
					Scalar L = m_pQuadGridMesh->getBounds().second[0] - m_pQuadGridMesh->getBounds().first[0];
					Scalar x = pVertex->getPosition().x + m_pQuadGridMesh->getGridSpacing() * 0.5f;
					Scalar velocity = 4.f * m_inflowVelocity * x * (L - x) / (L * L);
					Scalar newStreamfunction = -velocity * gridSpacing + previousStreamfunction;
					OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", newStreamfunction);
					previousStreamfunction = newStreamfunction;
				}
				else
					OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", previousStreamfunction);
			}
			else if (pVertex->getBoundaryCondition() == inflow) {
				if (m_inflowVelocity != 0.) {
					Scalar newStreamfunction = -m_inflowVelocity * gridSpacing + previousStreamfunction;
					OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", newStreamfunction);
					previousStreamfunction = newStreamfunction;
				}
			}
			else if(pVertex->getBoundaryCondition() == inflow2){
				if(m_inflowVelocity != 0.){
					if (m_southBoundary == inflow2) {
						Scalar L = m_pQuadGridMesh->getBounds().second[0] - m_pQuadGridMesh->getBounds().first[0];
						Scalar x = pVertex->getPosition().x - m_pQuadGridMesh->getGridSpacing() * 0.5f;
						Scalar velocity = 4.f * m_inflowVelocity * x * (L - x) / (L * L);
						Scalar newStreamfunction = -velocity * gridSpacing + previousStreamfunction;
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", newStreamfunction);
						previousStreamfunction = newStreamfunction;
					}
					else if (m_northBoundary == inflow2) {
						Scalar L = m_pQuadGridMesh->getBounds().second[0] - m_pQuadGridMesh->getBounds().first[0];
						Scalar x = pVertex->getPosition().x + m_pQuadGridMesh->getGridSpacing() * 0.5f;
						Scalar velocity = 4.f * m_inflowVelocity * x * (L - x) / (L * L);
						Scalar newStreamfunction = -velocity * gridSpacing + previousStreamfunction;
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", newStreamfunction);
						previousStreamfunction = newStreamfunction;
					}
				}
			}
		}

		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::streamfunctionBC() {
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();
			//We visit the boundary in counter clockwise order. For the moment only one outflow condition is allowed. If there is one, we start to enforce BC from the boundary after the outflow BC.
			if (m_northBoundary == outflow) {
				Scalar previousStreamfunction = 0.f; //Assume that the streamfunction on the last vertex of the otuflow is 0
				
				//West boundary
				for (int j = gridDimensions.y; j >= 0; --j) {
					auto pVertex = m_pQuadGridMesh->getVertex(0, j);
					if(j == gridDimensions.y)
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", previousStreamfunction);
					else
						applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//South boundary
				for (int i = 1; i < gridDimensions.x; ++i) {
					auto pVertex = m_pQuadGridMesh->getVertex(i, 0);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//East boundary
				for (int j = 0; j <= gridDimensions.y; ++j) {
					auto pVertex = m_pQuadGridMesh->getVertex(gridDimensions.x, j);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
			}
			else if (m_westBoundary == outflow) {
				Scalar previousStreamfunction = 0.; //Assume that the streamfunction on the last vertex of the outflow is 0

				//South boundary
				for (int i = 1; i <= gridDimensions.x; ++i) {
					auto pVertex = m_pQuadGridMesh->getVertex(i, 0);
					if (i == 1)
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", previousStreamfunction);
					else
						applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//East boundary
				for (int j = 1; j <= gridDimensions.y; ++j) {
					auto pVertex = m_pQuadGridMesh->getVertex(gridDimensions.x, j);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//North boundary
				for (int i = gridDimensions.x - 1; i >= 0; --i) {
					auto pVertex = m_pQuadGridMesh->getVertex(i, gridDimensions.y);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
			}
			else if (m_southBoundary == outflow) {
				Scalar previousStreamfunction = 0.; //Assume that the streamfunction on the last vertex of the otuflow is 0

				//East boundary
				for (int j = 0; j <= gridDimensions.y; ++j) {
					auto pVertex = m_pQuadGridMesh->getVertex(gridDimensions.x, j);
					if (j == 1)
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", previousStreamfunction);
					else
						applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//North boundary
				for (int i = gridDimensions.x - 1; i > 0; --i) {
					auto pVertex = m_pQuadGridMesh->getVertex(i, gridDimensions.y);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//West boundary
				for (int j = gridDimensions.y; j >= 0; --j) {
					auto pVertex = m_pQuadGridMesh->getVertex(0, j);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
			}
			else if (m_eastBoundary == outflow) {
				Scalar previousStreamfunction = 0.; //Assume that the streamfunction on the last vertex of the otuflow is 0

				//North boundary
				for (int i = gridDimensions.x - 1; i >= 0; --i) {
					auto pVertex = m_pQuadGridMesh->getVertex(i, gridDimensions.y);
					if (i == gridDimensions.x - 1)
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("streamfunction", previousStreamfunction);
					else
						applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//West boundary
				for (int j = gridDimensions.y - 1; j >= 0; --j) {
					auto pVertex = m_pQuadGridMesh->getVertex(0, j);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//South boundary
				for (int i = 1; i <= gridDimensions.x; ++i) {
					auto pVertex = m_pQuadGridMesh->getVertex(i, 0);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
			}
			else { //no outflow --> also no inflow (at least for the moment)
				Scalar previousStreamfunction = 0.f;

				//North boundary
				for (int i = gridDimensions.x - 1; i > 0; --i) {
					auto pVertex = m_pQuadGridMesh->getVertex(i, gridDimensions.y);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//Weast boundary
				for (int j = gridDimensions.y; j >= 0; --j) {
					auto pVertex = m_pQuadGridMesh->getVertex(0, j);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//South boundary
				for (int i = 1; i < gridDimensions.x; ++i) {
					auto pVertex = m_pQuadGridMesh->getVertex(i, 0);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
				//East boundary
				for (int j = 0; j <= gridDimensions.y; ++j) {
					auto pVertex = m_pQuadGridMesh->getVertex(gridDimensions.x, j);
					applyStreamfunctionBC(pVertex, previousStreamfunction);
				}
			}
		}
		#pragma endregion

		#pragma region UtilityFunctions
		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::processPrimalGrid() {
			//Boundary Conditions
			auto gridDimensions = m_pQuadGridMesh->getGridDimensions();
			for (int i = 1; i < gridDimensions.x; ++i) {
					m_pQuadGridMesh->getVertex(i, 0)->setBoundaryCondition(m_southBoundary);
					m_pQuadGridMesh->getVertex(i, gridDimensions.y)->setBoundaryCondition(m_northBoundary);
			}
			for (int j = 0; j <= gridDimensions.y; ++j) {
					m_pQuadGridMesh->getVertex(gridDimensions.x, j)->setBoundaryCondition(m_eastBoundary);
					m_pQuadGridMesh->getVertex(0, j)->setBoundaryCondition(m_westBoundary);
			}

			//Obstacles -->solid vertices and internalBorderVertices
			for (int i = 0; i <= gridDimensions.x; ++i) {
				for (int j = 0; j <= gridDimensions.y; ++j) {
					shared_ptr<Cell<VectorType>> pCell1, pCell2, pCell3, pCell4;
					auto pVertex = m_pQuadGridMesh->getVertex(i, j);
					if (i == 0) {
						if (j == 0) {
							pCell1 = m_pQuadGridMesh->getCells(i, j).front();
							if (pCell1->isSolid())
								pVertex->setSolid(true);
						}
						else if (j == gridDimensions.y) {
							pCell1 = m_pQuadGridMesh->getCells(i, j - 1).front();
							if (pCell1->isSolid())
								pVertex->setSolid(true);
						}
						else {
							pCell1 = m_pQuadGridMesh->getCells(i, j).front();
							pCell2 = m_pQuadGridMesh->getCells(i, j-1).front();
							if (pCell1->isSolid() && pCell2->isSolid())
								pVertex->setSolid(true);
							else if (pCell1->isSolid())
								pVertex->setInternalBorder(true);
							else if(pCell2->isSolid())
								pVertex->setInternalBorder(true);
						}
					}
					else if (i == gridDimensions.x) {
						if (j == 0) {
							pCell1 = m_pQuadGridMesh->getCells(i - 1, j).front();
							if (pCell1->isSolid())
								pVertex->setSolid(true);
						}
						else if (j == gridDimensions.y) {
							pCell1 = m_pQuadGridMesh->getCells(i - 1, j - 1).front();
							if (pCell1->isSolid())
								pVertex->setSolid(true);
						}
						else {
							pCell1 = m_pQuadGridMesh->getCells(i - 1, j).front();
							pCell2 = m_pQuadGridMesh->getCells(i - 1, j - 1).front();
							if (pCell1->isSolid() && pCell2->isSolid())
								pVertex->setSolid(true);
							else if (pCell1->isSolid())
								pVertex->setInternalBorder(true);
							else if (pCell2->isSolid())
								pVertex->setInternalBorder(true);
						}
					}
					else {
						if (j == 0) {
							pCell1 = m_pQuadGridMesh->getCells(i, j).front();
							pCell2 = m_pQuadGridMesh->getCells(i - 1, j).front();
							if (pCell1->isSolid() && pCell2->isSolid())
								pVertex->setSolid(true);
							else if (pCell1->isSolid())
								pVertex->setInternalBorder(true);
							else if (pCell2->isSolid())
								pVertex->setInternalBorder(true);
						}
						else if (j == gridDimensions.y) {
							pCell1 = m_pQuadGridMesh->getCells(i, j - 1).front();
							pCell2 = m_pQuadGridMesh->getCells(i - 1, j - 1).front();
							if (pCell1->isSolid() && pCell2->isSolid())
								pVertex->setSolid(true);
							else if (pCell1->isSolid())
								pVertex->setInternalBorder(true);
							else if (pCell2->isSolid())
								pVertex->setInternalBorder(true);
						}
						else {
							pCell1 = m_pQuadGridMesh->getCells(i, j).front();
							pCell2 = m_pQuadGridMesh->getCells(i - 1, j).front();
							pCell3 = m_pQuadGridMesh->getCells(i - 1, j - 1).front();
							pCell4 = m_pQuadGridMesh->getCells(i, j - 1).front();

							int count = 0;
							if (pCell1->isSolid()) ++count;
							if (pCell2->isSolid()) ++count;
							if (pCell3->isSolid()) ++count;
							if (pCell4->isSolid()) ++count;

							if (count == 4)
								pVertex->setSolid(true);
							else if (count == 1) {
								pVertex->setInternalBorder(true);
								pVertex->setInternalCorner(true);
							}
							else if (count > 0) {
								pVertex->setInternalBorder(true);
							}
						}
					}
				}
			}

			//Grid information
			//Store number of internal and boundary vertices and create a mapping to the new vertices IDs.
			//Solid vertices are not mapped (since they won't be used), while internalBorderVertices are mapped all to the same ID (fake creation of a virtual vertex)
			int countInternalVertices = 0;
			int countBorderVertices = 0;
			auto pVertices = m_pQuadGridMesh->getVertices();
			int obstacleID = -1;

			m_primalVertexIDToNewVertexID = new int[pVertices.size()];

			//Count the number of internal vertices and store the new ID for them
			#pragma omp parallel for
			for (auto pVertex : pVertices) {
				if (!pVertex->isBorder() && !pVertex->isSolid()) { //internal vertex
					if (pVertex->isInternalBorder()) {
						if (obstacleID == -1) {
							obstacleID = countInternalVertices;
							++countInternalVertices;
						}
						m_primalVertexIDToNewVertexID[pVertex->getID()] = obstacleID;
						pVertex->setBoundaryCondition(m_internalBoundary);
					}
					else {
						m_primalVertexIDToNewVertexID[pVertex->getID()] = countInternalVertices;
						++countInternalVertices;
					}
				}
				else if (pVertex->getBoundaryCondition() == outflow) { //boundary vertex but outflow
					m_primalVertexIDToNewVertexID[pVertex->getID()] = countInternalVertices;
					++countInternalVertices;
				}
			}

			//Count the number of border vertices and store the new ID for them
			#pragma omp parallel for
			for (auto pVertex : pVertices) {
				if (pVertex->isBorder() && pVertex->getBoundaryCondition() != outflow) {
					m_primalVertexIDToNewVertexID[pVertex->getID()] = countInternalVertices + countBorderVertices; //the border vertices are the last stored!
					++countBorderVertices;
				}
			}

			m_primalInternalVertices = countInternalVertices;
			m_primalBorderVertices = countBorderVertices;
		}
		
		template <typename SolverType>
		void VoxelizedStreamfunctionGridSolverDEC2D<SolverType>::processDualGrid() {
			auto pDualCells = m_pDualMesh->getCells();
			m_dualPaddingCells = paddingCells(pDualCells);

			auto pDualEdges = m_pDualMesh->getElements();
			m_dualPaddingEdges = paddingEdges(pDualEdges);

			auto pDualVertices = m_pDualMesh->getVertices();
			m_dualPaddingVertices = paddingVertices(pDualVertices);
		}
		#pragma endregion

		template class VoxelizedStreamfunctionGridSolverDEC2D<FlowSolverType2D>;
	}
}
