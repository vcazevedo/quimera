
#include "Solvers/3D/GhostLiquidSolver3D.h"
#include "Solvers/LinearSystemCudaSolver.h"

#include "igl/writeOBJ.h"

#include <boost/format.hpp>

namespace Chimera {
	namespace Solvers {
		#pragma region Constructors
		GhostLiquidSolver3D::GhostLiquidSolver3D(const params_t &params, StructuredGrid<Vector3, Array3D> *pGrid,
				const vector<BoundaryCondition<Vector3, Array3D> *> &boundaryConditions,
				LiquidRepresentation<Vector3, Array3D> *pLiquidRepresentation)
				: VoxelizedGridSolver3D(params, pGrid),
				m_airDensityCoeff(1.201f), m_liquidDensityCoeff(1.0f), m_liquidViscosityCoeff(params.liquidViscosity), // constant parameters
				m_cellTypes(pGrid->getGridData()->getGridCellTypes()) {
			// initialize class members
			m_pGrid = pGrid;
			m_pGridData = m_pGrid->getGridData3D();
			m_dimensions = m_pGrid->getDimensions();
			m_boundaryConditions = boundaryConditions;
			m_pLiquidRepresentation = pLiquidRepresentation;

			// Initializing Poisson Matrix and Solver
			assert(params.pPoissonSolverParams->solverCategory == Krylov);
			m_pPoissonMatrix = createPoissonMatrix();
			initializePoissonSolver();
			updatePoissonMatrix();

			// Boundary conditions for initialization
			enforceBoundaryConditions();

			// Velocity and intermediary velocities interpolants
			initializeInterpolants();

			// Initialize Advection
			m_pAdvection = nullptr;
			assert(m_params.pAdvectionParams->advectionCategory == LagrangianAdvection);
			ParticleBasedAdvection<Vector3, Array3D>::params_t *pPbaParams = dynamic_cast<ParticleBasedAdvection<Vector3, Array3D>::params_t *>(m_params.pAdvectionParams);
			assert(pPbaParams != nullptr);
			pPbaParams->samplingMethod = m_pLiquidRepresentation->getParams().samplingMethod;
			pPbaParams->resampleParticles = m_pLiquidRepresentation->getParams().resampleParticles;
			pPbaParams->particlesPerCell = m_pLiquidRepresentation->getParams().particlesPerCell;
			ParticleBasedAdvection<Vector3, Array3D> *pParticleBasedAdvection;
			m_pAdvection = pParticleBasedAdvection = new ParticleBasedAdvection<Vector3, Array3D>(pPbaParams, m_pVelocityInterpolant, m_pGrid->getGridData(), m_pLiquidRepresentation->getParticlesSampler());
			pParticleBasedAdvection->getParticlesSampler()->setBoundaryConditions(m_boundaryConditions);
			pParticleBasedAdvection->addScalarBasedAttribute("density", m_pDensityInterpolant);
		}
		#pragma endregion

		#pragma region Update Functions
		void GhostLiquidSolver3D::saveLiquidMesh(Scalar elapsedTime) const {
			auto t  = std::time(nullptr);
			auto tm = *std::localtime(&t);
			std::stringstream outputFileName;
			outputFileName << m_params.saveMeshFolder
				<< std::put_time(&tm, "%Y-%m-%d_%H-%M-%S")
				<< boost::format("_et%012.4f.obj") % elapsedTime;
			auto &mesh = m_pLiquidRepresentation->getObjectMesh();
			igl::writeOBJ(outputFileName.str().c_str(), get<0>(mesh), get<1>(mesh));
		}
		void GhostLiquidSolver3D::update(Scalar dt) {
			m_numIterations++;
			m_totalSimulationTimer.start();

			ParticleBasedAdvection<Vector3, Array3D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector3, Array3D> *>(m_pAdvection);
			if (pParticleBasedAdv == nullptr) {
				throw(std::logic_error("CutCellSolver3D: only particle based advection methods are supported now"));
			}

			if (PhysicsCore<Vector3>::getInstance()->getElapsedTime() < dt) {
				applyForces(dt);
				pParticleBasedAdv->updateGridAttributes();
				enforceBoundaryConditions();
				updateDivergents(dt);
				enforceBoundaryConditions();
				solvePressure();
				enforceBoundaryConditions();
				project(dt);
				enforceBoundaryConditions();

				return;
			}

			m_solveViscosityTimer.start();
			updateViscosityMatrix(dt);
			solveViscosity();
			enforceBoundaryConditions();
			pParticleBasedAdv->updateParticleAttributes();
			m_solveViscosityTimer.stop();
			m_solveViscosityTime = m_solveViscosityTimer.secondsElapsed();

			applyForces(dt);
			enforceBoundaryConditions();

			m_advectionTimer.start();

			enforceBoundaryConditions();
			/*
			 * TODO: Implement an Interpolant for Streamfunctions in 3D
			CubicStreamfunctionInterpolant2D<Vector2> *pCInterpolant = dynamic_cast<CubicStreamfunctionInterpolant2D<Vector2> *>(pParticleBasedAdv->getParticleBasedIntegrator()->getInterpolant());
			if (pCInterpolant)
				pCInterpolant->computeStreamfunctions();
			else {
				BilinearStreamfunctionInterpolant2D<Vector2> *pSInterpolant = dynamic_cast<BilinearStreamfunctionInterpolant2D<Vector2> *>(pParticleBasedAdv->getParticleBasedIntegrator()->getInterpolant());
				if (pSInterpolant)
					pSInterpolant->computeStreamfunctions();
			}
			*/

			enforceBoundaryConditions();
			extrapolateVelocities();
			pParticleBasedAdv->updatePositions(dt);
			if (m_pLiquidRepresentation) {
				m_pLiquidRepresentation->updateMeshes();
			}
			updatePoissonMatrix();
			pParticleBasedAdv->updateGridAttributes();


			m_advectionTimer.stop();
			m_advectionTime = m_advectionTimer.secondsElapsed();
			enforceBoundaryConditions();

			// Solve pressure
			m_solvePressureTimer.start();
			updateDivergents(dt);
			solvePressure();

			// Ghost Fluid Velocity Update
			correctVelocityGhostFluid(dt);

			enforceBoundaryConditions();
			m_solvePressureTimer.stop();
			m_solvePressureTime = m_solvePressureTimer.secondsElapsed();

			// Project velocity
			m_projectionTimer.start();
			project(dt);
			m_projectionTimer.stop();
			m_projectionTime = m_projectionTimer.secondsElapsed();
			enforceBoundaryConditions();

			enforceBoundaryConditions();

			if (pParticleBasedAdv) {
				pParticleBasedAdv->updateParticleAttributes();
				m_pGridData->getDensityBuffer().swapBuffers();
				m_pGridData->getTemperatureBuffer().swapBuffers();
			}


			enforceBoundaryConditions();
			updateVorticity();
			updateKineticEnergy();

			m_totalSimulationTimer.stop();
			m_totalSimulationTime = m_totalSimulationTimer.secondsElapsed();

			if (!m_params.saveMeshFolder.empty()) {
				saveLiquidMesh(PhysicsCore<Vector3>::getInstance()->getElapsedTime());
			}
		}
		#pragma endregion

		#pragma region Simulation Functions
		void GhostLiquidSolver3D::divergenceFree(Scalar dt) {
			const Scalar dx = m_pGridData->getGridSpacing();
			const Scalar gradientCoeffAir = dt / (m_airDensityCoeff * dx);
			const Scalar gradientCoeffFluid = dt / (m_liquidDensityCoeff * dx);

			for (int i = 1; i < m_dimensions.x - 1; i++) {
				for (int j = 1; j < m_dimensions.y - 1; j++) {
					for (int k = 1; k < m_dimensions.z - 1; k++) {
						Vector3 vel;
						const dimensions_t currCellIndex(i, j, k);
						const cellType_t currCellType = m_cellTypes(i, j, k);
						// aux velocity
						const Vector3 &currAuxVel = m_pGridData->getAuxiliaryVelocity(i, j, k);
						// pressures
						const Scalar &currPressure   = m_pGridData->getPressure(i, j, k);
						const Scalar &leftPressure   = m_pGridData->getPressure(i - 1, j, k);
						const Scalar &bottomPressure = m_pGridData->getPressure(i, j - 1, k);
						const Scalar &backPressure   = m_pGridData->getPressure(i, j, k - 1);
						// gradient coefficient
						const Scalar gradientCoeffComplete = (currCellType & airCell) ? gradientCoeffAir : gradientCoeffFluid;
						// case complete cell:
						if (currCellType == airCell || currCellType == fluidCell) {
							vel.x = currAuxVel.x - gradientCoeffComplete * (currPressure - leftPressure);
							vel.y = currAuxVel.y - gradientCoeffComplete * (currPressure - bottomPressure);
							vel.z = currAuxVel.z - gradientCoeffComplete * (currPressure - backPressure);
						}
						// case boundary cell:
						else {
							for (halfCellLocation_t faceLocation : { leftHalfCell, bottomHalfCell, backHalfCell }) {
								const dimensions_t nextCellIndex = getNeighborIndex(currCellIndex, faceLocation);
								const cellType_t nextCellType = m_cellTypes(nextCellIndex);
								if ((currCellType >> 1) == (nextCellType >> 1)) { // check if same type of cell
									switch(faceLocation) {
										case leftHalfCell:   vel.x = currAuxVel.x - gradientCoeffComplete * (currPressure - leftPressure);   break;
										case bottomHalfCell: vel.y = currAuxVel.y - gradientCoeffComplete * (currPressure - bottomPressure); break;
										case backHalfCell:   vel.z = currAuxVel.z - gradientCoeffComplete * (currPressure - backPressure);   break;
									}
								}
								else {
									const Scalar theta = (currCellType == boundaryFluidCell) ?
										thetaHelper(phiHelper(currCellIndex), phiHelper(nextCellIndex))
										: thetaHelper(phiHelper(nextCellIndex), phiHelper(currCellIndex));
									const Scalar gradientCoeff = theta * gradientCoeffAir + (1.0f - theta) * gradientCoeffFluid;
									switch (faceLocation) {
										case leftHalfCell:   vel.x = currAuxVel.x - gradientCoeff * (currPressure - leftPressure);   break;
										case bottomHalfCell: vel.y = currAuxVel.y - gradientCoeff * (currPressure - bottomPressure); break;
										case backHalfCell:   vel.z = currAuxVel.z - gradientCoeff * (currPressure - backPressure);   break;
									}
								}
							}
						}
						m_pGridData->setVelocity(vel, i, j, k);
					}
				}
			}

		}
		Scalar GhostLiquidSolver3D::calculateFluxDivergent(int i, int j, int k) {
			Scalar divergent = 0;
			const Scalar dx = m_pGridData->getGridSpacing();
			const cellType_t cellType = m_cellTypes(i, j, k);
			if (cellType & fluidCell) {
				// current auxVelocity
				const Vector3 &curr = m_pGridData->getAuxiliaryVelocity(i, j, k);
				// neighbor auxVelocities
				const Vector3 &right = m_pGridData->getAuxiliaryVelocity(i + 1, j, k);
				const Vector3 &top   = m_pGridData->getAuxiliaryVelocity(i, j + 1, k);
				const Vector3 &front = m_pGridData->getAuxiliaryVelocity(i, j, k + 1);
				// compute divergence
				const Scalar Dx = right.x - curr.x;
				const Scalar Dy =   top.y - curr.y;
				const Scalar Dz = front.z - curr.z;
				divergent = (Dx + Dy + Dz) / dx;
			}
			return divergent;
		}
		void GhostLiquidSolver3D::applyForces(Scalar dt) {
			const Scalar dx = m_pGridData->getGridSpacing();
			ParticleBasedAdvection<Vector3, Array3D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector3, Array3D> *>(m_pAdvection);
			ParticlesData<Vector3> *pParticlesData = pParticleBasedAdv->getParticlesData();
			for (Vector3 &vel : pParticlesData->getVelocities()) {
				vel.y -= 9.81 * dt / dx;
			}
		}
		#pragma endregion


		#pragma region Ghost Fluid Method Helper Methods
		Scalar GhostLiquidSolver3D::phiHelper(dimensions_t cellIndex) const {
			const int i = cellIndex.x, j = cellIndex.y, k = cellIndex.z;
			const Array3D<Scalar> &levelSet = m_pGridData->getFineGridScalarFieldArray();
			const uint numSubDiv = std::pow(2, m_pLiquidRepresentation->getParams().levelSetGridSubdivisions);
			const int idx_x = (numSubDiv * (2 * i + 1)) / 2;
			const int idx_y = (numSubDiv * (2 * j + 1)) / 2;
			const int idx_z = (numSubDiv * (2 * k + 1)) / 2;
			return levelSet(idx_x, idx_y, idx_z);
		}
		Scalar GhostLiquidSolver3D::thetaHelper(Scalar inside, Scalar outside) const {
			const Scalar denom = inside - outside;
			if (denom > -1e-4) return 0.5f; // should always be neg, and large enough...
			else return std::max(static_cast<Scalar>(0), std::min(static_cast<Scalar>(1), inside / denom));
		}
		Scalar GhostLiquidSolver3D::ghostFluidHelper(const dimensions_t &liquidCellIndex, const dimensions_t &otherCellIndex) const {
			const Scalar phiCell = phiHelper(liquidCellIndex);
			const Scalar phiOther = phiHelper(otherCellIndex);
			const Scalar theta = thetaHelper(phiCell, phiOther);
			const Scalar alpha = 1.0f - (1.0f / theta);
			return alpha;
		}
		void GhostLiquidSolver3D::applyGhostFluidDiagonal() {
			#pragma omp parallel for
			for (int i = 1; i < m_dimensions.x - 1; i++) {
				for (int j = 1; j < m_dimensions.y - 1; j++) {
					for (int k = 1; k < m_dimensions.z - 1; k++) {
						const dimensions_t currCellIndex(i, j, k);
						if (m_cellTypes(currCellIndex) == boundaryFluidCell) {
							vector<dimensions_t> neighbors;
							getNeighborIndices(currCellIndex, neighbors);
							const uint rowIndex = m_pPoissonMatrix->getRowIndex(i - 1, j - 1, k - 1);
							Scalar pc = 6.0f;
							for (const dimensions_t &nextCellIndex : neighbors) {
								if (m_cellTypes(nextCellIndex) == boundaryAirCell) {
									pc -= ghostFluidHelper(currCellIndex, nextCellIndex);
								}
							}
							m_pPoissonMatrix->setCentralValue(rowIndex, pc);
						}
					}
				}
			}
		}
		void GhostLiquidSolver3D::correctVelocityGhostFluid(Scalar dt) {
			const Scalar dx = m_pGridData->getGridSpacing();
			const Array3D<Vector3> &velocities = m_pGridData->getVelocityArray();
			for (int i = 1; i < m_dimensions.x - 1; i++) {
				for (int j = 1; j < m_dimensions.y - 1; j++) {
					for (int k = 1; k < m_dimensions.z - 1; k++) {
						const dimensions_t currCellIndex(i, j, k);
						if (m_cellTypes(currCellIndex) & boundaryCell) {
							vector<dimensions_t> neighbors;
							getNeighborIndices(currCellIndex, neighbors);
							Vector3 currVel = velocities(currCellIndex);
							const Scalar currPressure = m_pGridData->getPressure(i, j, k);
							if (m_cellTypes(currCellIndex) == boundaryFluidCell) {
								const Scalar c_pressure = dt * currPressure / (m_liquidDensityCoeff * dx);

								for (uint l = 0; l < neighbors.size(); l++) {
									const dimensions_t &nextCellIndex = neighbors[l];
									if (m_cellTypes(nextCellIndex) == boundaryAirCell) {
										const Scalar coeff = c_pressure * ghostFluidHelper(currCellIndex, nextCellIndex);
										switch (l) {
											case leftHalfCell:
											case rightHalfCell:
												currVel.x += coeff;
												break;
											case topHalfCell:
											case bottomHalfCell:
												currVel.y += coeff;
												break;
											case frontHalfCell:
											case backHalfCell:
												currVel.z += coeff;
												break;
											default:
												throw std::logic_error("unrecognized halfFaceLocation_t");
										}
									}
								}
							}
							else { // boundaryAirCell
								getNeighborIndices(currCellIndex, neighbors);
								bool xChanged = false, yChanged = false, zChanged = false;
								const Scalar c_pressure = dt * currPressure / (m_airDensityCoeff * dx);
								for (uint l = 0; l < neighbors.size(); l++) {
									const dimensions_t &nextCellIndex = neighbors[l];
									if (m_cellTypes(nextCellIndex) == boundaryFluidCell) {
										const Scalar coeff = c_pressure * ghostFluidHelper(nextCellIndex, currCellIndex);
										switch (l) {
											case leftHalfCell:
											case rightHalfCell:
												currVel.x -= coeff;
												xChanged = true;
												break;
											case topHalfCell:
											case bottomHalfCell:
												currVel.y -= coeff;
												yChanged = true;
												break;
											case frontHalfCell:
											case backHalfCell:
												currVel.z -= coeff;
												zChanged = true;
												break;
											default:
												throw std::logic_error("unrecognized halfFaceLocation_t");
										}
									}
								}
								if (!xChanged) currVel.x = 0.0f;
								if (!yChanged) currVel.y = 0.0f;
								if (!zChanged) currVel.z = 0.0f;
							}
							m_pGridData->setVelocity(currVel, i, j, k);
						}
					}
				}
			}
		}
		#pragma endregion

		#pragma region Internal Functionalities
		void GhostLiquidSolver3D::updateViscosityMatrix(Scalar dt) {
			const Array3D<cellType_t> &cellTypes = m_pGridData->getGridCellTypes();
			const dimensions_t &dim = m_pGridData->getDimensions();
			const Scalar dx = m_pGridData->getGridSpacing();
			const uint N = dim.x * dim.y;
			// clear entries for next iteration
			m_viscosityMatrixEntries.clear();
			// at most, we will have 5 entries per row
			m_viscosityMatrixEntries.reserve(5 * N);
			const Scalar coeff = dt * m_liquidViscosityCoeff / (dx * dx * dx);
			for (int k = 0; k < dim.z; k++) {
				for (int j = 0; j < dim.y; j++) {
					for (int i = 0; i < dim.x; i++) {
						const uint rowIndex = k * (dim.y * dim.x) + j * dim.x + i;
						if (i == 0 || i + 1 == dim.x || j == 0 || j + 1 == dim.y || k == 0 || k + 1 == dim.z || cellTypes(i, j, k) & airCell) {
							m_viscosityMatrixEntries.emplace_back(rowIndex, rowIndex, 1);
						}
						else if (cellTypes(i, j, k) & fluidCell) {
							const uint idxTop = rowIndex + dim.x;
							const uint idxLeft = rowIndex - 1;
							const uint idxRight = rowIndex + 1;
							const uint idxBottom = rowIndex - dim.x;
							const uint idxBack = rowIndex - dim.y * dim.x;
							const uint idxFront = rowIndex + dim.y * dim.x;
							Scalar ps = 0, pw = 0, pc = 0, pe = 0, pn = 0, pb = 0, pf = 0;
							if (cellTypes(i, j + 1, k) & fluidCell) m_viscosityMatrixEntries.emplace_back(rowIndex, idxTop,    pn = -coeff);
							if (cellTypes(i - 1, j, k) & fluidCell) m_viscosityMatrixEntries.emplace_back(rowIndex, idxLeft,   pw = -coeff);
							if (cellTypes(i + 1, j, k) & fluidCell) m_viscosityMatrixEntries.emplace_back(rowIndex, idxRight,  pe = -coeff);
							if (cellTypes(i, j - 1, k) & fluidCell) m_viscosityMatrixEntries.emplace_back(rowIndex, idxBottom, ps = -coeff);
							if (cellTypes(i, j, k - 1) & fluidCell) m_viscosityMatrixEntries.emplace_back(rowIndex, idxBack,   pb = -coeff);
							if (cellTypes(i, j, k + 1) & fluidCell) m_viscosityMatrixEntries.emplace_back(rowIndex, idxFront,  pf = -coeff);
							m_viscosityMatrixEntries.emplace_back(rowIndex, rowIndex, pc = 1 - (pn + pw + pe + ps + pb + pf));
						}
						else {

						}
					}
				}
			}
		}
		void GhostLiquidSolver3D::solveViscosity() {
			const dimensions_t &dim = m_pGridData->getDimensions();
			const uint M = dim.z * dim.y * dim.x;
			Scalar *pVelocities = static_cast<Scalar*>(m_pGridData->getVelocityArrayPtr()->getRawDataPointer());
			auto velMap = Eigen::Map<Eigen::Matrix<Scalar, Eigen::Dynamic, 3, Eigen::RowMajor>>(pVelocities, M, 3);
			if (m_params.pPoissonSolverParams->platform == PlataformCPU) {
				Eigen::SparseMatrix<Scalar, Eigen::ColMajor> A(M, M);
				A.setFromTriplets(m_viscosityMatrixEntries.begin(), m_viscosityMatrixEntries.end());
				Eigen::ConjugateGradient<Eigen::SparseMatrix<Scalar>> cgSolver(A);
				const Eigen::Matrix<Scalar, Eigen::Dynamic, 3> rhs = velMap;
				velMap = cgSolver.solve(rhs); // solve and directly write into velocity array
			}
			else if (m_params.pPoissonSolverParams->platform == PlataformGPU) {
				static LinearSystemCudaSolver lsSolver;
				Eigen::SparseMatrix<Scalar, Eigen::RowMajor> A(M, M);
				A.setFromTriplets(m_viscosityMatrixEntries.begin(), m_viscosityMatrixEntries.end());
				for (uint l = 0; l < 3; l++) { // solve for each dimension
					Eigen::Matrix<Scalar, Eigen::Dynamic, 1> rhs = velMap.col(l);
					lsSolver.solveCG(A, rhs.data(), rhs.data());
					velMap.col(l) = rhs; // write back to velocity array
				}
			}
			else {
				throw std::logic_error("platform not recognized");
			}
		}
		bool GhostLiquidSolver3D::updatePoissonMatrix() {
			const Scalar dx = m_pGridData->getGridSpacing();

			#pragma omp parallel for
			for (int i = 1; i < m_dimensions.x - 1; i++) {
				for (int j = 1; j < m_dimensions.y - 1; j++) {
					for (int k = 1; k < m_dimensions.z - 1; k++) {
						const uint rowIndex = m_pPoissonMatrix->getRowIndex(i - 1, j - 1, k - 1);
						switch (m_cellTypes(i, j, k)) {
							case fluidCell:
							case boundaryFluidCell:
								m_pPoissonMatrix->setRow(rowIndex, -1, -1, -1, 6, -1, -1, -1);
								break;
							case airCell:
							case boundaryAirCell:
								m_pPoissonMatrix->setRow(rowIndex, 0, 0, 0, 1, 0, 0, 0);
								break;
							default:
								throw std::logic_error("unrecognized cell type");
						}
					}
				}
			}

			// update boundary cases with ghost fluid method
			applyGhostFluidDiagonal();

			// update boundary cases
			for (BoundaryCondition<Vector3, Array3D> *pBoundaryCondition : m_boundaryConditions) {
				pBoundaryCondition->updatePoissonMatrix(m_pPoissonMatrix);
			}

			if (m_pPoissonMatrix->isSingular()) {
				m_pPoissonMatrix->applyCorrection(1e-3);
			}

			m_pPoissonMatrix->updateCudaData();

			return false;
		}
		void GhostLiquidSolver3D::extrapolateVelocities() {
			const Array3D<Vector3> &velocity = m_pGridData->getVelocityArray();
			const Array3D<Vector3> &auxVelocity = m_pGridData->getAuxVelocityArray();

			#pragma omp parallel for
			for (int i = 0; i < m_dimensions.x; i++) {
				for (int j = 0; j < m_dimensions.y; j++) {
					for (int k = 0; k < m_dimensions.z; k++) {
						const dimensions_t currCellIndex(i, j, k);
						const cellType_t typeCurr = m_cellTypes(currCellIndex);
						if (typeCurr == boundaryAirCell) {
							vector<dimensions_t> neighbors;
							getNonBoundaryNeighborIndices(currCellIndex, neighbors);

							int n = 0;
							Vector3 newVelocity(0, 0, 0);
							Vector3 newAuxiliaryVelocity(0, 0, 0);
							for (const dimensions_t &cellIndex : neighbors) {
								if (m_cellTypes(cellIndex) & fluidCell) {
									newVelocity += velocity(cellIndex);
									newAuxiliaryVelocity += auxVelocity(cellIndex);
									n++;
								}
							}

							if (n != 0) {
								newVelocity *= (1.0f / n);
								newAuxiliaryVelocity *= (1.0f / n);
								m_pGridData->setVelocity(newVelocity, i, j, k);
								m_pGridData->setAuxiliaryVelocity(newAuxiliaryVelocity, i, j, k);
							}
						}
					}
				}
			}
		}
		#pragma endregion

	}
}
