//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#include "Solvers/3D/VoxelizedGridSolver3D.h"
#include <boost/math/special_functions/binomial.hpp>

namespace Chimera {

	namespace Solvers {

		#pragma region Constructors
		VoxelizedGridSolver3D::VoxelizedGridSolver3D(const params_t &params, StructuredGrid<Vector3, Array3D> *pGrid, 
												const vector<BoundaryCondition<Vector3, Array3D> *> &boundaryConditions,
												const vector<shared_ptr<SolidObject<Vector3, IntersectedPolyMesh>>> &rigidObjects)
			: FlowSolver(params, pGrid) {

			/** Initialize line meshes - potential objects */
			for (int i = 0; i < rigidObjects.size(); i++) {
				m_polyMeshes.push_back(rigidObjects[i]->getMesh());
			}

			m_pGrid = pGrid;
			m_pGridData = m_pGrid->getGridData3D();
			m_dimensions = m_pGrid->getDimensions();
			m_boundaryConditions = boundaryConditions;
			m_pAdvection = nullptr;

			m_pPoissonMatrix = createPoissonMatrix();
			initializePoissonSolver();

			/** Boundary conditions for initialization */
			enforceBoundaryConditions();

			Scalar dx = m_pGridData->getGridSpacing();

			/** Velocity and intermediary velocities interpolants */
			initializeInterpolants();

			/** Update solid cells and set poisson conditions */
			updateSolidCells();
			BoundaryCondition<Vector3, Array3D>::updateSolidWalls(m_pPoissonMatrix, m_pGridData);

			/* Initialize density and temperature fields simulate with 0 time-step */
			applyHotSmokeSources(0);

			// Initialize Tornado Forces
			applySwirlingFields(0);

			m_pAdvection = initializeAdvectionClass();
			
			if (m_params.pAdvectionParams->advectionCategory == LagrangianAdvection) {
				ParticleBasedAdvection<Vector3, Array3D> *pPBAdv = dynamic_cast<ParticleBasedAdvection<Vector3, Array3D> *>(m_pAdvection);
				pPBAdv->addScalarBasedAttribute("density", m_pDensityInterpolant);
				pPBAdv->addScalarBasedAttribute("temperature", m_pTemperatureInterpolant);
			}
		}
		#pragma endregion 

		#pragma region Functionalities
		void VoxelizedGridSolver3D::update(Scalar dt) {
			m_numIterations++;
			m_totalSimulationTimer.start();

			/** First time-step: Do an additional solve to get div-free velocities from boundary conditions everywhere */
			if (PhysicsCore<Vector3>::getInstance()->getElapsedTime() < dt) {
				applyForces(dt);
				enforceBoundaryConditions();
				enforceSolidWallsConditions(Vector3(0, 0, 0));
				updateDivergents(dt);
				solvePressure();
				project(dt);
				enforceBoundaryConditions();
				enforceSolidWallsConditions(Vector3(0, 0, 0));

				/** If using a particle-based advection, we need to initialize their initial velocities by fully interpolating
					from the grid */
				if (m_params.pAdvectionParams->advectionCategory == LagrangianAdvection) {
					ParticleBasedAdvection<Vector3, Array3D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector3, Array3D> *>(m_pAdvection);
					pParticleBasedAdv->getParticlesSampler()->interpolateVelocities(m_pVelocityInterpolant);
				}

				/** If not initial time-step, apply forcing functions into the grid */
				applyForces(dt);
				if (m_params.vorticityConfinementStrength) {
					vorticityConfinement(dt);
				}
				if (m_params.pAdvectionParams->advectionCategory == LagrangianAdvection) {
					//Update particles velocities just before the time-step, after buoyancy
					m_pAdvection->postProjectionUpdate(dt);
				}
			}
			else {
				/** If not initial time - step, apply forcing functions into the grid */
				applyForces(dt);
				if (m_params.vorticityConfinementStrength) {
					vorticityConfinement(dt);
				}

				/** If particle-based velocities, update their velocities at the beginning of the time-step*/
				if (m_params.pAdvectionParams->advectionCategory == LagrangianAdvection) {
					//Update particles velocities just before the time-step, after buoyancy
					m_pAdvection->postProjectionUpdate(dt);
				}
			}

			m_advectionTimer.start();
			m_pAdvection->advect(dt);
			m_advectionTimer.stop();
			m_advectionTime = m_advectionTimer.secondsElapsed();
			
			/** Solve pressure */
			enforceBoundaryConditions();
			enforceSolidWallsConditions(Vector3(0, 0, 0));
			m_solvePressureTimer.start();
			updateDivergents(dt);
			solvePressure();
			m_solvePressureTimer.stop();
			m_solvePressureTime = m_solvePressureTimer.secondsElapsed();

			/** Project velocity */
			m_projectionTimer.start();
			project(dt);
			m_projectionTimer.stop();
			m_projectionTime = m_projectionTimer.secondsElapsed();
			enforceBoundaryConditions();
			enforceSolidWallsConditions(Vector3(0, 0, 0));
			
			///** Post projection update for advection methods */
			//if (m_pAdvection->getParams().advectionCategory == EulerianAdvection) {
			//	/**Advect densities on grid-based advection schemes */
			//	m_pAdvection->postProjectionUpdate(dt);
			//}

			m_pAdvection->postProjectionUpdate(dt);
			
			/** Swap buffers for density and temperature */
			m_pGridData->getDensityBuffer().swapBuffers();
			m_pGridData->getTemperatureBuffer().swapBuffers();

			/** We also have to do this for advection methods' interpolants */
			if (m_pAdvection->getParams().advectionCategory == EulerianAdvection) {
				SemiLagrangianAdvection<Vector3, Array3D> *pSLAdv = dynamic_cast<SemiLagrangianAdvection<Vector3, Array3D> *>(m_pAdvection);
				//MCcormack should also work in this cast, since it is derived from SL;
				if (pSLAdv) {
					//Swapping interpolants buffers means that the main interpolant becomes its sibling
					//and its sibling its automatically the old interpolant
					pSLAdv->setDensityInterpolant(pSLAdv->getDensityInterpolant()->getSibilingInterpolant());
					pSLAdv->setTemperatureInterpolant(pSLAdv->getTemperatureInterpolant()->getSibilingInterpolant());
				}
			}
			else if (m_pAdvection->getParams().advectionCategory == LagrangianAdvection) {
				ParticleBasedAdvection<Vector3, Array3D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector3, Array3D> *>(m_pAdvection);
				auto densityAttributeIter = pParticleBasedAdv->getGridToParticles()->getScalarBasedAttribute("density");
				densityAttributeIter->second = densityAttributeIter->second->getSibilingInterpolant();

				auto temperatureAttributeIter = pParticleBasedAdv->getGridToParticles()->getScalarBasedAttribute("temperature");
				temperatureAttributeIter->second = temperatureAttributeIter->second->getSibilingInterpolant();
			}

			updateVorticity();
			enforceBoundaryConditions();

			m_totalSimulationTimer.stop();
			m_totalSimulationTime = m_totalSimulationTimer.secondsElapsed();
		}

		void VoxelizedGridSolver3D::updatePoissonSolidWalls() {
			for (unsigned int i = 0; i < m_boundaryConditions.size(); i++) {
				m_boundaryConditions[i]->updatePoissonMatrix(m_pPoissonMatrix);
			}

			//BoundaryCondition<Vector3, Array3D>::updateSolidWalls(m_pPoissonMatrix, Array3D<bool>(m_pGrid->getSolidMarkers(), m_dimensions), m_params.getPressureSolverParams().getMethodCategory() == Krylov);

			if (m_pPoissonMatrix->isSingular() && m_params.pPoissonSolverParams->solverMethod == Krylov)
				m_pPoissonMatrix->applyCorrection(1e-3);

			m_pPoissonMatrix->updateCudaData();
		}

		void VoxelizedGridSolver3D::applyHotSmokeSources(Scalar dt) {
			Scalar dx = m_pGridData->getGridSpacing();
			#pragma omp parallel for
			for (int currSource = 0; currSource < m_params.smokeSources.size(); currSource++) {
				for (int i = 1; i < m_pGridData->getDimensions().x - 1; i++) {
					for (int j = 1; j < m_pGridData->getDimensions().y - 1; j++) {
						for (int k = 1; k < m_pGridData->getDimensions().z - 1; k++) {
							Scalar distance = (Vector3(i + 0.5, j + 0.5, k + 0.5)*dx - m_params.smokeSources[currSource]->position).length();
							if (distance < m_params.smokeSources[currSource]->size) {
								Scalar noise = rand() / ((float)RAND_MAX);
								noise *= m_params.smokeSources[currSource]->densityVariation;
								m_pGridData->getDensityBuffer().setValueBothBuffers(m_params.smokeSources[currSource]->densityValue - noise, i, j, k);

								noise = rand() / ((float)RAND_MAX);
								noise *= m_params.smokeSources[currSource]->temperatureVariation;
								m_pGridData->getTemperatureBuffer().setValueBothBuffers(m_params.smokeSources[currSource]->temperatureValue - noise, i, j, k);

								Vector3 currVelocity = m_pGridData->getVelocity(i, j, k);
								m_pGridData->setVelocity(currVelocity + m_params.smokeSources[currSource]->velocity*dt, i, j, k);
							}
						}
						
					}
				}

				if (m_params.pAdvectionParams->advectionCategory == LagrangianAdvection && dt != 0) { //dt == 0 initialization step
					ParticleBasedAdvection<Vector3, Array3D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector3, Array3D> *>(m_pAdvection);
					ParticlesData<Vector3> *pParticlesData = pParticleBasedAdv->getParticlesData();
					const vector<Vector3> &particlePositions = pParticlesData->getPositions();
					
					for (int i = 0; i < particlePositions.size(); i++) {
						Scalar distance = (particlePositions[i] - m_params.smokeSources[currSource]->position).length();
						if (distance < m_params.smokeSources[currSource]->size) {
							if (pParticlesData->hasScalarBasedAttribute("density")) {
								vector<Scalar> &densities = pParticlesData->getScalarBasedAttribute("density");
								densities[i] = m_pDensityInterpolant->interpolate(particlePositions[i]);
							}

							if (pParticlesData->hasScalarBasedAttribute("temperature")) {
								vector<Scalar> &temperatures = pParticlesData->getScalarBasedAttribute("temperature");
								temperatures[i] = m_pTemperatureInterpolant->interpolate(particlePositions[i]);
							}

							pParticlesData->getVelocities()[i] += m_params.smokeSources[currSource]->velocity*dt;
						}
					}
				}
			}
		}

		void VoxelizedGridSolver3D::addBuyoancy(Scalar dt) {
			Scalar dx = m_pGridData->getGridSpacing();

			Array3D<Vector3> buoyancyForces(m_pGridData->getDimensions());

			#pragma omp parallel for
			if (m_params.smokeSources.size() > 0) {
				for (int i = 1; i < m_pGridData->getDimensions().x - 1; i++) {
					for (int j = 1; j < m_pGridData->getDimensions().y - 1; j++) {
						for (int k = 1; k < m_pGridData->getDimensions().z - 1; k++) {
							//Temperature is normalized
							Scalar temperature = m_pGridData->getTemperatureBuffer().getValue(i, j, k);
							Scalar density = m_pGridData->getDensityBuffer().getValue(i, j, k);
							Vector3 buoyancyForce;
							buoyancyForce.y = temperature*m_params.smokeSources.front()->temperatureBuoyancyCoefficient 
												- density*m_params.smokeSources.front()->densityBuoyancyCoefficient;
							buoyancyForces(i, j, k) = buoyancyForce*dt;//dx;
						}
					}
				}
			}

			#pragma omp parallel for
			for (int i = 1; i < m_pGridData->getDimensions().x - 1; i++) {
				for (int j = 1; j < m_pGridData->getDimensions().y - 1; j++) {
					for (int k = 1; k < m_pGridData->getDimensions().z - 1; k++) {
						Vector3 interpolatedBuoyancy;
						
						if (i == 1)
							interpolatedBuoyancy.x = buoyancyForces(i, j, k).x;
						else
							interpolatedBuoyancy.x = (buoyancyForces(i, j, k).x + buoyancyForces(i - 1, j, k).x)*0.5;
						if(j == 1) 
							interpolatedBuoyancy.y = buoyancyForces(i, j, k).y;
						else
							interpolatedBuoyancy.y = (buoyancyForces(i, j, k).y + buoyancyForces(i, j - 1, k).y)*0.5;
						if(k == 1)
							interpolatedBuoyancy.z = buoyancyForces(i, j, k).z;
						else
							interpolatedBuoyancy.z = (buoyancyForces(i, j, k).z + buoyancyForces(i, j, k - 1).z)*0.5;
						
						m_pGridData->setVelocity(m_pGridData->getVelocity(i, j, k) + interpolatedBuoyancy, i, j, k);
					}
				}
			}
			
		}

		void VoxelizedGridSolver3D::vorticityConfinement(Scalar dt) {
			//Updates vorticity before computing vorticity confinement forces
			updateVorticity();
			
			Scalar dx = m_pGridData->getGridSpacing();
			Array3D<Vector3> vorticityConfinementForces(m_pGridData->getDimensions());

			//Apply only a bit further away from boundary conditions
			//Also, calculate forces for grid-centered, then interpolate it to velocity locations
			#pragma omp parallel for
			for (int i = 2; i < m_pGridData->getDimensions().x - 2; i++) {
				for (int j = 2; j < m_pGridData->getDimensions().y - 2; j++) {
					for (int k = 2; k < m_pGridData->getDimensions().z - 2; k++) {
						Vector3 vorticityGradient;

						vorticityGradient.x = (m_pGridData->getVorticity(i + 1, j, k) - m_pGridData->getVorticity(i, j, k)) / dx;
						vorticityGradient.y = (m_pGridData->getVorticity(i, j + 1, k) - m_pGridData->getVorticity(i, j, k)) / dx;
						vorticityGradient.z = (m_pGridData->getVorticity(i, j, k + 1) - m_pGridData->getVorticity(i, j, k)) / dx;

						vorticityGradient.normalize();

						Vector3 vorticity = calculateVorticity(i, j, k);

						Vector3 vConfinementForce = vorticityGradient.cross(vorticity)*dx*m_params.vorticityConfinementStrength*dt;
						vorticityConfinementForces(i, j, k) = vConfinementForce;
					}
				}
			}

			#pragma omp parallel for
			for (int i = 2; i < m_pGridData->getDimensions().x - 2; i++) {
				for (int j = 2; j < m_pGridData->getDimensions().y - 2; j++) {
					for (int k = 2; k < m_pGridData->getDimensions().z - 2; k++) {
						Vector3 interpolatedVorticityConfinement = vorticityConfinementForces(i, j, k) + vorticityConfinementForces(i - 1, j - 1, k - 1);
						interpolatedVorticityConfinement *= 0.5;
					
						m_pGridData->setVelocity(m_pGridData->getVelocity(i, j, k) + interpolatedVorticityConfinement, i, j, k);
					}
				}
			}


		}
		#pragma endregion 

		#pragma region Example External Forces
		void VoxelizedGridSolver3D::applySwirlingFields(Scalar dt) {
			// Method Constants
			const int max_iteration = 100;
			const Scalar addedVelocityCoefficient = 0.1;
			// Random Engine for Uniform Random Floating Point Numbers
			static std::mt19937_64 randEngine;
			static std::uniform_real_distribution<float> rand(0, 1);
			// Shortcuts:
			const Vector3 &maxBoundary = m_pGridData->getMaxBoundary();
			DoubleBuffer<Scalar, Array3D> &densityBuffer = m_pGridData->getDensityBuffer();
			const auto binomial = boost::math::binomial_coefficient<float>;
			Array3D<Vector3> linearVelocity(m_dimensions); linearVelocity.assign(Vector3(0, 0, 0));
			// Apply for each Swirling field
			for (swirlingField_t *pSwirlingField : m_params.swirlingFields) {
				const Vector3 &angularVelocity = pSwirlingField->angularVelocity;
				const int pointSize = pSwirlingField->controlPoints.size();
				#pragma omp parallel for
				for (int k = 0; k < m_dimensions.z; k++) {
					for (int j = 0; j < m_dimensions.y; j++) {
						for (int i = 0; i < m_dimensions.x; i++) {
							const Vector3 &position = m_pGridData->getCenterPoint(i, j, k);
							// Initialize density area every frame
							if (position.y > maxBoundary.y - pSwirlingField->densityArea) {
								#pragma omp critical
								const Scalar randNum = rand(randEngine);
								const Scalar noise = randNum * pSwirlingField->densityVariation;
								densityBuffer.setValueBothBuffers(pSwirlingField->densityValue - noise, i, j, k);
							}
							// Define the intialized velocity region and velocity
							if (position.y > pSwirlingField->bottomBoundary && position.y < pSwirlingField->topBoundary) {
								//Bezier Curve:
								// The curve represents the center of the tornado
								// Find the point on the curve which is closest to point p(position) by Netown's method 
								// binomial function calculates binomial coefficients.
								// Since the curve is defined in a parametric form, so variable t can defince the position of the point that we are looking for
								Scalar t = 0.5f;
								Vector3 f0(0, 0, 0), f1(0, 0, 0), f2(0, 0, 0);
								for (int n = 0; n < max_iteration; n++) {
									for (int a = 0, b = pointSize - 1; a < pointSize; a++, b--) {
										const Scalar binom = binomial(pointSize - 1, a);
										const Scalar powTA = std::powf(t, a);
										const Scalar powTAm1 = std::powf(t, a - 1);
										const Scalar powTAm2 = std::powf(t, a - 2);
										const Scalar pow1mTB = std::powf(1 - t, b);
										const Scalar pow1mTBm1 = std::powf(1 - t, b - 1);
										const Scalar pow1mTBm2 = std::powf(1 - t, b - 2);
										const Vector3 ptBinom = pSwirlingField->controlPoints[a] * binom;
										f0 += ptBinom * powTA * pow1mTB;
										f1 += ptBinom * (a * powTAm1 * pow1mTB - b * powTA * pow1mTBm1);
										f2 += ptBinom * (a * (a - 1) * powTAm2 * pow1mTB - 2 * a * b * powTAm1 * pow1mTBm1 + b * (b - 1) * powTA * pow1mTBm2);
									}
									const Scalar F0 = (position - f0).dot(f1);
									const Scalar F1 = -f1.dot(f1) + (position - f0).dot(f2);
									t = t - F0 / F1;
								}
								// calculate the position(f) of the point on the curve and the tangential direction(f1) on the point according to t, t ranging from 0 to 1
								f0 = Vector3(0, 0, 0);
								f1 = Vector3(0, 0, 0);
								for (int a = 0, b = pointSize - 1; a < pointSize; a++, b--) {
									const Scalar binom = binomial(pointSize - 1, a);
									const Scalar powTA = std::powf(t, a);
									const Scalar pow1mTB = std::powf(1 - t, b);
									const Scalar powTAm1 = std::powf(t, a - 1);
									const Scalar pow1mTBm1 = std::powf(1 - t, b - 1);
									f0 += pSwirlingField->controlPoints[a] * binom * powTA * pow1mTB;
									f1 += pSwirlingField->controlPoints[a] * binom * (a * powTAm1 * pow1mTB - b * powTA * pow1mTBm1);
								}
								const Scalar distance = (position - f0).length();
								// radius definition r = R*sqrt(t), the radius of cone is defined according to t
								const Scalar radius = pSwirlingField->maxRadius * std::powf(t, 0.5f);
								/* velocity defination
								 * if distance < r
								 *   tangentialv = w*r*pow(z,1/5)
								 *   radialv = -r*distance*z/(radialscalar*radialscalar)
									*
									*if distance>r
									*tangentialv = w*R*R/r*pow(z,1/5)
									*radialv = -r*z/distance if r>a
									*
									*verticalv = z*z/sqrt(2*pi)*exp(-(r-R)*(r-R)*z*z)*upscalar
									*z = B(t).y, y component of the posisiton of the point
									* radialscalar and upscalar are all constant to scale the magnitude of radialV and verticalV respectively
									*/
								Vector3 tangentialVelocity, radialVelocity;
								if (distance < radius) {
									tangentialVelocity = (f0 - position).cross(f1).normalized() * angularVelocity * radius * std::powf(f0.y, 0.2f);
									radialVelocity = (f0 - position).normalized() * radius * distance * f0.y / std::powf(pSwirlingField->radialScalar, 2);
								}
								else {
									tangentialVelocity = (f0 - position).cross(f1).normalized() * angularVelocity * std::powf(radius, 2) / distance * std::powf(f0.y, 0.2f);
									radialVelocity = (f0 - position).normalized() * radius * f0.y / distance;
								}
								const Vector3 verticalVelocity = f1.normalized() * std::powf(f0.y, 2) / std::sqrtf(2 * PI) * std::expf(-std::powf(f0.y * (distance - radius), 2)) * pSwirlingField->upScalar;
								Vector3 linVelocity = tangentialVelocity + radialVelocity - verticalVelocity;
								// In every frame, small amount of initialized velocity is added
								if (dt != 0) {
									linVelocity = addedVelocityCoefficient * (0.5f * (tangentialVelocity + radialVelocity) - verticalVelocity);
								}
								m_pGridData->setVelocity(m_pGridData->getVelocity(i, j, k) + linVelocity, i, j, k);
								linearVelocity(i, j, k) += linVelocity;
								if (dt == 0) {
									m_pGridData->setAuxiliaryVelocity(linearVelocity(i, j, k), i, j, k);
								}
							}
						}
					}
				}
			}

			// Update Particles too (linearly interpolate)
			BilinearEdgeStaggeredInterpolant3D<Vector3> linearVelocityInterpolant(linearVelocity, m_pGridData->getGridSpacing());
			if (m_params.pAdvectionParams->advectionCategory == LagrangianAdvection && dt != 0) { //dt == 0 initialization step
				ParticleBasedAdvection<Vector3, Array3D> *pParticleBasedAdv = dynamic_cast<ParticleBasedAdvection<Vector3, Array3D> *>(m_pAdvection);
				ParticlesData<Vector3> *pParticlesData = pParticleBasedAdv->getParticlesData();
				const vector<Vector3> &particlePositions = pParticlesData->getPositions();
				vector<Vector3> &particlesVelocities = pParticlesData->getVelocities();
				vector<Scalar> &densities = pParticlesData->getScalarBasedAttribute("density");

				for (int i = 0; i < particlePositions.size(); i++) {
					densities[i] = m_pDensityInterpolant->interpolate(particlePositions[i]);
					particlesVelocities[i] += linearVelocityInterpolant.interpolate(particlePositions[i]);
				}
			}
		}
		#pragma endregion


		#pragma region InitializationFunctions
		PoissonMatrix * VoxelizedGridSolver3D::createPoissonMatrix() {
			dimensions_t poissonMatrixDim = m_pGridData->getDimensions();
			if (m_params.pPoissonSolverParams->solverCategory == Krylov) {
				poissonMatrixDim.x += -2; poissonMatrixDim.y += -2; poissonMatrixDim.z += -2;
			}

			PoissonMatrix *pMatrix = NULL;
			if (m_params.pPoissonSolverParams->solverMethod == EigenCG ||
				m_params.pPoissonSolverParams->solverMethod == CPU_CG) {
				pMatrix = new PoissonMatrix(poissonMatrixDim, false);
			}
			else {
				pMatrix = new PoissonMatrix(poissonMatrixDim);
			}

			for (int i = 0; i < poissonMatrixDim.x; i++) {
				for (int j = 0; j < poissonMatrixDim.y; j++) {
					for (int k = 0; k < poissonMatrixDim.z; k++) {
						pMatrix->setRow(pMatrix->getRowIndex(i, j, k), -1, -1, -1, 6, -1, -1, -1);
					}
				}
			}

			for (unsigned int i = 0; i < m_boundaryConditions.size(); i++) {
				m_boundaryConditions[i]->updatePoissonMatrix(pMatrix);
			}

			//BoundaryCondition<Vector3, Array3D>::updateSolidWalls(pMatrix, Array3D<bool>(m_pGrid->getSolidMarkers(), m_dimensions), m_params.getPressureSolverParams().getMethodCategory() == Krylov);

			if (pMatrix->isSingular() && m_params.pPoissonSolverParams->solverMethod == Krylov)
				pMatrix->applyCorrection(1e-3);


			if (m_params.pPoissonSolverParams->solverMethod != EigenCG &&
				m_params.pPoissonSolverParams->solverMethod != CPU_CG)
				pMatrix->updateCudaData();

			return pMatrix;
		}

		void VoxelizedGridSolver3D::initializeInterpolants() {
			Scalar dx = m_pGridData->getGridSpacing();

			/** Velocity and intermediary velocities interpolants */
			m_pVelocityInterpolant = new TrilinearStaggeredInterpolant3D<Vector3>(m_pGridData->getVelocityArray(), dx);
			m_pAuxVelocityInterpolant = new TrilinearStaggeredInterpolant3D<Vector3>(m_pGridData->getAuxVelocityArray(), dx);
			m_pVelocityInterpolant->setSiblingInterpolant(m_pAuxVelocityInterpolant);

			m_pDensityInterpolant = new TrilinearStaggeredInterpolant3D<Scalar>(*m_pGridData->getDensityBuffer().getBufferArray1(), dx);
			m_pDensityInterpolant->setSiblingInterpolant(new TrilinearStaggeredInterpolant3D<Scalar>(*m_pGridData->getDensityBuffer().getBufferArray2(), dx));
			m_pDensityInterpolant->getSibilingInterpolant()->setSiblingInterpolant(m_pDensityInterpolant);

			m_pTemperatureInterpolant = new TrilinearStaggeredInterpolant3D<Scalar>(*m_pGridData->getTemperatureBuffer().getBufferArray1(), dx);
			m_pTemperatureInterpolant->setSiblingInterpolant(new TrilinearStaggeredInterpolant3D<Scalar>(*m_pGridData->getTemperatureBuffer().getBufferArray2(), dx));
			m_pTemperatureInterpolant->getSibilingInterpolant()->setSiblingInterpolant(m_pTemperatureInterpolant);

		}
		#pragma endregion 

		#pragma region BoundaryConditions
		void VoxelizedGridSolver3D::enforceSolidWallsConditions(const Vector3 &solidVelocity) {
			switch (m_params.solidBoundaryType) {
			case Solid_FreeSlip:
				FreeSlipBC<Vector3, Array3D>::enforceSolidWalls(dynamic_cast<HexaGrid *>(m_pGrid), solidVelocity);
				break;
			case Solid_NoSlip:
				NoSlipBC<Vector3, Array3D>::enforceSolidWalls(dynamic_cast<HexaGrid *>(m_pGrid), solidVelocity);
				break;

			case Solid_Interpolation:
				BoundaryCondition<Vector3, Array3D>::zeroSolidBoundaries(m_pGrid->getGridData3D());
				break;
			}
		}

		void VoxelizedGridSolver3D::updateSolidCells() {
			Scalar dx = m_pGridData->getGridSpacing();
			for (int i = 0; i < m_pGridData->getDimensions().x; i++) {
				for (int j = 0; j < m_pGridData->getDimensions().y; j++) {
					for (int k = 0; k < m_pGridData->getDimensions().z; k++) {
						Vector3 gridPoint((i + 0.5f)*dx, (j + 0.5f)*dx, (k + 0.5)*dx);
						
						/*Vector3 spherePosition(3.0, 4.5, 3.0);
						Scalar sphereRadius = 1.0;
						if ((gridPoint - spherePosition).length() < sphereRadius) {
							m_pGrid->setSolidCell(i, j, k);
						}
*/
						/** Todo: proper way to initialize */
						/*for (auto pPolyMesh : m_polyMeshes) {
							if (pPolyMesh->isPointInside(gridPoint)) {
								m_pGrid->setSolidCell(i, j, k);
							}
						}	*/
					}
				}
			}
		}
		#pragma endregion 

		#pragma region PressureProjection
		Scalar VoxelizedGridSolver3D::calculateFluxDivergent(int i, int j, int k) {
			return (m_pGridData->getAuxiliaryVelocity(i + 1, j, k).x - m_pGridData->getAuxiliaryVelocity(i, j, k).x) / m_pGridData->getScaleFactor(i, j, k).x +
				(m_pGridData->getAuxiliaryVelocity(i, j + 1, k).y - m_pGridData->getAuxiliaryVelocity(i, j, k).y) / m_pGridData->getScaleFactor(i, j, k).y +
				(m_pGridData->getAuxiliaryVelocity(i, j, k + 1).z - m_pGridData->getAuxiliaryVelocity(i, j, k).z) / m_pGridData->getScaleFactor(i, j, k).z;
		}

		void VoxelizedGridSolver3D::divergenceFree(Scalar dt) {
			int i, j;
			//#pragma omp parallel for
			for (i = 1; i < m_dimensions.x - 1; i++) {
				//#pragma omp parallel for private(j)
				for (j = 1; j < m_dimensions.y - 1; j++) {
					for (int k = 1; k < m_dimensions.z - 1; k++) {
						Vector3 velocity;

						if (m_pGrid->isSolidCell(i, j, k)) {
							m_pGridData->setVelocity(Vector3(0, 0, 0), i, j, k);
							continue;
						}


						Scalar dx = m_pGridData->getScaleFactor(i, j, k).x;
						Scalar dy = m_pGridData->getScaleFactor(i, j, k).y;
						Scalar dz = m_pGridData->getScaleFactor(i, j, k).z;
						Scalar pressG = (m_pGridData->getPressure(i, j, k) - m_pGridData->getPressure(i, j - 1, k)) / dy;
						velocity.x = m_pGridData->getAuxiliaryVelocity(i, j, k).x - dt*((m_pGridData->getPressure(i, j, k) - m_pGridData->getPressure(i - 1, j, k)) / dx);
						velocity.y = m_pGridData->getAuxiliaryVelocity(i, j, k).y - dt*((m_pGridData->getPressure(i, j, k) - m_pGridData->getPressure(i, j - 1, k)) / dy);
						velocity.z = m_pGridData->getAuxiliaryVelocity(i, j, k).z - dt*((m_pGridData->getPressure(i, j, k) - m_pGridData->getPressure(i, j, k - 1)) / dz);

						if (m_pGrid->isSolidCell(i - 1, j, k))
							velocity.x = 0;
						if (m_pGrid->isSolidCell(i, j - 1, k))
							velocity.y = 0;
						if (m_pGrid->isSolidCell(i, j, k - 1))
							velocity.z = 0;

						m_pGridData->setVelocity(velocity, i, j, k);
					}
				}
			}
		}
		#pragma endregion 

		#pragma region InternalUpdateFunctions
		void VoxelizedGridSolver3D::updateVorticity() {
			for (int i = 1; i < m_dimensions.x - 1; i++) {
				for (int j = 1; j < m_dimensions.y - 1; j++) {
					for (int k = 1; k < m_dimensions.z - 1; k++) {
						Vector3 vorticityVec = calculateVorticity(i, j, k);
						m_pGridData->setVorticity(vorticityVec.length(), i, j, k);
					}
				}
			}
		}

		Vector3 VoxelizedGridSolver3D::calculateVorticity(uint i, uint j, uint k) {
			Scalar dx2 = m_pGridData->getGridSpacing()*2;
			Vector3 vorticityVec;
			vorticityVec.x = (m_pGridData->getVelocity(i, j + 1, k).z - m_pGridData->getVelocity(i, j - 1, k).z) / dx2;
			vorticityVec.x -= (m_pGridData->getVelocity(i, j, k + 1).y - m_pGridData->getVelocity(i, j, k - 1).y) / dx2;

			vorticityVec.y = (m_pGridData->getVelocity(i, j, k + 1).x - m_pGridData->getVelocity(i, j, k - 1).x) / dx2;
			vorticityVec.y -= (m_pGridData->getVelocity(i + 1, j, k).z - m_pGridData->getVelocity(i - 1, j, k).z) / dx2;

			vorticityVec.z = (m_pGridData->getVelocity(i + 1, j, k).y - m_pGridData->getVelocity(i - 1, j, k).y) / dx2;
			vorticityVec.z -= (m_pGridData->getVelocity(i, j + 1, k).x - m_pGridData->getVelocity(i, j - 1, k).x) / dx2;
			
			return vorticityVec;
		}
		#pragma endregion 
	}
	
}
