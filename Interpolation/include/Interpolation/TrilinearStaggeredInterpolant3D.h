#pragma once
#pragma once
//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef _MATH_TRILINEAR_INTERPOLANT_3D_H_
#define _MATH_TRILINEAR_INTERPOLANT_3D_H_

#pragma once

#include "Interpolation/Interpolant.h"

namespace Chimera {

	using namespace Core;

	namespace Interpolation {

		/** Bilinear staggered interpolation for regular grids*/
		template <typename GridMeshType>
		class TrilinearStaggeredInterpolant3D : public Interpolant<typename GridMeshType::VectorType, GridMeshType> {

		protected:
			//Useful definitions
			typedef typename GridMeshType::VectorType VectorType;
			using ParentClass = Interpolant<VectorType, GridMeshType>;

		public:

			#pragma region Constructors
			TrilinearStaggeredInterpolant3D(shared_ptr<GridMesh<GridMeshType>> pMesh, const string& attributeName)
				: TrilinearStaggeredInterpolant3D::Interpolant(pMesh, attributeName) {

				HexaGridMesh<GridMeshType>* pHexaGridMeshPtr = dynamic_cast<HexaGridMesh<GridMeshType>*>(pMesh.get());
				if (!pHexaGridMeshPtr) {
					throw std::logic_error("TrilinearStaggeredInterpolant3D: mesh is not a valid hexaGridMesh");
				}
				isVector3<VectorType> vectorType;
				if (!vectorType.value) {
					throw std::logic_error("TrilinearStaggeredInterpolant3D: just valid for Vector3 types");
				}

				m_pHexaMesh = shared_ptr<HexaGridMesh<GridMeshType>>(pHexaGridMeshPtr);
			}
			#pragma endregion


			#pragma region Functionalities
			/* Basic interpolation function */
			virtual VectorType interpolate(const VectorType &position);
			#pragma endregion

		protected:
			#pragma region ClassMembers
			shared_ptr<HexaGridMesh<GridMeshType>> m_pHexaMesh;
			#pragma endregion

			#pragma region PrivateFunctionalities
			/** Interpolates single component */
			virtual void interpolateComponent(int i, int j, int k, const VectorType&position, const VectorType &cellIniPosition, const VectorType &cellFinalPosition, velocityComponent_t velocityComponent, VectorType &interpolatedVelocity);
			#pragma endregion
		};
	}

}
#endif
