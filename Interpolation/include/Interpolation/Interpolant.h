#pragma once
//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef _MATH_INTERPOLANT_2D_H_
#define _MATH_INTERPOLANT_2D_H_

#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "ChimeraMesh.h"

namespace Chimera {

	using namespace Grids;
	namespace Interpolation {

		/** Interpolant base classWorks for scalarFields or vectorFields, just pass it on the template. Pure-virtual 
		 ** class, concrete 2D and 3D, linear and non-linear classes will derive from this. First template argument is
		 ** the type (e.g., scalar or vectors) of the interpolated/original values, second template is the gridMeshType,
		 ** which defines voxelType (e.g. QuadCell<valueType> for 2-D, HexaCell<valueType> for 3-D) and also the 
		 ** vectorType (Vector2, Vector3, etc.) of the position that will be interpolated.*/
		template <class valueType, typename GridMeshType>
		class Interpolant  {

		public:

			#pragma region Constructors
			/** Constructor receives the mesh in which the values will be interpolated from. Additionally, it receives
					the attribute name that will be interpolated from the mesh structure stored internally.*/
			Interpolant(shared_ptr<GridMesh<GridMeshType>> pGridMesh, const string &attributeName) : m_pGridMesh(pGridMesh), m_attributeName(attributeName) {
				
			}
			#pragma endregion
			
			#pragma region Functionalities
			/* Basic interpolation function */
			virtual valueType interpolate(const typename GridMeshType::VectorType &position) = 0;
			#pragma endregion

			#pragma region Access Functions
			/** Access to mesh where values are being interpolated */
			shared_ptr<GridMesh<GridMeshType>> getGridMesh() {
				return m_pGridMesh;
			}

			/** Returns the interpolated attribute string that is accessed from mesh elements for interpolation */
			const string& getInterpolatedAttributeName() const {
				return m_attributeName;
			}

			/** Sibling interpolants are useful for algorithms that need interpolation for values before and after 
				advection (e.g., FLIP). Removes the need of the cloning functions. */
			FORCE_INLINE void setSiblingInterpolant(shared_ptr<Interpolant<valueType, GridMeshType>> pInterpolant) {
				m_pSiblingInterpolant = pInterpolant;
			}

			FORCE_INLINE shared_ptr<Interpolant<valueType, GridMeshType>> getSibilingInterpolant() {
				return m_pSiblingInterpolant.lock();
			}
			#pragma endregion
		protected:
			#pragma region ClassMembers
			/** GridMesh for interpolation */
			shared_ptr<GridMesh<GridMeshType>> m_pGridMesh;

			/** Attribute name for accessing on internal mesh structures */
			string m_attributeName;

			/** Sibling interpolant: does not have array ownership to avoid cyclic references*/
			weak_ptr<Interpolant<valueType, GridMeshType>> m_pSiblingInterpolant;
			#pragma endregion
		};
	}
	

}
#endif
