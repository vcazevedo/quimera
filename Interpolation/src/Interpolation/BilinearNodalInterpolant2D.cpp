#include "Interpolation/BilinearNodalInterpolant2D.h"


namespace Chimera {
	namespace Interpolation {

		#pragma region Functionalities
		template<class valueType, class VectorType>
		/** Scalar-based nodal interpolation in regular grids: assume that the values on the Array2D are stored in nodal locations.
		  * This means no off-set from current position. */
		valueType BilinearNodalInterpolant2D<valueType, VectorType>::interpolate(const VectorType &position) {
			VectorType gridSpacePosition = m_pQuadMesh->getGridSpacePosition(position);

			/** If the cell has a complex geometry, use cut-cells interpolation */
			if (m_pQuadMesh->isCutCell(floor(gridSpacePosition.x), floor(gridSpacePosition.y))) {
				return meanValueCoordinates(position);
			}

			const string& attributeName(ParentClass::m_attributeName);

			int i = static_cast <int> (floor(gridSpacePosition.x));
			int j = static_cast <int> (floor(gridSpacePosition.y));

			i = clamp(i, 0, m_pQuadMesh->getGridDimensions().x);
			j = clamp(j, 0, m_pQuadMesh->getGridDimensions().y);

			int nextI = clamp(i + 1, 0, m_pQuadMesh->getGridDimensions().x + 1);
			int nextJ = clamp(j + 1, 0, m_pQuadMesh->getGridDimensions().y + 1);

			Vector2 x1Position(i + 0.0f, j + 0.0f);
			Vector2 x2Position(i + 1.0f, j + 1.0f);

			/** Values are located on edges, regular grid interpolation assumes there's only one value on the edge*/
			valueType v1 = OwnCustomAttribute<valueType>::get(m_pQuadMesh->getVertex(i, j))->getAttribute(attributeName);
			valueType v2 = OwnCustomAttribute<valueType>::get(m_pQuadMesh->getVertex(nextI, j))->getAttribute(attributeName);
			valueType v3 = OwnCustomAttribute<valueType>::get(m_pQuadMesh->getVertex(i, nextJ))->getAttribute(attributeName);
			valueType v4 = OwnCustomAttribute<valueType>::get(m_pQuadMesh->getVertex(nextI, nextJ))->getAttribute(attributeName);

			if (i == m_pQuadMesh->getGridDimensions().x - 1 && j == m_pQuadMesh->getGridDimensions().y - 1) //Last possible cell
				return v1;
			else if (i == m_pQuadMesh->getGridDimensions().x - 1) //Right boundary
				return v1 * (x2Position.y - gridSpacePosition.y) + v3 * (gridSpacePosition.y - x1Position.y);
			else if (j == m_pQuadMesh->getGridDimensions().y - 1) //Top boundary
				return v1 * (x2Position.x - gridSpacePosition.x) + v2 * (gridSpacePosition.x - x1Position.x);
			else //Normal cell
				return	v1 * (x2Position.x - gridSpacePosition.x) * (x2Position.y - gridSpacePosition.y) +
								v2 * (gridSpacePosition.x - x1Position.x) * (x2Position.y - gridSpacePosition.y) +
								v3 * (x2Position.x - gridSpacePosition.x) * (gridSpacePosition.y - x1Position.y) +
								v4 * (gridSpacePosition.x - x1Position.x) * (gridSpacePosition.y - x1Position.y);
		}
		#pragma endregion


		#pragma region PrivateFunctionalities
		template<class valueType, class VectorType>
		valueType BilinearNodalInterpolant2D<valueType, VectorType>::meanValueCoordinates(const VectorType& position) {
			VectorType gridSpacePosition = m_pQuadMesh->getGridSpacePosition(position);
			const string& attributeName(ParentClass::m_attributeName);

			int i = static_cast <int> (floor(gridSpacePosition.x));
			int j = static_cast <int> (floor(gridSpacePosition.y));

			i = clamp(i, 0, m_pQuadMesh->getGridDimensions().x);
			j = clamp(j, 0, m_pQuadMesh->getGridDimensions().y);

			shared_ptr<Cell<VectorType>> pCell = m_pQuadMesh->getQuadCell(i, j)->getCell(position);
			const vector<shared_ptr<HalfEdge<VectorType>>> &cellHalfEdges = pCell->getHalfCells().first->getHalfEdges();
			vector<Scalar> weights(cellHalfEdges.size(), 0);
			VectorType rCurr, rPrev, rNext;
			Scalar rCurrLength;
			for (int i = 0; i < cellHalfEdges.size(); i++) {
				auto currVertex = cellHalfEdges[i]->getVertices().first;
				rCurr = currVertex->getPosition() - position;

				rCurrLength = rCurr.length();
				if (rCurrLength <= g_pointProximityLenght) {
					return OwnCustomAttribute<valueType>::get(currVertex)->getAttribute(attributeName);
				}

				int prevI = roundClamp<int>(i - 1, 0, cellHalfEdges.size());
				rPrev = cellHalfEdges[prevI]->getVertices().first->getPosition() - position;
				int nextI = roundClamp<int>(i + 1, 0, cellHalfEdges.size());
				rNext = cellHalfEdges[nextI]->getVertices().first->getPosition() - position;

				Scalar aVal = calculateADet(rPrev, rCurr) / 2;
				if (aVal != 0)
					weights[i] += (rPrev.length() - rPrev.dot(rCurr) / rCurrLength) / aVal;
				aVal = calculateADet(rCurr, rNext) / 2;
				if (aVal != 0)
					weights[i] += (rNext.length() - rNext.dot(rCurr) / rCurrLength) / aVal;
			}

			Scalar totalWeight = 0;
			for (int i = 0; i < weights.size(); i++) {
				totalWeight += weights[i];
			}

			valueType result;
			for (int i = 0; i < weights.size(); i++) {
					auto currVertex = cellHalfEdges[i]->getVertices().first;
					result += OwnCustomAttribute<valueType>::get(currVertex)->getAttribute(attributeName) * (weights[i] / totalWeight);
			}
			return result;
		}
		
		#pragma endregion

		/** Template linker trickerino for templated classes in CPP*/
		template class BilinearNodalInterpolant2D<Scalar, QuadGridType>;
		template class BilinearNodalInterpolant2D<Scalar, QuadGridTypeDouble>;
		template class BilinearNodalInterpolant2D<Vector2, QuadGridType>;
		template class BilinearNodalInterpolant2D<Vector2D, QuadGridTypeDouble>;
	}
}
