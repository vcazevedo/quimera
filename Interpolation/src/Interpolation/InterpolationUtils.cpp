#include "Interpolation/InterpolationUtils.h"

namespace Chimera {
	namespace Interpolation {
		
		template<typename GridMeshType>
		void staggeredToNodeCentered(shared_ptr<QuadGridMesh<GridMeshType>> pQuadMesh, string attributeName) {
			typedef typename GridMeshType::VectorType VectorType;

			for (int i = 0; i < pQuadMesh->getGridDimensions().x; i++) {
				for (int j = 0; j < pQuadMesh->getGridDimensions().y; j++) {
					int prevI = i > 0 ? i - 1 : 0;
					int prevJ = j > 0 ? j - 1 : 0;

					//X-component for the velocity
					{
						Scalar totalArea = pQuadMesh->getEdges(dimensions_t(i, prevJ), yAlignedEdge).back()->getLength() +
																pQuadMesh->getEdges(dimensions_t(i, j), yAlignedEdge).front()->getLength();

						Scalar alfa = pQuadMesh->getEdges(dimensions_t(i, j), yAlignedEdge).front()->getLength() / totalArea;

						VectorType vx1 = OwnCustomAttribute<VectorType>::get(pQuadMesh->getEdges(dimensions_t(i, prevJ), yAlignedEdge).back())->getAttribute(attributeName);
						VectorType vx2 = OwnCustomAttribute<VectorType>::get(pQuadMesh->getEdges(dimensions_t(i, j), yAlignedEdge).front())->getAttribute(attributeName);
						OwnCustomAttribute<VectorType>::get(pQuadMesh->getVertex(i, j))->setAttribute(attributeName, alfa * vx1 + (1 - alfa) * vx2);
					}
					//Y-component for the velocity
					{
						Scalar totalArea = pQuadMesh->getEdges(dimensions_t(prevI, j), xAlignedEdge).back()->getLength() +
							pQuadMesh->getEdges(dimensions_t(i, j), xAlignedEdge).front()->getLength();

						Scalar alfa = pQuadMesh->getEdges(dimensions_t(i, j), xAlignedEdge).front()->getLength() / totalArea;

						Scalar vy1 = OwnCustomAttribute<VectorType>::get(pQuadMesh->getEdges(dimensions_t(prevI, j), xAlignedEdge).back())->getAttribute(attributeName)[1];
						Scalar vy2 = OwnCustomAttribute<VectorType>::get(pQuadMesh->getEdges(dimensions_t(i, j), xAlignedEdge).front())->getAttribute(attributeName)[1];

						//Keep the x-component interpolated from previous vector
						VectorType previousVelocity = OwnCustomAttribute<VectorType>::get(pQuadMesh->getVertex(i, j))->getAttribute(attributeName);
						previousVelocity[1] = alfa * vy1 + (1 - alfa) * vy2;
						OwnCustomAttribute<VectorType>::get(pQuadMesh->getVertex(i, j))->setAttribute(attributeName, previousVelocity);
					}

				}
			}
		}


		template<typename GridMeshType>
		void staggeredToNodeCentered(shared_ptr<HexaGridMesh<GridMeshType>> pHexaMesh, string attributeName) {
			/*for (int i = 1; i < sourceStaggered.getDimensions().x - 1; i++) {
				for (int j = 1; j < sourceStaggered.getDimensions().y - 1; j++) {
					for (int k = 1; k < sourceStaggered.getDimensions().z - 1; k++) {

					}
				}
			}*/
		}

		template void staggeredToNodeCentered<QuadGridType>(shared_ptr<QuadGridMesh<QuadGridType>> pQuadMesh, string attributeName);
		template void staggeredToNodeCentered<QuadGridTypeDouble>(shared_ptr<QuadGridMesh<QuadGridTypeDouble>> pQuadMesh, string attributeName);
	
		template void staggeredToNodeCentered<HexaGridType>(shared_ptr<HexaGridMesh<HexaGridType>> pHexaMesh, string attributeName);
		template void staggeredToNodeCentered<HexaGridTypeDouble>(shared_ptr<HexaGridMesh<HexaGridTypeDouble>> pHexaMesh, string attributeName);
	}
}
