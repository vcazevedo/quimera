#include "Interpolation/BilinearStaggeredInterpolant2D.h"

namespace Chimera {
	namespace Interpolation {
		
		#pragma region Functionalities
		template<class valueType, typename GridMeshType>
		typename valueType BilinearStaggeredInterpolant2D<valueType, GridMeshType>::interpolate(const typename GridMeshType::VectorType &position) {
			typedef typename GridMeshType::VectorType VectorType;

			VectorType gridSpacePosition = m_pQuadMesh->getGridSpacePosition(position);
			VectorType interpolatedVelocity;

			int i = static_cast <int> (floor(gridSpacePosition.x));
			int j = static_cast <int> (floor(gridSpacePosition.y - 0.5f));

			i = clamp(i, 0, m_pQuadMesh->getGridDimensions().x);
			j = clamp(j, 0, m_pQuadMesh->getGridDimensions().y);

			int nextI = clamp(i + 1, 0, m_pQuadMesh->getGridDimensions().x + 1);
			int nextJ = clamp(j + 1, 0, m_pQuadMesh->getGridDimensions().y + 1);

			/** Interpolate for X component first*/
			VectorType x1Position(static_cast <Scalar>(i), j + 0.5f);
			VectorType x2Position(i + 1.0f, j + 1.5f);

			/** Values are located on edges, regular grid interpolation assumes there's only one value on the edge*/
			const string& attributeName(ParentClass::m_attributeName);
			Scalar v1 = OwnCustomAttribute<Scalar>::get(m_pQuadMesh->getEdges(dimensions_t(i, j), yAlignedEdge).front())->getAttribute(attributeName);
			Scalar v2 = OwnCustomAttribute<Scalar>::get(m_pQuadMesh->getEdges(dimensions_t(nextI, j), yAlignedEdge).front())->getAttribute(attributeName);
			Scalar v3 = OwnCustomAttribute<Scalar>::get(m_pQuadMesh->getEdges(dimensions_t(i, nextJ), yAlignedEdge).front())->getAttribute(attributeName);
			Scalar v4 = OwnCustomAttribute<Scalar>::get(m_pQuadMesh->getEdges(dimensions_t(nextI, nextJ), yAlignedEdge).front())->getAttribute(attributeName);

			if (i == m_pQuadMesh->getGridDimensions().x - 1 && j == m_pQuadMesh->getGridDimensions().y - 1)
				interpolatedVelocity.x = v1;
			else if (i == m_pQuadMesh->getGridDimensions().x - 1)
				interpolatedVelocity.x = v1 * (x2Position.y - gridSpacePosition.y) + v3 * (gridSpacePosition.y - x1Position.y);
			else if (j == m_pQuadMesh->getGridDimensions().y - 1)
				interpolatedVelocity.x = v1 * (x2Position.x - gridSpacePosition.x) + v2 * (gridSpacePosition.x - x1Position.x);
			else
				interpolatedVelocity.x =	v1 * (x2Position.x - gridSpacePosition.x) * (x2Position.y - gridSpacePosition.y) +
																	v2 * (gridSpacePosition.x - x1Position.x) * (x2Position.y - gridSpacePosition.y) +
																	v3 * (x2Position.x - gridSpacePosition.x) * (gridSpacePosition.y - x1Position.y) +
																	v4 * (gridSpacePosition.x - x1Position.x) * (gridSpacePosition.y - x1Position.y);

			i = static_cast <int> (floor(gridSpacePosition.x - 0.5f));
			j = static_cast <int> (floor(gridSpacePosition.y));

			i = clamp(i, 0, m_pQuadMesh->getGridDimensions().x);
			j = clamp(j, 0, m_pQuadMesh->getGridDimensions().y);

			nextI = clamp(i + 1, 0, m_pQuadMesh->getGridDimensions().x + 1);
			nextJ = clamp(j + 1, 0, m_pQuadMesh->getGridDimensions().y + 1);

			/** Values are located on edges, regular grid interpolation assumes there's only one value on the edge*/
			v1 = OwnCustomAttribute<Scalar>::get(m_pQuadMesh->getEdges(dimensions_t(i, j), xAlignedEdge).front())->getAttribute(attributeName);
		  v2 = OwnCustomAttribute<Scalar>::get(m_pQuadMesh->getEdges(dimensions_t(nextI, j), xAlignedEdge).front())->getAttribute(attributeName);
			v3 = OwnCustomAttribute<Scalar>::get(m_pQuadMesh->getEdges(dimensions_t(i, nextJ), xAlignedEdge).front())->getAttribute(attributeName);
			v4 = OwnCustomAttribute<Scalar>::get(m_pQuadMesh->getEdges(dimensions_t(nextI, nextJ), xAlignedEdge).front())->getAttribute(attributeName);

			/** Interpolate for Y component later*/
			VectorType y1Position, y2Position;
			y1Position = VectorType(i + 0.5f, static_cast<Scalar>(j));
			y2Position = VectorType(i + 1.5f, j + 1.0f);

			if (i == m_pQuadMesh->getGridDimensions().x - 1 && j == m_pQuadMesh->getGridDimensions().y - 1)
				interpolatedVelocity.y = v1;
			else if (i == m_pQuadMesh->getGridDimensions().x - 1)
				interpolatedVelocity.y = v1 * (x2Position.y - gridSpacePosition.y) + v3 * (gridSpacePosition.y - x1Position.y);
			else if (j == m_pQuadMesh->getGridDimensions().y - 1)
				interpolatedVelocity.y = v1 * (x2Position.x - gridSpacePosition.x) + v2 * (gridSpacePosition.x - x1Position.x);
			else 
				interpolatedVelocity.y = v1 * (y2Position.x - gridSpacePosition.x) * (y2Position.y - gridSpacePosition.y) +
																 v2 * (gridSpacePosition.x - y1Position.x) * (y2Position.y - gridSpacePosition.y) +
																 v3 * (y2Position.x - gridSpacePosition.x) * (gridSpacePosition.y - y1Position.y) +
																 v4 * (gridSpacePosition.x - y1Position.x) * (gridSpacePosition.y - y1Position.y);
			
			return interpolatedVelocity;
		}
		#pragma endregion

		#pragma region PrivateFunctionalities
		template<class valueType, typename GridMeshType>
		typename GridMeshType::VectorType BilinearStaggeredInterpolant2D<valueType, GridMeshType>::interpolateFaceAttribute(int i, int j, velocityComponent_t velocityComponent) {
			typedef typename GridMeshType::VectorType VectorType;

			const string& attributeName(ParentClass::m_attributeName);
			VectorType faceVelocity;

			if (velocityComponent == xComponent) {
				int nextJ = j == m_pQuadMesh->getGridDimensions().y - 1 ? j : j + 1;
				int prevI = i == 0 ? 0 : i - 1;
				
				faceVelocity.x = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(i, j), yAlignedEdge).front())->getAttribute(attributeName).x;

				/** Values are located on edges, regular grid interpolation assumes there's only one value on the edge*/
				Scalar v1 = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(i, j), xAlignedEdge).front())->getAttribute(attributeName).y;
				Scalar v2 = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(prevI, j), xAlignedEdge).front())->getAttribute(attributeName).y;
				Scalar v3 = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(i, nextJ), xAlignedEdge).front())->getAttribute(attributeName).y;
				Scalar v4 = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(prevI, nextJ), xAlignedEdge).front())->getAttribute(attributeName).y;

				faceVelocity.y = 0.25f*(v1 + v2 + v3 + v4);
			}
			else if (velocityComponent == yComponent) {
				int nextI = i == m_pQuadMesh->getGridDimensions().x - 1? i : i + 1;
				int prevJ = j == 0 ? 0 : j - 1;

				faceVelocity.y = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(i, j), xAlignedEdge).front())->getAttribute(attributeName).y;

				/** Values are located on edges, regular grid interpolation assumes there's only one value on the edge*/
				Scalar v1 = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(i, j), yAlignedEdge).front())->getAttribute(attributeName).x;
				Scalar v2 = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(nextI, j), yAlignedEdge).front())->getAttribute(attributeName).x;
				Scalar v3 = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(i, prevJ), yAlignedEdge).front())->getAttribute(attributeName).x;
				Scalar v4 = OwnCustomAttribute<VectorType>::get(m_pQuadMesh->getEdges(dimensions_t(nextI, prevJ), yAlignedEdge).front())->getAttribute(attributeName).x;

				faceVelocity.x = 0.25f * (v1 + v2 + v3 + v4);
			}

			return faceVelocity;
		}
		#pragma endregion
		///** Template linker trickerino for templated classes in CPP*/
		template class BilinearStaggeredInterpolant2D<Vector2, QuadGridType>;
		template class BilinearStaggeredInterpolant2D<Vector2D, QuadGridTypeDouble>;
	}
}
