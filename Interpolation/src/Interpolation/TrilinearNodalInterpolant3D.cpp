#include "Interpolation/TrilinearNodalInterpolant3D.h"


namespace Chimera {
	namespace Interpolation {

		#pragma region Functionalities
		/** Vector-based nodal interpolation in regular grids: assume that the values on the Array3D are stored in nodal locations.
		  * This means no off-set from current position. */
		template<class valueType, typename GridMeshType>
		valueType TrilinearNodalInterpolant3D<valueType, GridMeshType>::interpolate(const typename GridMeshType::VectorType &position) {
			typedef typename GridMeshType::VectorType VectorType;

			VectorType gridSpacePosition(position / m_pHexaMesh->getGridSpacing());

			/** If the cell has a complex geometry, use cut-cells interpolation */
			if (m_pHexaMesh->isCutVoxel(floor(gridSpacePosition.x), floor(gridSpacePosition.y), floor(gridSpacePosition.z))) {
				return meanValueCoordinates(position);
			}

			int i = static_cast <int> (floor(gridSpacePosition.x - 0.5f));
			int j = static_cast <int> (floor(gridSpacePosition.y - 0.5f));
			int k = static_cast <int> (floor(gridSpacePosition.z - 0.5f));
			i = clamp(i, 0, m_pHexaMesh->getGridDimensions().x);
			j = clamp(j, 0, m_pHexaMesh->getGridDimensions().y);
			k = clamp(k, 0, m_pHexaMesh->getGridDimensions().z);

			VectorType x1Position(i + 0.5f, j + 0.5f, k + 0.5f);
			VectorType x2Position(i + 1.5f, j + 1.5f, k + 1.5f);

			int nextI = clamp(i + 1, 0, m_pHexaMesh->getGridDimensions().x + 1);
			int nextJ = clamp(j + 1, 0, m_pHexaMesh->getGridDimensions().y + 1);
			int nextK = clamp(k + 1, 0, m_pHexaMesh->getGridDimensions().z + 1);


			const string& attributeName(ParentClass::getInterpolatedAttributeName());

			/** Values are located on edges, regular grid interpolation assumes there's only one value on the edge*/
			valueType v1 = OwnCustomAttribute<valueType>::get(m_pHexaMesh->getVertex(i, j, k))->getAttribute(attributeName);
			valueType v2 = OwnCustomAttribute<valueType>::get(m_pHexaMesh->getVertex(nextI, j, k))->getAttribute(attributeName);
			valueType v3 = OwnCustomAttribute<valueType>::get(m_pHexaMesh->getVertex(i, nextJ, k))->getAttribute(attributeName);
			valueType v4 = OwnCustomAttribute<valueType>::get(m_pHexaMesh->getVertex(nextI, nextJ, k))->getAttribute(attributeName);

			/** Values are located on edges, regular grid interpolation assumes there's only one value on the edge*/
			valueType v5 = OwnCustomAttribute<valueType>::get(m_pHexaMesh->getVertex(i, j, nextK))->getAttribute(attributeName);
			valueType v6 = OwnCustomAttribute<valueType>::get(m_pHexaMesh->getVertex(nextI, j, nextK))->getAttribute(attributeName);
			valueType v7 = OwnCustomAttribute<valueType>::get(m_pHexaMesh->getVertex(i, nextJ, nextK))->getAttribute(attributeName);
			valueType v8 = OwnCustomAttribute<valueType>::get(m_pHexaMesh->getVertex(nextI, nextJ, nextK))->getAttribute(attributeName);

			valueType vk1 =	v1 * (x2Position.x - gridSpacePosition.x) * (x2Position.y - gridSpacePosition.y) +
											v2 * (gridSpacePosition.x - x1Position.x) * (x2Position.y - gridSpacePosition.y) +
											v3 * (x2Position.x - gridSpacePosition.x) * (gridSpacePosition.y - x1Position.y) +
											v4 * (gridSpacePosition.x - x1Position.x) * (gridSpacePosition.y - x1Position.y);

			valueType vk2 =	v5 * (x2Position.x - gridSpacePosition.x) * (x2Position.y - gridSpacePosition.y) +
											v6 * (gridSpacePosition.x - x1Position.x) * (x2Position.y - gridSpacePosition.y) +
											v7 * (x2Position.x - gridSpacePosition.x) * (gridSpacePosition.y - x1Position.y) +
											v8 * (gridSpacePosition.x - x1Position.x) * (gridSpacePosition.y - x1Position.y);

			Scalar alpha = gridSpacePosition.z - x1Position.z;
			return v1*(1 - alpha) + v2*alpha;
		}
		#pragma endregion

		#pragma region PrivateFunctionalities
		template<class valueType, typename GridMeshType>
		valueType TrilinearNodalInterpolant3D<valueType, GridMeshType>::meanValueCoordinates(const typename GridMeshType::VectorType& position) {
			typedef typename GridMeshType::VectorType VectorType;

			VectorType gridSpacePosition(position / m_pHexaMesh->getGridSpacing());
			const string& attributeName(ParentClass::m_attributeName);

			int i = static_cast <int> (floor(gridSpacePosition.x));
			int j = static_cast <int> (floor(gridSpacePosition.y));
			int k = static_cast <int> (floor(gridSpacePosition.z));

			i = clamp(i, 0, m_pHexaMesh->getGridDimensions().x);
			j = clamp(j, 0, m_pHexaMesh->getGridDimensions().y);
			k = clamp(k, 0, m_pHexaMesh->getGridDimensions().z);

			shared_ptr<Volume<VectorType>> pVolume = m_pHexaMesh->getHexaVolume(i, j, k)->getVolume(position);
			uint numberVertices = pVolume->getVerticesMap().size();

			// Auxiliary vectors
			vector<DoubleScalar> weights(numberVertices, 0);
			vector<DoubleScalar> dist(numberVertices, 0);
			vector<VectorType> uVec(numberVertices);
			vector<VectorType> vfs(pVolume->getCells().size());
			vector<vector<DoubleScalar>> perFaceLambdas(pVolume->getCells().size());
			map<uint, DoubleScalar> weightMap;

			static const double eps = 1e-6;

			shared_ptr<HalfVolume<VectorType>> pHalfVolume = pVolume->getHalfVolumes().first;

			/* Calculate VF for each face*/
			for (int i = 0; i < pHalfVolume->getHalfCells().size(); i++) {
				VectorType vf;

				/** First compute VFs per face and the total denominator */
				auto halfEdges = pHalfVolume->getHalfCells()[i]->getHalfEdges();
				for (int j = 0; j < halfEdges.size(); j++) {
					VectorType currV = halfEdges[j]->getVertices().first->getPosition() - position;
					VectorType nextV = halfEdges[j]->getVertices().second->getPosition() - position;

					VectorType currNormal = currV.cross(nextV).normalized();
					Scalar normalizedDot = currV.dot(nextV) / (currV.length() * nextV.length());
					DoubleScalar tetaAngle = acos(clamp<Scalar>(normalizedDot, -(1.f - eps), 1.f - eps));
					if (abs(tetaAngle) < eps) {
						vf = currV;
						return OwnCustomAttribute<valueType>::get(halfEdges[j]->getVertices().first)->getAttribute(attributeName);
					}
					vf += (currNormal) * 0.5 * tetaAngle;
				}

				vector<DoubleScalar> currFaceLambdas(halfEdges.size());
				vfs[i] = vf;
				VectorType v = vfs[i].normalized();

				DoubleScalar denominator = 0;
				for (int j = 0; j < halfEdges.size(); j++) {
					int prevJ = roundClamp<int>(j - 1, 0, halfEdges.size());

					VectorType prevV = halfEdges[prevJ]->getVertices().first->getPosition() - position;
					VectorType currV = halfEdges[j]->getVertices().first->getPosition() - position;
					VectorType nextV = halfEdges[j]->getVertices().second->getPosition() - position;

					//Calculate lambda first
					DoubleScalar lambda = vfs[i].length() / (currV.length() + eps);

					//Normalize everythin
					prevV.normalize();
					currV.normalize();
					nextV.normalize();

					DoubleScalar tetaAngle = acos(clamp<Scalar>(v.dot(currV), -(1.f - eps), 1.f - eps));
					prevV = v.cross(prevV).normalized();
					currV = v.cross(currV).normalized();
					nextV = v.cross(nextV).normalized();

					DoubleScalar prevAlfaAngle = acos(clamp<Scalar>(prevV.dot(currV), -(1.f - eps), 1.f - eps));
					DoubleScalar alfaAngle = acos(clamp<Scalar>(currV.dot(nextV), -(1.f - eps), 1.f - eps));

					DoubleScalar tempLambda = std::tan(prevAlfaAngle * 0.5) + std::tan(alfaAngle * 0.5);
					lambda *= tempLambda / (std::sin(tetaAngle) + eps);

					denominator += tempLambda * std::tan(1.57079632679489661923 - tetaAngle);
					currFaceLambdas[j] = lambda;
				}

				/** Point is exactly on the face, break the search on all polygons, return current weights undivided by denominator */
				if (abs(denominator) < eps) {
					perFaceLambdas[i] = currFaceLambdas;
					DoubleScalar sumCurrLambdas = 0;
					for (int j = 0; j < currFaceLambdas.size(); j++) {
						sumCurrLambdas += currFaceLambdas[j];
					}

					for (int j = 0; j < currFaceLambdas.size(); j++) {
						currFaceLambdas[j] *= 1 / ((DoubleScalar)currFaceLambdas.size());
					}
					valueType result;
					for (int j = 0; j < currFaceLambdas.size(); j++) {
						result += OwnCustomAttribute<valueType>::get(halfEdges[j]->getVertices().first)->getAttribute(attributeName)* currFaceLambdas[j];
					}
					return result;
				}
				for (int j = 0; j < currFaceLambdas.size(); j++) {
					currFaceLambdas[j] /= denominator;
				}
				perFaceLambdas[i] = currFaceLambdas;
			}

			for (int i = 0; i < pHalfVolume->getHalfCells().size(); i++) {
				auto halfEdges = pHalfVolume->getHalfCells()[i]->getHalfEdges();
				for (int j = 0; j < halfEdges.size(); j++) {
					weightMap[halfEdges[j]->getVertices().first->getID()] += perFaceLambdas[i][j];
				}
			}

			DoubleScalar allWeightsSum = 0;
			for (auto iter = weightMap.begin(); iter != weightMap.end(); iter++) {
				allWeightsSum += iter->second;
			}

			if (abs(allWeightsSum) > eps) {
				for (auto iter = weightMap.begin(); iter != weightMap.end(); iter++) {
					iter->second /= allWeightsSum;
				}
			}

			valueType result;
			const map<uint, shared_ptr<Vertex<VectorType>>> &vertexMap = pVolume->getVerticesMap();
			for (auto iter = weightMap.begin(); iter != weightMap.end(); iter++) {
				auto vertexIter = vertexMap.find(iter->first);
				result += OwnCustomAttribute<valueType>::get(vertexIter->second)->getAttribute(attributeName) * iter->second;
			}

			return result;
		}
		#pragma endregion

		/** Template linker trickerino for templated classes in CPP*/
		template class TrilinearNodalInterpolant3D<Scalar, HexaGridType>;
		template class TrilinearNodalInterpolant3D<Vector3, HexaGridType>;
		//template class TrilinearNodalInterpolant3D<Scalar, HexaGridTypeDouble>;
		//template class TrilinearNodalInterpolant3D<Vector3, HexaGridTypeDouble>;

		//template class TrilinearNodalInterpolant3D<VectorType>;
		//template class TrilinearNodalInterpolant3D<VectorTypeD>;
	}
}
