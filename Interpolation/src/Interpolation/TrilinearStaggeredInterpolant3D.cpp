#include "Interpolation/TrilinearStaggeredInterpolant3D.h"

namespace Chimera {
	namespace Interpolation {

		#pragma region Functionalities
		/** Vector-based staggered interpolation in regular grids */
		template <typename GridMeshType>
		typename GridMeshType::VectorType TrilinearStaggeredInterpolant3D<GridMeshType>::interpolate(const VectorType& position) {
			typedef typename GridMeshType::VectorType VectorType;

			VectorType gridSpacePosition = position / m_pHexaMesh->getGridSpacing();
			VectorType interpolatedVelocity;
			int i, j, k;

			//X component interpolation
			{
				i = static_cast <int> (floor(gridSpacePosition.x));
				j = static_cast <int> (floor(gridSpacePosition.y - 0.5f));
				k = static_cast <int> (floor(gridSpacePosition.z - 0.5f));

				i = clamp(i, 0, m_pHexaMesh->getGridDimensions().x);
				j = clamp(j, 0, m_pHexaMesh->getGridDimensions().y);
				k = clamp(k, 0, m_pHexaMesh->getGridDimensions().z);

				VectorType x1Position = VectorType(static_cast <Scalar>(i), j + 0.5f, k + 0.5f);
				VectorType x2Position = x1Position + VectorType(1, 1, 1);
				interpolateComponent(i, j, k, gridSpacePosition, x1Position, x2Position, xComponent, interpolatedVelocity);
			}

			//Y component interpolation
			{
				i = static_cast <int> (floor(gridSpacePosition.x - 0.5F));
				j = static_cast <int> (floor(gridSpacePosition.y));
				k = static_cast <int> (floor(gridSpacePosition.z - 0.5f));
				i = clamp(i, 0, m_pHexaMesh->getGridDimensions().x);
				j = clamp(j, 0, m_pHexaMesh->getGridDimensions().y);
				k = clamp(k, 0, m_pHexaMesh->getGridDimensions().z);

				VectorType y1Position = VectorType(i + 0.5f, static_cast <Scalar>(j), k + 0.5f);
				VectorType y2Position = y1Position + VectorType(1, 1, 1);

				interpolateComponent(i, j, k, gridSpacePosition, y1Position, y2Position, yComponent, interpolatedVelocity);
			}

			//Z component interpolation
			{
				i = static_cast <int> (floor(gridSpacePosition.x - 0.5F));
				j = static_cast <int> (floor(gridSpacePosition.y - 0.5f));
				k = static_cast <int> (floor(gridSpacePosition.z));
				i = clamp(i, 0, m_pHexaMesh->getGridDimensions().x);
				j = clamp(j, 0, m_pHexaMesh->getGridDimensions().y);
				k = clamp(k, 0, m_pHexaMesh->getGridDimensions().z);

				VectorType z1Position = VectorType(i + 0.5f, j + 0.5f, static_cast <Scalar>(k));
				VectorType z2Position = z1Position + VectorType(1, 1, 1);
				interpolateComponent(i, j, k, gridSpacePosition, z1Position, z2Position, zComponent, interpolatedVelocity);
			}

			return interpolatedVelocity;
		}
		#pragma endregion

		#pragma region PrivateFunctionalities
		template<typename GridMeshType>
		void TrilinearStaggeredInterpolant3D<GridMeshType>::interpolateComponent(int i, int j, int k, const VectorType &position, const VectorType &cellInitialPosition, const VectorType &cellFinalPosition, velocityComponent_t velocityComponent, VectorType &interpolatedVelocity) {
			typedef typename GridMeshType::VectorType VectorType;

			cellLocation_t componentPlane;
			switch (velocityComponent) {
				case Chimera::xComponent:
					componentPlane = YZPlane;
				break;
				case Chimera::yComponent:
					componentPlane = XZPlane;
				break;
				case Chimera::zComponent:
					componentPlane = XYPlane;
				break;
				default:
					throw std::logic_error("TrilinearStaggeredInterpolant::interpolateComponent() invalid fullVector component choice.");
				break;
			}

			int nextI = clamp(i + 1, 0, m_pHexaMesh->getGridDimensions().x + 1);
			int nextJ = clamp(j + 1, 0, m_pHexaMesh->getGridDimensions().y + 1);
			int nextK = clamp(k + 1, 0, m_pHexaMesh->getGridDimensions().z + 1);

			const string& attributeName(ParentClass::m_attributeName);
			VectorType v1 = OwnCustomAttribute<VectorType>::get(m_pHexaMesh->getCells(dimensions_t(i, j, k), componentPlane).front())->getAttribute(attributeName);
			VectorType v2 = OwnCustomAttribute<VectorType>::get(m_pHexaMesh->getCells(dimensions_t(nextI, j, k), componentPlane).front())->getAttribute(attributeName);
			VectorType v3 = OwnCustomAttribute<VectorType>::get(m_pHexaMesh->getCells(dimensions_t(i, nextJ, k), componentPlane).front())->getAttribute(attributeName);
			VectorType v4 = OwnCustomAttribute<VectorType>::get(m_pHexaMesh->getCells(dimensions_t(nextI, nextJ, k), componentPlane).front())->getAttribute(attributeName);

			VectorType v5 = OwnCustomAttribute<VectorType>::get(m_pHexaMesh->getCells(dimensions_t(i, j, nextK), componentPlane).front())->getAttribute(attributeName);
			VectorType v6 = OwnCustomAttribute<VectorType>::get(m_pHexaMesh->getCells(dimensions_t(nextI, j, nextK), componentPlane).front())->getAttribute(attributeName);
			VectorType v7 = OwnCustomAttribute<VectorType>::get(m_pHexaMesh->getCells(dimensions_t(i, nextJ, nextK), componentPlane).front())->getAttribute(attributeName);
			VectorType v8 = OwnCustomAttribute<VectorType>::get(m_pHexaMesh->getCells(dimensions_t(nextI, nextJ, nextK), componentPlane).front())->getAttribute(attributeName);

				Scalar vk1 =	v1[velocityComponent] * (cellFinalPosition.x - position.x) * (cellFinalPosition.y - position.y) +
											v2[velocityComponent] * (position.x - cellInitialPosition.x) * (cellFinalPosition.y - position.y) +
											v3[velocityComponent] * (cellFinalPosition.x - position.x) * (position.y - cellInitialPosition.y) +
											v4[velocityComponent] * (position.x - cellInitialPosition.x) * (position.y - cellInitialPosition.y);

				Scalar vk2 =	v5[velocityComponent] * (cellFinalPosition.x - position.x) * (cellFinalPosition.y - position.y) +
											v6[velocityComponent] * (position.x - cellInitialPosition.x) * (cellFinalPosition.y - position.y) +
											v7[velocityComponent] * (cellFinalPosition.x - position.x) * (position.y - cellInitialPosition.y) +
											v8[velocityComponent] * (position.x - cellInitialPosition.x) * (position.y - cellInitialPosition.y);

				Scalar alpha = position.z - cellInitialPosition.z;
				interpolatedVelocity[velocityComponent] = vk1 *(1 - alpha) + vk2*alpha;

		}
		#pragma endregion
		/** Template linker trickerino for templated classes in CPP*/
		template class TrilinearStaggeredInterpolant3D<HexaGridType>;
		template class TrilinearStaggeredInterpolant3D<HexaGridTypeDouble>;
	}
}
