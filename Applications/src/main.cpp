#include "stdio.h"

#include "ChimeraApplications.h"

//Global application class
Chimera::Applications::ApplicationBase *pApplication;

#pragma region Callbacks

static void RenderCallback() {
	pApplication->draw();
	pApplication->update();
}

#pragma endregion

static void IdleCallback() {
	//glutPostRedisplay();
}

int main(int argc, char** argv) {
	/** To-be initialized within initialization functions */
	string chimeraFile;
	shared_ptr<Chimera::IO::XMLDoc> pMainConfigFile;

	bool is2D = false;
	bool initializeGUI = false;
	try {
		//Static configuration file - do not change
		shared_ptr<Chimera::IO::XMLDoc> pTempConfigFile = Chimera::IO::ResourceManager::getInstance()->loadXMLDocument("Configuration/ChimeraCFG.xml");
		chimeraFile = pTempConfigFile->FirstChildElement("ChimeraFile")->GetText();
		if (chimeraFile.find("2D") != string::npos) {
			is2D = true;
		} else if(chimeraFile.find("3D")) {
			is2D = false;
		}
		else {
			throw("Invalid folder setup: configuration files should be under Configuration/2D/ or Configuration/3D/");
		}
		pMainConfigFile = Chimera::IO::ResourceManager::getInstance()->loadXMLDocument(chimeraFile);
	}
	catch (exception e) {
		Chimera::Core::exitProgram(e.what());
	}

	TiXmlElement *pChimeraConfig = pMainConfigFile->FirstChildElement("ChimeraConfig");
	string tempValue;
	try {
		tempValue = pChimeraConfig->FirstChildElement("SimulationType")->GetText();
		transform(tempValue.begin(), tempValue.end(), tempValue.begin(), ::tolower);
		if (is2D) {
			if (tempValue == "meshcutter") {
				//pApplication = new MeshCutterApplication(argc, argv, pChimeraConfig);
			}
			else if (tempValue == "realtimesimulation") {
				pApplication = new Chimera::Applications::RealtimeSimulation<Chimera::Solvers::FlowSolverType2D>(argc, argv, pChimeraConfig);
				initializeGUI = true;
			}
			else if (tempValue == "precomputedanimation") {
				//pApplication = new PrecomputedAnimation3D(argc, argv, pChimeraConfig);
			}
			else if (tempValue == "offlinesimulation") {
				//pApplication2D = new Chimera::Applications::OfflineSimulation2D(argc, argv, pChimeraConfig);
			}
		}
		else {
			if (tempValue == "meshcutter") {
				//pApplication = new MeshCutterApplication(argc, argv, pChimeraConfig);
			}
			else if (tempValue == "realtimesimulation") {
				//pApplication3D = new Chimera::Applications::RealtimeSimulation3D<Chimera::Core::Vector3>(argc, argv, pChimeraConfig);
				//initializeGUI = true;
			}
			else if (tempValue == "precomputedanimation") {
				//pApplication3D = new Chimera::Applications::PrecomputedAnimation3D<Chimera::Core::Vector3>(argc, argv, pChimeraConfig);
				//initializeGUI = true;
			}
			else if (tempValue == "offlinesimulation") {
				//pApplication3D = new Chimera::Applications::OfflineSimulation3D<Chimera::Core::Vector3>(argc, argv, pChimeraConfig);
			}
		}
	} catch (exception e) {
		Chimera::Core::exitProgram(e.what());
	}

	if (initializeGUI) {
		while (1)
		{
			pApplication->draw();
			pApplication->update();
		}
	}
	else {

		

	}

	return 0;
}
