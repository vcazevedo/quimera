
#include "2D/OfflineSimulation2D.h"

namespace Chimera {

	namespace Applications {
		
		#pragma region Constructors
		OfflineSimulation2D::OfflineSimulation2D(int argc, char** argv, TiXmlElement *m_pMainNode) : ApplicationBase(argc, argv, m_pMainNode) {
			m_pQuadGrid = nullptr;
			
			try {
				/** Load grid */
				TiXmlElement *pGridNode = m_pMainNode->FirstChildElement("Grid");
				if(pGridNode) {
					m_pQuadGrid = GridLoader::getInstance()->loadQuadGrid(pGridNode);
				}
				
				/** Load boundary conditions */
				TiXmlElement *pBoundaryConditionsNode = m_pMainNode->FirstChildElement("boundaryConditionsFile");
				if(pBoundaryConditionsNode && m_pQuadGrid) {
					string boundaryConditionsFile = pBoundaryConditionsNode->GetText();
					m_boundaryConditions = BoundaryConditionFactory::getInstance()->loadBCs<Vector2, Array2D>(boundaryConditionsFile, m_pQuadGrid->getDimensions());
				}

				/** Load meshes */
				TiXmlElement *pObjectsNode = m_pMainNode->FirstChildElement("Objects");
				if (pObjectsNode && m_pQuadGrid) {
					auto m_tempPhysicalObjects = SolidsLoader::getInstance()->loadPhysicalObjects<Vector2>(pObjectsNode, m_pFlowSolverParams->solverType, m_pQuadGrid->getDimensions(), m_pQuadGrid->getGridData2D()->getGridSpacing());
					for (auto pPhysicalObject : m_tempPhysicalObjects) {
						m_physicalObjects.push_back(shared_ptr<PhysicalObject<Vector2>>(pPhysicalObject));
					}
				}

				/** Load Liquid Params */
				TiXmlElement *pLiquidsNode = m_pMainNode->FirstChildElement("Liquids");
				if (pLiquidsNode && m_pQuadGrid) {
					m_pLiquidRepresentationParams = LiquidsLoader::getInstance()->loadLiquidObjects<Vector2, Array2D>(pLiquidsNode, m_pQuadGrid->getDimensions(), m_pQuadGrid->getGridData2D()->getGridSpacing());
					m_pLiquidRepresentationParams->pGridData = m_pQuadGrid->getGridData2D();
					// Verify if CutCells Solver, then set computeCrossings = true
					if (m_pFlowSolverParams->solverType == cutCellGhostLiquids) {
						m_pLiquidRepresentationParams->computeCrossings = true;
					}
					m_pLiquidRepresentation = new LiquidRepresentation<Vector2, Array2D>(*m_pLiquidRepresentationParams);
				}
			} catch (exception e) {
				exitProgram(e.what());
			}

			/** Initialization phase: */
			{
				m_pPhysicsCore = PhysicsCore<Vector2>::getInstance();
				m_pPhysicsCore->initialize(*m_pPhysicsCoreParams);
			}

			/** Transform PhysicalObject -> RigidObject */
			vector<shared_ptr<SolidObject<Vector2, IntersectedLineMesh>>> rigidObjects;
			for (auto pPhysicalObject : m_physicalObjects) {
				auto pRigidObject1 = dynamic_pointer_cast<SolidObject<Vector2, LineMesh>>(pPhysicalObject);
				auto pRigidObject2 = dynamic_pointer_cast<SolidObject<Vector2, IntersectedLineMesh>>(pPhysicalObject);
				if (pRigidObject2 != nullptr) {
					rigidObjects.push_back(pRigidObject2);
				}
				else if (pRigidObject1 != nullptr) {
					auto pRigidObject_Cast = static_pointer_cast<SolidObject<Vector2, IntersectedLineMesh>>(std::shared_ptr<void>(pRigidObject1));
					rigidObjects.push_back(pRigidObject_Cast);
				}
			}
			
			/** FlowSolver Initialization */
			switch (m_pFlowSolverParams->solverType) 
			{
				case finiteDifferenceMethod:
					m_pFlowSolver = new VoxelizedGridSolver2D(*m_pFlowSolverParams, m_pQuadGrid, m_boundaryConditions, rigidObjects);
				break;

				case streamfunctionVorticity:
					m_pFlowSolver = new StreamfunctionSolverVoxelized2D(*m_pFlowSolverParams, m_pQuadGrid, m_boundaryConditions, rigidObjects);
				break;

				case cutCellMethod:
					m_pFlowSolver = new CutCellSolver2D(*m_pFlowSolverParams, m_pQuadGrid, m_boundaryConditions, rigidObjects);
					break;
				case cutCellSOMethod:
					m_pFlowSolver = new CutCellSolverSO2D(*m_pFlowSolverParams, m_pQuadGrid, m_boundaryConditions, rigidObjects);
					break;

				case ghostLiquids:
					m_pFlowSolver = new GhostLiquidViscositySolver2D(*m_pFlowSolverParams, m_pQuadGrid, m_boundaryConditions, m_pLiquidRepresentation);
				break;

				default:
					throw("Solver type not supported!");
				break;
			}
			
			/** Data Logger */
			{
				if (m_pDataLoggerParams && m_pQuadGrid) {
					m_pDataLoggerParams->setScreenSize(0, 0);
					m_pDataLogger = new DataExporter<Vector2, Array2D>(*m_pDataLoggerParams, m_pQuadGrid->getDimensions());
					m_pDataLogger->setFlowSolver(m_pFlowSolver);
				}
			}

			m_pPhysicsCore->addObject(m_pFlowSolver);
		}
		
		
		#pragma region Functionalities
		void OfflineSimulation2D::draw() { }

		void OfflineSimulation2D::update() {
			Scalar dt = m_pPhysicsCore->getParams()->timestep;

			static bool firstTimeUpdate = true;
			bool updateRenderer = false;
			if (m_pPhysicsCore->isRunningSimulation() || m_pPhysicsCore->isSteppingSimulation()) {
				updateRenderer = true;
			}

			m_pPhysicsCore->update();

			if (updateRenderer) {
				if(m_pDataLogger)
					m_pDataLogger->log(m_pPhysicsCore->getElapsedTime());
			}
		}

	}
}
