//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#include "RealtimeSimulation.h"
#include "FlowSolverRenderer.h"

namespace Chimera {

	namespace Applications {

		#pragma region Constructors
		template <typename FlowSolverType>
		RealtimeSimulation<FlowSolverType>::RealtimeSimulation(int argc, char** argv, TiXmlElement *pChimeraConfig) : RealtimeSimulation::ApplicationBase(argc, argv, pChimeraConfig) {

			/** Initializing Classes */
			try {
				shared_ptr<TiXmlElement> pFlowSolverNode = shared_ptr<TiXmlElement>(ParentClass::m_pMainNode->FirstChildElement("FlowSolver"));
				auto pObstacleNode = pFlowSolverNode->FirstChildElement("Obstacle");
				m_pFlowSolver = FlowSolverLoader::getInstance()->loadSolver<FlowSolverType>(pFlowSolverNode);

				int argc = 0; char** argv = nullptr;
				auto pFlowSolverRenderer = make_shared<FlowSolverRenderer<FlowSolverType>>(m_pFlowSolver, Magnum::Platform::Application::Arguments({ argc, argv }));
				m_pRenderer = pFlowSolverRenderer;

				if (pObstacleNode != nullptr) {
					auto pMesh = IO::MeshLoader::getInstance()->loadIntersectedLineMesh<typename FlowSolverType::VectorType>(pObstacleNode->FirstChildElement("Geometry"),
						m_pFlowSolver->getSimulationGrid()->getGridDimensions(), m_pFlowSolver->getSimulationGrid()->getGridSpacing());


					vector<shared_ptr<Mesh<typename FlowSolverType::EmbeddedMeshType>>> m_meshes;
					m_meshes.push_back(pMesh);
					pFlowSolverRenderer->addMeshRenderer(m_meshes);
				}
					
				m_pRenderer->exec();
				exit(0);
			}
			catch (exception e) {
				exitProgram(e.what());
			}

			
		}

		template <class FlowSolverType>
		void RealtimeSimulation<FlowSolverType>::update() {
			
		}

		template <class VectorT>
		void RealtimeSimulation<VectorT>::draw() {
			//m_pRenderer->renderLoop();
		}

		template class RealtimeSimulation<FlowSolverType2D>;
	}

}
