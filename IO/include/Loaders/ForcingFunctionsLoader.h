//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_FORCING_FUNCTIONS_LOADER_H_
#define __CHIMERA_FORCING_FUNCTIONS_LOADER_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "ChimeraInterpolation.h"
#include "ChimeraSolvers.h"

namespace Chimera {

	using namespace Solvers;
	namespace IO {


		class ForcingFunctionsLoader : public Singleton<ForcingFunctionsLoader> {
		public:

			#pragma region LoadingFunctions
			/*template <typename FlowSolverType>
			typename FlowSolver<FlowSolverType>>::hotSmokeSource_t * loadHotSmokeSource(shared_ptr<TiXmlElement>pHotSmokeSourceNode);

			template <typename FlowSolverType>
			typename FlowSolver<FlowSolverType>>::swirlingField_t * loadSwirlingField(shared_ptr<TiXmlElement>pSwirlingFieldNode);


			template <typename FlowSolverType>
			vector<typename FlowSolver<FlowSolverType>>::hotSmokeSource_t *> loadHotSmokeSources(shared_ptr<TiXmlElement>pForcingFunctionsNode) {
				vector<typename FlowSolver<FlowSolverType>>::hotSmokeSource_t *> forcingFunctions;
				auto pHotSmokeNode = pForcingFunctionsNode->FirstChildElement("HotSmokeSource");
				while (pHotSmokeNode != NULL) {
					forcingFunctions.push_back(loadHotSmokeSource<VectorT, VoxelType, MeshType>(pHotSmokeNode));
					pHotSmokeNode = pHotSmokeNode->NextSiblingElement("HotSmokeSource");
				}
				return forcingFunctions;
			}

			template <typename FlowSolverType>
			vector<typename FlowSolver<FlowSolverType>>::swirlingField_t *> loadSwirlingFields(shared_ptr<TiXmlElement>pForcingFunctionsNode) {
				vector<typename FlowSolver<FlowSolverType>>::swirlingField_t *> forcingFunctions;
				auto pSwirlingFieldNode = pForcingFunctionsNode->FirstChildElement("SwirlingField");
				while (pSwirlingFieldNode != NULL) {
					forcingFunctions.push_back(loadSwirlingField<VectorT, VoxelType, MeshType>(pSwirlingFieldNode));
					pSwirlingFieldNode = pSwirlingFieldNode->NextSiblingElement("SwirlingField");
				}
				return forcingFunctions;
			}*/

			template <class VectorT>
			vector<VectorT> loadControlPoints(shared_ptr<TiXmlElement>pControlPointsNode) {
				vector<VectorT> controlPoints;
				shared_ptr<TiXmlElement>pPointNode = make_shared<TiXmlElement>(pControlPointsNode->FirstChildElement("Point")); // added make_shared<TiXmlElement>()
				while (pPointNode != NULL) {
					controlPoints.push_back(XMLParamsLoader::getInstance()->loadVectorFromNode<VectorT>(pPointNode));
					pPointNode = make_shared<TiXmlElement>(pPointNode->NextSiblingElement("Point")); // added make_shared<TiXmlElement>()
				}
				return controlPoints;
			}
			#pragma endregion
		};
	}
}

#endif
