//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_LIQUIDS_LOADER_H_
#define __CHIMERA_LIQUIDS_LOADER_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "ChimeraParticles.h"
#include "ChimeraInterpolation.h"
#include "ChimeraResources.h"
#include "ChimeraPoisson.h"
#include "ChimeraSolvers.h"
#include "ChimeraRendering.h"
#include "ChimeraIO.h"
#include "ChimeraSolids.h"

#include "Loaders/SolidsLoader.h"
#include "XMLParamsLoader.h"

namespace Chimera {

	using namespace Resources;
	using namespace Solvers;
	using namespace Solids;

	namespace Loaders {


		class LiquidsLoader : public Singleton<LiquidsLoader> {
		public:

			#pragma region LiquidsLoadingFunctions
			template <typename EmbeddedMeshType, typename GridMeshType>
			typename LiquidRepresentation<EmbeddedMeshType, GridMeshType>::params_t * loadLiquidObjects(shared_ptr<TiXmlElement>pLiquidsNode, const dimensions_t &gridDimensions, Scalar dx);
			/*{
				LiquidRepresentation<VectorT, ArrayType>::params_t *pLiquidParams = new LiquidRepresentation<VectorT, ArrayType>::params_t();
				shared_ptr<TiXmlElement>pLiquidObjectNode = pLiquidsNode->FirstChildElement("LiquidObject");
				if (pLiquidsNode->FirstChildElement("LevelSetGridSubdivisions")) {
					pLiquidParams->levelSetGridSubdivisions = atof(pLiquidsNode->FirstChildElement("LevelSetGridSubdivisions")->GetText());
				}
				if (pLiquidsNode->FirstChildElement("Sampler")) {
					shared_ptr<TiXmlElement>pParticelsPerCell = pLiquidsNode->FirstChildElement("Sampler")->FirstChildElement("ParticlesPerCell");
					if (pParticelsPerCell) {
						pLiquidParams->particlesPerCell = atoi(pParticelsPerCell->GetText());
					}
					shared_ptr<TiXmlElement>pSampler = pLiquidsNode->FirstChildElement("Sampler")->FirstChildElement("Type");
					if (pSampler) {
						pLiquidParams->samplingMethod = XMLParamsLoader::getInstance()->loadSampler(pSampler);
					}
					shared_ptr<TiXmlElement>pResampleParticles = pLiquidsNode->FirstChildElement("Sampler")->FirstChildElement("ResampleParticles");
					if (pResampleParticles) {
						pLiquidParams->resampleParticles = XMLParamsLoader::getInstance()->loadTrueOrFalse(pResampleParticles);
					}
				}
				else {
					throw(std::logic_error("loadLiquidObjects: Sampler node not found!"));
				}
				if(pLiquidObjectNode == nullptr) {
					throw(std::logic_error("loadLiquidObjects: LiquidObject node not found!"));
				}
				while (pLiquidObjectNode != NULL) {
					if (std::is_same<VectorT, Vector2>::value) {
						LineMesh<Vector2> *pLineMesh = MeshLoader::getInstance()->loadLineMesh<Vector2>(pLiquidObjectNode);
						shared_ptr<LineMesh<Vector2>> pSharedLineMesh(pLineMesh);
						pLiquidParams->initialLineMeshes.push_back(pSharedLineMesh);
					} else {
						shared_ptr<TiXmlElement>nextMesh = pLiquidObjectNode->FirstChildElement("Mesh");
						while (nextMesh) {
							PolygonalMesh<Vector3> *pPolygonalMesh = MeshLoader::getInstance()->loadPolyMesh<Vector3>(nextMesh);
							shared_ptr<PolygonalMesh<Vector3>> pSharedPolygonalMesh(pPolygonalMesh);
							pLiquidParams->initialPolygonalMeshes.push_back(pSharedPolygonalMesh);
							nextMesh = nextMesh->NextSiblingElement("Mesh");
						}
					}
					pLiquidObjectNode = pLiquidObjectNode->NextSiblingElement("LiquidObject");
				}
				return pLiquidParams;
			} */
			#pragma endregion
		};
	}
}

#endif
