//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_SOLIDS_LOADER_H_
#define __CHIMERA_SOLIDS_LOADER_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "ChimeraParticles.h"
#include "ChimeraInterpolation.h"
#include "ChimeraResources.h"
#include "ChimeraPoisson.h"
#include "ChimeraSolvers.h"
#include "ChimeraRendering.h"
#include "ChimeraIO.h"
#include "ChimeraSolids.h"
#include "XMLParamsLoader.h"

#include "Loaders/MeshLoader.h"

namespace Chimera {

	using namespace Resources;
	using namespace Solvers;
	using namespace Solids;

	namespace Loaders {


		class SolidsLoader : public Singleton<SolidsLoader> {

			#pragma region SolidsLoadingFunctions
			private:
			// Declaration & Implementation for generic object loading. Avoids having to define empty polygonalMeshes Vector2
			// or lineMeshes Vector3 functions
			template <class VectorT, bool is3D>
			struct loadPhysicalObjects_impl {
				/** Generic implementation returns null */
				shared_ptr<PhysicalObject<VectorT>> loadPhysicalObject(TiXmlElement *pObjectNode, solverType_t solverType, const dimensions_t &gridDimensions, Scalar dx) {
					return nullptr;
				}
			};

			/** Vector2 implementation */
			template <class VectorT>
			struct loadPhysicalObjects_impl<VectorT, true> {
				shared_ptr<PhysicalObject<VectorT>> loadPhysicalObject(TiXmlElement *pObjectNode, solverType_t solverType, const dimensions_t &gridDimensions, Scalar dx) {
					string nodeStr(pObjectNode->Value());
					transform(nodeStr.begin(), nodeStr.end(), nodeStr.begin(), ::tolower);

					if (nodeStr == "rigidobject") {
						if (solverType == cutCellMethod || solverType == cutCellSOMethod || solverType == streamfunctionVorticityCutCells) {
							return SolidsLoader::getInstance()->loadRigidObject2D<VectorT, IntersectedLineMesh>(pObjectNode, gridDimensions, dx);
						}
						else {
							return SolidsLoader::getInstance()->loadRigidObject2D<VectorT, LineMesh>(pObjectNode, gridDimensions, dx);
						}
					}
					return nullptr;
				}
			};
			/** Vector3 implementation */
			template <class VectorT>
			struct loadPhysicalObjects_impl<VectorT, false> {
				shared_ptr<PhysicalObject<VectorT>> loadPhysicalObject(TiXmlElement *pMeshNode, solverType_t solverType, const dimensions_t &gridDimensions, Scalar dx) {
					string nodeStr(pMeshNode->Value());
					transform(nodeStr.begin(), nodeStr.end(), nodeStr.begin(), ::tolower);

					if (nodeStr == "rigidobject") {
						if (solverType == cutCellMethod || solverType == cutCellSOMethod) {
							return SolidsLoader::getInstance()->loadRigidObject3D<VectorT, IntersectedPolyMesh>(pMeshNode, gridDimensions, dx);
						}
						else {
							return SolidsLoader::getInstance()->loadRigidObject3D<VectorT, PolygonalMesh>(pMeshNode, gridDimensions, dx);
						}
					}
					return nullptr;
				}
			};

			public:

			/** Dynamic mesh loader. Loads a mesh type passed by argument*/
			template<class VectorT>
			shared_ptr<PhysicalObject<VectorT>> loadPhysicalObject(TiXmlElement *pMeshNode, solverType_t solverType, const dimensions_t &gridDimensions, Scalar dx) {
				loadPhysicalObjects_impl<VectorT, isVector2<VectorT>::value> loadPhysicalObjects_implObj;
				return loadPhysicalObjects_implObj.loadPhysicalObject(pMeshNode, solverType, gridDimensions, dx);
			}


			/** Loads deformable/rigid objects and cast them back to physical objects so they can be handled by proper
				classes later. Rigid/Deformable objects depends on the solver type to decide which type of mesh will
				be loaded (e.g., intersected vs non-intersected). */
			template<class VectorT>
			vector<shared_ptr<PhysicalObject<VectorT>>> loadPhysicalObjects(TiXmlElement *pObjectsNode, solverType_t solverType, const dimensions_t &gridDimensions, Scalar dx) {
				vector<shared_ptr<PhysicalObject<VectorT>>> physicalObjects;
				TiXmlElement *pObjectNode = make_shared<TiXmlElement>(pObjectsNode->FirstChildElement()); // added make_shared<TiXmlElement>()
				while (pObjectNode != NULL) {
					shared_ptr<PhysicalObject<VectorT>> solidObject = loadPhysicalObject<VectorT>(pObjectNode, solverType, gridDimensions, dx);
					if(solidObject)
						physicalObjects.push_back(solidObject);
					pObjectNode = make_shared<TiXmlElement>(pObjectNode->NextSiblingElement()); // added make_shared<TiXmlElement>()
				}
				return physicalObjects;
			}

			template <class VectorT, template <class> class MeshType>
			vector<shared_ptr<SolidObject<VectorT, MeshType>>> loadRigidObjects2D(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions = dimensions_t(), Scalar dx = 0) {
				vector<shared_ptr<SolidObject<VectorT, MeshType>>> rigidObjects;
				TiXmlElement *pRigidObjectNode = make_shared<TiXmlElement>(pObjectsNode->FirstChildElement("RigidObject")); // added make_shared<TiXmlElement>()
				while (pRigidObjectNode != NULL) {
					rigidObjects.push_back(loadRigidObject2D<VectorT, MeshType>(pRigidObjectNode, gridDimensions, dx));
					pRigidObjectNode = make_shared<TiXmlElement>(pRigidObjectNode->NextSiblingElement()); // added make_shared<TiXmlElement>()
				}
				return rigidObjects;
			}
			template <class VectorT, template <class> class MeshType>
			shared_ptr<SolidObject<VectorT, MeshType>> loadRigidObject2D(TiXmlElement *pRigidObjectNode, const dimensions_t &gridDimensions, Scalar dx);

			template <class VectorT, template <class> class MeshType>
			vector<shared_ptr<SolidObject<VectorT, MeshType>>> loadRigidObjects3D(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions = dimensions_t(), Scalar dx = 0) {
				vector<shared_ptr<SolidObject<VectorT, MeshType>>> rigidObjects;
				TiXmlElement *pRigidObjectNode = make_shared<TiXmlElement>(pObjectsNode->FirstChildElement("SolidObject")); // added make_shared<TiXmlElement>()
				while (pRigidObjectNode != NULL) {
					rigidObjects.push_back(loadRigidObject3D<VectorT, MeshType>(pRigidObjectNode, gridDimensions, dx));
					pRigidObjectNode = make_shared<TiXmlElement>(pRigidObjectNode->NextSiblingElement("SolidObject")); // added make_shared<TiXmlElement>()
				}
				return rigidObjects;
			}

			template <class VectorT, template <class> class MeshType>
			shared_ptr<SolidObject<VectorT, MeshType>> loadRigidObject3D(TiXmlElement *pRigidObjectNode, const dimensions_t &gridDimensions, Scalar dx);

			#pragma endregion

			#pragma region LoadingUtils
			template <class VectorT>
			typename PhysicalObject<VectorT>::positionUpdate_t * loadPositionUpdate(TiXmlElement *pPositionUpdateNode);

			template <class VectorT>
			typename PhysicalObject<VectorT>::rotationUpdate_t * loadRotationUpdate(TiXmlElement *pRotationUpdateNode);
			#pragma endregion
		};
	}
}

#endif
