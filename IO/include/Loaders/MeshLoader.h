//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_MESH_LOADER_H_
#define __CHIMERA_MESH_LOADER_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraMesh.h"

#include "Loaders/XMLParamsLoader.h"

namespace Chimera {

	namespace IO {

		using namespace Meshes;

		class MeshLoader : public Singleton<MeshLoader> {
		public:
			MeshLoader() {

			}

			template<class VectorT>
			typename LineMesh<VectorT>::params_t loadParams(TiXmlElement* pLineMeshNode);


			#pragma region 2DMeshLoading
			template<class VectorT>
			vector<VectorT> createGearGeometry(TiXmlElement * pGearNode, uint numSubdivis, const VectorT &position);
			
			template<class VectorT>
			void loadLineMeshFromFile(const string& fileStr, const VectorT &initialPosition, vector<VectorT> &linePoints);

			template<class VectorT>
			shared_ptr<LineMesh<VectorT>> loadLineMesh(TiXmlElement * pLineMeshNode);

			template<class VectorT>
			shared_ptr<IntersectedLineMesh<VectorT>> loadIntersectedLineMesh(TiXmlElement * pLineMeshNode, const dimensions_t &gridDimensions, Scalar dx);
			#pragma endregion

			#pragma region 3DMeshLoading
			/** Polygonal Meshes do not intersect with the grid */

			template<class VectorT>
			shared_ptr<PolygonalMesh<VectorT>> loadPolyMesh(TiXmlElement * pObjectsNode);

			template<class VectorT>
			vector<shared_ptr<PolygonalMesh<VectorT>>> loadPolyMeshes(TiXmlElement * pObjectsNode);

			/** IntersectedPolyMeshes intersect with the grid */
			template<class VectorT>
			shared_ptr<IntersectedPolyMesh<VectorT>> loadIntersectedPolyMesh(TiXmlElement * pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);

			template<class VectorT>
			vector<shared_ptr<IntersectedPolyMesh<VectorT>>> loadIntersectedPolyMeshes(TiXmlElement * pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);

			template<class VectorT>
			shared_ptr<IntersectedPolyMesh<VectorT>> loadIntersectedPolyMeshFromSet(TiXmlElement * pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);

			template<class VectorT>
			vector<vector<VectorT>> loadAnimPolyMeshesVertices(TiXmlElement * pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);
			#pragma endregion


			// declaration & implementation for generic mesh loading. Avoids having to define empty polygonalMeshes Vector2
			// or lineMeshes Vector3 functions

			template <class VectorT, template <class> class MeshType, bool is3D>
			struct loadMesh_impl {
				/** Generic implementation returns null */
				shared_ptr<MeshType<VectorT>> loadMesh(TiXmlElement * pMeshNode, const dimensions_t &gridDimensions = dimensions_t(), Scalar dx = 0) {
					return nullptr;
				}
			};

			/** Vector2 implementation */
			template <class VectorT, template <class> class MeshType>
			struct loadMesh_impl<VectorT, MeshType, true> {
				shared_ptr<MeshType<VectorT>> loadMesh(TiXmlElement * pMeshNode, const dimensions_t &gridDimensions = dimensions_t(), Scalar dx = 0) {
					/*if (typeid(MeshType<VectorT>) == typeid(LineMesh<VectorT>)) {
						return reinterpret_cast<shared_ptr<MeshType<VectorT>>>(MeshLoader::getInstance()->loadLineMesh<VectorT>(pMeshNode));
					}
					else if (typeid(MeshType<VectorT>) == typeid(IntersectedLineMesh<VectorT>)) {
						return reinterpret_cast<shared_ptr<MeshType<VectorT>>>(MeshLoader::getInstance()->loadIntersectedLineMesh<VectorT>(pMeshNode, gridDimensions, dx));
					}*/
					return nullptr;
				}
			};
			/** Vector3 implementation */
			template <class VectorT, template <class> class MeshType>
			struct loadMesh_impl<VectorT, MeshType, false> {
				shared_ptr<MeshType<VectorT>> loadMesh(TiXmlElement * pMeshNode, const dimensions_t &gridDimensions = dimensions_t(), Scalar dx = 0) {
					/*if (typeid(MeshType<VectorT>) == typeid(PolygonalMesh<VectorT>)) {
						return shared_ptr<MeshType<VectorT>>(MeshLoader::getInstance()->loadPolyMesh<VectorT>(pMeshNode));
					}
					else if (typeid(MeshType<VectorT>) == typeid(IntersectedPolyMesh<VectorT>)) {
						return shared_ptr<MeshType<VectorT>>(MeshLoader::getInstance()->loadIntersectedPolyMesh<VectorT>(pMeshNode, gridDimensions, dx));
					}*/
					return nullptr;
				}
			};

			/** Dynamic mesh loader. Loads a mesh type passed by argument*/
			template<class VectorT, template <class> class MeshType>
			shared_ptr<MeshType<VectorT>> loadMesh(TiXmlElement * pMeshNode, const dimensions_t &gridDimensions = dimensions_t(), Scalar dx = 0) {
				loadMesh_impl<VectorT, MeshType, isVector2<VectorT>::value> loadMesh_implObj;
				return loadMesh_implObj.loadMesh(pMeshNode, gridDimensions, dx);
			}
		};
	}
}

#endif
