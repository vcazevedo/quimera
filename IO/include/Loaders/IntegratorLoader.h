//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef _CHIMERA_INTEGRATOR_LOADER_H_
#define _CHIMERA_INTEGRATOR_LOADER_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraAdvection.h"
#include "ChimeraInterpolation.h"

#include "Loaders/XMLParamsLoader.h"

namespace Chimera {
	
	namespace IO {
		using namespace Advection;

		/** Main XML loader Singleton. */
		class IntegratorLoader : public Singleton<IntegratorLoader> {
		
		public:
			IntegratorLoader() {
			}

			#pragma region LoadingFunctions
			template <typename GridMeshType>
			shared_ptr<PositionIntegrator<GridMeshType>> loadIntegrator(shared_ptr<TiXmlElement> pIntegratorNode, shared_ptr<GridMesh<GridMeshType>> pGridMesh, shared_ptr<Interpolant<typename GridMeshType::VectorType, GridMeshType>> pInterpolant);
			#pragma endregion

		protected:
			#pragma region PrivateFunctionalities
			integrationMethod_t loadIntegrationMethodParams(shared_ptr<TiXmlElement> pIntegrationNode) {
				auto pTempNode = pIntegrationNode->FirstChildElement();
				string integrationType(pTempNode->Value());
				transform(integrationType.begin(), integrationType.end(), integrationType.begin(), ::tolower);
				if (integrationType == "rungekuttaadaptive") {
					return RungeKutta_Adaptive;
				}
				else if (integrationType == "rungekutta2") {
					return RungeKutta_2;
				}
				else if (integrationType == "rungekutta4") {
					return RungeKutta_4;
				}
				else if (integrationType == "euler") {
					return forwardEuler;
				}
				//Default
				return RungeKutta_2;
			}
			#pragma endregion


		};
	}
}

#endif
