//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_GRID_LOADER_H_
#define __CHIMERA_GRID_LOADER_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"

#include "Loaders/XMLParamsLoader.h"


namespace Chimera {

	/** Structure that encapsulates velocityFields */
	template<class VectorType, template <class> class ArrayType>
	struct velocityField_t {
		//Velocity field loaded, since its a big structure we will deal with pointers
		vector<ArrayType<VectorType>> * pVelocityFields;
		
		//Timestep used on the velocity field simulation
		Scalar timeStep;

		velocityField_t() {
			timeStep = 0;
			pVelocityFields = nullptr;
		}
	};

	/** Structure that encapsulates scalarFields */
	template<template <class> class ArrayType>
	struct scalarField_t {
		//Velocity field loaded, since its a big structure we will deal with pointers
		vector<ArrayType<Scalar>> * pScalarField;

		//Timestep used on the velocity field simulation
		Scalar timeStep;

		scalarField_t() {
			timeStep = 0;
			pScalarField = nullptr;
		}
	};


	namespace IO {

		using namespace Grids;

		class GridLoader : public Singleton<GridLoader> {
		public:

			#pragma region GridLoadingFunctions
			template <typename GridMeshType>
			shared_ptr<GridMesh<GridMeshType>> loadGridMesh(shared_ptr<TiXmlElement> pGridNode, TiXmlElement *pObstacleNode);

			template <typename GridMeshType>
			shared_ptr<HexaGridMesh<GridMeshType>> loadHexaGrid(shared_ptr<TiXmlElement>pGridNode);
			
			template <typename GridMeshType>
			shared_ptr<QuadGridMesh<GridMeshType>> loadQuadGrid(shared_ptr<TiXmlElement>pGridNode, TiXmlElement* pObstacleNode);

			template <typename EmbeddedMeshType, typename GridMeshType>
			shared_ptr<LevelSetGridMesh<EmbeddedMeshType, GridMeshType>> loadLevelSetGrid(shared_ptr<TiXmlElement> pGridNode, shared_ptr<GridMesh<GridMeshType>>);

			#pragma endregion

			#pragma region LoadingUtils
			template <template<class> class ArrayType>
			scalarField_t<ArrayType> * loadScalarFields(shared_ptr<TiXmlElement>pDensityFieldNode, const dimensions_t &gridDimensions);

			template<class VectorType, template<class> class ArrayType>
			velocityField_t<VectorType, ArrayType> * loadVelocityFields(shared_ptr<TiXmlElement>pGridNode, const dimensions_t &gridDimensions);

			/** Loads scalar fields in .log format */
			void loadScalarField(const string &scalarFieldFilename, Array3D<Scalar> &scalarField);

			///** Loads scalar fields in .vdb format */
			//void loadScalarFieldVDB(const string &scalarFieldFilename, Array3D<Scalar> &scalarField, const string &gridname);
			#pragma endregion

		};
	}
}

#endif
