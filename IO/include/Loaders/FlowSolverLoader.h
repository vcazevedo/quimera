//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_FLOW_SOLVER_LOADER_H_
#define __CHIMERA_FLOW_SOLVER_LOADER_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "ChimeraSolvers.h"
#include "ChimeraAdvection.h"

#include "TinyXML/tinyxml.h"

namespace Chimera {
	
	namespace IO {

		using namespace Solvers;
		using namespace Advection;

		/** Main XML loader Singleton. */
		class FlowSolverLoader : public Singleton<FlowSolverLoader> {
		
		public:
			FlowSolverLoader() {
			}

			#pragma region LoadingFunctions
			template <typename FlowSolverType>
			shared_ptr<FlowSolver<FlowSolverType>> loadSolver(shared_ptr<TiXmlElement> pSolverNode);
			#pragma endregion

		protected:

			#pragma region InternalLoadingFunctions
			
			shared_ptr<TiXmlElement> m_pGridNode;
			shared_ptr<TiXmlElement> m_pLevelSetNode;
			solverType_t loadFlowSolverType(shared_ptr<TiXmlElement> pSolverNode);

			
			template <typename FlowSolverType>
			shared_ptr<LevelSetGridMesh<typename FlowSolverType::EmbeddedMeshClass, typename FlowSolverType::GridMeshType>> loadSmokeSource(TiXmlElement *pSmokeSource, shared_ptr<GridMesh<typename FlowSolverType::GridMeshType>> pGridMesh);
			
			template <typename FlowSolverType>
			shared_ptr<LevelSetGridMesh<typename FlowSolverType::EmbeddedMeshClass, typename FlowSolverType::GridMeshType>> loadObstacle(TiXmlElement* pObstacleNode, shared_ptr<GridMesh<typename FlowSolverType::GridMeshType>> pGridMesh);
			#pragma endregion
		};
	}
}

#endif
