#include "Loaders/MeshLoader.h"

namespace Chimera {

	namespace IO {


		template<class VectorT>
		typename LineMesh<VectorT>::params_t MeshLoader::loadParams(TiXmlElement* pLineMeshNode) {
			typename LineMesh<VectorT>::params_t lineMeshParams;
			if (pLineMeshNode->FirstChildElement("Position")) {
				double positionX;
				double positionY;

				pLineMeshNode->FirstChildElement("Position")->QueryDoubleAttribute("x", &positionX);
				pLineMeshNode->FirstChildElement("Position")->QueryDoubleAttribute("y", &positionY);

				lineMeshParams.position.x = positionX;
				lineMeshParams.position.y = positionY;
			}

			if (pLineMeshNode->FirstChildElement("ExtrudeAlongNormalsSize")) {
				Scalar extrudeSize = atof(pLineMeshNode->FirstChildElement("ExtrudeAlongNormalsSize")->GetText());
				lineMeshParams.extrudeAlongNormalWidth = extrudeSize;
			}
			if (pLineMeshNode->FirstChildElement("File")) {
				string lineStr = "Geometry/2D/";
				lineStr += pLineMeshNode->FirstChildElement("File")->GetText();

				loadLineMeshFromFile(lineStr, lineMeshParams.position, lineMeshParams.initialPoints);

				if (pLineMeshNode->FirstChildElement("ClosedMesh")) {
					string closedMeshStr = pLineMeshNode->FirstChildElement("ClosedMesh")->GetText();
				}
			}
			else {
				Scalar lengthSize;
				if (pLineMeshNode->FirstChildElement("lengthSize")) {
					lengthSize = atof(pLineMeshNode->FirstChildElement("lengthSize")->GetText());
				}
				int numSubdivis;
				if (pLineMeshNode->FirstChildElement("NumberVertices")) {
					numSubdivis = atoi(pLineMeshNode->FirstChildElement("NumberVertices")->GetText());
				}

				if (pLineMeshNode->FirstChildElement("SinFunction")) {
					Scalar amplitude, frequency;
					pLineMeshNode->FirstChildElement("SinFunction")->QueryFloatAttribute("amplitude", &amplitude);
					pLineMeshNode->FirstChildElement("SinFunction")->QueryFloatAttribute("frequency", &frequency);

					Scalar dx = lengthSize / (numSubdivis - 1);
					for (int i = 0; i < numSubdivis; i++) {
						VectorT thinObjectPoint;
						thinObjectPoint.x = dx*i - lengthSize*0.5;
						thinObjectPoint.y = sin(dx*i*PI*frequency)*amplitude;
						lineMeshParams.initialPoints.push_back(thinObjectPoint + lineMeshParams.position);
					}
				}
				else if (pLineMeshNode->FirstChildElement("VerticalLine")) {
					Scalar dx = lengthSize / (numSubdivis - 1);
					for (int i = 0; i < numSubdivis; i++) {
						VectorT thinObjectPoint;
						thinObjectPoint.x = 0;
						thinObjectPoint.y = dx*i - lengthSize*0.5;
						lineMeshParams.initialPoints.push_back(thinObjectPoint + lineMeshParams.position);
					}
				}
				else if (pLineMeshNode->FirstChildElement("HorizontalLine")) {
					Scalar dx = lengthSize / (numSubdivis - 1);
					for (int i = 0; i < numSubdivis; i++) {
						VectorT thinObjectPoint;
						thinObjectPoint.x = dx*i - lengthSize*0.5;
						thinObjectPoint.y = 0;
						lineMeshParams.initialPoints.push_back(thinObjectPoint + lineMeshParams.position);
					}
				}
				else if (pLineMeshNode->FirstChildElement("Circle")) {
					Scalar radius = 0.0f;
					if (pLineMeshNode->FirstChildElement("Circle")->FirstChildElement("Radius")) {
						radius = atof(pLineMeshNode->FirstChildElement("Circle")->FirstChildElement("Radius")->GetText());
					}
					Scalar dx = 2.0f / (numSubdivis - 1);
					for (int i = 0; i < numSubdivis; i++) {
						VectorT thinObjectPoint;
						thinObjectPoint.x = cos(dx*i*PI)*radius;
						thinObjectPoint.y = sin(dx*i*PI)*radius;
						lineMeshParams.initialPoints.push_back(thinObjectPoint + lineMeshParams.position);
					}
					if (lineMeshParams.initialPoints.back() != lineMeshParams.initialPoints.front()) {
						lineMeshParams.initialPoints.push_back(lineMeshParams.initialPoints.front());
					}
				}
				else if (pLineMeshNode->FirstChildElement("GearLine")) {
					lineMeshParams.initialPoints = createGearGeometry(pLineMeshNode->FirstChildElement("GearLine"), numSubdivis, lineMeshParams.position);
				}
				else if (pLineMeshNode->FirstChildElement("RectangularLine")) {
					VectorT rectangleSize(1, 1);

					if (pLineMeshNode->FirstChildElement("RectangularLine")->FirstChildElement("size")) {
						double rectangleSizeX;
						double rectangleSizeY;

						pLineMeshNode->FirstChildElement("RectangularLine")->FirstChildElement("size")->QueryDoubleAttribute("x", &rectangleSizeX);
						pLineMeshNode->FirstChildElement("RectangularLine")->FirstChildElement("size")->QueryDoubleAttribute("y", &rectangleSizeY);

						rectangleSize.x = rectangleSizeX;
						rectangleSize.y = rectangleSizeY;
					}

					//Position is the centroid of the rectangularLine
					lineMeshParams.initialPoints.push_back(lineMeshParams.position + VectorT(-rectangleSize.x, -rectangleSize.y)*0.5);
					lineMeshParams.initialPoints.push_back(lineMeshParams.position + VectorT(rectangleSize.x, -rectangleSize.y)*0.5);
					lineMeshParams.initialPoints.push_back(lineMeshParams.position + VectorT(rectangleSize.x, rectangleSize.y)*0.5);
					lineMeshParams.initialPoints.push_back(lineMeshParams.position + VectorT(-rectangleSize.x, rectangleSize.y)*0.5);
					lineMeshParams.initialPoints.push_back(lineMeshParams.position + VectorT(-rectangleSize.x, -rectangleSize.y)*0.5);
				}
			}
			return lineMeshParams;
		}

		#pragma region 2DMeshLoading
		template<class VectorT>
		vector<VectorT> MeshLoader::createGearGeometry(TiXmlElement * pGearNode, uint numSubdivis, const VectorT &position) {
			vector<VectorT> linePoints;
			Scalar innerRadius = 0.0f;
			int numberOfDents = 0;
			Scalar dentSmoothing = 0.75;
			Scalar dentSize = 0.07;
			Scalar dentAngleCorrection = 0;
			if (pGearNode->FirstChildElement("Radius")) {
				innerRadius = atof(pGearNode->FirstChildElement("Radius")->GetText());
			}

			if (pGearNode->FirstChildElement("DentSize")) {
				dentSize = atof(pGearNode->FirstChildElement("DentSize")->GetText());
			}

			if (pGearNode->FirstChildElement("AngleCorrection")) {
				dentAngleCorrection = DegreeToRad(atof(pGearNode->FirstChildElement("AngleCorrection")->GetText()));
			}

			if (pGearNode->FirstChildElement("NumberOfDents")) {
				numberOfDents = atoi(pGearNode->FirstChildElement("NumberOfDents")->GetText());
			}

			//Adjusting number of subdivisions
			int numberOfSubdivisPointPerDent = max((int)floor(numSubdivis / numberOfDents), 2);
			numSubdivis = numberOfSubdivisPointPerDent*numberOfDents;

			Scalar angleDx = DegreeToRad(180.0f / numSubdivis);
			Scalar angleBiggerDx = DegreeToRad(180.0f / numberOfDents);

			Scalar angleTemp = (DegreeToRad(180.f) - angleBiggerDx) / 2;
			DoubleScalar angleCorrection = max(DegreeToRad(90.f) - angleTemp, 0.f);
			angleCorrection += dentAngleCorrection;

			for (int i = 0; i < numberOfDents; i++) {
				//Circular part
				for (int j = 0; j < numberOfSubdivisPointPerDent; j++) {
					VectorT circlePoint;
					circlePoint.x = cos(i*angleBiggerDx * 2 + angleDx*j);
					circlePoint.y = sin(i*angleBiggerDx * 2 + angleDx*j);
					if (j == 0) { //First, create gear dent
						VectorT gearPoint = circlePoint*dentSize;
						gearPoint.rotate(-angleCorrection);
						linePoints.push_back(circlePoint*innerRadius + gearPoint + position);
						linePoints.push_back(circlePoint*innerRadius + position);
					}
					else if (j == numberOfSubdivisPointPerDent - 1) { //Last, create gear dent
						linePoints.push_back(circlePoint*innerRadius + position);
						VectorT gearPoint = circlePoint*dentSize;
						gearPoint.rotate(angleCorrection);
						linePoints.push_back(circlePoint*innerRadius + gearPoint + position);
					}
					else {
						linePoints.push_back(circlePoint*innerRadius + position);
					}
				}
			}
			linePoints.push_back(linePoints.front());


			return linePoints;
		}

		template<class VectorT>
		void MeshLoader::loadLineMeshFromFile(const string &fileStr, const VectorT &initialPosition, vector<VectorT>& linePoints) {
			shared_ptr<ifstream> fileStream(new ifstream(fileStr.c_str()));
			if (fileStream->fail())
				throw("File not found: " + fileStr);

			Scalar temp;
			VectorT currPoint;
			int numPoints;
			(*fileStream) >> numPoints;
			for (int i = 0; i < numPoints; i++) {
				(*fileStream) >> currPoint.x;
				(*fileStream) >> currPoint.y;
				(*fileStream) >> temp;
				linePoints.push_back(currPoint + initialPosition);
			}
		}

		template<class VectorT>
		shared_ptr<LineMesh<VectorT>> MeshLoader::loadLineMesh(TiXmlElement * pLineMeshNode) {
			return make_shared<LineMesh<VectorT>>(loadParams<VectorT>(pLineMeshNode));
		}

		template<class VectorT>
		shared_ptr<IntersectedLineMesh<VectorT>> MeshLoader::loadIntersectedLineMesh(TiXmlElement* pLineMeshNode, const dimensions_t &gridDimensions, Scalar dx) {
			return make_shared<IntersectedLineMesh<VectorT>>(loadParams<VectorT>(pLineMeshNode), gridDimensions, dx);
		}
		#pragma endregion

		#pragma region 3DMeshLoading
		template<class VectorT>
		shared_ptr<PolygonalMesh<VectorT>> MeshLoader::loadPolyMesh(TiXmlElement *pObjectsNode) {
			if (pObjectsNode->FirstChildElement("Filename")) {
				string cgalObjMeshFile = "Geometry/3D/";
				string cgalObjShortFile = pObjectsNode->FirstChildElement("Filename")->GetText();
				string objectName;
				size_t tempFound = cgalObjShortFile.rfind("/");
				if (tempFound != string::npos) {
					string tempStr = cgalObjShortFile.substr(tempFound, cgalObjShortFile.length() - tempFound);
					objectName = tempStr.substr(1, tempStr.rfind(".") - 1);
				}
				else {
					objectName = cgalObjShortFile.substr(0, cgalObjShortFile.rfind(".") - 1);
				}
				cgalObjMeshFile += cgalObjShortFile;

				double positionX = 0;
				double positionY = 0;
				double positionZ = 0;
				if (pObjectsNode->FirstChildElement("position")) {
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("x", &positionX);
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("y", &positionY);
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("z", &positionZ);
				}

				VectorT position(positionX, positionY, positionZ);

				shared_ptr<PolygonalMesh<VectorT>> retMesh = make_shared<PolygonalMesh<VectorT>>(cgalObjMeshFile);
				retMesh->setName(objectName);
				return retMesh;
			}
			else {
				throw(std::logic_error("loadPolyMeshes: Mesh filename not found inside mesh node!"));
			}
		}

		template<class VectorT>
		vector<shared_ptr<PolygonalMesh<VectorT>>> MeshLoader::loadPolyMeshes(TiXmlElement * pObjectsNode) {
			vector<shared_ptr<PolygonalMesh<VectorT>>> meshes;
			auto pMeshNode = pObjectsNode->FirstChildElement("Mesh");
			while (pMeshNode != NULL) {
				meshes.push_back(loadPolyMesh<VectorT>(pObjectsNode));
				pMeshNode = pMeshNode->NextSiblingElement("Mesh");
			}
			return meshes;
		}

		template<class VectorT>
		shared_ptr<IntersectedPolyMesh<VectorT>> MeshLoader::loadIntersectedPolyMesh(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx) {
			if (pObjectsNode->FirstChildElement("Filename")) {
				string cgalObjMeshFile = "Geometry/3D/";
				string cgalObjShortFile = pObjectsNode->FirstChildElement("Filename")->GetText();
				string objectName;
				size_t tempFound = cgalObjShortFile.rfind("/");
				if (tempFound != string::npos) {
					string tempStr = cgalObjShortFile.substr(tempFound, cgalObjShortFile.length() - tempFound);
					objectName = tempStr.substr(1, tempStr.rfind(".") - 1);
				}
				else {
					objectName = cgalObjShortFile.substr(0, cgalObjShortFile.rfind(".") - 1);
				}
				cgalObjMeshFile += cgalObjShortFile;

				double positionX = 0;
				double positionY = 0;
				double positionZ = 0;
				if (pObjectsNode->FirstChildElement("position")) {
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("x", &positionX);
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("y", &positionY);
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("z", &positionZ);
				}

				VectorT position(positionX, positionY, positionZ);
				Pose<VectorT> objectPose;
				objectPose.setCentroid(position);

				shared_ptr<IntersectedPolyMesh<VectorT>> retMesh = make_shared<IntersectedPolyMesh<VectorT>>(cgalObjMeshFile, gridDimensions, dx, true, objectPose);
				retMesh->setName(objectName);
				return retMesh;
			}
			else {
				throw(std::logic_error("loadPolyMeshes: Mesh filename not found inside mesh node!"));
			}
		}

		template<class VectorT>
		vector<shared_ptr<IntersectedPolyMesh<VectorT>>> MeshLoader::loadIntersectedPolyMeshes(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx) {
			vector<shared_ptr<IntersectedPolyMesh<VectorT>>> meshes;
			TiXmlElement *pMeshNode = make_shared<TiXmlElement>(pObjectsNode->FirstChildElement("Mesh")); // added make_shared<TiXmlElement>()
			while (pMeshNode != NULL) {
				meshes.push_back(loadIntersectedPolyMesh<VectorT>(pObjectsNode, gridDimensions, dx));
				pMeshNode = make_shared<TiXmlElement>(pMeshNode->NextSiblingElement("Mesh")); // added make_shared<TiXmlElement>()
			}
			return meshes;
		}

		template<class VectorT>
		shared_ptr<IntersectedPolyMesh<VectorT>> MeshLoader::loadIntersectedPolyMeshFromSet(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx) {
			if (pObjectsNode->FirstChildElement("Fileset")) {
				string cgalObjMeshFile = "Geometry/3D/";
				string cgalObjShortFile = pObjectsNode->FirstChildElement("Fileset")->GetText();
				string objectName;
				size_t tempFound = cgalObjShortFile.rfind("/");
				if (tempFound != string::npos) {
					string tempStr = cgalObjShortFile.substr(tempFound, cgalObjShortFile.length() - tempFound);
					objectName = tempStr;
				}
				else {
					objectName = cgalObjShortFile;
				}
				cgalObjMeshFile += cgalObjShortFile;
				cgalObjMeshFile += "0.obj";

				double positionX = 0;
				double positionY = 0;
				double positionZ = 0;
				if (pObjectsNode->FirstChildElement("position")) {
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("x", &positionX);
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("y", &positionY);
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("z", &positionZ);
				}

				VectorT position(positionX, positionY, positionZ);
				Pose<VectorT> pose;
				pose.setCentroid(position);

				shared_ptr<IntersectedPolyMesh<VectorT>> retMesh = make_shared<IntersectedPolyMesh<VectorT>>(cgalObjMeshFile, gridDimensions, dx, true, pose);
				retMesh->setName(objectName);
				return retMesh;
			}
			else {
				throw(std::logic_error("loadPolyMeshes: Mesh filename not found inside mesh node!"));
			}
		}

		template<class VectorT>
		vector<vector<VectorT>> MeshLoader::loadAnimPolyMeshesVertices(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx) {
			vector<vector<VectorT>> vertexSetVector;
			if (pObjectsNode->FirstChildElement("Fileset")) {
				string cgalObjMeshFile = "Geometry/3D/";
				string cgalObjShortFile = pObjectsNode->FirstChildElement("Fileset")->GetText();
				string objectName;
				size_t tempFound = cgalObjShortFile.rfind("/");
				if (tempFound != string::npos) {
					string tempStr = cgalObjShortFile.substr(tempFound, cgalObjShortFile.length() - tempFound);
					objectName = tempStr;
				}
				else {
					objectName = cgalObjShortFile;
				}
				cgalObjMeshFile += cgalObjShortFile;

				double positionX = 0;
				double positionY = 0;
				double positionZ = 0;
				if (pObjectsNode->FirstChildElement("position")) {
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("x", &positionX);
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("y", &positionY);
					pObjectsNode->FirstChildElement("position")->QueryDoubleAttribute("z", &positionZ);
				}

				VectorT position(positionX, positionY, positionZ);

				const uint numFrames = atof(pObjectsNode->FirstChildElement("Frames")->GetText());

				for (uint i = 0; i < numFrames; ++i) {
					string cgalObjMeshFrameFile = cgalObjMeshFile;
					cgalObjMeshFrameFile += std::to_string(i);
					cgalObjMeshFrameFile += ".obj";

					vertexSetVector.emplace_back();
					/*std::tie(vertexSetVector.back(), std::ignore) = OBJMeshImporter<VectorT>::getInstance()->loadMesh(cgalObjMeshFrameFile);
					std::vector<VectorT>& vertices = vertexSetVector.back();
					for (size_t i = 0; i < vertices.size(); i++) {
						VectorT& v = vertices[i];
						v = v + position;
					}*/
				}
			}
			else {
				throw(std::logic_error("loadPolyMeshes: Mesh filename not found inside mesh node!"));
			}
			return vertexSetVector;
		}
		#pragma endregion


		template shared_ptr<PolygonalMesh<Vector3>>  MeshLoader::loadPolyMesh<Vector3>(TiXmlElement *pObjectsNode);
		template shared_ptr<PolygonalMesh<Vector3D>>  MeshLoader::loadPolyMesh<Vector3D>(TiXmlElement *pObjectsNode);

		template vector<shared_ptr<PolygonalMesh<Vector3>>>  MeshLoader::loadPolyMeshes<Vector3>(TiXmlElement *pObjectsNode);
		template vector<shared_ptr<PolygonalMesh<Vector3D>>>  MeshLoader::loadPolyMeshes<Vector3D>(TiXmlElement *pObjectsNode);

		template shared_ptr<IntersectedPolyMesh<Vector3>>  MeshLoader::loadIntersectedPolyMesh<Vector3>(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);
		template shared_ptr<IntersectedPolyMesh<Vector3D>>  MeshLoader::loadIntersectedPolyMesh<Vector3D>(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);


		template shared_ptr<IntersectedPolyMesh<Vector3>>  MeshLoader::loadIntersectedPolyMeshFromSet<Vector3>(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);
		template shared_ptr<IntersectedPolyMesh<Vector3D>>  MeshLoader::loadIntersectedPolyMeshFromSet<Vector3D>(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);
		template vector<vector<Vector3>>  MeshLoader::loadAnimPolyMeshesVertices<Vector3>(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);
		template vector<vector<Vector3D>>  MeshLoader::loadAnimPolyMeshesVertices<Vector3D>(TiXmlElement *pObjectsNode, const dimensions_t &gridDimensions, Scalar dx);

		template shared_ptr<LineMesh<Vector2>>  MeshLoader::loadLineMesh<Vector2>(TiXmlElement *pObjectsNode);
		template shared_ptr<LineMesh<Vector2D>>  MeshLoader::loadLineMesh<Vector2D>(TiXmlElement *pObjectsNode);

		template shared_ptr<IntersectedLineMesh<Vector2>>  MeshLoader::loadIntersectedLineMesh(TiXmlElement* pLineMeshNode, const dimensions_t &gridDimensions, Scalar dx);
		template shared_ptr<IntersectedLineMesh<Vector2D>>  MeshLoader::loadIntersectedLineMesh(TiXmlElement* pLineMeshNode, const dimensions_t &gridDimensions, Scalar dx);

		template vector<Vector2>  MeshLoader::createGearGeometry<Vector2>(TiXmlElement *pGearNode, uint numSubdivis, const Vector2 &position);
		template vector<Vector2D>  MeshLoader::createGearGeometry<Vector2D>(TiXmlElement *pGearNode, uint numSubdivis, const Vector2D &position);
	}
}
