#include "Loaders/FlowSolverLoader.h"
#include "Loaders/XMLParamsLoader.h"
#include "Loaders/ForcingFunctionsLoader.h"
#include "Loaders/AdvectionLoader.h"
#include "Loaders/GridLoader.h"
#include "Loaders/MeshLoader.h"

//#include <direct.h> // not gcc compatible
#include <filesystem>

namespace Chimera {
	namespace IO {

		#pragma region LoadingFunctions

		template <typename FlowSolverType>
		shared_ptr<FlowSolver<FlowSolverType>> FlowSolverLoader::loadSolver(shared_ptr<TiXmlElement> pSolverNode) {
			solverType_t solverType = loadFlowSolverType(pSolverNode);

			m_pGridNode = shared_ptr<TiXmlElement>(pSolverNode->FirstChildElement("Grid"));
			shared_ptr<GridMesh<typename FlowSolverType::GridMeshType>> pGrid;
			
			if (solverType == voxelizedSolver || solverType == voxelizedDECSolver || solverType == streamfunctionDECSolver) {
				pGrid = GridLoader::getInstance()->loadGridMesh<typename FlowSolverType::GridMeshType>(m_pGridNode, nullptr);
			}
			else {
				pGrid = GridLoader::getInstance()->loadGridMesh<typename FlowSolverType::GridMeshType>(m_pGridNode, pSolverNode->FirstChildElement("Obstacle")->FirstChildElement("Geometry"));
			}
			
			map<string, shared_ptr<LevelSetGridMesh<typename FlowSolverType::EmbeddedMeshClass, typename FlowSolverType::GridMeshType>>> levelSetMaps;

			if (pSolverNode->FirstChildElement("SmokeSource")) {
				levelSetMaps["smoke"] = loadSmokeSource<FlowSolverType>(pSolverNode->FirstChildElement("SmokeSource"), pGrid);
			}

			if (pSolverNode->FirstChildElement("Obstacle")) {
				levelSetMaps["obstacle"] = loadObstacle <FlowSolverType>(pSolverNode->FirstChildElement("Obstacle"), pGrid);

				if (pSolverNode->FirstChildElement("Obstacle")->FirstChildElement("Geometry")) {
					auto pGeometryNode = pSolverNode->FirstChildElement("Obstacle")->FirstChildElement("Geometry");
				}
			}

			

			if constexpr (isVector2<typename FlowSolverType::VectorType>::value) {
				if (solverType == voxelizedSolver) {
					//auto pInterpolant = make_shared<BilinearStaggeredInterpolant2D<typename FlowSolverType::VectorType, typename FlowSolverType::GridMeshType>>(pGrid, "velocity");
					//auto pAdvection = AdvectionLoader::getInstance()->loadAdvection<typename FlowSolverType::GridMeshType>(shared_ptr<TiXmlElement>(pSolverNode->FirstChildElement("Advection")), pGrid, pInterpolant);
					return make_shared<VoxelizedGridSolver2D<FlowSolverType>>(pGrid, levelSetMaps);
				}
			}
		}

		template shared_ptr<FlowSolver<FlowSolverType2D>> FlowSolverLoader::loadSolver<FlowSolverType2D>(shared_ptr<TiXmlElement> pSolverNode);

		#pragma endregion

		#pragma region InternalLoadingFunctions
		solverType_t FlowSolverLoader::loadFlowSolverType(shared_ptr<TiXmlElement> pSolverNode) {
			if (pSolverNode->FirstChildElement("FlowSolverType") != nullptr) {
				string solverType = pSolverNode->FirstChildElement("FlowSolverType")->GetText();
				transform(solverType.begin(), solverType.end(), solverType.begin(), ::tolower);
				if (solverType == "regular" || solverType == "voxelized") {
					return voxelizedSolver;
				}
				else if (solverType == "regulardec" || solverType == "voxelizeddec") {
					return voxelizedDECSolver;
				}
				else if (solverType == "cutcell" || solverType == "cutvoxel") {
					return cutCellMethod;
				}
				else if (solverType == "streamfunctiondec") {
					return streamfunctionDECSolver;
				}
			}
		}

		template<typename FlowSolverType>
		shared_ptr<LevelSetGridMesh<typename FlowSolverType::EmbeddedMeshClass, typename FlowSolverType::GridMeshType>> FlowSolverLoader::loadSmokeSource(TiXmlElement* pSmokeSource, shared_ptr<GridMesh<typename FlowSolverType::GridMeshType>> pGridMesh) {
			uint numberSubdivisions = 0;
			typename FlowSolverType::VectorType sourcePosition;
			Scalar sourceSize;

			QuadGridMesh<typename FlowSolverType::GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<typename FlowSolverType::GridMeshType>*>(pGridMesh.get());
			

			if (pSmokeSource->FirstChildElement("NumberSubdivisions")) {
				numberSubdivisions = atoi(pSmokeSource->FirstChildElement("NumberSubdivisions")->GetText());
			}
			//auto pLevelSetMesh = make_shared<LevelSetGridMesh<typename FlowSolverType::EmbeddedMeshClass, typename FlowSolverType::GridMeshType>>(pQuadGridMesh->getDualQuadGridMesh(), numberSubdivisions);
			auto pLevelSetMesh = make_shared<LevelSetGridMesh<typename FlowSolverType::EmbeddedMeshClass, typename FlowSolverType::GridMeshType>>(pGridMesh, numberSubdivisions);

			TiXmlElement* pGeometryNode = pSmokeSource->FirstChildElement("Geometry");
			if (pGeometryNode) {
					TiXmlElement* pPositionNode = pGeometryNode->FirstChildElement("Position");
					if (pPositionNode) {
						sourcePosition = XMLParamsLoader::getInstance()->loadVectorFromNode<typename FlowSolverType::VectorType>(pPositionNode);
					}
					else {
						throw std::logic_error("FlowSolverLoader::loadSmokeSource: Position node not found!");
					}

					TiXmlElement* pCircleNode = pGeometryNode->FirstChildElement("Circle");
					if (pCircleNode) {
						TiXmlElement* pRadius = pCircleNode->FirstChildElement("Radius");
						if (pRadius) {
							sourceSize = XMLParamsLoader::getInstance()->loadScalarFromNode(pRadius);
						}
						else {
							throw std::logic_error("FlowSolverLoader::loadSmokeSource: Radius node not found!");
						}
					}

					pLevelSetMesh->initializeSphere(sourcePosition, sourceSize);
			}

			TiXmlElement* pDensityNode = pSmokeSource->FirstChildElement("Density");
			if (pDensityNode) {
				TiXmlElement* pValueNode = pDensityNode->FirstChildElement("Value");
				Scalar value;
				if (pValueNode) {
					value = XMLParamsLoader::getInstance()->loadScalarFromNode(pValueNode);
				}
				else {
					throw std::logic_error("FlowSolverLoader::loadSmokeSource: Value node not found!");
				}
				
				pLevelSetMesh->getGrid()->template addVerticesAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, typename FlowSolverType::GridMeshType>>("smoke", 0.0f);
				for (auto pVertex : pLevelSetMesh->getGrid()->getVertices()) {
					if (OwnCustomAttribute<Scalar>::get(pVertex)->getAttribute("levelSet") <= 0) {
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("smoke", value);
					}
				}
			}


			TiXmlElement* pTemperatureNode = pSmokeSource->FirstChildElement("Temperature");
			if (pTemperatureNode) {
				TiXmlElement* pValueNode = pTemperatureNode->FirstChildElement("Value");
				Scalar value;
				if (pValueNode) {
					value = XMLParamsLoader::getInstance()->loadScalarFromNode(pValueNode);
				}
				else {
					throw std::logic_error("FlowSolverLoader::loadSmokeSource: Value node not found!");
				}

				pLevelSetMesh->getGrid()->template addVerticesAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, typename FlowSolverType::GridMeshType>>("temperature", 0.0f);

				for (auto pVertex : pLevelSetMesh->getGrid()->getVertices()) {
					if (OwnCustomAttribute<Scalar>::get(pVertex)->getAttribute("levelSet") <= 0) {
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("temperature", value);
					}
				}

			}

		
			return pLevelSetMesh;
		}

		template<typename FlowSolverType>
		shared_ptr<LevelSetGridMesh<typename FlowSolverType::EmbeddedMeshClass, typename FlowSolverType::GridMeshType>> FlowSolverLoader::loadObstacle(TiXmlElement* pObstacleNode, shared_ptr<GridMesh<typename FlowSolverType::GridMeshType>> pGridMesh) {
			uint numberSubdivisions = 0;
			typename FlowSolverType::VectorType sourcePosition;
			Scalar sourceSize;

			if (pObstacleNode->FirstChildElement("NumberSubdivisions")) {
				numberSubdivisions = atoi(pObstacleNode->FirstChildElement("NumberSubdivisions")->GetText());
			}
			auto pLevelSetMesh = make_shared<LevelSetGridMesh<typename FlowSolverType::EmbeddedMeshClass, typename FlowSolverType::GridMeshType>>(pGridMesh, numberSubdivisions);

			TiXmlElement* pGeometryNode = pObstacleNode->FirstChildElement("Geometry");
			if (pGeometryNode) {
				TiXmlElement* pPositionNode = pGeometryNode->FirstChildElement("Position");
				if (pPositionNode) {
					sourcePosition = XMLParamsLoader::getInstance()->loadVectorFromNode<typename FlowSolverType::VectorType>(pPositionNode);
				}
				else {
					throw std::logic_error("FlowSolverLoader::loadObstacle: Position node not found!");				}

				TiXmlElement* pCircleNode = pGeometryNode->FirstChildElement("Circle");
				if (pCircleNode) {
					TiXmlElement* pRadius = pCircleNode->FirstChildElement("Radius");
					if (pRadius) {
						sourceSize = XMLParamsLoader::getInstance()->loadScalarFromNode(pRadius);
					}
					else {
						throw std::logic_error("FlowSolverLoader::loadObstacle: Radius node not found!");					}
				}
				else {
					throw exception("FlowSolverLoader::loadObstacle: Geometry node not found!");
				}

				pLevelSetMesh->getGrid()->template addVerticesAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, typename FlowSolverType::GridMeshType>>("levelset", 0.0f);
				pLevelSetMesh->initializeSphere(sourcePosition, sourceSize);
				return pLevelSetMesh;
			}
		}

		template shared_ptr<LevelSetGridMesh<typename FlowSolverType2D::EmbeddedMeshClass, typename FlowSolverType2D::GridMeshType>> FlowSolverLoader::loadSmokeSource<FlowSolverType2D>(TiXmlElement* pSmokeSource, shared_ptr<GridMesh<typename FlowSolverType2D::GridMeshType>> pGridMesh);
		template shared_ptr<LevelSetGridMesh<typename FlowSolverType2D::EmbeddedMeshClass, typename FlowSolverType2D::GridMeshType>> FlowSolverLoader::loadObstacle<FlowSolverType2D>(TiXmlElement* pSmokeSource, shared_ptr<GridMesh<typename FlowSolverType2D::GridMeshType>> pGridMesh);
		#pragma endregion
	}
}
