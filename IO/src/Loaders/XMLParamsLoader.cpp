
#include "Loaders/XMLParamsLoader.h"

namespace Chimera {
	namespace IO {
		#pragma region LoadingFunctions
		bool XMLParamsLoader::loadTrueOrFalse(TiXmlElement *pNode) {
			string trueOrFalseStr = pNode->GetText();
			transform(trueOrFalseStr.begin(), trueOrFalseStr.end(), trueOrFalseStr.begin(), ::tolower);
			if (trueOrFalseStr == "true") {
				return true;
			}
			else if (trueOrFalseStr == "false") {
				return false;
			}
			return false;
		}
		platform_t XMLParamsLoader::loadPlatform(TiXmlElement *pPlatformNode) {
			string platformStr = pPlatformNode->GetText();
			transform(platformStr.begin(), platformStr.end(), platformStr.begin(), ::tolower);
			if (platformStr == "gpu")
				return PlatformGPU;
			else if (platformStr == "cpu")
				return PlatformCPU;
			return PlatformCPU;
		}

		gridArrangement_t XMLParamsLoader::loadGridArrangement(TiXmlElement *pGridArrangementNode) {
			string gridArrangement = pGridArrangementNode->GetText();
			transform(gridArrangement.begin(), gridArrangement.end(), gridArrangement.begin(), ::tolower);
			if (gridArrangement == "staggered") {
				return staggeredArrangement;
			}
			else if (gridArrangement == "nodebased" || gridArrangement == "nodalbased" || gridArrangement == "nodal") {
				return nodalArrangement;
			}
			return staggeredArrangement;
		}

		kernelTypes_t XMLParamsLoader::loadKernel(TiXmlElement *pKernelNode) {
			string kernelType = pKernelNode->GetText();
			transform(kernelType.begin(), kernelType.end(), kernelType.begin(), ::tolower);
			if (kernelType == "sph") {
				return SPHkernel;
			}
			else if (kernelType == "bilinear") {
				return bilinearKernel;
			}
			else if (kernelType == "inversedistance") {
				return inverseDistance;
			}
			return SPHkernel;
		}

		particlesSampling_t XMLParamsLoader::loadSampler(TiXmlElement *pSamplerNode) {
			string samplerType = pSamplerNode->GetText();
			transform(samplerType.begin(), samplerType.end(), samplerType.begin(), ::tolower);
			if (samplerType == "stratified" || samplerType == "stratifiedampler") {
				return stratifiedSampling;
			}
			else if (samplerType == "poisson" || samplerType == "poissonsampler" || samplerType == "poissondisc") {
				return poissonSampling;
			}
			return stratifiedSampling;
		}
		
		collisionDetectionMethod_t XMLParamsLoader::loadCollisionDetectionMethod(TiXmlElement *pCollisionDetectionMethod) {
			string collisionDetectionMethod = pCollisionDetectionMethod->GetText();
			transform(collisionDetectionMethod.begin(), collisionDetectionMethod.end(), collisionDetectionMethod.begin(), ::tolower);
			if (collisionDetectionMethod == "ccdbrochu") {
				return continuousCollisionBrochu;
			}
			else if (collisionDetectionMethod == "ccdwang") {
				return continuousCollisionWang;
			}
			else if (collisionDetectionMethod == "nocollisiondetection") {
				return noCollisionDetection;
			}
			else if (collisionDetectionMethod == "cgalsegmentintersection") {
				return cgalSegmentIntersection;
			}
		}

		interpolationMethod_t XMLParamsLoader::loadInterpolation(TiXmlElement *pInterpolationNode) {
			if (pInterpolationNode->FirstChildElement("Method")) {
				auto pMethodNode = pInterpolationNode->FirstChildElement("Method");
				if (pMethodNode) {
					string interpolationMethodStr = pMethodNode->GetText();
					transform(interpolationMethodStr.begin(), interpolationMethodStr.end(), interpolationMethodStr.begin(), ::tolower);

					if (interpolationMethodStr == "bilinearNodal" || interpolationMethodStr == "bilinearstaggered") {
						return Linear;
					}
					else if (interpolationMethodStr == "meanvaluecoordinates") {
						return MeanValueCoordinates;
					}
					else if (interpolationMethodStr == "turbulenceinterpolant") {
						return Turbulence;
					}
					else if (interpolationMethodStr == "bilinearstreamfunction") {
						return LinearStreamfunction;
					}
					else if (interpolationMethodStr == "quadraticstreamfunction") {
						return QuadraticStreamfunction;
					}
					else if (interpolationMethodStr == "subdivisionstreamfunction") {
						return SubdivisStreamfunction;
					}
					else if (interpolationMethodStr == "cubicstreamfunction") {
						return CubicStreamfunction;
					}
					else if (interpolationMethodStr == "mlslinear") {
						return MLSLinear;
					}
				}
			}
			return NotDefined;
		}

		

		solidBoundaryType_t XMLParamsLoader::loadSolidWallConditions(TiXmlElement *pSolidWallNode) {
			auto pTempNode = pSolidWallNode->FirstChildElement();
			string integrationType(pTempNode->Value());
			transform(integrationType.begin(), integrationType.end(), integrationType.begin(), ::tolower);
			if (integrationType == "freeslip") {
				return Solid_FreeSlip;
			}
			else if (integrationType == "noslip") {
				return Solid_NoSlip;
			}
			else if (integrationType == "solidliquid") {
				return Solid_Liquid;
			}
			return Solid_NoSlip;
		}

		
		//template<class VectorType, template <class> class ArrayType>
		//typename DataExporter<VectorType, ArrayType>::configParams_t * XMLParamsLoader::loadLoggingParams(TiXmlElement *pLoggingNode) {
		//	typename DataExporter<VectorType, ArrayType>::configParams_t * pParams = new typename DataExporter<VectorType, ArrayType>::configParams_t();
		//	if (pLoggingNode->FirstChildElement("LogVelocity")) {
		//		pParams->logVelocity = true;
		//		pParams->setVelocityFilename(pLoggingNode->FirstChildElement("LogVelocity")->GetText());
		//	}
		//	else {
		//		pParams->logVelocity = false;
		//	}
		//	if (pLoggingNode->FirstChildElement("LogPressure")) {
		//		pParams->logPressure = true;
		//		pParams->setPressureFilename(pLoggingNode->FirstChildElement("LogPressure")->GetText());
		//	}
		//	else {
		//		pParams->logPressure = false;
		//	}
		//	if (pLoggingNode->FirstChildElement("LogDensity")) {
		//		pParams->logDensity = true;
		//		string openVDBStr;
		//		const char *strAttribute = pLoggingNode->FirstChildElement("LogDensity")->Attribute("openVDB");
		//		if(strAttribute) {
		//			openVDBStr = strAttribute;
		//			transform(openVDBStr.begin(), openVDBStr.end(), openVDBStr.begin(), tolower);
		//			if (openVDBStr == "true") {
		//				pParams->useOpenVDB = true;
		//			}
		//		}
		//		pParams->setDensityFilename(pLoggingNode->FirstChildElement("LogDensity")->GetText());
		//	}
		//	else {
		//		pParams->logDensity = false;
		//	}
		//	if (pLoggingNode->FirstChildElement("LogCutCells")) {
		//		pParams->logSpecialCells = true;
		//		pParams->setCutCellsFilename(pLoggingNode->FirstChildElement("LogCutCells")->GetText());
		//	}
		//	else {
		//		pParams->logSpecialCells = false;
		//	}
		//	if (pLoggingNode->FirstChildElement("LogScreenshot")) {
		//		pParams->logScreenshot = true;
		//		pParams->setScreenshotFilename(pLoggingNode->FirstChildElement("LogScreenshot")->GetText());
		//	}
		//	else {
		//		pParams->logScreenshot = false;
		//	}
		//	if (pLoggingNode->FirstChildElement("ExportAmira")) {
		//		pParams->exportAmira = true;
		//		pParams->setAmiraFilename(pLoggingNode->FirstChildElement("ExportAmira")->GetText());
		//	}
		//	else {
		//		pParams->exportAmira = false;
		//	}

		//	return pParams;
		//}

		//GLRenderer3D::params_t * XMLParamsLoader::loadRendererParams3D(TiXmlElement *pRenderingNode) {
		//	TiXmlElement *pTempNode;
		//	GLRenderer3D::params_t *pParams = new GLRenderer3D::params_t();
		//	if ((pTempNode = pRenderingNode->FirstChildElement("RenderingOptions")) != NULL) {
		//		/**FlowSolver type and flowSolverParams initialization */
		//		if (pTempNode->FirstChildElement("VisualizeGrid") != NULL) {
		//			string visualizeGridStr = pTempNode->FirstChildElement("VisualizeGrid")->GetText();
		//			transform(visualizeGridStr.begin(), visualizeGridStr.end(), visualizeGridStr.begin(), ::tolower);
		//			/*if (visualizeGridStr == "true") {
		//			m_initializeGridVisualization = true;
		//			}
		//			else if (visualizeGridStr == "false") {
		//			m_initializeGridVisualization = false;
		//			}*/
		//		}
		//	}
		//	return pParams;
		//}
		//
		//template typename DataExporter<Vector2, Array2D>::configParams_t *  XMLParamsLoader::loadLoggingParams<Vector2, Array2D>(TiXmlElement *pLoggingNode);
		//template typename DataExporter<Vector3, Array3D>::configParams_t *  XMLParamsLoader::loadLoggingParams<Vector3, Array3D>(TiXmlElement *pLoggingNode);
		
		#pragma endregion
	}
}
