#include "Loaders/ForcingFunctionsLoader.h"
#include "Loaders/XMLParamsLoader.h"

namespace Chimera {

	namespace Loaders {
		#pragma region LoadingFunctions
		/*template <class VectorT, template <class> class VoxelType, template <class> class MeshType>
		typename FlowSolver<VectorT, VoxelType, MeshType>::hotSmokeSource_t * ForcingFunctionsLoader::loadHotSmokeSource(shared_ptr<TiXmlElement>pHotSmokeSourceNode) {
			typename FlowSolver<VectorT, VoxelType, MeshType>::hotSmokeSource_t *pHotSmokeSource = new typename FlowSolver<VectorT, VoxelType, MeshType>::hotSmokeSource_t();
			shared_ptr<TiXmlElement>pPositionNode = pHotSmokeSourceNode->FirstChildElement("Position");
			if (pPositionNode) {
				pHotSmokeSource->position = XMLParamsLoader::getInstance()->loadVectorFromNode<VectorT>(pPositionNode);
			}
			else {
				throw std::logic_error("ForcingFunctionsLoader::loadHotSmokeSource: Position node not found!");
			}
			shared_ptr<TiXmlElement>pSizeNode = pHotSmokeSourceNode->FirstChildElement("Size");
			if (pSizeNode) {
				pHotSmokeSource->size = XMLParamsLoader::getInstance()->loadScalarFromNode(pSizeNode);
			}
			else {
				throw std::logic_error("ForcingFunctionsLoader::loadHotSmokeSource: Size node not found!");
			}

			shared_ptr<TiXmlElement>pVelocityNode = pHotSmokeSourceNode->FirstChildElement("Velocity");
			if (pVelocityNode) {
				pHotSmokeSource->velocity = XMLParamsLoader::getInstance()->loadVectorFromNode<VectorT>(pVelocityNode);
			}

			shared_ptr<TiXmlElement>pDensityValueNode = pHotSmokeSourceNode->FirstChildElement("DensityValue");
			if (pDensityValueNode) {
				pHotSmokeSource->densityValue = XMLParamsLoader::getInstance()->loadScalarFromNode(pDensityValueNode);
			}
			else {
				pHotSmokeSource->densityValue = 1;
			}

			shared_ptr<TiXmlElement>pDensityVariationNode = pHotSmokeSourceNode->FirstChildElement("DensityVariation");
			if (pDensityVariationNode) {
				pHotSmokeSource->densityVariation = XMLParamsLoader::getInstance()->loadScalarFromNode(pDensityVariationNode);
			}
			else {
				pHotSmokeSource->densityVariation = 0;
			}

			shared_ptr<TiXmlElement>pDensityCoefficientNode = pHotSmokeSourceNode->FirstChildElement("DensityBuoyancyCoefficient");
			if (pDensityCoefficientNode) {
				pHotSmokeSource->densityBuoyancyCoefficient = XMLParamsLoader::getInstance()->loadScalarFromNode(pDensityCoefficientNode);
			}
			else {
				pHotSmokeSource->densityBuoyancyCoefficient = 6;
			}

			shared_ptr<TiXmlElement>pTemperatureValueNode = pHotSmokeSourceNode->FirstChildElement("TemperatureValue");
			if (pTemperatureValueNode) {
				pHotSmokeSource->temperatureValue = XMLParamsLoader::getInstance()->loadScalarFromNode(pTemperatureValueNode);
			}
			else {
				pHotSmokeSource->temperatureValue = 1;
			}

			shared_ptr<TiXmlElement>pTemperatureVariationNode = pHotSmokeSourceNode->FirstChildElement("TemperatureVariation");
			if (pTemperatureVariationNode) {
				pHotSmokeSource->temperatureVariation = XMLParamsLoader::getInstance()->loadScalarFromNode(pTemperatureVariationNode);
			}
			else {
				pHotSmokeSource->temperatureVariation = 0;
			}

			shared_ptr<TiXmlElement>pTemperatureCoefficientNode = pHotSmokeSourceNode->FirstChildElement("TemperatureBuoyancyCoefficient");
			if (pTemperatureCoefficientNode) {
				pHotSmokeSource->temperatureBuoyancyCoefficient = XMLParamsLoader::getInstance()->loadScalarFromNode(pTemperatureCoefficientNode);
			}
			else {
				pHotSmokeSource->temperatureBuoyancyCoefficient = 6;
			}
			return pHotSmokeSource;
		}


		template <class VectorT, template <class> class VoxelType, template <class> class MeshType>
		typename FlowSolver<VectorT, VoxelType, MeshType>::swirlingField_t * ForcingFunctionsLoader::loadSwirlingField(shared_ptr<TiXmlElement>pSwirlingFieldNode) {
			typename FlowSolver<VectorT, VoxelType, MeshType>::swirlingField_t *pSwirlingField = new typename FlowSolver<VectorT, VoxelType, MeshType>::swirlingField_t();
			shared_ptr<TiXmlElement>pControlPointsNode = pSwirlingFieldNode->FirstChildElement("ControlPoints");
			if (pControlPointsNode) {
				pSwirlingField->controlPoints = loadControlPoints<VectorT>(pControlPointsNode);
			}

			shared_ptr<TiXmlElement>pMaxRadiusNode = pSwirlingFieldNode->FirstChildElement("MaxRadius");
			if (pMaxRadiusNode) {
				pSwirlingField->maxRadius = XMLParamsLoader::getInstance()->loadScalarFromNode(pMaxRadiusNode);
			}
			else {
				pSwirlingField->maxRadius = 1;
			}

			shared_ptr<TiXmlElement>pUpScalarNode = pSwirlingFieldNode->FirstChildElement("UpScalar");
			if (pUpScalarNode) {
				pSwirlingField->upScalar = XMLParamsLoader::getInstance()->loadScalarFromNode(pUpScalarNode);
			}
			else {
				pSwirlingField->upScalar = 1;
			}

			shared_ptr<TiXmlElement>pRadialScalarNode = pSwirlingFieldNode->FirstChildElement("RadialScalar");
			if (pRadialScalarNode) {
				pSwirlingField->radialScalar = XMLParamsLoader::getInstance()->loadScalarFromNode(pRadialScalarNode);
			}
			else {
				pSwirlingField->upScalar = 1;
			}

			shared_ptr<TiXmlElement>pBottomBoundaryNode = pSwirlingFieldNode->FirstChildElement("BottomBoundary");
			if (pBottomBoundaryNode) {
				pSwirlingField->bottomBoundary = XMLParamsLoader::getInstance()->loadScalarFromNode(pBottomBoundaryNode);
			}
			else {
				pSwirlingField->bottomBoundary = 0.5;
			}

			shared_ptr<TiXmlElement>pTopBoundaryNode = pSwirlingFieldNode->FirstChildElement("TopBoundary");
			if (pTopBoundaryNode) {
				pSwirlingField->topBoundary = XMLParamsLoader::getInstance()->loadScalarFromNode(pTopBoundaryNode);
			}
			else {
				pSwirlingField->topBoundary = 5.5;
			}

			shared_ptr<TiXmlElement>pAngularVelocityNode = pSwirlingFieldNode->FirstChildElement("AngularVelocity");
			if (pAngularVelocityNode) {
				pSwirlingField->angularVelocity = XMLParamsLoader::getInstance()->loadScalarFromNode(pAngularVelocityNode);
			}
			else {
				pSwirlingField->angularVelocity = 1;
			}

			shared_ptr<TiXmlElement>pDensityAreaNode = pSwirlingFieldNode->FirstChildElement("DensityArea");
			if (pDensityAreaNode) {
				pSwirlingField->densityArea = XMLParamsLoader::getInstance()->loadScalarFromNode(pDensityAreaNode);
			}
			else {
				pSwirlingField->densityArea = 0.25;
			}

			shared_ptr<TiXmlElement>pDensityValueNode = pSwirlingFieldNode->FirstChildElement("DensityValue");
			if (pDensityValueNode) {
				pSwirlingField->densityValue = XMLParamsLoader::getInstance()->loadScalarFromNode(pDensityValueNode);
			}
			else {
				pSwirlingField->densityValue = 1;
			}

			shared_ptr<TiXmlElement>pDensityVariationNode = pSwirlingFieldNode->FirstChildElement("DensityVariation");
			if (pDensityVariationNode) {
				pSwirlingField->densityVariation = XMLParamsLoader::getInstance()->loadScalarFromNode(pDensityVariationNode);
			}
			else {
				pSwirlingField->densityVariation = 0;
			}

			return pSwirlingField;
		}*/

		#pragma endregion
		#pragma region FunctionDeclarations
		//template typename FlowSolver<Vector2, QuadCell, Edge>::hotSmokeSource_t * ForcingFunctionsLoader::loadHotSmokeSource<Vector2, Array2D>(shared_ptr<TiXmlElement>pHotSmokeSourceNode);
		//template typename FlowSolver<Vector3, HexaVolume, Cell>::hotSmokeSource_t * ForcingFunctionsLoader::loadHotSmokeSource<Vector3, Array3D>(shared_ptr<TiXmlElement>pHotSmokeSourceNode);

		//template typename FlowSolver<Vector2, QuadCell, Edge>::swirlingField_t * ForcingFunctionsLoader::loadSwirlingField<Vector2, Array2D>(shared_ptr<TiXmlElement>pSwirlingFieldNode);
		//template typename FlowSolver<Vector3, HexaVolume, Cell>::swirlingField_t * ForcingFunctionsLoader::loadSwirlingField<Vector3, Array3D>(shared_ptr<TiXmlElement>pSwirlingFieldNode);
		#pragma endregion
	}
}
