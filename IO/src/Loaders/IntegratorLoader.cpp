#include "Loaders/IntegratorLoader.h"

#include <filesystem>

namespace Chimera {
	namespace IO {

		#pragma region LoadingFunctions
		
		template <typename GridMeshType>
		shared_ptr<PositionIntegrator<GridMeshType>> IntegratorLoader::loadIntegrator(shared_ptr<TiXmlElement> pIntegratorNode, shared_ptr<GridMesh<GridMeshType>> pGridMesh, shared_ptr<Interpolant<typename GridMeshType::VectorType, GridMeshType>> pInterpolant) {
			integrationMethod_t integrationMethod = loadIntegrationMethodParams(pIntegratorNode);

			shared_ptr<PositionIntegrator<GridMeshType>> pPositionIntegrator;
			switch (integrationMethod) {
				case forwardEuler:
					pPositionIntegrator = make_shared<ForwardEulerIntegrator<GridMeshType>>(pGridMesh, pInterpolant);
				break;
				case RungeKutta_2:
					pPositionIntegrator = make_shared<RungeKutta2Integrator<GridMeshType>>(pGridMesh, pInterpolant);
				break;
			}

			return pPositionIntegrator;
		}

		template shared_ptr<PositionIntegrator<QuadGridType>> IntegratorLoader::loadIntegrator<QuadGridType>(shared_ptr<TiXmlElement> pIntegratorNode, shared_ptr<GridMesh<QuadGridType>> pGridMesh, shared_ptr<Interpolant<typename QuadGridType::VectorType, QuadGridType>> pInterpolant);
		#pragma endregion
	}
}
