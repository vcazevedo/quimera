#include "Loaders/SolidsLoader.h"

namespace Chimera {
	namespace Loaders {

		#pragma region SolidsLoadingFunctions
		template <class VectorT, template <class> class MeshType>
		shared_ptr<SolidObject<VectorT, MeshType>> SolidsLoader::loadRigidObject2D(TiXmlElement *pRigidObjectNode, const dimensions_t &gridDimensions, Scalar dx) {
			shared_ptr<SolidObject<VectorT, MeshType>>pRigidObject;
			shared_ptr<MeshType<VectorT>> pMesh = MeshLoader::getInstance()->loadMesh<VectorT, MeshType>(pRigidObjectNode, gridDimensions, dx);

			typename PhysicalObject<VectorT>::positionUpdate_t *pPositionUpdate = nullptr;
			typename PhysicalObject<VectorT>::rotationUpdate_t *pRotationUpdate = nullptr;

			auto pPositionUpdateNode = pRigidObjectNode->FirstChildElement("PositionUpdate");
			if (pPositionUpdateNode) {
				pPositionUpdate = loadPositionUpdate<VectorT>(pPositionUpdateNode);
			}

			auto pRotationUpdateNode = pRigidObjectNode->FirstChildElement("RotationUpdate");
			if (pRotationUpdateNode) {
				pRotationUpdate = loadRotationUpdate<VectorT>(pRotationUpdateNode);
			}
			if (pPositionUpdate && pRotationUpdate) {
				return make_shared<SolidObject<VectorT, MeshType>>(pMesh, *pPositionUpdate, *pRotationUpdate);
			}
			else if (pPositionUpdate) {
				return make_shared<SolidObject<VectorT, MeshType>>(pMesh, *pPositionUpdate);
			}
			else if (pRotationUpdate) {
				pPositionUpdate = new typename PhysicalObject<VectorT>::positionUpdate_t();
				return make_shared<SolidObject<VectorT, MeshType>>(pMesh, *pPositionUpdate, *pRotationUpdate);
			}

			safeDelete(pPositionUpdate);
			safeDelete(pRotationUpdate);

			return make_shared<SolidObject<VectorT, MeshType>>(pMesh);
		}

		template <class VectorT, template <class> class MeshType>
		shared_ptr<SolidObject<VectorT, MeshType>> SolidsLoader::loadRigidObject3D(TiXmlElement *pRigidObjectNode, const dimensions_t &gridDimensions, Scalar dx) {
			shared_ptr<SolidObject<VectorT, MeshType>>pRigidObject;

			shared_ptr<MeshType<VectorT>> pPolyMesh = MeshLoader::getInstance()->loadMesh<VectorT, MeshType>(pRigidObjectNode, gridDimensions, dx);

			typename PhysicalObject<VectorT>::positionUpdate_t *pPositionUpdate = nullptr;
			typename PhysicalObject<VectorT>::rotationUpdate_t *pRotationUpdate = nullptr;

			auto pPositionUpdateNode = pRigidObjectNode->FirstChildElement("PositionUpdate");
			if (pPositionUpdateNode) {
				pPositionUpdate = loadPositionUpdate<VectorT>(pPositionUpdateNode);
			}

			auto pRotationUpdateNode = pRigidObjectNode->FirstChildElement("RotationUpdate");
			if (pRotationUpdateNode) {
				pRotationUpdate = loadRotationUpdate<VectorT>(pRotationUpdateNode);
			}

			if (pPositionUpdate && pRotationUpdate) {
				return make_shared<SolidObject<VectorT, MeshType>>(pPolyMesh, *pPositionUpdate, *pRotationUpdate);
			}
			else if (pPositionUpdate) {
				return make_shared<SolidObject<VectorT, MeshType>>(pPolyMesh, *pPositionUpdate);
			}
			else if (pRotationUpdate) {
				pPositionUpdate = new typename PhysicalObject<VectorT>::positionUpdate_t();
				return make_shared<SolidObject<VectorT, MeshType>>(pPolyMesh, *pPositionUpdate, *pRotationUpdate);
			}

			bool mergeWithOtherValue = false;
			const auto mergeWithOtherElement = pRigidObjectNode->FirstChildElement("MergeWithOther");
			if (mergeWithOtherElement != nullptr) {
				mergeWithOtherValue = XMLParamsLoader::getInstance()->loadTrueOrFalse(mergeWithOtherElement);
			}

			//pPolyMesh->setMergeWithOthers(mergeWithOtherValue);

			return make_shared<SolidObject<VectorT, MeshType>>(pPolyMesh);
		}
		#pragma endregion

		#pragma region LoadingUtils
		template <class VectorT>
		typename PhysicalObject<VectorT>::positionUpdate_t * SolidsLoader::loadPositionUpdate(TiXmlElement *pPositionUpdateNode) {
			typename PhysicalObject<VectorT>::positionUpdate_t *pPositionUpdate = new typename PhysicalObject<VectorT>::positionUpdate_t();
			if (pPositionUpdateNode->FirstChildElement("SinFunction")) {
				pPositionUpdateNode->FirstChildElement("SinFunction")->QueryFloatAttribute("amplitude", &pPositionUpdate->amplitude);
				pPositionUpdateNode->FirstChildElement("SinFunction")->QueryFloatAttribute("frequency", &pPositionUpdate->frequency);
				pPositionUpdate->positionUpdateType = positionUpdateType_t::sinFunction;
			}
			else if (pPositionUpdateNode->FirstChildElement("CosineFunction")) {
				pPositionUpdateNode->FirstChildElement("CosineFunction")->QueryFloatAttribute("amplitude", &pPositionUpdate->amplitude);
				pPositionUpdateNode->FirstChildElement("CosineFunction")->QueryFloatAttribute("frequency", &pPositionUpdate->frequency);
				pPositionUpdate->positionUpdateType = positionUpdateType_t::cosineFunction;
			}
			else if (pPositionUpdateNode->FirstChildElement("UniformFunction")) {
				pPositionUpdateNode->FirstChildElement("UniformFunction")->QueryFloatAttribute("amplitude", &pPositionUpdate->amplitude);
				pPositionUpdate->positionUpdateType = positionUpdateType_t::uniformFunction;
			}
			else if (pPositionUpdateNode->FirstChildElement("Path")) {
				string lineStr = "Geometry/2D/";
				lineStr += pPositionUpdateNode->FirstChildElement("Path")->FirstChildElement("File")->GetText();

				MeshLoader::getInstance()->loadLineMeshFromFile(lineStr, VectorT(0), pPositionUpdate->pathMesh);
				pPositionUpdate->positionUpdateType = positionUpdateType_t::pathAnimation;
				pPositionUpdateNode->FirstChildElement("Path")->QueryFloatAttribute("amplitude", &pPositionUpdate->amplitude);
				if (pPositionUpdateNode->FirstChildElement("Path")->FirstChildElement("position")) {
					VectorT position;
					pPositionUpdateNode->FirstChildElement("Path")->FirstChildElement("position")->QueryFloatAttribute("x", &position.x);
					pPositionUpdateNode->FirstChildElement("Path")->FirstChildElement("position")->QueryFloatAttribute("y", &position.y);
					for (int i = 0; i < pPositionUpdate->pathMesh.size(); i++) {
						pPositionUpdate->pathMesh[i] += position;
					}
				}
			}
			if (pPositionUpdateNode->FirstChildElement("Direction")) {
				Scalar tempz = 0;
				pPositionUpdateNode->FirstChildElement("Direction")->QueryFloatAttribute("z", &tempz);

				//use universal initializer to initialize z value (if there) to avoid having to cast
				pPositionUpdate->direction = VectorT(tempz);
				pPositionUpdateNode->FirstChildElement("Direction")->QueryFloatAttribute("x", &pPositionUpdate->direction.x);
				pPositionUpdateNode->FirstChildElement("Direction")->QueryFloatAttribute("y", &pPositionUpdate->direction.y);
				pPositionUpdate->direction.normalize();
			}
			return pPositionUpdate;
		}

		template <class VectorT>
		typename PhysicalObject<VectorT>::rotationUpdate_t * SolidsLoader::loadRotationUpdate(TiXmlElement *pRotationUpdateNode) {
			typename PhysicalObject<VectorT>::rotationUpdate_t *pRotationUpdate = new typename PhysicalObject<VectorT>::rotationUpdate_t();
			auto pRotationType = pRotationUpdateNode->FirstChildElement();
			string rotationTypeStr(pRotationType->Value());
			transform(rotationTypeStr.begin(), rotationTypeStr.end(), rotationTypeStr.begin(), ::tolower);
			pRotationUpdate->rotationType = constantRotation;
			if (rotationTypeStr == "alternating" || rotationTypeStr == "alternate") {
				pRotationUpdate->rotationType = alternatingRotation;
				auto pMinAngleNode = pRotationType->FirstChildElement("MinAngle");
				if (pMinAngleNode) {
					pRotationUpdate->minAngle = DegreeToRad(atof(pMinAngleNode->GetText()));
				}
				else {
					throw(std::logic_error("SolidsLoader::loadRotationUpdate MinAngle node not found inside alternating rotation!"));
				}
				auto pMaxAngleNode = pRotationType->FirstChildElement("MaxAngle");
				if (pMaxAngleNode) {
					pRotationUpdate->maxAngle = DegreeToRad(atof(pMaxAngleNode->GetText()));
				}
				else {
					throw(std::logic_error("SolidsLoader::loadRotationUpdate MaxAngle node not found inside alternating rotation!"));
				}
			}
			if (pRotationUpdateNode->FirstChildElement("InitialAngle")) {
				pRotationUpdate->initialRotation = DegreeToRad(atof(pRotationUpdateNode->FirstChildElement("InitialAngle")->GetText()));
			}
			if (pRotationUpdateNode->FirstChildElement("AngularSpeed")) {
				pRotationUpdate->speed = atof(pRotationUpdateNode->FirstChildElement("AngularSpeed")->GetText());
			}
			if (pRotationUpdateNode->FirstChildElement("AngularAcceleration")) {
				pRotationUpdate->acceleration = atof(pRotationUpdateNode->FirstChildElement("AngularAcceleration")->GetText());
			}

			if (pRotationUpdateNode->FirstChildElement("StartingTime")) {
				pRotationUpdate->startingTime = atof(pRotationUpdateNode->FirstChildElement("StartingTime")->GetText());
			}

			if (pRotationUpdateNode->FirstChildElement("EndingTime")) {
				pRotationUpdate->endingTime = atof(pRotationUpdateNode->FirstChildElement("EndingTime")->GetText());
			}

			if (pRotationUpdateNode->FirstChildElement("axis")) {
				Scalar tempz;
				pRotationUpdateNode->FirstChildElement("Direction")->QueryFloatAttribute("z", &tempz);
				pRotationUpdate->axis = VectorT(tempz);
				pRotationUpdateNode->FirstChildElement("Direction")->QueryFloatAttribute("z", &pRotationUpdate->axis.x);
				pRotationUpdateNode->FirstChildElement("Direction")->QueryFloatAttribute("z", &pRotationUpdate->axis.y);
				pRotationUpdate->axis.normalize();
			}
			return pRotationUpdate;
		}
		#pragma endregion

		#pragma region FunctionDeclarations
		template shared_ptr<SolidObject<Vector2, LineMesh>> SolidsLoader::loadRigidObject2D<Vector2, LineMesh>(TiXmlElement *pRigidObjectNode, const dimensions_t &gridDimensions, Scalar dx);
		template shared_ptr<SolidObject<Vector3, PolygonalMesh>> SolidsLoader::loadRigidObject3D<Vector3, PolygonalMesh>(TiXmlElement *pRigidObjectNode, const dimensions_t &gridDimensions, Scalar dx);

		template shared_ptr<SolidObject<Vector2, IntersectedLineMesh>> SolidsLoader::loadRigidObject2D<Vector2, IntersectedLineMesh>(TiXmlElement *pRigidObjectNode, const dimensions_t &gridDimensions, Scalar dx);
		template shared_ptr<SolidObject<Vector3, IntersectedPolyMesh>> SolidsLoader::loadRigidObject3D<Vector3, IntersectedPolyMesh>(TiXmlElement *pRigidObjectNode, const dimensions_t &gridDimensions, Scalar dx);

		template typename PhysicalObject<Vector2>::positionUpdate_t * SolidsLoader::loadPositionUpdate<Vector2>(TiXmlElement *pPositionUpdateNode);
		template typename PhysicalObject<Vector2>::rotationUpdate_t * SolidsLoader::loadRotationUpdate<Vector2>(TiXmlElement *pRotationUpdateNode);
		template typename PhysicalObject<Vector3>::positionUpdate_t * SolidsLoader::loadPositionUpdate<Vector3>(TiXmlElement *pPositionUpdateNode);
		template typename PhysicalObject<Vector3>::rotationUpdate_t * SolidsLoader::loadRotationUpdate<Vector3>(TiXmlElement *pRotationUpdateNode);
		#pragma endregion
	}
}
