#include "Loaders/GridLoader.h"
#include <regex>
#include "Loaders/MeshLoader.h"

namespace Chimera {

	namespace IO {

		#pragma region GridLoadingFunctions
		template <typename GridMeshType>
		shared_ptr<GridMesh<GridMeshType>> GridLoader::loadGridMesh(shared_ptr<TiXmlElement> pGridNode, TiXmlElement* pObstacleNode) {
			if constexpr (isVector2<typename GridMeshType::VectorType>::value) {
				return loadQuadGrid<GridMeshType>(pGridNode, pObstacleNode);
			}
			else {
				return nullptr; //loadHexaGrid<GridMeshType>(pGridNode);
			}
		}

		template <typename HexaGridMeshType>
		shared_ptr<HexaGridMesh<HexaGridMeshType>> GridLoader::loadHexaGrid(shared_ptr<TiXmlElement> pGridNode) {
			Scalar gridSpacing;
			Vector3 initialBoundary, finalBoundary;
			TiXmlElement* pTempNode;
			if (pTempNode == pGridNode->FirstChildElement("InitialPoint")) {
				pTempNode->QueryFloatAttribute("x", &initialBoundary.x);
				pTempNode->QueryFloatAttribute("y", &initialBoundary.y);
				pTempNode->QueryFloatAttribute("z", &initialBoundary.z);
			}
			if (pTempNode == pGridNode->FirstChildElement("FinalPoint")) {
				pTempNode->QueryFloatAttribute("x", &finalBoundary.x);
				pTempNode->QueryFloatAttribute("y", &finalBoundary.y);
				pTempNode->QueryFloatAttribute("z", &finalBoundary.z);
			}
			if (pTempNode == pGridNode->FirstChildElement("Spacing")) {
				gridSpacing = atof(pTempNode->GetText());
			}
			else if (pTempNode == pGridNode->FirstChildElement("Dimensions")) {
				dimensions_t tempDimensions;
				pTempNode->QueryIntAttribute("x", &tempDimensions.x);
				pTempNode->QueryIntAttribute("y", &tempDimensions.y);
				pTempNode->QueryIntAttribute("z", &tempDimensions.z);
				gridSpacing = (finalBoundary.x - initialBoundary.x) / (tempDimensions.x + 2);
			}

			return make_shared<HexaGridMesh<HexaGridMeshType>>(initialBoundary, finalBoundary, gridSpacing);
		}

		template <typename QuadGridMeshType>
		shared_ptr<QuadGridMesh<QuadGridMeshType>> GridLoader::loadQuadGrid(shared_ptr<TiXmlElement> pGridNode, TiXmlElement* pObstacleNode) {
			Scalar gridSpacing;
			Vector2 initialBoundary, finalBoundary;
			TiXmlElement* pTempNode;

			if (pTempNode = pGridNode->FirstChildElement("InitialPoint")) {
				pTempNode->QueryFloatAttribute("x", &initialBoundary.x);
				pTempNode->QueryFloatAttribute("y", &initialBoundary.y);
			}
			if (pTempNode = pGridNode->FirstChildElement("FinalPoint")) {
				pTempNode->QueryFloatAttribute("x", &finalBoundary.x);
				pTempNode->QueryFloatAttribute("y", &finalBoundary.y);
			}
			if (pTempNode = pGridNode->FirstChildElement("Spacing")) {
				gridSpacing = atof(pTempNode->GetText());
			}
			else if (pTempNode = pGridNode->FirstChildElement("Dimensions")) {
				dimensions_t tempDimensions;
				pTempNode->QueryIntAttribute("x", &tempDimensions.x);
				pTempNode->QueryIntAttribute("y", &tempDimensions.y);
				gridSpacing = (finalBoundary.x - initialBoundary.x) / (tempDimensions.x + 2);
			}

			vector<shared_ptr<IntersectedLineMesh<typename QuadGridMeshType::VectorType>>> intersectedLineMeshes;
			if (pObstacleNode) {
				/**Compute temporary grid dimensions */
				typename QuadGridMeshType::VectorType boundariesLength(finalBoundary - initialBoundary);

				dimensions_t gridDimensions;
				gridDimensions.x = static_cast<int>(boundariesLength.x / gridSpacing);
				gridDimensions.y = static_cast<int>(boundariesLength.y / gridSpacing);

				intersectedLineMeshes.push_back(IO::MeshLoader::getInstance()->loadIntersectedLineMesh<typename QuadGridMeshType::VectorType>(pObstacleNode, gridDimensions, gridSpacing));
			}
			auto pGridMesh = new QuadGridMesh<QuadGridMeshType>(initialBoundary, finalBoundary, gridSpacing, XYPlane, intersectedLineMeshes);
			shared_ptr<QuadGridMesh<QuadGridMeshType>> pQuadGridMesh = shared_ptr<QuadGridMesh<QuadGridMeshType>>(pGridMesh);
			return pQuadGridMesh;
		}

		template <typename EmbeddedMeshType, typename GridMeshType>
		shared_ptr<LevelSetGridMesh<EmbeddedMeshType, GridMeshType>> GridLoader::loadLevelSetGrid(shared_ptr<TiXmlElement> pGridNode, shared_ptr<GridMesh<GridMeshType>> pGrid) {
			uint numberSubdivisions = 0;
			if (pGridNode->FirstChildElement("NumberSubdivisions")) {
				numberSubdivisions = atoi(pGridNode->FirstChildElement("NumberSubdivisions")->GetText());
			}
			return make_shared<LevelSetGridMesh<EmbeddedMeshType, GridMeshType>>(pGrid, numberSubdivisions);
		}

		#pragma endregion

		#pragma region LoadingUtils
		template<>
		scalarField_t<Array3D> * GridLoader::loadScalarFields<Array3D>(shared_ptr<TiXmlElement>pDensityNode, const dimensions_t &gridDimensions) {
			scalarField_t<Array3D> * pDensityField = new scalarField_t<Array3D>();
			pDensityField->pScalarField = new vector<Array3D<Scalar>>();

			if (pDensityNode->FirstChildElement("DensityField")) {
				auto pDensityFieldNode = pDensityNode->FirstChildElement("DensityField");
				string filename;
				if (pDensityFieldNode->FirstChildElement("Filename")) {
					filename = pDensityFieldNode->FirstChildElement("Filename")->GetText();
				}
				else {
					throw std::logic_error("GridLoader::loadVelocityFields invalid velocity field filename");
				}

				/**Checks if the file extension ends with .vdb */
				bool isOpenVDB = false;
				const char *strAttribute = pDensityFieldNode->FirstChildElement("Filename")->Attribute("openVDB");
				if (strAttribute) {
					string openVDBStr(strAttribute);
					transform(openVDBStr.begin(), openVDBStr.end(), openVDBStr.begin(), ::tolower);
					if (openVDBStr == "true") {
						isOpenVDB = true;
					}
				}

				if (pDensityFieldNode->FirstChildElement("timeStep")) {
					pDensityField->timeStep = atof(pDensityFieldNode->FirstChildElement("timeStep")->GetText());
				}

				Scalar tempScalar;
				int numFrames = 1;
				if (pDensityFieldNode->FirstChildElement("numFrames")) {
					numFrames = atoi(pDensityFieldNode->FirstChildElement("numFrames")->GetText());
				}

				//Load the file onto the precomputed stream
				for (int i = 0; i < numFrames; i++) {
					pDensityField->pScalarField->push_back(Array3D<Scalar>(gridDimensions));
					if (isOpenVDB) {
						string fullFilename = "Flow logs/3D/Density/" + filename + intToStr(i) + ".vdb";
						//loadScalarFieldVDB(fullFilename, pDensityField->pScalarField->back(), "density");
					}
					else {
						string fullFilename = "Flow logs/3D/Density/" + filename + intToStr(i) + ".log";
						loadScalarField(fullFilename, pDensityField->pScalarField->back());
					}
				}
			}
			return pDensityField;
		}



		template<>
		velocityField_t<Vector3, Array3D> * GridLoader::loadVelocityFields<Vector3, Array3D>(shared_ptr<TiXmlElement>pGridNode, const dimensions_t &gridDimensions) {
			velocityField_t<Vector3, Array3D> * pVelocityField = new velocityField_t<Vector3, Array3D>();
			pVelocityField->pVelocityFields = new vector<Array3D<Vector3>>();

			if (pGridNode->FirstChildElement("VelocityField")) {
				auto pVelocityFieldNode = pGridNode->FirstChildElement("VelocityField");
				string filename;
				if (pVelocityFieldNode->FirstChildElement("Filename")) {
					filename = pVelocityFieldNode->FirstChildElement("Filename")->GetText();
				}
				else {
					throw std::logic_error("GridLoader::loadVelocityFields invalid velocity field filename");
				}
				if (pVelocityFieldNode->FirstChildElement("timeStep")) {
					pVelocityField->timeStep = atof(pVelocityFieldNode->FirstChildElement("timeStep")->GetText());
				}

				//Load the file onto the precomputed stream

				Vector3 tempVec;
				if (pVelocityFieldNode->FirstChildElement("numFrames")) {
					int numFrames = atoi(pVelocityFieldNode->FirstChildElement("numFrames")->GetText());
					for(int i = 0; i < numFrames; i++) {
						string fullFilename = "Flow logs/3D/Velocity/" + filename + intToStr(i) + ".log";
						unique_ptr<ifstream> precomputedStream(new ifstream(fullFilename.c_str(), ifstream::binary)); // replaced auto_ptr by unique_ptr
						pVelocityField->pVelocityFields->push_back(Array3D<Vector3>(gridDimensions));
						for (int i = 0; i < gridDimensions.x; i++) {
							for (int j = 0; j < gridDimensions.y; j++) {
								for (int k = 0; k < gridDimensions.z; k++) {
									precomputedStream->read(reinterpret_cast<char *>(&tempVec), sizeof(Scalar) * 3);
									pVelocityField->pVelocityFields->back()(i, j, k) = tempVec;
								}
							}
						}
					}
				} else { //Load a single frame then
					string fullFilename = "Flow logs/Velocity/" + filename + intToStr(0) + ".log";
					unique_ptr<ifstream> precomputedStream(new ifstream(fullFilename.c_str(), ifstream::binary)); // replaced auto_ptr by unique_ptr
					pVelocityField->pVelocityFields->push_back(Array3D<Vector3>(gridDimensions));

					for (int i = 0; i < gridDimensions.x; i++) {
						for (int j = 0; j < gridDimensions.y; j++) {
							for (int k = 0; k < gridDimensions.z; k++) {
								precomputedStream->read(reinterpret_cast<char *>(&tempVec), sizeof(Scalar) * 3);
								pVelocityField->pVelocityFields->back()(i, j, k) = tempVec;
							}
						}
					}
				}
				return pVelocityField;
			}
		}

		void GridLoader::loadScalarField(const string &scalarFieldname, Array3D<Scalar> &scalarField) {
			unique_ptr<ifstream> precomputedStream(new ifstream(scalarFieldname.c_str(), ifstream::binary)); // replaced auto_ptr by unique_ptr

			Scalar tempScalar;
			precomputedStream->read(reinterpret_cast<char*>(&tempScalar), sizeof(Scalar));		//Scalar
			precomputedStream->read(reinterpret_cast<char*>(&tempScalar), sizeof(Scalar));		//Scalar
			precomputedStream->read(reinterpret_cast<char*>(&tempScalar), sizeof(Scalar));		//Scalar

			for (int i = 0; i < scalarField.getDimensions().x; i++) {
				for (int j = 0; j < scalarField.getDimensions().y; j++) {
					for (int k = 0; k < scalarField.getDimensions().z; k++) {
						precomputedStream->read(reinterpret_cast<char *>(&tempScalar), sizeof(Scalar));
						scalarField(i, j, k) = tempScalar;
					}
				}
			}
		}

		#pragma endregion

		template shared_ptr<LevelSetGridMesh<IntersectedLineMesh<typename QuadGridType::VectorType>, QuadGridType>> GridLoader::loadLevelSetGrid<IntersectedLineMesh<typename QuadGridType::VectorType>, QuadGridType>(shared_ptr<TiXmlElement> pGridNode, shared_ptr<GridMesh<QuadGridType>> pGrid);

		template shared_ptr<QuadGridMesh<QuadGridType>> GridLoader::loadQuadGrid<QuadGridType>(shared_ptr<TiXmlElement> pGridNode, TiXmlElement *pObstacleNode);
		template shared_ptr<GridMesh<QuadGridType>> GridLoader::loadGridMesh<QuadGridType>(shared_ptr<TiXmlElement> pGridNode, TiXmlElement* pObstacleNode);
	}
}
