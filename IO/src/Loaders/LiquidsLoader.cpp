#include "Loaders/LiquidsLoader.h"

#include "igl/readOBJ.h"

namespace Chimera {

	using namespace Resources;
	using namespace Solvers;
	using namespace Solids;

	namespace Loaders {

		//template <class VectorT, template <class> class VoxelType, template <class> class ElementType>
		//void loadLiquidObjectsAnyDim(shared_ptr<TiXmlElement>pLiquidsNode, typename LiquidRepresentation<VectorT, VoxelType, ElementType>::params_t *pLiquidParams, shared_ptr<TiXmlElement>pLiquidObjectNode) {
		//	/*if (auto pLevelSetGridSubdivisions = pLiquidsNode->FirstChildElement("LevelSetGridSubdivisions")) {
		//		pLiquidParams->levelSetGridSubdivisions = atof(pLevelSetGridSubdivisions->GetText());
		//	}
		//	if (auto pLevelSetKernelSize = pLiquidsNode->FirstChildElement("LevelSetKernelSize")) {
		//		pLiquidParams->levelSetGridSubdivisions = atof(pLevelSetKernelSize->GetText());
		//	}*/
		//	if (pLiquidsNode->FirstChildElement("Sampler")) {
		//		/*auto pParticelsPerCell = pLiquidsNode->FirstChildElement("Sampler")->FirstChildElement("ParticlesPerCell");
		//		if (pParticelsPerCell) {
		//			pLiquidParams->particlesPerCell = atoi(pParticelsPerCell->GetText());
		//		}
		//		auto pSampler = pLiquidsNode->FirstChildElement("Sampler")->FirstChildElement("Type");
		//		if (pSampler) {
		//			pLiquidParams->samplingMethod = XMLParamsLoader::getInstance()->loadSampler(pSampler);
		//		}
		//		auto pResampleParticles = pLiquidsNode->FirstChildElement("Sampler")->FirstChildElement("ResampleParticles");
		//		if (pResampleParticles) {
		//			pLiquidParams->resampleParticles = XMLParamsLoader::getInstance()->loadTrueOrFalse(pResampleParticles);
		//		}*/
		//	}
		//	else {
		//		throw(std::logic_error("loadLiquidObjects: Sampler node not found!"));
		//	}
		//	if (pLiquidObjectNode == nullptr) {
		//		throw(std::logic_error("loadLiquidObjects: LiquidObject node not found!"));
		//	}
		//}

		//template<>
		//LiquidRepresentation<Vector2, QuadCell, Edge>::params_t * LiquidsLoader::loadLiquidObjects<Vector2, QuadCell, Edge>(shared_ptr<TiXmlElement>pLiquidsNode, const dimensions_t &gridDimensions, Scalar dx) {
		//	LiquidRepresentation<Vector2, QuadCell, Edge>::params_t *pLiquidParams = new LiquidRepresentation<Vector2, QuadCell, Edge>::params_t();
		//	auto pLiquidObjectNode = pLiquidsNode->FirstChildElement("LiquidObject");
		//	loadLiquidObjectsAnyDim<Vector2, QuadCell, Edge>(pLiquidsNode, pLiquidParams, shared_ptr<TiXmlElement>(pLiquidObjectNode));
		//	if (pLiquidObjectNode == nullptr) {
		//		throw(std::logic_error("loadLiquidObjects: LiquidObject node not found!"));
		//	}
		//	while (pLiquidObjectNode != nullptr) {
		//		shared_ptr<LineMesh<Vector2>> pLineMesh = MeshLoader::getInstance()->loadLineMesh<Vector2>(shared_ptr<TiXmlElement>(pLiquidObjectNode));
		//		//pLiquidParams->initialLineMeshes.push_back(pLineMesh);
		//		pLiquidObjectNode = pLiquidObjectNode->NextSiblingElement("LiquidObject");
		//	}
		//	return pLiquidParams;
		//}

		//template<>
		//LiquidRepresentation<Vector3, HexaVolume, Cell>::params_t * LiquidsLoader::loadLiquidObjects<Vector3, HexaVolume, Cell>(shared_ptr<TiXmlElement>pLiquidsNode, const dimensions_t &gridDimensions, Scalar dx) {
		//	LiquidRepresentation<Vector3, HexaVolume, Cell>::params_t *pLiquidParams = new LiquidRepresentation<Vector3, HexaVolume, Cell>::params_t();
		//	auto pLiquidObjectNode = pLiquidsNode->FirstChildElement("LiquidObject");
		//	loadLiquidObjectsAnyDim<Vector3, HexaVolume, Cell>(pLiquidsNode, pLiquidParams, shared_ptr<TiXmlElement>(pLiquidObjectNode));
		//	//pLiquidParams->initial3dObjects.clear();
		//	if (pLiquidObjectNode == nullptr) {
		//		throw(std::logic_error("loadLiquidObjects: LiquidObject node not found!"));
		//	}
		//	while (pLiquidObjectNode != nullptr) {
		//		auto pMeshNode = pLiquidObjectNode->FirstChildElement("Mesh");
		//		if (pMeshNode) {
		//			Eigen::RowVector3d scale, position;
		//			const string objMeshFileFolder = "Geometry/3D/";
		//			const string objMeshFileName = pMeshNode->FirstChildElement("Filename")->GetText();
		//			const string objMeshFilePath = objMeshFileFolder + objMeshFileName;

		//			auto pScaleNode = pMeshNode->FirstChildElement("Scale");
		//			if (pScaleNode) {
		//				pScaleNode->QueryDoubleAttribute("x", &scale.x());
		//				pScaleNode->QueryDoubleAttribute("y", &scale.y());
		//				pScaleNode->QueryDoubleAttribute("z", &scale.z());
		//			}
		//			auto pPositionNode = pMeshNode->FirstChildElement("position");
		//			if (pPositionNode) {
		//				pPositionNode->QueryDoubleAttribute("x", &position.x());
		//				pPositionNode->QueryDoubleAttribute("y", &position.y());
		//				pPositionNode->QueryDoubleAttribute("z", &position.z());
		//			}

		//			Eigen::MatrixX3i F; // triangles
		//			Eigen::MatrixX3d V; // vertices
		//			igl::readOBJ(objMeshFilePath, V, F);
		//			V = V / (2 * V.maxCoeff()); // normalize size of object (maximal extension = 1.0)
		//			V = V * scale.asDiagonal(); // apply scale
		//			V.rowwise() += position; // apply translation
		//			//pLiquidParams->initial3dObjects.push_back(new pair<Eigen::MatrixX3d, Eigen::MatrixX3i>(V, F));
		//		}

		//		//pLiquidParams->initialLineMeshes.push_back(MeshLoader::getInstance()->loadLineMesh<Vector3>(pLiquidObjectNode, gridDimensions, dx));
		//		//vector<PolygonalMesh<Vector3>*> polyMeshes = MeshLoader::getInstance()->loadPolyMeshes<Vector3>(pLiquidObjectNode, gridDimensions, dx);
		//		// TODO: export meshes into pLiquidParams
		//		pLiquidObjectNode = pLiquidObjectNode->NextSiblingElement("LiquidObject");
		//	}
		//	return pLiquidParams;
		//}

	}
}
