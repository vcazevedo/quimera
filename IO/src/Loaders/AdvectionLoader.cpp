#include "Loaders/AdvectionLoader.h"
#include "Loaders/XMLParamsLoader.h"
#include "Loaders/IntegratorLoader.h"
#include <filesystem>

namespace Chimera {
	namespace IO {

		#pragma region LoadingFunctions
		
		template <typename GridMeshType>
		shared_ptr<AdvectionBase> AdvectionLoader::loadAdvection(shared_ptr<TiXmlElement> pAdvectionNode, shared_ptr<GridMesh<GridMeshType>> pGridMesh, shared_ptr<Interpolant<typename GridMeshType::VectorType, GridMeshType>> pInterpolant) {
			AdvectionBase::baseParams_t* pAdvectionParams = loadAdvectionParams(pAdvectionNode);
			shared_ptr<AdvectionBase> pAdvectionBase;
			if (pAdvectionParams->advectionCategory == LagrangianAdvection) {
			//	ParticleBasedAdvection<VectorT, ArrayType>::params_t* pPbaParams = dynamic_cast<ParticleBasedAdvection<VectorT, ArrayType>::params_t*>(m_params.pAdvectionParams);
			//	if (pPbaParams == nullptr) {
			//		throw exception("Invalid custom parameters configuration for initializing advection class.");
			//	}
			//	ParticleBasedAdvection<VectorT, ArrayType>* pParticleBasedAdvection = new ParticleBasedAdvection<VectorT, ArrayType>(pPbaParams, m_pVelocityInterpolant, m_pGrid->getGridData());
			//	pParticleBasedAdvection->getParticlesSampler()->setBoundaryConditions(m_boundaryConditions);
			//	return pParticleBasedAdvection;
			//}
			//else if (m_params.pAdvectionParams->advectionCategory == EulerianAdvection) {
			//	PositionIntegrator<VectorT, ArrayType>* pPositionIntegrator = nullptr;
			//	Scalar dx = m_pGrid->getGridData()->getGridSpacing();
			//	switch (m_params.pAdvectionParams->integrationMethod)
			//	{
			//	case forwardEuler:
			//		pPositionIntegrator = new ForwardEulerIntegrator<VectorT, ArrayType>(nullptr, m_pGrid->getGridData(), m_pVelocityInterpolant, dx);
			//		break;

			//	case RungeKutta_2:
			//		pPositionIntegrator = new RungeKutta2Integrator<VectorT, ArrayType>(nullptr, m_pGrid->getGridData(), m_pVelocityInterpolant, dx);
			//		break;
			} else if (pAdvectionParams->advectionCategory == EulerianAdvection) {
				m_pIntegratorNode = shared_ptr<TiXmlElement>(pAdvectionNode->FirstChildElement("PositionIntegration"));
				auto pIntegrator = IntegratorLoader::getInstance()->loadIntegrator(m_pIntegratorNode, pGridMesh, pInterpolant);
				if (pAdvectionParams->gridBasedAdvectionMethod == SemiLagrangian) {
					pAdvectionBase = make_shared<SemiLagrangianAdvection<GridMeshType>>(*pAdvectionParams, pGridMesh, pIntegrator, pInterpolant);
				}
				else if (pAdvectionParams->gridBasedAdvectionMethod == MacCormack) {
					pAdvectionBase = make_shared<MacCormackAdvection<GridMeshType>>(*pAdvectionParams, pGridMesh, pIntegrator, pInterpolant);
				}
			}

			return pAdvectionBase;
		}

		template shared_ptr<AdvectionBase> AdvectionLoader::loadAdvection<QuadGridType>(shared_ptr<TiXmlElement> pAdvectionNode, shared_ptr<GridMesh<QuadGridType>> pGridMesh, shared_ptr<Interpolant<typename QuadGridType::VectorType, QuadGridType>> pInterpolant);
		#pragma endregion
		#pragma region InternalLoadingFunctions
		AdvectionBase::baseParams_t * AdvectionLoader::loadAdvectionParams(shared_ptr<TiXmlElement> pAdvectionNode) {
			AdvectionBase::baseParams_t * pAdvParams = nullptr;

			auto pTempNode = pAdvectionNode->FirstChildElement();
			while (pTempNode) {
				string advectionType(pTempNode->Value());
				transform(advectionType.begin(), advectionType.end(), advectionType.begin(), ::tolower);
				if (advectionType == "particlebasedadvection") {
					/*typename ParticleBasedAdvection<VectorType, VoxelType, MeshType>::params_t *pPBAParams = new typename ParticleBasedAdvection<VectorType, VoxelType, MeshType>::params_t();
					pPBAParams->advectionCategory = LagrangianAdvection;
					if (pTempNode->FirstChildElement("PositionIntegration")) {
						pPBAParams->integrationMethod = XMLParamsLoader::getInstance()->loadIntegrationMethodParams(pTempNode->FirstChildElement("PositionIntegration"));

						shared_ptr<TiXmlElement>pInterpolationNode = pTempNode->FirstChildElement("PositionIntegration")->FirstChildElement("Interpolant");
						if (pInterpolationNode) {
							pPBAParams->positionIntegrationInterpolation = XMLParamsLoader::getInstance()->loadInterpolation(pInterpolationNode);
						}
						else {
							pPBAParams->positionIntegrationInterpolation = Linear;
						}
					}

					if (pTempNode->FirstChildElement("GridToParticle")) {
						shared_ptr<TiXmlElement>pMethodNode = pTempNode->FirstChildElement("GridToParticle")->FirstChildElement("Method");
						if (pMethodNode) {
							string gridToParticleType = pMethodNode->GetText();
							transform(gridToParticleType.begin(), gridToParticleType.end(), gridToParticleType.begin(), ::tolower);
							if (gridToParticleType == "flip") {
								pPBAParams->gridToParticleTransferMethod = ParticleBasedAdvection<VectorType, VoxelType, MeshType>::params_t::gridToParticle_t::FLIP;
								Scalar previousVal = pPBAParams->mixFLIP;
								if (pMethodNode->QueryFloatAttribute("mixPIC", &pPBAParams->mixFLIP) == TIXML_NO_ATTRIBUTE) {
									pPBAParams->mixFLIP = previousVal;
								}
							}
							else if (gridToParticleType == "pic") {
								pPBAParams->gridToParticleTransferMethod = ParticleBasedAdvection<VectorType, VoxelType, MeshType>::params_t::gridToParticle_t::PIC;
							}
							else if (gridToParticleType == "apic") {
								pPBAParams->gridToParticleTransferMethod = ParticleBasedAdvection<VectorType, ArrayType>::params_t::gridToParticle_t::APIC;
							}
							else if (gridToParticleType == "rpic") {
								pPBAParams->gridToParticleTransferMethod = ParticleBasedAdvection<VectorType, ArrayType>::params_t::gridToParticle_t::RPIC;
							}
						}
					}

					if (pTempNode->FirstChildElement("ParticleToGrid")) {
						shared_ptr<TiXmlElement>pGridNode = pTempNode->FirstChildElement("ParticleToGrid")->FirstChildElement("GridArrangement");
						if (pGridNode) {
							pPBAParams->gridArrangement = XMLParamsLoader::getInstance()->loadGridArrangement(pGridNode);
						}

						shared_ptr<TiXmlElement>pKernel = pTempNode->FirstChildElement("ParticleToGrid")->FirstChildElement("Kernel");
						if (pKernel) {
							pPBAParams->kernelType = XMLParamsLoader::getInstance()->loadKernel(pKernel);
						}

						shared_ptr<TiXmlElement>pKernelDanglingCell = pTempNode->FirstChildElement("ParticleToGrid")->FirstChildElement("KernelDanglingCells");
						if (pKernelDanglingCell) {
							pPBAParams->kernelDanglingCells = XMLParamsLoader::getInstance()->loadKernel(pKernelDanglingCell);
						}
					}
					if (pTempNode->FirstChildElement("Sampler")) {
						shared_ptr<TiXmlElement>pParticelsPerCell = pTempNode->FirstChildElement("Sampler")->FirstChildElement("ParticlesPerCell");
						if (pParticelsPerCell) {
							pPBAParams->particlesPerCell = atoi(pParticelsPerCell->GetText());
						}
						shared_ptr<TiXmlElement>pSampler = pTempNode->FirstChildElement("Sampler")->FirstChildElement("Type");
						if (pSampler) {
							pPBAParams->samplingMethod = XMLParamsLoader::getInstance()->loadSampler(pSampler);
						}
						shared_ptr<TiXmlElement>pResampleParticles = pTempNode->FirstChildElement("Sampler")->FirstChildElement("ResampleParticles");
						if (pResampleParticles) {
							pPBAParams->resampleParticles = XMLParamsLoader::getInstance()->loadTrueOrFalse(pResampleParticles);
						}
					}

					pAdvParams = pPBAParams;*/
				}
				else if (advectionType == "gridbasedadvection") {
					pAdvParams = new AdvectionBase::baseParams_t();
					pAdvParams->advectionCategory = EulerianAdvection;
					auto pMethod = pTempNode->FirstChildElement("Method");
					string methodType(pMethod->GetText());
					transform(methodType.begin(), methodType.end(), methodType.begin(), ::tolower);

					if (methodType == "semilagrangian") {
						pAdvParams->gridBasedAdvectionMethod = SemiLagrangian;
					}
					else if (methodType == "semilagrangiandec") {
						pAdvParams->gridBasedAdvectionMethod = SemiLagrangianDEC;
					}
					else if (methodType == "modifiedmaccormack" || methodType == "maccormack") {
						pAdvParams->gridBasedAdvectionMethod = MacCormack;
					}
				}
				pTempNode = pTempNode->NextSiblingElement();
			}

			if (!pAdvParams) {
				throw (std::logic_error("No supported advection method inside <AdvectionMethod> found!"));
			}
			
			return pAdvParams;
		}
		#pragma endregion
		
	}
}
