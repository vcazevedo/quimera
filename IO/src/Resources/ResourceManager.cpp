//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

/************************************************************************/
/* IO                                                            */
/************************************************************************/
#include "Resources/ResourceManager.h"


namespace Chimera {

	namespace IO {

		/************************************************************************/
		/* ctors and dtors                                                      */
		/************************************************************************/

		ResourceManager::ResourceManager() {
			m_branchPath = "";
		}

		ResourceManager::ResourceManager(const ResourceManager &rhs) {

		}

		ResourceManager & ResourceManager::operator= (const ResourceManager &rhs) {
			return(*this);
		}
		ResourceManager::~ResourceManager() {

		}


		shared_ptr<XMLDoc> ResourceManager::loadXMLDocument(const string &filePath)
		{
			shared_ptr<Resource> pResource = getInstance()->getObject(filePath).lock();
			shared_ptr<XMLDoc> pXML;
			if(pResource == NULL) {
				pXML = shared_ptr<XMLDoc>(new XMLDoc(filePath));

				if(!pXML->loadFile())
					throw std::logic_error(filePath.c_str());

				getInstance()->createObject(filePath, pXML);
			} else {
				pXML = shared_ptr<XMLDoc>(dynamic_pointer_cast<XMLDoc>(pResource));
			}
			return(pXML);
		}

		shared_ptr<Font> ResourceManager::loadFont(const string& filePath)
		{
			shared_ptr<Resource> pResource = getInstance()->getObject(filePath).lock();
			shared_ptr<Font> pFont;
			if (pResource == NULL) {
				pFont = make_shared<Font>(filePath);
				getInstance()->createObject(filePath, pFont);
			}
			else {
				pFont = shared_ptr<Font>(dynamic_pointer_cast<Font>(pResource));
			}
			return(pFont);
		}
	}



}
