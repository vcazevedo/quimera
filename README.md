# Quimera installation instructions

## Windows
For windows users, we need to download and install vckpg. Make sure that once you run the cmake gui, the CMAKE_TOOLCHAIN_FILE correctly points out to the vcpackage installation. The currently shipped CMAKE configuration looks for an environment variable called VCPKG_DIR. This environment variable should point out to your vcpkg base installation on Windows.

## VCPKG configuration
Corrade, Magnum and Magnum Plugins are only known to be working on their static version. So we need to target using a static triplet. This can be changed in cmake's GUI vcpackage by changing the VCPKG-TARGET-TRIPLET to x64-windows-static. Then, from the vcpkg folder, install the following libraries by running
```
./vcpkg.exe install boost:x64-windows-static
./vcpkg.exe install tbb:x64-windows-static
./vcpkg.exe install eigen3:x64-windows-static
./vcpkg.exe install imgui:x64-windows-static
./vcpkg.exe install corrade:x64-windows-static
./vcpkg.exe install magnum:x64-windows-static
./vcpkg.exe install magnum-integration:x64-windows-static
./vcpkg.exe install magnum-integration[imgui]:x64-windows-static --recurse
./vcpkg.exe install magnum-plugins[StbTrueTypeFont]:x64-windows-static --recurse
```


