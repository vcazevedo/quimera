//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#include "FlowSolverRenderer.h"

using namespace Magnum::Math::Literals;

namespace Chimera {

	namespace Rendering {

		template <typename SolverType>
		FlowSolverRenderer<SolverType>::FlowSolverRenderer(shared_ptr<FlowSolver<SolverType>> pFlowSolver, const Magnum::Platform::Application::Arguments& arguments) :
			Magnum::Platform::Application{ arguments, Magnum::NoCreate }, m_pFlowSolver(pFlowSolver), m_runSimulation(false), m_pMeshesRenderer(nullptr) {

			// Setup Window Creation
				{
					const Magnum::Vector2 dpiScaling = this->dpiScaling({});
					Configuration windowConfig = Configuration();
					windowConfig.setTitle("Chimera");
					windowConfig.setSize(windowConfig.size(), dpiScaling);
					windowConfig.addWindowFlags(Configuration::WindowFlag::Maximized);
					windowConfig.addWindowFlags(Configuration::WindowFlag::Resizable);
					
					GLConfiguration glConf;
					glConf.setSampleCount(dpiScaling.max() < 2.0f ? 8 : 2);
					glConf.setStencilBufferSize(8);
					if (!tryCreate(windowConfig, glConf)) {
						create(windowConfig, glConf.setSampleCount(0));
					}
				}

				/** Setup ImGui, load a better font */
				{
					ImGui::CreateContext();
					ImGui::StyleColorsDark();

					m_imgui = Magnum::ImGuiIntegration::Context{ *ImGui::GetCurrentContext(),
																												Magnum::Vector2{windowSize()} / dpiScaling(), windowSize(), framebufferSize() };

					/* Setup proper blending to be used by ImGui */
					Magnum::GL::Renderer::setBlendFunction(
						Magnum::GL::Renderer::BlendFunction::SourceAlpha,
						Magnum::GL::Renderer::BlendFunction::OneMinusSourceAlpha);
				}

				/** Configure SceneGraph and Camera */
				{
					gridCentroid.x() = m_pFlowSolver->getSimulationGrid()->getGridCentroid().x;
					gridCentroid.y() = m_pFlowSolver->getSimulationGrid()->getGridCentroid().y;

					m_pScene = make_shared<Magnum::Scene2D>();
					m_pObjCamera = make_shared<Magnum::Object2D>(m_pScene.get());
					m_pObjCamera->setTransformation(Magnum::Matrix3::translation(gridCentroid));
					m_pCamera = make_shared<Magnum::SceneGraph::Camera2D>(*m_pObjCamera.get());
				}

				/** Camera and Input Configuration*/
				{
					
					m_zDistance = 2;
					float boundsX = m_pFlowSolver->getSimulationGrid()->getBounds().second.x - m_pFlowSolver->getSimulationGrid()->getBounds().first.x;
					float boundsY = m_pFlowSolver->getSimulationGrid()->getBounds().second.y - m_pFlowSolver->getSimulationGrid()->getBounds().first.y;
					m_minZDistance = m_pFlowSolver->getSimulationGrid()->getGridSpacing()*2;
					m_maxZDistance = max(boundsX * 1.5, boundsY * 1.5);
					m_mouseMaxSensivity = 0.0625;
					m_mouseSensitivity = 0.0375;
					m_mouseMinSensivity = 0.00001;
				}

				const Magnum::Vector2 domainDisplaySize{ m_zDistance, m_zDistance };

				m_pCamera->setAspectRatioPolicy(Magnum::SceneGraph::AspectRatioPolicy::Extend)
					.setProjectionMatrix(Magnum::Matrix3::projection(domainDisplaySize))
					.setViewport(Magnum::GL::defaultFramebuffer.viewport().size());

				// Configure update rate and VSync
				{
					#if !defined(MAGNUM_TARGET_WEBGL) && !defined(CORRADE_TARGET_ANDROID)
					/* Have some sane speed, please */
					Magnum::Platform::Application::setMinimalLoopPeriod(16);
					Magnum::Platform::Application::setSwapInterval(1);
					#endif
				}


				/** Rendering options */
				{
					Magnum::GL::Renderer::setLineWidth(1.0f);
					Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::ProgramPointSize);
				}

				/** Setup sub-renderers */
				{
					m_pObject = make_shared<Object2D>(m_pScene.get());
					m_pDrawableGroup = make_shared<Magnum::SceneGraph::DrawableGroup2D>();

					
					auto pDrawableGridMeshSimulation = make_shared<SelectableDrawableMesh2D<GridMeshType>>(m_pFlowSolver->getSimulationGrid(), m_pObject, m_pDrawableGroup);

					m_verticesRenderers["simulation"] = make_shared<VerticesRenderer<GridMeshType>>("simulation", pDrawableGridMeshSimulation, m_pScene, m_pCamera);
					m_edgesRenderers["simulation"] = make_shared<EdgesRenderer<GridMeshType>>("simulation", pDrawableGridMeshSimulation, m_pScene, m_pCamera);

					m_gridRenderers["simulation"] = make_shared<GridRenderer<GridMeshType>>("simulation", pDrawableGridMeshSimulation, m_pScene, m_pCamera);
					m_vectorFieldRenderers["simulation"] = make_shared<VectorFieldRenderer<GridMeshType>>("simulation", m_pFlowSolver->getSimulationGrid(), m_pScene, m_pCamera);
					m_scalarFieldRenderers["simulation"] = make_shared<ScalarFieldRenderer<GridMeshType>>("simulation", m_pFlowSolver->getSimulationGrid(), m_pScene, m_pCamera);
					// m_pParticlesRenderer = make_unique<ParticlesRenderer<VectorType>>(m_pParticlesData, renderingParams, m_pFlowSolver->getSimulationGrid()->getGridSpacing(), m_pScene, m_pCamera, m_pObjCamera);

					//Setup rendering of other grids
					{
						auto levelSets = m_pFlowSolver->getLevelSets();
						for (auto levelSetIter : levelSets) {
							auto pDrawableQuadGridMesh = make_shared<DrawableMesh2D<GridMeshType>>(levelSetIter.second->getGrid(), m_pObject, m_pDrawableGroup);
							//m_gridRenderers[levelSetIter.first] = make_shared<GridRenderer<GridMeshType>>(levelSetIter.first, pDrawableQuadGridMesh, m_pScene, m_pCamera);
							//m_vectorFieldRenderers[levelSetIter.first] = make_shared<VectorFieldRenderer<GridMeshType>>(levelSetIter.second->getGrid(), m_pScene, m_pCamera);
							//m_scalarFieldRenderers[levelSetIter.first] = make_shared<ScalarFieldRenderer<GridMeshType>>(levelSetIter.first, levelSetIter.second->getGrid(), m_pScene, m_pCamera);
						}
					}

					/* Compute maximum bounds to define the camera distance to the grid*/
					VectorType bounds = m_pFlowSolver->getSimulationGrid()->getBounds().second - m_pFlowSolver->getSimulationGrid()->getBounds().first;
					m_zDistance = bounds.x > bounds.y ? bounds.x : bounds.y;
					m_zDistance *= 1.05; // Set the camera a bit further away

				}

		}

		template<typename SolverType>
		void FlowSolverRenderer<SolverType>::drawEvent() {
			Magnum::GL::defaultFramebuffer.clear(Magnum::GL::FramebufferClear::Color | Magnum::GL::FramebufferClear::Depth | Magnum::GL::FramebufferClear::Stencil);
			// Magnum::GL::defaultFramebuffer.clearColor(Magnum::Color4(1.0, 1.0, 1.0, 1.0)); /* White background */

		

			/** Process Inputs */
			{

			}

			/** Draw objects */
			{
				const Magnum::Vector2 domainDisplaySize{ m_zDistance, m_zDistance };
				m_pCamera->setAspectRatioPolicy(Magnum::SceneGraph::AspectRatioPolicy::Extend)
					.setProjectionMatrix(Magnum::Matrix3::projection(domainDisplaySize))
					.setViewport(Magnum::GL::defaultFramebuffer.viewport().size());

				/* Draw other objects (ground grid) */
				m_pCamera->draw(*m_pDrawableGroup);


				for (auto pVertexRenderer : m_verticesRenderers) {
					pVertexRenderer.second->draw();
				}

				for (auto pEdgeRenderer : m_edgesRenderers) {
					pEdgeRenderer.second->draw();
				}

				for (auto pVectorFieldRenderer : m_vectorFieldRenderers) {
					pVectorFieldRenderer.second->draw();
				}
				for (auto pGridRenderer : m_gridRenderers) {
					pGridRenderer.second->draw();
				}
				for (auto pScalarFieldRenderer : m_scalarFieldRenderers) {
					pScalarFieldRenderer.second->draw();
				}
				if (m_pMeshesRenderer) {
					//m_pMeshesRenderer->draw();
				}
				// m_pParticlesRenderer->draw();

			}

			Magnum::GL::Renderer::setBlendEquation(Magnum::GL::Renderer::BlendEquation::Add,
				Magnum::GL::Renderer::BlendEquation::Add);
			Magnum::GL::Renderer::setBlendFunction(Magnum::GL::Renderer::BlendFunction::SourceAlpha,
				Magnum::GL::Renderer::BlendFunction::OneMinusSourceAlpha);

			m_imgui.newFrame();

			/** Enable text input, if needed */
			if (ImGui::GetIO().WantTextInput && !isTextInputActive())
				startTextInput();
			else if (!ImGui::GetIO().WantTextInput && isTextInputActive())
				stopTextInput();

			/** Update application cursor */
			drawMenu();
			m_imgui.updateApplicationCursor(*this);

			/* Set appropriate states. If you only draw ImGui, it is sufficient to
				 just enable blending and scissor test in the constructor. */
			Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::Blending);
			Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::ScissorTest);
			Magnum::GL::Renderer::disable(Magnum::GL::Renderer::Feature::FaceCulling);
			Magnum::GL::Renderer::disable(Magnum::GL::Renderer::Feature::DepthTest);

			m_imgui.drawFrame();

			/* Reset state. Only needed if you want to draw something else with
				 different state after. */
			Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::DepthTest);
			Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::FaceCulling);
			Magnum::GL::Renderer::disable(Magnum::GL::Renderer::Feature::ScissorTest);
			Magnum::GL::Renderer::disable(Magnum::GL::Renderer::Feature::Blending);

			swapBuffers();
			redraw();
		}

		template <typename SolverType>
		void FlowSolverRenderer<SolverType>::drawMenu() {

			ImGui::Begin("Visualization");
			ImGui::SetNextTreeNodeOpen(true);
			if (ImGui::CollapsingHeader("Simulation")) {
				m_vectorFieldRenderers["simulation"]->drawMenu();
				m_scalarFieldRenderers["simulation"]->drawMenu();

				m_verticesRenderers["simulation"]->drawMenu();
				m_edgesRenderers["simulation"]->drawMenu();
				m_gridRenderers["simulation"]->drawMenu();
			}

			auto levelSets = m_pFlowSolver->getLevelSets();
			for (auto levelSetIter : levelSets) {
				string headerName = levelSetIter.first;
				headerName[0] = toupper(headerName[0]);
				if (ImGui::CollapsingHeader(headerName.c_str())) {
					m_gridRenderers[levelSetIter.first]->drawMenu();
					m_scalarFieldRenderers[levelSetIter.first]->drawMenu();
				}
			}
			ImGui::End();

			ImGui::Begin("Simulation Control");
			if (ImGui::Button("Step simulation")) {
				update();
			}

			if (ImGui::Checkbox("Run Simulation", &m_runSimulation)) {

			}

			if (ImGui::DragFloat("Time step", &m_timeStep, 0, 1)) {

			}

			if (m_runSimulation)
			{
				update();
			}

			ImGui::End();
		}

		template <typename SolverType>
		void FlowSolverRenderer<SolverType>::update() {
			for (auto pVectorFieldRenderer : m_vectorFieldRenderers) {
				pVectorFieldRenderer.second->update();
			}
			for (auto pScalarFieldRenderer : m_scalarFieldRenderers) {
				pScalarFieldRenderer.second->update();
			}
		}

		template<typename SolverType>
		void FlowSolverRenderer<SolverType>::viewportEvent(ViewportEvent& event) {
			Magnum::GL::defaultFramebuffer.setViewport({ {}, event.framebufferSize() });

			m_imgui.relayout(Magnum::Vector2{ event.windowSize() } / event.dpiScaling(),
				event.windowSize(), event.framebufferSize());

			m_pCamera->setViewport(event.framebufferSize());
		}

		template<typename SolverType>
		void FlowSolverRenderer<SolverType>::keyPressEvent(KeyEvent& event) {
			if (m_imgui.handleKeyPressEvent(event)) return;
			switch (event.key()) {
				/** Resets camera position */
			case KeyEvent::Key::R:
				// m_zDistance = 2;

				VectorType bounds = m_pFlowSolver->getSimulationGrid()->getBounds().second - m_pFlowSolver->getSimulationGrid()->getBounds().first;
				m_zDistance = bounds.x > bounds.y ? bounds.x : bounds.y;
				m_zDistance *= 1.05; //Set the camera a bit further away

				m_pObjCamera->resetTransformation();
				break;
			}
		}

		template<typename SolverType>
		void FlowSolverRenderer<SolverType>::keyReleaseEvent(KeyEvent& event) {
			if (m_imgui.handleKeyReleaseEvent(event)) return;
		}

		template<typename SolverType>
		void FlowSolverRenderer<SolverType>::mousePressEvent(MouseEvent& event) {
			if (m_imgui.handleMousePressEvent(event)) return;
			if (!m_verticesRenderers["simulation"]->mousePressEvent(event, windowSize())) {
				if (!m_edgesRenderers["simulation"]->mousePressEvent(event, windowSize())) {
					if (m_gridRenderers["simulation"]->mousePressEvent(event, windowSize())) {
						m_verticesRenderers["simulation"]->setSelectedVertex(-1);
						m_edgesRenderers["simulation"]->setSelectedEdge(-1);
					}
				}
				else {
					m_verticesRenderers["simulation"]->setSelectedVertex(-1);
					m_gridRenderers["simulation"]->setSelectedCell(-1);
				}
			}
			else {
				m_edgesRenderers["simulation"]->setSelectedEdge(-1);
				m_gridRenderers["simulation"]->setSelectedCell(-1);
			}
		}

		template<typename SolverType>
		void FlowSolverRenderer<SolverType>::mouseReleaseEvent(MouseEvent& event) {
			if (m_imgui.handleMouseReleaseEvent(event)) return;
		}

		template<typename SolverType>
		void FlowSolverRenderer<SolverType>::mouseMoveEvent(MouseMoveEvent& event) {
			if (m_imgui.handleMouseMoveEvent(event)) return;

			auto mouseSensitivity = Magnum::Math::clamp<Magnum::Float>(m_mouseSensitivity * m_zDistance * m_zDistance, m_mouseMinSensivity, m_mouseMaxSensivity);

			/** Middle button panning */
			if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_MIDDLE)) {
				const Magnum::Vector2 translationVec(-event.relativePosition().x() * mouseSensitivity*0.125, event.relativePosition().y() * mouseSensitivity * 0.125);
				m_pObjCamera->translateLocal(translationVec);
			}

			/** Right button zooms */
			if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT)) {
				m_zDistance += event.relativePosition().y() * mouseSensitivity;
				m_zDistance = Magnum::Math::clamp<Magnum::Float>(m_zDistance, m_minZDistance, m_maxZDistance);

			}
		}

		template<typename SolverType>
		void FlowSolverRenderer<SolverType>::mouseScrollEvent(MouseScrollEvent& event) {
			auto mouseSensitivity = Magnum::Math::clamp<Magnum::Float>(m_mouseSensitivity * m_zDistance * m_zDistance, m_mouseMinSensivity, m_mouseMaxSensivity);

			if (m_imgui.handleMouseScrollEvent(event)) {
				/* Prevent scrolling the page */
				event.setAccepted();
				return;
			}

			m_zDistance -= event.offset().y() * m_mouseSensitivity;
			m_zDistance = Magnum::Math::clamp<Magnum::Float>(m_zDistance, 0, 100); // (m_zDistance, 0, 10);
		}

		template<typename SolverType>
		void FlowSolverRenderer<SolverType>::textInputEvent(TextInputEvent& event) {
			if (m_imgui.handleTextInputEvent(event)) return;
		}

		template class FlowSolverRenderer<FlowSolverType2D>;
	}
}
