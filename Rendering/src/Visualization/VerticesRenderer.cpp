//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.


#include "Visualization/VerticesRenderer.h"
#include "Corrade/Containers/PointerStl.h"

namespace Chimera {

	namespace Rendering {

		#pragma region Constructors

		template <typename GridMeshType>
		VerticesRenderer<GridMeshType>::VerticesRenderer(const string& verticesRendererName, shared_ptr<SelectableDrawableMesh2D<GridMeshType>> pDrawableMesh, shared_ptr<Magnum::Scene2D> pScene, shared_ptr<Magnum::SceneGraph::Camera2D> pCamera)
			: m_verticesRendererName(verticesRendererName), m_pCamera(pCamera), m_pScene(pScene), m_pDrawableMesh(pDrawableMesh) {
			m_pObject = make_shared<Magnum::Object2D>(m_pScene.get());
			m_selectedVertex = -1;
			m_drawSelectedVertex = m_drawVertices = m_drawVerticesIDs = false;
			m_pDrawableGroup = make_shared<Magnum::SceneGraph::DrawableGroup2D>();
			m_pDrawableGroupLabels = make_shared<Magnum::SceneGraph::DrawableGroup2D>();
			m_isSolid = false;
			m_hasDualElement = false;

			Scalar offset = 1e-2;
			for (auto pVertex : pDrawableMesh->getMesh()->getVertices()) {
				Magnum::Vector2 vertexPosition(pVertex->getPosition().x - offset, pVertex->getPosition().y + offset);
				m_verticesIDsText[pVertex->getID()] = make_shared<DrawableText2D>(to_string(pVertex->getID()),
																																					vertexPosition,
																																					m_pScene, m_pDrawableGroupLabels);
			}
		}
		#pragma endregion

		#pragma region Functionalities
		template <typename GridMeshType>
		void VerticesRenderer<GridMeshType>::draw() {
			if (m_drawVertices) {
				(static_cast<DrawableMesh2D<GridMeshType> *>(m_pDrawableMesh.get()))->addDrawingMode(drawingMeshMode_t::drawVertices);
			}
			else {
				(static_cast<DrawableMesh2D<GridMeshType>*>(m_pDrawableMesh.get()))->removeDrawingMode(drawingMeshMode_t::drawVertices);
			}
			if (m_selectedVertex != -1 && m_drawSelectedVertex) {
				m_pDrawableMesh->addDrawingMode(selectedDrawingMode_t::drawSelectedVertex);
			}
			else {
				m_pDrawableMesh->removeDrawingMode(selectedDrawingMode_t::drawSelectedVertex);
			}


			m_pCamera->draw(*m_pDrawableGroup);

			if (m_drawVerticesIDs) {
				m_pCamera->draw(*m_pDrawableGroupLabels);
			}
		}
		
		template <typename GridMeshType>
		void VerticesRenderer<GridMeshType>::drawMenu() {
			ImGui::SetNextTreeNodeOpen(true);
			if (ImGui::CollapsingHeader("Vertex Visualization")) {

				ImGui::BeginChild("Vertices Debug Information", ImVec2(0, 50), true);
				ImGui::Text("Vertices Debug Information");
				ImGui::Text(string("Selected Vertex id " + to_string(m_selectedVertex)).c_str());
				ImGui::Text(string("Is selected Vertex solid: " + to_string(m_isSolid)).c_str());
				ImGui::Text(string("Has dual element: " + to_string(m_hasDualElement)).c_str());
				ImGui::Text(string("auxVorticity: " + to_string(m_auxVorticity)).c_str());
				ImGui::Text(string("streamfunction: " + to_string(m_streamfunction)).c_str());
				ImGui::Text(string("vorticity: " + to_string(m_vorticity)).c_str());
				ImGui::EndChild();

				ImGui::BeginChild("Vertices Drawing Options", ImVec2(0, 100), true);
				ImGui::Text("Drawing Options");
				ImGui::Checkbox(string("Draw Vertices##" + m_verticesRendererName).c_str(), &m_drawVertices);
				ImGui::Checkbox(string("Draw Vertices IDs##" + m_verticesRendererName).c_str(), &m_drawVerticesIDs);
				ImGui::Checkbox(string("Draw Selected Vertex##" + m_verticesRendererName).c_str(), &m_drawSelectedVertex);
				ImGui::EndChild();

				if (ImGui::TreeNode("Misc")) {
					ImGui::DragFloat(string("Vertex Size##" + m_verticesRendererName).c_str(), &m_pDrawableMesh->getVertexSize(), 0.1, 0, 30);
					ImGui::TreePop();
				}
			}
			
		}

		template <typename GridMeshType>
		bool VerticesRenderer<GridMeshType>::mousePressEvent(Magnum::Platform::Sdl2Application::MouseEvent& event, Magnum::Vector2i pWindowSize) {
			if (event.button() == Magnum::Platform::Sdl2Application::MouseEvent::Button::Left) {
				//Computing the position in global coordinates of the mouse event
				const Magnum::Vector2i viewPosition{ event.position().x(), pWindowSize.y() - event.position().y() - 1 };
				const Magnum::Vector2 in{ 2 * Magnum::Vector2(viewPosition) / Magnum::Vector2(pWindowSize) - Magnum::Vector2(1.0f) };
				auto mousePosition = (m_pCamera->object().absoluteTransformationMatrix() * m_pCamera->projectionMatrix().inverted()).transformPoint(in);

				auto pVertex = m_pDrawableMesh->getMesh()->getVertex(Vector2(mousePosition.x(), mousePosition.y()), 7.5e-4*m_pDrawableMesh->getVertexSize());
				if (pVertex) {
					m_pDrawableMesh->setSelectedVertex(pVertex);
					m_selectedVertex = pVertex->getID();
					m_hasDualElement = (pVertex->getDualElement2D() != nullptr);
					m_isSolid = pVertex->isSolid();
					m_auxVorticity = OwnCustomAttribute<Scalar>::get(pVertex)->getAttribute("auxVorticity");
					m_vorticity = OwnCustomAttribute<Scalar>::get(pVertex)->getAttribute("vorticity");
					m_streamfunction = OwnCustomAttribute<Scalar>::get(pVertex)->getAttribute("streamfunction");

					/*	auto pDualCell = pVertex->getDualElement2D();
						if(pDualCell) {
							if (m_dualMeshes.find(pVertex->getID()) == m_dualMeshes.end()) {
								m_dualMeshes[pVertex->getID()] = make_shared<DrawableMesh2D<GridMeshType>>(pDualCell, m_pObject, m_pDrawableGroup, 0xffcc00_rgbf);
							}
							m_dualMeshes[pVertex->getID()]->setDrawingMode(drawingMeshMode_t::drawEdges);
						}

					m_pDrawableMesh->setSelectedVertex(pVertex);*/

					return true;
				}
			}
			return false;
		}
		#pragma endregion

		template class VerticesRenderer<QuadGridType>;
	}
}
