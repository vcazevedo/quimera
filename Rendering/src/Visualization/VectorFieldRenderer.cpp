#include "Visualization/VectorFieldRenderer.h"
#include "Magnum/Math/Color.h"

#include "Interpolation/BilinearNodalInterpolant2D.h"

#include "DrawableObjects/DrawableVectors2D.h"
namespace Chimera {
	namespace Rendering {
		template <typename GridMeshType>
		VectorFieldRenderer<GridMeshType>::VectorFieldRenderer(const string &vectorFieldRendererName, shared_ptr<GridMesh<GridMeshType>> pGrid, shared_ptr<Scene2D> pScene, shared_ptr<Magnum::SceneGraph::Camera2D> pCamera) :
			m_vectorFieldRendererName(vectorFieldRendererName), m_pGridMesh(pGrid), m_pCamera(pCamera), m_pScene(pScene) {
			m_pFlatShader = make_shared<Magnum::Shaders::Flat2D>();
			m_pObject2D = make_shared<Magnum::Object2D>(m_pScene.get());

			m_vectorColor = Magnum::Color3(1.0, 1.0, 1.0);
			if constexpr (isVector2<VectorType>::value) {
				initializeVectors();
			}
			else {
			}

		}

		#pragma region Initialization
		template <typename GridMeshType>
		void VectorFieldRenderer<GridMeshType>::initializeVectors() {
			/* The following is for testing. Remove this later */
			//if constexpr(isQuadGridType<GridMeshType>::value) {
			//	m_pGridMesh->addCellsAttribute<VectorType, Interpolation::BilinearNodalInterpolant2D<Scalar, GridMeshType>>("blah", 1.0f);
			//	m_pGridMesh->addVerticesAttribute<VectorType, Interpolation::BilinearNodalInterpolant2D<VectorType, GridMeshType>>("blahblahblah", -5.0f);
			//	for(auto &edge : m_pGridMesh->getEdges()) {
			//		VectorType &vel = edge->OwnCustomAttribute<VectorType>::getAttribute("velocity");
			//		vel.x = 2.0; vel.y = 1.0;
			//		VectorType &auxVel = edge->OwnCustomAttribute<VectorType>::getAttribute("auxVelocity");
			//		auxVel.x = -1.0; auxVel.y = 2.0;
			//	}
			//}

			int accOffset = 0;
			initializeVertexVectorFields();
			m_nodalVectorFields.m_offset = accOffset;
			accOffset += m_nodalVectorFields.m_idToName.size();

			initializeEdgeVectorFields();
			m_edgeVectorFields.m_offset = accOffset;
			accOffset += m_edgeVectorFields.m_idToName.size();

			initializeFaceVectorFields();
			m_faceVectorFields.m_offset = accOffset;
			accOffset += m_faceVectorFields.m_idToName.size();
		}

		template <typename GridMeshType>
		void VectorFieldRenderer<GridMeshType>::initializeVertexVectorFields() {

			vector<VectorType> vertexPositions;
			vertexPositions.reserve(m_pGridMesh->getVertices().size());
			for(auto &vertex : m_pGridMesh->getVertices()) {
				vertexPositions.push_back(vertex->getPosition());
			}

			m_vectorFieldNames.push_back("None");

			auto &vertexAttMap = m_pGridMesh->getVertices().front()->OwnCustomAttribute<VectorType>::getAttributesMap();
			for(auto &pair : vertexAttMap) {
				string attName = pair.first;
				uint id = m_nodalVectorFields.m_idToName.size();
				m_nodalVectorFields.m_idToName.push_back(attName);
				m_nodalVectorFields.m_nameToID[attName] = id;
				m_vectorFieldNames.push_back(string("Vertex: ") + attName);

				vector<VectorType> nodalVectors;
				nodalVectors.reserve(m_pGridMesh->getVertices().size());
				for(auto &vertex : m_pGridMesh->getVertices()) {
					nodalVectors.push_back(vertex->OwnCustomAttribute<VectorType>::getAttribute(attName));
				}
				m_nodalVectorFields.m_drawableGroups.push_back(
					make_shared<Magnum::SceneGraph::DrawableGroup2D>()
				);
				m_nodalVectorFields.m_vectorFieldElements.push_back(
					make_unique<DrawableVectors2D<VectorType>>(
						vertexPositions,
						nodalVectors,
						m_vectorScale,
						m_pScene,
						m_pFlatShader,
						m_pObject2D,
						m_vectorColor,
						m_nodalVectorFields.m_drawableGroups[id]
				));
			}
		}

		template <typename GridMeshType>
		void VectorFieldRenderer<GridMeshType>::initializeEdgeVectorFields() {
			vector<VectorType> edgeCentroidPositions;
			edgeCentroidPositions.reserve(m_pGridMesh->getEdges().size());
			for(auto &edge : m_pGridMesh->getEdges()) {
				edgeCentroidPositions.push_back(edge->getCentroid());
			}


			//These are inherently scalars stored on edges
			auto &edgeAttMap = m_pGridMesh->getEdges().front()->OwnCustomAttribute<Scalar>::getAttributesMap();
			for(auto &pair : edgeAttMap) {
				string attName = pair.first;
				uint id = m_edgeVectorFields.m_idToName.size();
				m_edgeVectorFields.m_idToName.push_back(attName);
				m_edgeVectorFields.m_nameToID[attName] = id;
				m_vectorFieldNames.push_back(string("Edge: ") + attName);

				vector<VectorType> edgeVectors;
				edgeVectors.reserve(m_pGridMesh->getEdges().size());
				for(auto &edge : m_pGridMesh->getEdges()) {
					VectorType edgeVelocity;
					if (edge->getType() == yAlignedEdge) {
						edgeVelocity[0] = edge->OwnCustomAttribute<Scalar>::getAttribute(attName);
					}
					else {
						edgeVelocity[1] = edge->OwnCustomAttribute<Scalar>::getAttribute(attName);
					}
					edgeVectors.push_back(edgeVelocity);
				}
				m_edgeVectorFields.m_drawableGroups.push_back(
					make_shared<Magnum::SceneGraph::DrawableGroup2D>()
				);
				m_edgeVectorFields.m_vectorFieldElements.push_back(
					make_unique<DrawableVectors2D<VectorType>>(
						edgeCentroidPositions,
						edgeVectors,
						m_vectorScale,
						m_pScene,
						m_pFlatShader,
						m_pObject2D,
						m_vectorColor,
						m_edgeVectorFields.m_drawableGroups[id]
				));
			}
		}
		template <typename GridMeshType>
		void VectorFieldRenderer<GridMeshType>::initializeFaceVectorFields() {
			vector<VectorType> faceCentroidPositions;
			faceCentroidPositions.reserve(m_pGridMesh->getCells().size());
			for(auto &face : m_pGridMesh->getCells()) {
				faceCentroidPositions.push_back(face->getCentroid());
			}

			auto &faceAttMap = m_pGridMesh->getCells().front()->OwnCustomAttribute<VectorType>::getAttributesMap();
			for(auto &pair : faceAttMap) {
				string attName = pair.first;
				uint id = m_faceVectorFields.m_idToName.size();
				m_faceVectorFields.m_idToName.push_back(attName);
				m_faceVectorFields.m_nameToID[attName] = id;
				m_vectorFieldNames.push_back(string("Face: ") + attName);

				vector<VectorType> faceVectors;
				faceVectors.reserve(m_pGridMesh->getCells().size());
				for(auto &cell : m_pGridMesh->getCells()) {
					faceVectors.push_back(cell->OwnCustomAttribute<VectorType>::getAttribute(attName));
				}
				m_faceVectorFields.m_drawableGroups.push_back(
					make_shared<Magnum::SceneGraph::DrawableGroup2D>()
				);
				m_faceVectorFields.m_vectorFieldElements.push_back(
					make_unique<DrawableVectors2D<VectorType>>(
						faceCentroidPositions,
						faceVectors,
						m_vectorScale,
						m_pScene,
						m_pFlatShader,
						m_pObject2D,
						m_vectorColor,
						m_faceVectorFields.m_drawableGroups[id]
				));
			}
		}
		#pragma endregion

		#pragma region Functionalities
		template <typename GridMeshType>
		void VectorFieldRenderer<GridMeshType>::update() {
			auto &vertexAttMap = m_pGridMesh->getVertices().front()->OwnCustomAttribute<VectorType>::getAttributesMap();
			for(auto &pair : vertexAttMap) {
				string attName = pair.first;

				vector<VectorType> nodalVectors;
				nodalVectors.reserve(m_pGridMesh->getVertices().size());
				for(auto &vertex : m_pGridMesh->getVertices()) {
					nodalVectors.push_back(vertex->OwnCustomAttribute<VectorType>::getAttribute(attName));
				}
				m_nodalVectorFields.m_vectorFieldElements[m_nodalVectorFields.m_nameToID[attName]]->updateVectors(nodalVectors);
			}
			auto &edgeAttMap = m_pGridMesh->getEdges().front()->OwnCustomAttribute<Scalar>::getAttributesMap();
			for(auto &pair : edgeAttMap) {
				string attName = pair.first;

				vector<VectorType> edgeVectors;
				edgeVectors.reserve(m_pGridMesh->getEdges().size());
				for(auto &edge : m_pGridMesh->getEdges()) {
					VectorType edgeVelocity;
					if (edge->getType() == yAlignedEdge) {
						edgeVelocity[0] = edge->OwnCustomAttribute<Scalar>::getAttribute(attName);
					}
					else {
						edgeVelocity[1] = edge->OwnCustomAttribute<Scalar>::getAttribute(attName);
					}
					edgeVectors.push_back(edgeVelocity);
				}
				m_edgeVectorFields.m_vectorFieldElements[m_edgeVectorFields.m_nameToID[attName]]->updateVectors(edgeVectors);
			}
			auto &faceAttMap = m_pGridMesh->getCells().front()->OwnCustomAttribute<VectorType>::getAttributesMap();
			for(auto &pair : faceAttMap) {
				string attName = pair.first;

				vector<VectorType> cellVectors;
				cellVectors.reserve(m_pGridMesh->getCells().size());
				for(auto &cell : m_pGridMesh->getCells()) {
					cellVectors.push_back(cell->OwnCustomAttribute<VectorType>::getAttribute(attName));
				}
				m_faceVectorFields.m_vectorFieldElements[m_faceVectorFields.m_nameToID[attName]]->updateVectors(cellVectors);
			}

		}
		template <typename GridMeshType>
		void VectorFieldRenderer<GridMeshType>::draw() {
			if (m_selectedField != 0) { 
				uint selectedField = m_selectedField - 1;
				if(selectedField < m_nodalVectorFields.m_offset + m_nodalVectorFields.m_idToName.size()) {
					m_pCamera->draw(*(m_nodalVectorFields.m_drawableGroups[selectedField - m_nodalVectorFields.m_offset]));
				}
				else if(selectedField < m_edgeVectorFields.m_offset + m_edgeVectorFields.m_idToName.size()) {
					m_pCamera->draw(*(m_edgeVectorFields.m_drawableGroups[selectedField - m_edgeVectorFields.m_offset]));
				}
				else if(selectedField < m_faceVectorFields.m_offset + m_faceVectorFields.m_idToName.size()) {
					m_pCamera->draw(*(m_faceVectorFields.m_drawableGroups[selectedField - m_faceVectorFields.m_offset]));
				}
			}
		}

		template <typename GridMeshType>
		void VectorFieldRenderer<GridMeshType>::drawMenu() {
			if (ImGui::BeginCombo(string("Vector Field##" + m_vectorFieldRendererName).c_str(), m_vectorFieldNames[m_selectedField].c_str(), 0)) {
				for (int i = 0; i < m_vectorFieldNames.size(); ++i) {
					auto& vectorFieldName = m_vectorFieldNames[i];
					bool isSelected = (i == m_selectedField);
					if (ImGui::Selectable(vectorFieldName.c_str(), &isSelected)) {
						m_selectedField = i;
					}
					if (isSelected) {
						ImGui::SetItemDefaultFocus();
					}
				}
				ImGui::EndCombo();
			}
			Scalar oldScale = m_vectorScale;
			if (ImGui::InputFloat(string("Scale##" + m_vectorFieldRendererName).c_str(), &m_vectorScale, 0.01f, 0.1f)) {
				if (oldScale != m_vectorScale) {
					for (auto& vfElements : m_nodalVectorFields.m_vectorFieldElements) {
						vfElements->updateScale(m_vectorScale);
					}
					for (auto& vfElements : m_edgeVectorFields.m_vectorFieldElements) {
						vfElements->updateScale(m_vectorScale);
					}
					for (auto& vfElements : m_faceVectorFields.m_vectorFieldElements) {
						vfElements->updateScale(m_vectorScale);
					}
					for (auto& vfElements : m_voxelVectorFields.m_vectorFieldElements) {
						vfElements->updateScale(m_vectorScale);
					}
				}
			}
		}
		#pragma endregion

		template class VectorFieldRenderer<QuadGridType>;
		template class VectorFieldRenderer<QuadGridTypeDouble>;
		//template VectorFieldRenderer<HexaGridType>;
		//template VectorFieldRenderer<HexaGridTypeDouble>;
	}
}
