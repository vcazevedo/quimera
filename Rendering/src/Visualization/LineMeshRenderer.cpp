//#include "Visualization/LineMeshRenderer.h"
//
//namespace Chimera {
//
//	namespace Rendering {
//
//		#pragma region 2DRenderer
//		template <class VectorType>
//		void MeshRendererT<VectorType, Edge, true>::drawVertices(uint selectedIndex) {
//			glEnable(GL_POINT_SMOOTH);
//			glEnable(GL_BLEND);
//			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//
//			glPointSize(8.0f);
//			glBegin(GL_POINTS);
//			for (int i = 0; i < this->m_meshes[selectedIndex]->getVertices().size(); i++) {
//				if (this->m_meshes[selectedIndex]->getVertices()[i]->getType() == edgeVertex) {
//					glColor3f(0.25f, 0.80f, 0.12f);
//				}
//				else {
//					glColor3f(0.8f, 0.8f, 0.8f);
//				}
//				glVertex2f(this->m_meshes[selectedIndex]->getVertices()[i]->getPosition().x, this->m_meshes[selectedIndex]->getVertices()[i]->getPosition().y);
//			}
//
//			glColor3f(0.2f, 0.4f, 0.12f);
//			glVertex2f(this->m_meshes[selectedIndex]->getPose().getCentroid().x, this->m_meshes[selectedIndex]->getPose().getCentroid().y);
//			glEnd();
//
//			glDisable(GL_POINT_SMOOTH);
//			glDisable(GL_BLEND);
//		}
//
//		template<class VectorType>
//		void MeshRendererT<VectorType, Edge, true>::drawWireframe(uint selectedIndex) {
//			glDisable(GL_LIGHTING);
//
//			glColor3f(this->m_elementColor.getRed(), this->m_elementColor.getGreen(), this->m_elementColor.getBlue());
//			glLineWidth(this->m_lineWidth);
//			for (int i = 0; i < this->m_meshes[selectedIndex]->getElements().size(); i++) {
//				glBegin(GL_LINES);
//					const VectorType &v1 = this->m_meshes[selectedIndex]->getElement(i)->getVertex1()->getPosition();
//					const VectorType &v2 = this->m_meshes[selectedIndex]->getElement(i)->getVertex2()->getPosition();
//					glVertex2f(v1.x, v1.y);
//					glVertex2f(v2.x, v2.y);
//				glEnd();
//			}
//		}
//
//		template<class VectorType>
//		void MeshRendererT<VectorType, Edge, true>::drawNormals(uint selectedIndex) {
//			glDisable(GL_LIGHTING);
//
//			glColor3f(0.1f, 0.1f, 0.1f);
//			for (int i = 0; i < this->m_meshes[selectedIndex]->getElements().size(); i++) {
//				VectorType edgeCentroid = this->m_meshes[selectedIndex]->getElement(i)->getCentroid();
//				VectorType edgeNormal = this->m_meshes[selectedIndex]->getElement(i)->getHalfEdges().first->getNormal();
//
//				RenderingUtils::getInstance()->drawVector(edgeCentroid, edgeCentroid + edgeNormal*0.1);
//			}
//		}
//		#pragma endregion
//		#pragma region 3DRenderer
//		template <class VectorType>
//		void MeshRendererT<VectorType, Edge, false>::drawVertices(uint selectedIndex) {
//			glEnable(GL_POINT_SMOOTH);
//			glEnable(GL_BLEND);
//			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//
//			glPointSize(16.0f);
//			glBegin(GL_POINTS);
//			for (int i = 0; i < this->m_meshes[selectedIndex]->getVertices().size(); i++) {
//				if (this->m_meshes[selectedIndex]->getVertices()[i]->getType() == edgeVertex) {
//					glColor3f(this->m_vertexColor.getRed(), this->m_vertexColor.getGreen(), this->m_vertexColor.getBlue());
//				}
//				else {
//					glColor3f(0.1f, 0.1f, 0.1f);
//				}
//				glVertex3f(this->m_meshes[selectedIndex]->getVertices()[i]->getPosition().x,
//					this->m_meshes[selectedIndex]->getVertices()[i]->getPosition().y,
//					this->m_meshes[selectedIndex]->getVertices()[i]->getPosition().z);
//			}
//			glEnd();
//		}
//
//		template<class VectorType>
//		void MeshRendererT<VectorType, Edge, false>::drawWireframe(uint selectedIndex) {
//			glDisable(GL_LIGHTING);
//
//			glColor3f(this->m_elementColor.getRed(), this->m_elementColor.getGreen(), this->m_elementColor.getBlue());
//			for (int i = 0; i < this->m_meshes[selectedIndex]->getElements().size(); i++) {
//				glBegin(GL_LINES);
//				const VectorType &v1 = this->m_meshes[selectedIndex]->getElement(i)->getVertex1()->getPosition();
//				const VectorType &v2 = this->m_meshes[selectedIndex]->getElement(i)->getVertex2()->getPosition();
//				glVertex3f(v1.x, v1.y, v1.z);
//				glVertex3f(v2.x, v2.y, v2.z);
//				glEnd();
//			}
//		}
//		
//		template<class VectorType>
//		void MeshRendererT<VectorType, Edge, false>::drawNormals(uint slectedIndex) {
//
//		}
//		#pragma endregion
//
//		template class MeshRendererT<Vector2, Edge, isVector2<Vector2>::value>;
//		template class MeshRendererT<Vector2D, Edge, isVector2<Vector2D>::value>;
//
//		template class MeshRendererT<Vector3, Edge, isVector2<Vector3>::value>;
//		template class MeshRendererT<Vector3D, Edge, isVector2<Vector3D>::value>;
//	}
//
//}
