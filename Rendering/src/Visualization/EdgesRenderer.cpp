//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.


#include "Visualization/EdgesRenderer.h"

namespace Chimera {

	namespace Rendering {

		#pragma region Constructors

		template <typename GridMeshType>
		EdgesRenderer<GridMeshType>::EdgesRenderer(const string& edgesRendererName, shared_ptr<SelectableDrawableMesh2D<GridMeshType>> pDrawableMesh, shared_ptr<Magnum::Scene2D> pScene, shared_ptr<Magnum::SceneGraph::Camera2D> pCamera)
			: m_edgesRendererName(edgesRendererName), m_pCamera(pCamera), m_pScene(pScene), m_pDrawableMesh(pDrawableMesh) {
			m_pObject = make_shared<Magnum::Object2D>(m_pScene.get());
			m_selectedEdge = -1;
			m_drawSelectedEdge = m_drawEdges = m_drawEdgesIDs = false;
			m_pDrawableGroup = make_shared<Magnum::SceneGraph::DrawableGroup2D>();
			m_pDrawableGroupLabels = make_shared<Magnum::SceneGraph::DrawableGroup2D>();
			m_hasDualElement = false;

			Scalar offset = 1e-2;
			if constexpr (isElementPrimitiveQuadCell<typename GridMeshType::ElementPrimitiveType>::value) {
				Grids::GridMesh<GridMeshType>* pGridMesh = dynamic_cast<Grids::GridMesh<GridMeshType>*>(pDrawableMesh->getMesh().get());
				const vector<shared_ptr<Edge<VectorType>>> &edges = pGridMesh->getEdges();
				for (auto pEdge : edges) {
					Magnum::Vector2 edgePosition(pEdge->getCentroid().x, pEdge->getCentroid().y);
					if (pEdge->getType() == xAlignedEdge) {
						edgePosition.y() += offset;
					}
					if (pEdge->getType() == yAlignedEdge) {
						edgePosition.x() += offset;
					}

					m_edgesIDsText[pEdge->getID()] = make_shared<DrawableText2D>(to_string(pEdge->getID()),
																																				edgePosition,
																																				m_pScene, m_pDrawableGroupLabels);
				}
			}
		}
		#pragma endregion

		#pragma region Functionalities
		template <typename GridMeshType>
		void EdgesRenderer<GridMeshType>::draw() {
			if (m_drawEdges) {
				(static_cast<DrawableMesh2D<GridMeshType>*>(m_pDrawableMesh.get()))->addDrawingMode(drawingMeshMode_t::drawEdges);
			}
			else {
				(static_cast<DrawableMesh2D<GridMeshType>*>(m_pDrawableMesh.get()))->removeDrawingMode(drawingMeshMode_t::drawEdges);
			}

			if (m_drawSelectedEdge && m_selectedEdge != -1) {
				m_pDrawableMesh->addDrawingMode(selectedDrawingMode_t::drawSelectedEdge);
			}
			else {
				m_pDrawableMesh->removeDrawingMode(selectedDrawingMode_t::drawSelectedEdge);
			}

			if(m_drawEdgesIDs)
				m_pCamera->draw(*m_pDrawableGroupLabels);
			
		}
		
		template <typename GridMeshType>
		void EdgesRenderer<GridMeshType>::drawMenu() {
			ImGui::SetNextTreeNodeOpen(true);
			if (ImGui::CollapsingHeader("Edge Visualization")) {
				ImGui::BeginChild("Edges Debug Information", ImVec2(0, 50), true);
				ImGui::Text("Edges Debug Information");
				ImGui::Text(string("Selected Edge id " + to_string(m_selectedEdge)).c_str());
				ImGui::Text(string("Has dual element: " + to_string(m_hasDualElement)).c_str());
				ImGui::Text(string("Velocity: " + to_string(m_velocity)).c_str());
				ImGui::EndChild();

				ImGui::BeginChild("Edges Drawing Options", ImVec2(0, 100), true);
				ImGui::Text("Drawing Options");
				ImGui::Checkbox(string("Draw Edges##" + m_edgesRendererName).c_str(), &m_drawEdges);
				ImGui::Checkbox(string("Draw Edges IDs##" + m_edgesRendererName).c_str(), &m_drawEdgesIDs);
				ImGui::Checkbox(string("Draw Selected Edge##" + m_edgesRendererName).c_str(), &m_drawSelectedEdge);
				ImGui::EndChild();

				if (ImGui::TreeNode("Misc")) {
					ImGui::DragFloat(string("Edge Size##" + m_edgesRendererName).c_str(), &m_pDrawableMesh->getEdgeSize(), 0.1, 0, 30);
					ImGui::TreePop();
				}

			}
			
		}

		template <typename GridMeshType>
		bool EdgesRenderer<GridMeshType>::mousePressEvent(Magnum::Platform::Sdl2Application::MouseEvent& event, Magnum::Vector2i pWindowSize) {
			if (event.button() == Magnum::Platform::Sdl2Application::MouseEvent::Button::Left) {
				//Computing the position in global coordinates of the mouse event
				const Magnum::Vector2i viewPosition{ event.position().x(), pWindowSize.y() - event.position().y() - 1 };
				const Magnum::Vector2 in{ 2 * Magnum::Vector2(viewPosition) / Magnum::Vector2(pWindowSize) - Magnum::Vector2(1.0f) };
				auto mousePosition = (m_pCamera->object().absoluteTransformationMatrix() * m_pCamera->projectionMatrix().inverted()).transformPoint(in);
				Vector2 transformedPosition = Vector2(mousePosition.x(), mousePosition.y());
				if constexpr (isElementPrimitiveEdge<typename GridMeshType::ElementPrimitiveType>::value) {
					auto pEdge = m_pDrawableMesh->getMesh()->getElement(transformedPosition, 1e-3);
					if (pEdge) {
						m_selectedEdge = pEdge->getID();
						m_pDrawableMesh->setSelectedEdge(pEdge);
						m_hasDualElement = (pEdge->getDualElement2D() != nullptr);
						m_velocity = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity");
						return true;
					}
				}
				else if constexpr (isElementPrimitiveQuadCell<typename GridMeshType::ElementPrimitiveType>::value)  {
					Grids::QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<Grids::QuadGridMesh<GridMeshType>*>(m_pDrawableMesh->getMesh().get());

					// If the position is in the grid, select a cell, potentially an edge, potentially a vertex
					double gridOriginX = pQuadGridMesh->getGridCentroid().x - pQuadGridMesh->getGridDimensions().x * pQuadGridMesh->getGridSpacing() / 2.;
					double gridOriginY = pQuadGridMesh->getGridCentroid().y - pQuadGridMesh->getGridDimensions().y * pQuadGridMesh->getGridSpacing() / 2.;
					double gridEndX = gridOriginX + pQuadGridMesh->getGridDimensions().x * pQuadGridMesh->getGridSpacing();
					double gridEndY = gridOriginY + pQuadGridMesh->getGridDimensions().y * pQuadGridMesh->getGridSpacing();

					if (mousePosition.x() >= gridOriginX && mousePosition.y() >= gridOriginY && mousePosition.x() <= gridEndX && mousePosition.y() <= gridEndY) {
						auto gridPosition = Vector2(mousePosition.x(), mousePosition.y()) - Vector2(gridOriginX, gridOriginY);
						int i = gridPosition.x / pQuadGridMesh->getGridSpacing();
						int j = gridPosition.y / pQuadGridMesh->getGridSpacing();

						auto cell = pQuadGridMesh->getQuadCell(i, j)->getCell(transformedPosition);
						for (auto pEdge : cell->getEdges()) {
							if (pEdge->intersects(transformedPosition, 2.5e-3)) {
								///** Remove previous dual cell */
								//if (m_dualMeshes.find(m_selectedEdge) != m_dualMeshes.end()) {
								//	auto pPreviousDualEdge = m_dualMeshes[m_selectedEdge];
								//	if (pPreviousDualEdge) {
								//		pPreviousDualEdge->setDrawingMode(drawingMeshMode_t::dontDraw);
								//	}
								//}
								//

								//auto pDualEdge = pEdge->getDualElement2D();
								//if (pDualEdge && m_dualMeshes.find(pEdge->getID()) == m_dualMeshes.end()) {
								//	m_dualMeshes[pEdge->getID()] = make_shared<DrawableMesh2D<GridMeshType>>(pDualEdge, m_pObject, m_pDrawableGroup, 0xffcc00_rgbf);
								//}
								//m_dualMeshes[pEdge->getID()]->setDrawingMode(drawingMeshMode_t::drawEdges);

								m_selectedEdge = pEdge->getID();
								m_pDrawableMesh->setSelectedEdge(pEdge);
								m_hasDualElement = (pEdge->getDualElement2D() != nullptr);
								m_velocity = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity");
								return true;
							}
						}
					}

					return false;
				}
				
			}
			return false;
		}
		#pragma endregion

		template class EdgesRenderer<QuadGridType>;
	}
}
