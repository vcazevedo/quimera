#include "Visualization/GridRenderer.h"

#include "Magnum/ImGuiIntegration/Context.hpp"
#include <Magnum/Math/Matrix4.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/MeshTools/Interleave.h>
#include <Magnum/Math/Color.h>

using namespace Magnum::Math::Literals;

namespace Chimera {
	namespace Rendering {

		#pragma region Constructors
		template <typename GridMeshType>
		GridRenderer<GridMeshType>::GridRenderer(const string& gridRendererName, shared_ptr<SelectableDrawableMesh2D<GridMeshType>> pDrawableMesh, 
																							shared_ptr<Magnum::Scene2D> pScene, shared_ptr<Magnum::SceneGraph::Camera2D> pCamera)
			:  m_gridRendererName(gridRendererName), m_pCamera(pCamera), m_pScene(pScene), m_pDrawableMesh(pDrawableMesh) {
			m_pDrawableGroup = make_shared<Magnum::SceneGraph::DrawableGroup2D>();
			m_pDrawableGroupLabels = make_shared<Magnum::SceneGraph::DrawableGroup2D>();
			
			m_pObject = make_shared<Magnum::Object2D>(m_pScene.get());
			m_selectedCell = -1;
			m_isCellSolid = false;
			m_hasDualElement = false;
			m_drawCellIDs = m_drawSelectedCell = m_drawGrid = false;

			if constexpr (isElementPrimitiveQuadCell<typename GridMeshType::ElementPrimitiveType>::value) {
				Grids::GridMesh<GridMeshType>* pGridMesh = dynamic_cast<Grids::GridMesh<GridMeshType>*>(pDrawableMesh->getMesh().get());
				const vector<shared_ptr<Cell<VectorType>>> &cells= pGridMesh->getCells();
				for (auto pCell : cells) {
					Magnum::Vector2 cellCentroid(pCell->getCentroid().x, pCell->getCentroid().y);
					m_cellsIDText[pCell->getID()] = make_shared<DrawableText2D>(to_string(pCell->getID()),
																																					cellCentroid,
																																					m_pScene, m_pDrawableGroupLabels);
				}
			}
			
		}
		#pragma endregion

		#pragma region Initialization
		template <typename GridMeshType>
		void GridRenderer<GridMeshType>::draw() {
			if (m_drawSelectedCell  && m_selectedCell!= -1) {
				m_pDrawableMesh->addDrawingMode(selectedDrawingMode_t::drawSelectedCell);
			}
			else {
				m_pDrawableMesh->removeDrawingMode(selectedDrawingMode_t::drawSelectedCell);
			}
			if (m_drawSelectedCell && m_selectedCell != -1) {
			}
			
			if (m_drawCellIDs) {
				m_pCamera->draw(*m_pDrawableGroupLabels);
			}
			m_pCamera->draw(*m_pDrawableGroup);
		}
		
		template <typename GridMeshType>
		void GridRenderer<GridMeshType>::drawMenu() {
			ImGui::SetNextTreeNodeOpen(true);
			if (ImGui::CollapsingHeader("Grid Visualization")) {
				ImGui::BeginChild("Cell Debug Information", ImVec2(0, 50), true);
				ImGui::Text("Cell Debug Information");
				ImGui::Text(string("Selected Cell id " + to_string(m_selectedCell)).c_str());
				ImGui::Text(string("Is selected Cell solid: " + to_string(m_isCellSolid)).c_str());
				ImGui::Text(string("Has dual element: " + to_string(m_hasDualElement)).c_str());
				ImGui::EndChild();

				ImGui::BeginChild("Grid Drawing Options", ImVec2(0, 100), true);
				ImGui::Text("Drawing Options");
				ImGui::Checkbox(string("Draw Grid##" + m_gridRendererName).c_str(), &m_drawGrid);
				ImGui::Checkbox(string("Draw Cells IDs##" + m_gridRendererName).c_str(), &m_drawCellIDs);
				ImGui::Checkbox(string("Draw Selected Cell##" + m_gridRendererName).c_str(), &m_drawSelectedCell);
				ImGui::EndChild();
			}
		}


		template <typename GridMeshType>
		bool GridRenderer<GridMeshType>::mousePressEvent(Magnum::Platform::Sdl2Application::MouseEvent& event, Magnum::Vector2i pWindowSize) {
			if (event.button() == Magnum::Platform::Sdl2Application::MouseEvent::Button::Left) {
				//Computing the position in global coordinates of the mouse event
				const Magnum::Vector2i viewPosition{ event.position().x(), pWindowSize.y() - event.position().y() - 1 };
				const Magnum::Vector2 in{ 2 * Magnum::Vector2(viewPosition) / Magnum::Vector2(pWindowSize) - Magnum::Vector2(1.0f) };
				auto mousePosition = (m_pCamera->object().absoluteTransformationMatrix() * m_pCamera->projectionMatrix().inverted()).transformPoint(in);
				Vector2 transformedPosition = Vector2(mousePosition.x(), mousePosition.y());

				Grids::QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<Grids::QuadGridMesh<GridMeshType>*>(m_pDrawableMesh->getMesh().get());

				// If the position is in the grid, select a cell, potentially an edge, potentially a vertex
				double gridOriginX = pQuadGridMesh->getGridCentroid().x - pQuadGridMesh->getGridDimensions().x * pQuadGridMesh->getGridSpacing() / 2.;
				double gridOriginY = pQuadGridMesh->getGridCentroid().y - pQuadGridMesh->getGridDimensions().y * pQuadGridMesh->getGridSpacing() / 2.;
				double gridEndX = gridOriginX + pQuadGridMesh->getGridDimensions().x * pQuadGridMesh->getGridSpacing();
				double gridEndY = gridOriginY + pQuadGridMesh->getGridDimensions().y * pQuadGridMesh->getGridSpacing();

				if (mousePosition.x() >= gridOriginX && mousePosition.y() >= gridOriginY && mousePosition.x() <= gridEndX && mousePosition.y() <= gridEndY) {
					auto gridPosition = Vector2(mousePosition.x(), mousePosition.y()) - Vector2(gridOriginX, gridOriginY);
					int i = gridPosition.x / pQuadGridMesh->getGridSpacing();
					int j = gridPosition.y / pQuadGridMesh->getGridSpacing();

					auto pCell = pQuadGridMesh->getQuadCell(i, j)->getCell(transformedPosition);
					m_pDrawableMesh->setSelectedCell(pCell);
					m_selectedCell = pCell->getID();
					m_isCellSolid = pCell->isSolid();
					m_hasDualElement = (pCell->getDualElement2D() != nullptr);

					return true;
				}
			}

			return false;
		}
		#pragma endregion

		template class GridRenderer<QuadGridType>;
	}
}
