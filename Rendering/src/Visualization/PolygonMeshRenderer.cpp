//#include "Visualization/PolygonMeshRenderer.h"
//
//
//namespace Chimera {
//	namespace Rendering {
//
//		#pragma region Constructors
//		template <class VectorType, template <class> class ElementType>
//		PolygonMeshRendererT<VectorType, ElementType>::PolygonMeshRendererT(const vector<shared_ptr<Mesh<VectorType, ElementType>>> &g_meshes) 
//															 : PolygonMeshRendererT::MeshRendererT(g_meshes) {
//			this->m_selectedIndex = 0;
//			this->m_draw = true;
//		}
//		#pragma endregion
//
//		#pragma region InitializationFunctions
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::initializeWindowControls(BaseWindow *pBaseWindow) {
//			TwBar *pTwBar = pBaseWindow->getBaseTwBar();
//		}
//
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::initializeVBOs() {
//			for (int i = 0; i < this->m_meshes.size(); i++) {
//				initializeVBO(i);
//			}
//		}
//
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::initializeVBO(unsigned int meshIndex) {
//			/*	GLuint *pVertexVBO = new GLuint();
//			glGenBuffers(1, pVertexVBO);
//			glBindBuffer(GL_ARRAY_BUFFER, *pVertexVBO);
//			m_pVerticesVBOs.push_back(pVertexVBO);
//			void *pVertices = reinterpret_cast<void *>(&m_meshes[meshIndex]->getPoints()[0]);
//			unsigned int sizeVertices = m_meshes[meshIndex]->getPoints().size() * sizeof(Vector3);
//			glBufferData(GL_ARRAY_BUFFER, sizeVertices, pVertices, GL_DYNAMIC_DRAW);
//			glBindBuffer(GL_ARRAY_BUFFER, 0);
//
//			void *pNormals = reinterpret_cast<void *>(&m_meshes[meshIndex]->getPointsNormals()[0]);
//			GLuint *pNormalVBO = new GLuint();
//			glGenBuffers(1, pNormalVBO);
//			glBindBuffer(GL_ARRAY_BUFFER, *pNormalVBO);
//			m_pNormalsVBOs.push_back(pNormalVBO);
//			glBufferData(GL_ARRAY_BUFFER, sizeVertices, pNormals, GL_DYNAMIC_DRAW);
//			glBindBuffer(GL_ARRAY_BUFFER, 0);
//
//			GLuint *pIndicesVBO = new GLuint();
//			glGenBuffers(1, pIndicesVBO);
//			glBindBuffer(GL_ARRAY_BUFFER, *pIndicesVBO);
//			m_pIndicesVBOs.push_back(pIndicesVBO);
//			*/
//			/*vector<int> meshIndices;
//			auto faces = m_meshes[meshIndex]->getMeshPolygons();
//			for (int i = 0; i < faces.size(); i++) {
//			for (int j = 0; j < faces[i].edges.size(); j++) {
//			meshIndices.push_back(faces[i].edges[j].first);
//			}
//			}*/
//
//			/*m_numElementsToDraw.push_back(meshIndices.size());
//			unsigned int sizeIndices = meshIndices.size() * sizeof(int);
//			void *pIndices = reinterpret_cast<void *>(&meshIndices[0]);
//
//			glBufferData(GL_ARRAY_BUFFER, sizeIndices, pIndices, GL_DYNAMIC_DRAW);
//			glBindBuffer(GL_ARRAY_BUFFER, 0);*/
//		}
//
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::initializeShaders() {
//			m_pWireframeShader = ResourceManager::getInstance()->loadGLSLShader("Shaders/3D/PhongShading.glsl", "Shaders/3D/Wireframe.frag");
//			m_pPhongShader = ResourceManager::getInstance()->loadGLSLShader("Shaders/3D/PhongShading.glsl", "Shaders/3D/PhongShading.frag");
//			m_pPhongWireframeShader = ResourceManager::getInstance()->loadGLSLShader("Shaders/3D/PhongShading.glsl", "Shaders/3D/PhongShadingWireframe.frag");
//			m_lightPosLoc = glGetUniformLocation(m_pPhongShader->getProgramID(), "lightPos");
//			m_lightPosLocWire = glGetUniformLocation(m_pPhongWireframeShader->getProgramID(), "lightPos");
//		}
//		#pragma endregion
//
//		/*#pragma region Functionalities
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::draw() {
//			if (m_draw) {
//				for (int i = 0; i < m_meshes.size(); i++) {
//					if (m_meshes[i]->getRenderingAttributes().drawShaded) {
//						drawElements(i);
//					}
//				}
//			}
//			if (m_drawVertices) {
//				for (int i = 0; i < m_meshes.size(); i++) {
//					drawMeshVertices(i);
//				}
//			}
//			
// 		}		
//		#pragma endregion*/
//
//		#pragma region DrawingFunctions
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::drawVertices(uint selectedIndex) {
//			glDisable(GL_LIGHTING);
//			glDisable(GL_LIGHT0);
//			glColor3f(0.0f, 0.0f, 0.0f);
//			glPointSize(8.0f);
//			glBegin(GL_POINTS);
//			for (int i = 0; i < this->m_meshes[selectedIndex]->getVertices().size(); i++) {
//				glVertex3f(this->m_meshes[selectedIndex]->getVertices()[i]->getPosition().x,
//					this->m_meshes[selectedIndex]->getVertices()[i]->getPosition().y,
//					this->m_meshes[selectedIndex]->getVertices()[i]->getPosition().z);
//			}
//			glEnd();
//		}
//
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::drawShaded(uint selectedIndex) {
//			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//
//			glDisable(GL_BLEND);
//			glEnable(GL_DEPTH_TEST);
//			glDepthMask(GL_TRUE);
//			glEnable(GL_LIGHTING);
//			glEnable(GL_LIGHT0);
//
//			Vector3 cameraPosition = GLRenderer3D::getInstance()->getCamera()->getPosition();
//			GLfloat light_position[] = { cameraPosition.x, cameraPosition.y, cameraPosition.z, 0.0 };
//			m_pPhongShader->applyShader();
//
//			glUniform3f(m_lightPosLoc, cameraPosition.x, cameraPosition.y, cameraPosition.z);
//			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
//
//			glColor3f(0.5, 0.5, 0.5);
//			glLineWidth(1.0f);
//
//			drawPolygons(selectedIndex);
//
//			m_pPhongShader->removeShader();
//
//			glDisable(GL_LIGHTING);
//			glDisable(GL_LIGHT0);
//
//			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		}
//
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::drawWireframeOnShaded(uint selectedIndex) {
//			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//
//			glDisable(GL_BLEND);
//			glEnable(GL_DEPTH_TEST);
//			glDepthMask(GL_TRUE);
//			glEnable(GL_LIGHTING);
//			glEnable(GL_LIGHT0);
//			
//			Vector3 cameraPosition = GLRenderer3D::getInstance()->getCamera()->getPosition();
//			GLfloat light_position[] = { cameraPosition.x, cameraPosition.y, cameraPosition.z, 0.0 };
//			m_pPhongWireframeShader->applyShader();
//			
//			glUniform3f(m_lightPosLocWire, cameraPosition.x, cameraPosition.y, cameraPosition.z);
//			glLightfv(GL_LIGHT0, GL_POSITION, light_position);
//
//			glColor3f(0.5, 0.5, 0.5);
//			glLineWidth(1.0f);
//
//			drawPolygons(selectedIndex);
//
//			m_pPhongWireframeShader->removeShader();
//			
//			glDisable(GL_LIGHTING);
//			glDisable(GL_LIGHT0);
//
//			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		}
//		
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::drawWireframe(uint selectedIndex) {
//			//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//
//			glDisable(GL_BLEND);
//			glEnable(GL_DEPTH_TEST);
//			glDisable(GL_LIGHTING);
//			glDisable(GL_LIGHT0);
//
//			//m_pWireframeShader->applyShader();
//			
//			glColor3f(0.5, 0.5, 0.5);
//			glLineWidth(1.0f);
//
//			drawPolygons(selectedIndex);
//
//			//m_pPhongWireframeShader->removeShader();
//
//			glDisable(GL_LIGHTING);
//			glDisable(GL_LIGHT0);
//
//			//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		}
//
//		
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::drawNormals(uint selectedIndex) {
//			for (int i = 0; i < this->m_meshes[selectedIndex]->getVertices().size(); i++) {
//				VectorType vertexNormal = this->m_meshes[selectedIndex]->getVertices()[i]->getNormal();
//				VectorType vertexPosition = this->m_meshes[selectedIndex]->getVertices()[i]->getPosition();
//				RenderingUtils::getInstance()->drawVector(vertexPosition, vertexPosition + vertexNormal*0.1);
//			}
//		}
//		//template <>
//		//void PolygonMeshRendererT<Vector3, HalfFace>::drawPolygons(int selectedIndex) {
//		//	auto polygons = m_meshes[selectedIndex]->getElements();
//		//	//auto vertices = m_meshes[selectedIndex]->getVertices();
//		//	for (int i = 0; i < polygons.size(); i++) {
//		//		glBegin(GL_POLYGON);
//		//		for (int j = 0; j < polygons[i]->getHalfEdges().size(); j++) {
//		//			auto pVertex = polygons[i]->getHalfEdges()[j]->getVertices().first;
//		//			Vector3 vertexNormal = pVertex->getNormal();
//		//			Vector3 vertexPosition = pVertex->getPosition();
//		//			glNormal3f(vertexNormal.x, vertexNormal.y, vertexNormal.z);
//		//			glVertex3f(vertexPosition.x, vertexPosition.y, vertexPosition.z);
//		//		}
//		//		glEnd();
//		//	}
//		//}
//
//		//template <>
//		//void PolygonMeshRendererT<Vector3D, HalfFace>::drawPolygons(int selectedIndex) {
//		//	auto polygons = m_meshes[selectedIndex]->getElements();
//		//	//auto vertices = m_meshes[selectedIndex]->getVertices();
//		//	for (int i = 0; i < polygons.size(); i++) {
//		//		glBegin(GL_POLYGON);
//		//		for (int j = 0; j < polygons[i]->getHalfEdges().size(); j++) {
//		//			auto pVertex = polygons[i]->getHalfEdges()[j]->getVertices().first;
//		//			Vector3D vertexNormal = pVertex->getNormal();
//		//			Vector3D vertexPosition = pVertex->getPosition();
//		//			glNormal3f(vertexNormal.x, vertexNormal.y, vertexNormal.z);
//		//			glVertex3f(vertexPosition.x, vertexPosition.y, vertexPosition.z);
//		//		}
//		//		glEnd();
//		//	}
//		//}
//
//
//		template <>
//		void PolygonMeshRendererT<Vector3, Face>::drawPolygons(int selectedIndex) {
//			auto polygons = m_meshes[selectedIndex]->getElements();
//			//auto vertices = m_meshes[selectedIndex]->getVertices();
//
//			GLfloat model[16];
//			glGetFloatv(GL_PROJECTION_MATRIX, model);
//			const Vector3 cameraAngle(model[2], model[6], model[10]);
//
//			for (int i = 0; i < polygons.size(); i++) {
//				auto pHalfFace = polygons[i]->getHalfFaces().front();
//				if (pHalfFace->getNormal().dot(cameraAngle) > 0) {
//					pHalfFace = polygons[i]->getHalfFaces().back();
//				}
//
//				glBegin(GL_POLYGON);
//				for (auto pHalfEdge : pHalfFace->getHalfEdges()) {
//					shared_ptr<Vertex<Vector3>> pVertex = pHalfEdge->getVertices().first;
//					Vector3 vertexNormal = pVertex->getNormal();
//					Vector3 vertexPosition = pVertex->getPosition();
//					glNormal3f(vertexNormal.x, vertexNormal.y, vertexNormal.z);
//					glVertex3f(vertexPosition.x, vertexPosition.y, vertexPosition.z);
//				}
//				glEnd();
//			}
//		}
//
//		template <>
//		void PolygonMeshRendererT<Vector3D, Face>::drawPolygons(int selectedIndex) {
//			auto polygons = m_meshes[selectedIndex]->getElements();
//			//auto vertices = m_meshes[selectedIndex]->getVertices();
//
//			GLfloat model[16];
//			glGetFloatv(GL_PROJECTION_MATRIX, model);
//			const Vector3D cameraAngle(model[2], model[6], model[10]);
//
//			for (int i = 0; i < polygons.size(); i++) {
//				auto pHalfFace = polygons[i]->getHalfFaces().front();
//				if (pHalfFace->getNormal().dot(cameraAngle) > 0) {
//					pHalfFace = polygons[i]->getHalfFaces().back();
//				}
//
//				glBegin(GL_POLYGON);
//				for (auto pHalfEdge : pHalfFace->getHalfEdges()) {
//					shared_ptr<Vertex<Vector3D>> pVertex = pHalfEdge->getVertices().first;
//					Vector3D vertexNormal = pVertex->getNormal();
//					Vector3D vertexPosition = pVertex->getPosition();
//					glNormal3f(vertexNormal.x, vertexNormal.y, vertexNormal.z);
//					glVertex3f(vertexPosition.x, vertexPosition.y, vertexPosition.z);
//				}
//				glEnd();
//			}
//		}
//		
//		template <class VectorType, template <class> class ElementType>
//		void PolygonMeshRendererT<VectorType, ElementType>::drawTrianglesVBOs(int selectedIndex) {
//			glEnableClientState(GL_VERTEX_ARRAY);
//			glColor3f(0.21f, 0.54f, 1.0f);
//			glBindBuffer(GL_ARRAY_BUFFER, *m_pVerticesVBOs[selectedIndex]);
//			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *m_pIndicesVBOs[selectedIndex]);
//			glVertexPointer(3, GL_FLOAT, 0, 0);
//			glBindBuffer(GL_NORMAL_ARRAY, *m_pNormalsVBOs[selectedIndex]);
//			glNormalPointer(3, GL_FLOAT, 0);
//			glDrawElements(GL_TRIANGLES, m_numElementsToDraw[selectedIndex], GL_UNSIGNED_INT, 0);
//			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//			glBindBuffer(GL_ARRAY_BUFFER, 0);
//			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//			glDisableClientState(GL_VERTEX_ARRAY);
//		}
//		
//
//		#pragma endregion
//
//		template class PolygonMeshRendererT<Vector3, Face>;
//		template class PolygonMeshRendererT<Vector3D, Face>;
//		#pragma endregion
//	
//	}
//}
