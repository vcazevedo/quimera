//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#include "Visualization/MeshesRenderer.h"
#include <Magnum/Math/Color.h>

namespace Chimera {

	namespace Rendering {
		
		template<typename MeshType>
		#pragma region Constructors
		MeshesRenderer <MeshType>::MeshesRenderer(const string& meshRendererName, const vector<shared_ptr<Mesh<MeshType>>>& meshes, shared_ptr<Scene2D> pScene, shared_ptr<Magnum::Object2D> pObjTransform, shared_ptr<Magnum::SceneGraph::Camera2D> pCamera)
			: m_pCamera(pCamera), m_pScene(pScene), m_meshes(meshes) {
			m_pDrawableGroup = make_shared<Magnum::SceneGraph::DrawableGroup2D>();
			m_pObj = make_shared<Magnum::Object2D>(m_pScene.get());

			for (auto pMesh : m_meshes) {
				m_lineMeshesMagnum.push_back(make_shared<DrawableMesh2D<MeshType>>(pMesh, m_pObj, m_pDrawableGroup));
			}
			
		}
		#pragma endregion


		template class MeshesRenderer<LineMeshType<Vector2>>;
		template class MeshesRenderer<LineMeshType<Vector2D>>;
	}
}
