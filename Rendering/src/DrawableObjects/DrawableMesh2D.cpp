#include "DrawableObjects/DrawableMesh2D.h"


namespace Chimera {
	namespace Rendering {
		#pragma region Constructor
		template <typename MeshType>
		DrawableMesh2D<typename MeshType>::DrawableMesh2D(shared_ptr<Mesh<MeshType>> pMesh,
																												shared_ptr<Magnum::Object2D> pObjTransform,
																												shared_ptr<Magnum::SceneGraph::DrawableGroup2D> pDrawableGroup,
																												const Magnum::Color3 &baseColor)
			: Magnum::SceneGraph::Drawable2D{ *pObjTransform, pDrawableGroup.get() }, m_pMesh(pMesh), 
				m_pMagnumMeshVertices(nullptr), m_pMagnumMeshEdges(nullptr),
				m_pMagnumMeshCells(nullptr), m_baseColor(baseColor) {

			if constexpr (isVector2<typename MeshType::VectorType>::value) {
				initializeMagnumMeshes();
			}
			m_drawingMeshMode = drawingMeshModes_t(drawingMeshMode_t::drawVertices) | drawingMeshModes_t(drawingMeshMode_t::drawEdges);
			m_vertexSize = 10.0f;
			m_edgeSize = 2.0f;

		}
		#pragma endregion

		#pragma region Initialization
		template <typename MeshType>
		void DrawableMesh2D<typename MeshType>::initializeMagnumMeshes() {
			m_vertexData.resize(m_pMesh->getVertices().size());

			uint index = 0;

			m_verticesIDMap.clear();

			/** Initialize Vertices Mesh*/
			{

				/** Initializing vertex data, default color set to white */
				for (auto pVertex : m_pMesh->getVertices()) {
					m_vertexData[index].first.x() = pVertex->getPosition().x;
					m_vertexData[index].first.y() = pVertex->getPosition().y;
					m_vertexData[index].second = m_baseColor;

					m_verticesIDMap[pVertex->getID()] = index;
					index++;
				}
				m_vertexBuffer.setData(m_vertexData, Magnum::GL::BufferUsage::DynamicDraw);

				m_pMagnumMeshVertices = make_shared<Magnum::GL::Mesh>();
				m_pMagnumMeshVertices->setPrimitive(Magnum::GL::MeshPrimitive::Points).addVertexBuffer(m_vertexBuffer, 0, Magnum::Shaders::VertexColor2D::Position{}, Magnum::Shaders::VertexColor2D::Color3{});
				m_pMagnumMeshVertices->setCount(m_pMesh->getVertices().size());
			}

			/** Initialize Edges Mesh */
			{
				
				Magnum::GL::Buffer indexBuffer;

				shared_ptr<Edge<VectorType>> m_pFirstEdge;
				if constexpr (isElementPrimitiveQuadCell<typename MeshType::ElementPrimitiveType>::value) {
					//Here we are safe to assume that the mesh will be a GridMesh, so we cast it
					Grids::GridMesh<MeshType>* pGridMesh = dynamic_cast<Grids::GridMesh<MeshType>*>(m_pMesh.get());

					vector<Magnum::UnsignedInt> indices(pGridMesh->getEdges().size() * 2);
					index = 0;

					m_pFirstEdge = pGridMesh->getEdges().front();
					for (auto pEdge : pGridMesh->getEdges()) {
						indices[index++] = m_verticesIDMap[pEdge->getHalfEdges().first->getVertices().first->getID()];
						indices[index++] = m_verticesIDMap[pEdge->getHalfEdges().first->getVertices().second->getID()];
					}

					indexBuffer.setData(indices);
				}
				else {
					vector<Magnum::UnsignedInt> indices(m_pMesh->getElements().size() * 2);
					index = 0;

					m_pFirstEdge = m_pMesh->getElements().front();
					
					for (auto pEdge : m_pMesh->getElements()) {
						indices[index++] = m_verticesIDMap[pEdge->getHalfEdges().first->getVertices().first->getID()];
						indices[index++] = m_verticesIDMap[pEdge->getHalfEdges().first->getVertices().second->getID()];
					}

					indexBuffer.setData(indices);
				}
				

				m_pMagnumMeshEdges = make_shared<Magnum::GL::Mesh>();
				m_pMagnumMeshEdges->setPrimitive(Magnum::GL::MeshPrimitive::Lines).addVertexBuffer(m_vertexBuffer, 0, Magnum::Shaders::VertexColor2D::Position{}, Magnum::Shaders::VertexColor2D::Color3{});
				m_pMagnumMeshEdges->setIndexBuffer(indexBuffer, 0, Magnum::GL::MeshIndexType::UnsignedInt).setCount(index);
			}

			/** Initialize Polygons Mesh */
			{
				m_pMagnumMeshCells = make_shared<Magnum::GL::Mesh>();
				bool flipEdges = false;

				//Checks if the mesh is a QuadCell based one, if it is we will use this structure to create polygons
				if constexpr (isElementPrimitiveQuadCell<typename MeshType::ElementPrimitiveType>::value) {
					bool flipEdges = false;

					const vector<shared_ptr<typename MeshType::ElementPrimitiveType>> &cells = m_pMesh->getElements();

					/** First check if edges need to be flipped */
					{
						int initialIndex = -1;
						
						//Checks only the first cell
						auto pCell = m_pMesh->getElements().front()->getCells().front();
						for (auto pHalfEdge : pCell->getHalfCells().first->getHalfEdges()) {
							uint currIndex = m_verticesIDMap[pHalfEdge->getVertices().first->getID()];
							uint nextIndex = m_verticesIDMap[pHalfEdge->getVertices().second->getID()];
							if (initialIndex == -1) {
								initialIndex = currIndex;
							}
							if (currIndex != initialIndex && nextIndex != initialIndex) {
								typename MeshType::VectorType v1 = m_pMesh->getVertices()[initialIndex]->getPosition();
								typename MeshType::VectorType v2 = m_pMesh->getVertices()[currIndex]->getPosition();
								typename MeshType::VectorType v3 = m_pMesh->getVertices()[nextIndex]->getPosition();
								if (MeshUtils::checkClockwise(v1, v2, v3)) {
									flipEdges = true;
									break;
								}
							}
						}

						vector<Magnum::UnsignedInt> indices;
						indices.reserve(m_pMesh->getElements().size());

						/** Each cell organizes its internal primitives as triangle fans */
						for (auto pCell : cells) {
							int initialIndex = -1;
							for (auto pHalfEdge : pCell->getCells().front()->getHalfCells().first->getHalfEdges()) {
								uint currIndex = m_verticesIDMap[pHalfEdge->getVertices().first->getID()];
								uint nextIndex = m_verticesIDMap[pHalfEdge->getVertices().second->getID()];
								if (initialIndex == -1) {
									initialIndex = currIndex;
								}
								//Push one triangle per edge, unless its the first or last edges
								if (currIndex != initialIndex && nextIndex != initialIndex) {
									//Create a triangle with the initial, current and next indices
									if (flipEdges) {
										indices.push_back(currIndex);
										indices.push_back(initialIndex);
									}
									else {
										indices.push_back(initialIndex);
										indices.push_back(currIndex);
									}
									indices.push_back(nextIndex);
								}
							}
						}
						m_indexBuffer.setData(indices);

						m_pMagnumMeshCells->setPrimitive(Magnum::MeshPrimitive::Triangles).addVertexBuffer(m_vertexBuffer, 0, Magnum::Shaders::VertexColor2D::Position{}, Magnum::Shaders::VertexColor2D::Color3{});
						m_pMagnumMeshCells->setIndexBuffer(m_indexBuffer, 0, Magnum::GL::MeshIndexType::UnsignedInt).setCount(m_indexBuffer.size());
					}
				}
			}
		}
		#pragma endregion

		#pragma region Functionalities
		template <typename MeshType>
		void DrawableMesh2D<MeshType>::draw(const Magnum::Matrix3 &transformation, Magnum::SceneGraph::Camera2D &camera) {
			if (m_drawingMeshMode & drawingMeshMode_t::drawVertices && m_pMagnumMeshVertices) {
				Magnum::GL::Renderer::setPointSize(m_vertexSize);
				m_vertexColorShader.setTransformationProjectionMatrix(camera.projectionMatrix() * transformation).draw(*m_pMagnumMeshVertices);
			}
			if (m_drawingMeshMode & drawingMeshMode_t::drawEdges && m_pMagnumMeshEdges) {
				Magnum::GL::Renderer::setLineWidth(m_edgeSize);
				m_vertexColorShader.setTransformationProjectionMatrix(camera.projectionMatrix() * transformation).draw(*m_pMagnumMeshEdges);
			}
			if (m_drawingMeshMode& drawingMeshMode_t::drawCells && m_pMagnumMeshCells) {
				m_vertexColorShader.setTransformationProjectionMatrix(camera.projectionMatrix() * transformation).draw(*m_pMagnumMeshCells);
			}
		}
		#pragma endregion

		template class DrawableMesh2D<LineMeshType<Vector2>>;
		template class DrawableMesh2D<LineMeshType<Vector2D>>;

		template class DrawableMesh2D<Grids::QuadGridType>;
	}
}
