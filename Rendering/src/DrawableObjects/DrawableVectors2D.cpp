#include "Corrade/Containers/ArrayViewStl.h"
#include "DrawableObjects/DrawableVectors2D.h"
#include "Corrade/Containers/ArrayViewStl.h"

namespace Chimera {
	namespace Rendering {
		template <typename VectorType>
		DrawableVectors2D<VectorType>::DrawableVectors2D (
		    vector<VectorType> &positions,
				vector<VectorType> &vectorsAtPosition,
				Scalar scale,
				shared_ptr<Magnum::Scene2D> pScene,
				shared_ptr<Magnum::Shaders::Flat2D> pShader,
				shared_ptr<Magnum::Object2D> pObject,
				Magnum::Color3 color,
				shared_ptr<Magnum::SceneGraph::DrawableGroup2D> pDrawableGroup)
		: Magnum::Object2D(pScene.get()),
			Magnum::SceneGraph::Drawable2D{*pObject, pDrawableGroup.get()},
		  m_scale(scale),
			m_pShader(pShader),
			m_color(color) {

			assert(positions.size() == vectorsAtPosition.size());

			m_vectors.assign(vectorsAtPosition.begin(), vectorsAtPosition.end());
			m_vertexPositionsFloat.reserve(positions.size() * 4);
			m_vertexIndices.reserve(positions.size() * 2);
			m_arrowPositionsFloat.reserve(positions.size() * 6);
			m_arrowIndices.reserve(positions.size() * 3);
			for(int i = 0; i < positions.size(); ++i) {
				VectorType start = positions[i];
				VectorType end = start + vectorsAtPosition[i] * m_scale;
				m_vertexPositionsFloat.push_back(start[0]);
				m_vertexPositionsFloat.push_back(start[1]);
				m_vertexPositionsFloat.push_back(end[0]);
				m_vertexPositionsFloat.push_back(end[1]);
				m_vertexIndices.push_back(2*i);
				m_vertexIndices.push_back(2*i + 1);
				makeArrow(start, end);
			}
			m_vertexBuffer.setData(m_vertexPositionsFloat, Magnum::GL::BufferUsage::DynamicDraw);
			m_indexBuffer.setData(m_vertexIndices);
			m_arrowVertexBuffer.setData(m_arrowPositionsFloat, Magnum::GL::BufferUsage::DynamicDraw);
			m_arrowIndexBuffer.setData(m_arrowIndices);
			m_vectorMesh.setPrimitive(Magnum::GL::MeshPrimitive::Lines)
			            .addVertexBuffer(m_vertexBuffer, 0, Magnum::Shaders::Flat2D::Position {} )
									.setIndexBuffer(m_indexBuffer, 0, Magnum::GL::MeshIndexType::UnsignedInt)
									.setCount(m_vertexIndices.size());
			m_arrowMesh.setPrimitive(Magnum::GL::MeshPrimitive::Triangles)
			           .addVertexBuffer(m_arrowVertexBuffer, 0, Magnum::Shaders::Flat2D::Position {})
								 .setIndexBuffer(m_arrowIndexBuffer, 0, Magnum::GL::MeshIndexType::UnsignedInt)
								 .setCount(m_arrowIndices.size());
		}

		template <typename VectorType>
		void DrawableVectors2D<VectorType>::makeArrow(VectorType start, VectorType end) {
			uint currentIndex = m_arrowPositionsFloat.size()/2;
			Magnum::Vector2 t1(0.f, 0.f), t2(-0.2, 0.1f), t3(-0.2f, -0.1f);
			Magnum::Vector2 magnumEnd(end.x, end.y);
			VectorType angle = (end - start).normalized();
			Magnum::Matrix2x2 rotation;
			Scalar scale = (end - start).length();
			rotation[0][0] = angle.x;
			rotation[0][1] = angle.y;
			rotation[1][0] = -angle.y;
			rotation[1][1] = angle.x;
			Magnum::Vector2 shiftedVertex = rotation*scale*t1 + magnumEnd;
			m_arrowPositionsFloat.push_back(shiftedVertex.x());
			m_arrowPositionsFloat.push_back(shiftedVertex.y());
			shiftedVertex = rotation*scale*t2 + magnumEnd;
			m_arrowPositionsFloat.push_back(shiftedVertex.x());
			m_arrowPositionsFloat.push_back(shiftedVertex.y());
			shiftedVertex = rotation*scale*t3 + magnumEnd;
			m_arrowPositionsFloat.push_back(shiftedVertex.x());
			m_arrowPositionsFloat.push_back(shiftedVertex.y());

			m_arrowIndices.push_back(currentIndex);
			m_arrowIndices.push_back(currentIndex + 1);
			m_arrowIndices.push_back(currentIndex + 2);
		}

		template <typename VectorType>
		void DrawableVectors2D<VectorType>::updateVectors(vector<VectorType> &vectorsAtPosition) {
			assert(m_vertexPositionsFloat.size() == vectorsAtPosition.size() * 4);
			m_arrowPositionsFloat.clear();
			m_arrowIndices.clear();
			m_vectors.assign(vectorsAtPosition.begin(), vectorsAtPosition.end());
			for(uint i = 0; i < vectorsAtPosition.size(); ++i) {
				VectorType start(m_vertexPositionsFloat[4*i], m_vertexPositionsFloat[4*i+1]);
				VectorType end = start + m_scale*vectorsAtPosition[i];
				m_vertexPositionsFloat[4*i + 2] = end.x;
				m_vertexPositionsFloat[4*i + 3] = end.y;
				makeArrow(start, end);
			}
			m_vertexBuffer.setSubData(0, m_vertexPositionsFloat);
			m_arrowVertexBuffer.setSubData(0, m_arrowPositionsFloat);
		}

		template <typename VectorType>
		void DrawableVectors2D<VectorType>::updateScale(Scalar newScale) {
			m_arrowPositionsFloat.clear();
			m_arrowIndices.clear();
			m_scale = newScale;
			for(uint i = 0; i < m_vertexPositionsFloat.size(); i += 4) {
				VectorType start(m_vertexPositionsFloat[i], m_vertexPositionsFloat[i+1]);
				VectorType end = start + m_scale*m_vectors[i / 4];
				m_vertexPositionsFloat[i + 2] = end.x;
				m_vertexPositionsFloat[i + 3] = end.y;
				makeArrow(start, end);
			}
			m_vertexBuffer.setSubData(0, m_vertexPositionsFloat);
			m_arrowVertexBuffer.setSubData(0, m_arrowPositionsFloat);
		}

		template <typename VectorType>
		void DrawableVectors2D<VectorType>::draw(const Magnum::Matrix3 &transformation, Magnum::SceneGraph::Camera2D &camera) {
			m_pShader->setColor(m_color)
			        .setTransformationProjectionMatrix(camera.projectionMatrix()*transformation)
							.draw(m_vectorMesh);
			m_pShader->setColor(m_color)
			        .setTransformationProjectionMatrix(camera.projectionMatrix()*transformation)
							.draw(m_arrowMesh);
		}

		template class DrawableVectors2D<Vector2>;
		template class DrawableVectors2D<Vector2D>;
	}
}
