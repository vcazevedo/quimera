#include "Corrade/Containers/ArrayViewStl.h"
#include "DrawableObjects/SelectableDrawableMesh2D.h"
#include "Corrade/Containers/ArrayViewStl.h"

namespace Chimera {
	namespace Rendering {
		#pragma region Constructor
		template <typename MeshType>
		SelectableDrawableMesh2D<typename MeshType>::SelectableDrawableMesh2D(shared_ptr<Mesh<MeshType>> pMesh,
																												shared_ptr<Magnum::Object2D> pObjTransform,
																												shared_ptr<Magnum::SceneGraph::DrawableGroup2D> pDrawableGroup,
																												const Magnum::Color3 &baseColor,
																												const Magnum::Color3 &selectedColor)
			: ParentClass(pMesh, pObjTransform, pDrawableGroup, baseColor), m_selectedColor(selectedColor) {
			if constexpr (isVector2<typename MeshType::VectorType>::value) {
				initializeSelectedVertex();
				initializeSelectedEdge();
				initializeSelectedCell();
			}

			m_selectedDrawingMode = selectedDrawingModes_t(selectedDrawingMode_t::dontDraw);
		}
		#pragma endregion

		#pragma region AccessFunctions
		template <typename MeshType>
		void SelectableDrawableMesh2D<typename MeshType>::setSelectedVertex(shared_ptr<Vertex<VectorType>> pVertex) {
			m_selectedVertex.vertexData[0].first.x() = pVertex->getPosition().x;
			m_selectedVertex.vertexData[0].first.y() = pVertex->getPosition().y;
			m_selectedVertex.vertexData[0].second = m_selectedColor;

			m_selectedVertex.vertexBuffer.setData(m_selectedVertex.vertexData, Magnum::GL::BufferUsage::DynamicDraw);
		}

		template <typename MeshType>
		void SelectableDrawableMesh2D<typename MeshType>::setSelectedEdge(shared_ptr<Edge<VectorType>> pEdge) {
			m_selectedEdge.vertexData.front().first.x() = pEdge->getHalfEdges().first->getVertices().first->getPosition().x;
			m_selectedEdge.vertexData.front().first.y() = pEdge->getHalfEdges().first->getVertices().first->getPosition().y;
			m_selectedEdge.vertexData.front().second = m_selectedColor;

			m_selectedEdge.vertexData.back().first.x() = pEdge->getHalfEdges().first->getVertices().second->getPosition().x;
			m_selectedEdge.vertexData.back().first.y() = pEdge->getHalfEdges().first->getVertices().second->getPosition().y;
			m_selectedEdge.vertexData.back().second = m_selectedColor;

			m_selectedEdge.vertexBuffer.setData(m_selectedEdge.vertexData, Magnum::GL::BufferUsage::DynamicDraw);
		}

		template <typename MeshType>
		void SelectableDrawableMesh2D<typename MeshType>::setSelectedCell(shared_ptr<Cell<VectorType>> pCell) {
			m_selectedCell.vertexData.clear();
			auto pHalfEdges = pCell->getHalfCells().first->getHalfEdges();
			map<uint, uint> verticesIDMap;

			/** First initialize vertices and their mapping */
			for (auto pHalfEdge : pHalfEdges) {
				auto pFirstVertex = pHalfEdge->getVertices().first;
				if (verticesIDMap.find(pFirstVertex->getID()) == verticesIDMap.end()) {
					pair<Magnum::Vector2, Magnum::Color3> vertexEntry;
					vertexEntry.first.x() = pFirstVertex->getPosition().x;
					vertexEntry.first.y() = pFirstVertex->getPosition().y;
					vertexEntry.second = m_selectedColor;
					verticesIDMap[pFirstVertex->getID()] = m_selectedCell.vertexData.size();
					m_selectedCell.vertexData.push_back(vertexEntry);
				}

				auto pSecondVertex = pHalfEdge->getVertices().second;
				if (verticesIDMap.find(pSecondVertex->getID()) == verticesIDMap.end()) {
					pair<Magnum::Vector2, Magnum::Color3> vertexEntry;
					vertexEntry.first.x() = pSecondVertex->getPosition().x;
					vertexEntry.first.y() = pSecondVertex->getPosition().y;
					vertexEntry.second = m_selectedColor;
					verticesIDMap[pSecondVertex->getID()] = m_selectedCell.vertexData.size();
					m_selectedCell.vertexData.push_back(vertexEntry);
				}
			}

			m_selectedCell.vertexBuffer.setData(m_selectedCell.vertexData, Magnum::GL::BufferUsage::DynamicDraw);

			m_selectedCell.indexData.clear();

			/** Then initialize indices */
			for (auto pHalfEdge : pHalfEdges) {
				m_selectedCell.indexData.push_back(verticesIDMap[pHalfEdge->getVertices().first->getID()]);
				m_selectedCell.indexData.push_back(verticesIDMap[pHalfEdge->getVertices().second->getID()]);
			}

			m_selectedCell.indexBuffer.setData(m_selectedCell.indexData);
		}
		#pragma endregion

		#pragma region Initialization
		template <typename MeshType>
		void SelectableDrawableMesh2D<typename MeshType>::initializeSelectedVertex() {
			m_selectedVertex.vertexData.resize(1);
			setSelectedVertex(ParentClass::m_pMesh->getVertices().front());

			m_selectedVertex.pMagnumMesh = make_shared<Magnum::GL::Mesh>();
			m_selectedVertex.pMagnumMesh->setPrimitive(Magnum::GL::MeshPrimitive::Points).addVertexBuffer(m_selectedVertex.vertexBuffer, 0, Magnum::Shaders::VertexColor2D::Position{}, Magnum::Shaders::VertexColor2D::Color3{});
			m_selectedVertex.pMagnumMesh->setCount(1);
		}

		template <typename MeshType>
		void SelectableDrawableMesh2D<typename MeshType>::initializeSelectedEdge() {
			m_selectedEdge.vertexData.resize(2);
			m_selectedEdge.indexData.resize(2);
			m_selectedEdge.indexData[0] = 0;
			m_selectedEdge.indexData[1] = 1;
			m_selectedEdge.indexBuffer.setData(m_selectedEdge.indexData);

			shared_ptr<Edge<VectorType>> pFirstEdge;
			if constexpr (isElementPrimitiveQuadCell<typename MeshType::ElementPrimitiveType>::value) {
				Grids::GridMesh<MeshType>* pGridMesh = dynamic_cast<Grids::GridMesh<MeshType>*>(ParentClass::m_pMesh.get());
				pFirstEdge = pGridMesh->getEdges().front();
			} else {
				pFirstEdge = ParentClass::m_pMesh->getElements().front();
			}

			setSelectedEdge(pFirstEdge);

			m_selectedEdge.pMagnumMesh = make_shared<Magnum::GL::Mesh>();
			m_selectedEdge.pMagnumMesh->setPrimitive(Magnum::GL::MeshPrimitive::Lines).addVertexBuffer(m_selectedEdge.vertexBuffer, 0, Magnum::Shaders::VertexColor2D::Position{}, Magnum::Shaders::VertexColor2D::Color3{});
			m_selectedEdge.pMagnumMesh->setIndexBuffer(m_selectedEdge.indexBuffer, 0, Magnum::GL::MeshIndexType::UnsignedInt).setCount(2);
		}

		template <typename MeshType>
		void SelectableDrawableMesh2D<typename MeshType>::initializeSelectedCell() {

			shared_ptr<Cell<VectorType>> pFirstCell;

			if constexpr (isElementPrimitiveQuadCell<typename MeshType::ElementPrimitiveType>::value) {
				Grids::GridMesh<MeshType>* pGridMesh = dynamic_cast<Grids::GridMesh<MeshType>*>(ParentClass::m_pMesh.get());
				pFirstCell = pGridMesh->getElements().front()->getCells().front();
			}

			setSelectedCell(pFirstCell);

			m_selectedCell.pMagnumMesh = make_shared<Magnum::GL::Mesh>();
			m_selectedCell.pMagnumMesh->setPrimitive(Magnum::GL::MeshPrimitive::Lines).addVertexBuffer(m_selectedCell.vertexBuffer, 0, Magnum::Shaders::VertexColor2D::Position{}, Magnum::Shaders::VertexColor2D::Color3{});
			m_selectedCell.pMagnumMesh->setIndexBuffer(m_selectedCell.indexBuffer, 0, Magnum::GL::MeshIndexType::UnsignedInt).setCount(m_selectedCell.indexBuffer.size());
		}
		#pragma endregion

		#pragma region Functionalities
		template <typename MeshType>
		void SelectableDrawableMesh2D<MeshType>::draw(const Magnum::Matrix3 &transformation, Magnum::SceneGraph::Camera2D &camera) {
			ParentClass::draw(transformation, camera);

			if (m_selectedDrawingMode & selectedDrawingMode_t::drawSelectedVertex) {
				Magnum::GL::Renderer::setPointSize(ParentClass::m_vertexSize*2);
				ParentClass::m_vertexColorShader.setTransformationProjectionMatrix(camera.projectionMatrix() * transformation).draw(*m_selectedVertex.pMagnumMesh);
			}
			if (m_selectedDrawingMode & selectedDrawingMode_t::drawSelectedEdge) {
				Magnum::GL::Renderer::setLineWidth(ParentClass::m_edgeSize * 4);
				ParentClass::m_vertexColorShader.setTransformationProjectionMatrix(camera.projectionMatrix() * transformation).draw(*m_selectedEdge.pMagnumMesh);
			}
			if (m_selectedDrawingMode & selectedDrawingMode_t::drawSelectedCell) {
				Magnum::GL::Renderer::setLineWidth(ParentClass::m_edgeSize * 3);
				ParentClass::m_vertexColorShader.setTransformationProjectionMatrix(camera.projectionMatrix() * transformation).draw(*m_selectedCell.pMagnumMesh);
			}
		}
		#pragma endregion

		template class SelectableDrawableMesh2D<LineMeshType<Vector2>>;
		template class SelectableDrawableMesh2D<LineMeshType<Vector2D>>;

		template class SelectableDrawableMesh2D<Grids::QuadGridType>;
	}
}
