#include "DrawableObjects/DrawableText2D.h"

#include <Magnum/GL/Renderer.h>
#include <Corrade/Utility/FormatStl.h>
#include <Corrade/Utility/Resource.h>

namespace Chimera {
	namespace Rendering {

		#pragma region Constructor
		DrawableText2D::DrawableText2D(const string& text, const Magnum::Vector2& position,
																			shared_ptr<Magnum::Object2D> pParent,
																			shared_ptr<Magnum::SceneGraph::DrawableGroup2D> pDrawableGroup, 
																			const string& fontFilepath,
																			const Magnum::Color3& baseColor /* = 0xd1d1d1_rgbf */) : 
																			Magnum::Object2D{pParent.get()}, Magnum::SceneGraph::Drawable2D(*this, pDrawableGroup.get()),
																			m_baseColor(baseColor){
			m_pFont = ResourceManager::getInstance()->loadFont(fontFilepath);

			std::tie(m_textMesh, std::ignore) = Magnum::Text::Renderer2D::render(*m_pFont->getMagnumFont(), *m_pFont->getMagnumCache(), 0.01f, text, m_vertices, m_indices,
																																						Magnum::GL::BufferUsage::StaticDraw, Magnum::Text::Alignment::MiddleCenter);

			this->translate(position);
		}

		void DrawableText2D::draw(const Magnum::Matrix3& transformation, Magnum::SceneGraph::Camera2D& camera) {
			m_shader.bindVectorTexture(m_pFont->getMagnumCache()->texture());
			
			Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::Blending);
			Magnum::GL::Renderer::setBlendFunction(Magnum::GL::Renderer::BlendFunction::One, Magnum::GL::Renderer::BlendFunction::OneMinusSourceAlpha);
			Magnum::GL::Renderer::setBlendEquation(Magnum::GL::Renderer::BlendEquation::Add, Magnum::GL::Renderer::BlendEquation::Add);

			m_shader
				.setTransformationProjectionMatrix(camera.projectionMatrix() * transformation)
				.setColor(m_baseColor)
				.setSmoothness(0.025f)
				.draw(m_textMesh);

			Magnum::GL::Renderer::disable(Magnum::GL::Renderer::Feature::Blending);

		}
		#pragma endregion
	}
}
