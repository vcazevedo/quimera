//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#include "GLRenderer2D.h"

namespace Chimera {
	namespace Rendering {

		#pragma region Constructors
		template <typename FlowSolverType>
		GLRenderer2D<FlowSolverType>::GLRenderer2D(shared_ptr<FlowSolver<FlowSolverType>> pFlowSolver) {
			m_pFlowSolver = pFlowSolver;

			m_drawLiquidObjects = m_drawSolidObjects = true;
			m_pParticlesRenderer = nullptr;

			initGLFW();

			int display_w, display_h;
			glfwGetFramebufferSize(m_pWindow, &display_w, &display_h);

			ParentClass::m_windowWidth = ParentClass::m_screenWidth;
			ParentClass::m_windowHeight = ParentClass::m_screenHeight;
			
			if constexpr (isVector2<VectorType>::value) {
				m_pCamera = make_shared<Camera>(orthogonal2D);
			}
			else {
				m_pCamera = make_shared<Camera>(perspective3D);
			}

			m_pGridRenderers.first = 0;
			m_pGridRenderers.second.push_back(make_shared<GridRenderer<GridMeshType>>(pFlowSolver->getSimulationGrid()));
			m_pGridRenderers.second.push_back(make_shared<GridRenderer<GridMeshType>>(pFlowSolver->getLevelSet()->getGrid()));

		}
		#pragma endregion

		#pragma region Callbacks
		template <typename FlowSolverType>
		void GLRenderer2D<FlowSolverType>::keyboardCallback(unsigned char key, int x, int y) {
				//if (!TwEventKeyboardGLUT(key, x, y)) 
					ParentClass::keyboardCallback(key, x, y);
		}

		template <typename FlowSolverType>
		void GLRenderer2D<FlowSolverType>::mouseCallback(int button, int state, int x, int y) {
			//if (!TwEventMouseButtonGLUT(button, state, x, y))
				m_pCamera->mouseCallback(button, state, x, y);
		}
		#pragma endregion

		#pragma region Functionalities
		template <typename FlowSolverType>
		void GLRenderer2D<FlowSolverType>::renderLoop() {
			glfwPollEvents();

			// Start the Dear ImGui frame
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

			ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.
			ImGui::End();
			ImGui::Render();

			int display_w, display_h;
			glfwGetFramebufferSize(m_pWindow, &display_w, &display_h);
			glViewport(0, 0, display_w, display_h);

			ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
			glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
			glClear(GL_COLOR_BUFFER_BIT);
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

			m_pCamera->updateGL();

			// Setup modelview matrix
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			glPushMatrix();

			/** Draws selected grid*/
			m_pGridRenderers.second[m_pGridRenderers.first]->draw();

			glPopMatrix();

			glfwSwapBuffers(m_pWindow);
		}

		template <typename FlowSolverType>
		void GLRenderer2D<FlowSolverType>::update(Scalar dt) {
			if (m_pParticlesRenderer) {
				m_pParticlesRenderer->update(dt);
			}
			//updateWindows();
		}
		#pragma endregion

		#pragma region InitializationFunctions
		//template <typename FlowSolverType>
		//void GLRenderer2D<FlowSolverType>::initGLGlut() {
		//	int argc;  char **argv;
		//	argc = 0;
		//	argv = nullptr;

		//	glutInit(&argc, argv);
		//	int screenWidth = glutGet(GLUT_SCREEN_WIDTH);
		//	int screenHeight = glutGet(GLUT_SCREEN_HEIGHT);
		//	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
		//	glutInitWindowSize(screenWidth, screenHeight);
		//	glutCreateWindow("Fluids Renderer");

		//	GLenum err = glewInit();
		//	if (GLEW_OK != err) {
		//		Logger::get() << "GLEW initialization error! " << endl;
		//		exit(1);
		//	}

		//	///**GLUT and GLEW initialization */
		//	//const char* GLVersion = (const char*)glGetString(GL_VERSION);
		//	//Logger::get() << "OpenGL version: " << GLVersion << endl;

		//	//glClearColor(1.0f, 1.0f, 1.0f, 1.0);

		//	IMGUI_CHECKVERSION();
		//	ImGui::CreateContext();
		//	ImGuiIO& io = ImGui::GetIO(); (void)io;

		//	ImGui_ImplGLUT_Init();
		//	ImGui_ImplGLUT_InstallFuncs();
		//	ImGui_ImplOpenGL2_Init();
		//}

		static void glfw_error_callback(int error, const char* description)
		{
			fprintf(stderr, "Glfw Error %d: %s\n", error, description);
		}

		template <typename FlowSolverType>
		void GLRenderer2D<FlowSolverType>::initGLFW() {
			int argc;  char** argv;
			argc = 0;
			argv = nullptr;
			// Setup window
			glfwSetErrorCallback(glfw_error_callback);
			if (!glfwInit())
				return;

			const char* glsl_version = "#version 130";
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

			// Create window with graphics context
			m_pWindow = glfwCreateWindow(1280, 720, "Dear ImGui GLFW+OpenGL3 example", NULL, NULL);
			if (m_pWindow == NULL)
				return;

			glfwMakeContextCurrent(m_pWindow);
			glfwSwapInterval(1); // Enable vsync

			GLenum err = glewInit();
			if (GLEW_OK != err) {
				Logger::get() << "GLEW initialization error! " << endl;
				exit(1);
			}

			// Setup Dear ImGui context
			IMGUI_CHECKVERSION();
			ImGui::CreateContext();
			ImGuiIO& io = ImGui::GetIO(); (void)io;
			//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
			//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

			// Setup Dear ImGui style
			ImGui::StyleColorsDark();
			//ImGui::StyleColorsClassic();

			// Setup Platform/Renderer bindings
			ImGui_ImplGlfw_InitForOpenGL(m_pWindow, true);
			ImGui_ImplOpenGL3_Init(glsl_version);
		}

		
		#pragma endregion
	
		template class GLRenderer2D<VoxelizedGridSolverType2D>;
	}	
}
