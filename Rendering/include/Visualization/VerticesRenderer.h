//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.


#ifndef _CHIMERA_VERTICES_RENDERER_H_
#define _CHIMERA_VERTICES_RENDERER_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "ChimeraMesh.h"

#include "DrawableObjects/SelectableDrawableMesh2D.h"
#include "DrawableObjects/DrawableText2D.h"

#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/ImGuiIntegration/Context.hpp>

namespace Chimera {

	using namespace Grids;
	
	namespace Rendering {

		typedef Magnum::SceneGraph::Scene<Magnum::SceneGraph::MatrixTransformation2D> Scene2D;
		using Scene3D = Magnum::SceneGraph::Scene<Magnum::SceneGraph::MatrixTransformation3D>;
		typedef Magnum::SceneGraph::Object<Magnum::SceneGraph::MatrixTransformation2D> Object2D;
		typedef Magnum::SceneGraph::Object<Magnum::SceneGraph::MatrixTransformation3D> Object3D;
		/** Flow Variables renderer base class. Only makes sense for quad-cells,
			since HexaVolumes cannot be visualized properly with scalar field v
			visualization */
		template <typename GridMeshType>
		class VerticesRenderer {
		protected:
			//Useful definitions
			using VectorType = typename GridMeshType::VectorType;

		public:

			#pragma region Constructors
			VerticesRenderer(const string& verticesRendererName, shared_ptr<SelectableDrawableMesh2D<GridMeshType>> pDrawableMesh, shared_ptr<Magnum::Scene2D> pScene, shared_ptr<Magnum::SceneGraph::Camera2D> pCamera);
			#pragma endregion

			#pragma region Functionalities
			void drawMenu();
			void draw();
			bool mousePressEvent(Magnum::Platform::Sdl2Application::MouseEvent& event, Magnum::Vector2i pWindowSize);
			#pragma endregion

			#pragma region AccessFunctions
			void setSelectedVertex(int selectedVertex) {
				/*if (m_dualMeshes.find(m_selectedVertex) != m_dualMeshes.end()) {
					auto pPreviousDualCell = m_dualMeshes[m_selectedVertex];
					if (pPreviousDualCell) {
						pPreviousDualCell->setDrawingMode(drawingMeshMode_t::dontDraw);
					}
				}*/
				m_selectedVertex = selectedVertex;
			}
			#pragma endregion

		

		protected:

			#pragma region ClassMembers
			shared_ptr<Magnum::Scene2D> m_pScene;
			shared_ptr<Magnum::SceneGraph::Camera2D> m_pCamera;
			shared_ptr<Magnum::Object2D> m_pObject;
			
			string m_verticesRendererName;

			shared_ptr<SelectableDrawableMesh2D<GridMeshType>> m_pDrawableMesh;

			/** Initialize dual meshes as needed, and not at the beginning */
			//map<uint, shared_ptr<SelectableDrawableMesh2D<GridMeshType>>> m_dualMeshes;

			/** Drawable Group */
			shared_ptr<Magnum::SceneGraph::DrawableGroup2D> m_pDrawableGroup;

			/** Labels Drawable Group */
			shared_ptr<Magnum::SceneGraph::DrawableGroup2D> m_pDrawableGroupLabels;

			map<uint, shared_ptr<DrawableText2D>> m_verticesIDsText;

			bool m_drawVertices;
			bool m_drawSelectedVertex;
			bool m_drawVerticesIDs;
			int m_selectedVertex;
			bool m_isSolid;
			bool m_hasDualElement;
			Scalar m_auxVorticity;
			Scalar m_vorticity;
			Scalar m_streamfunction;
			#pragma endregion
		};

	}
}
#endif
