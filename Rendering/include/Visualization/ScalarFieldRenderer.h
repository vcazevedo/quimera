//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.


#ifndef _CHIMERA_SCALARFIELD_RENDERER_H_
#define _CHIMERA_SCALARFIELD_RENDERER_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "ChimeraMesh.h"

#include "Visualization/GridRenderer.h"

namespace Chimera {
	//using namespace Meshes;
	//using namespace IO;

	namespace Rendering {

		typedef Magnum::SceneGraph::Scene<Magnum::SceneGraph::MatrixTransformation2D> Scene2D;
		using Scene3D = Magnum::SceneGraph::Scene<Magnum::SceneGraph::MatrixTransformation3D>;
		typedef Magnum::SceneGraph::Object<Magnum::SceneGraph::MatrixTransformation2D> Object2D;
		typedef Magnum::SceneGraph::Object<Magnum::SceneGraph::MatrixTransformation3D> Object3D;
		/** Flow Variables renderer base class. Only makes sense for quad-cells,
			since HexaVolumes cannot be visualized properly with scalar field v
			visualization */
		template <typename GridMeshType>
		class ScalarFieldRenderer/* : public GridRenderer<GridMeshType>*/ {
		protected:
			//Useful definitions
			using VectorType = typename GridMeshType::VectorType;
			
		public:
			enum scalarColorScheme_t {
				singleColor,
				viridis,
				jet,
				grayscale,
				randomColors
			};

			#pragma region Constructors
			ScalarFieldRenderer(const string& scalarFieldRendererName, shared_ptr<GridMesh<GridMeshType>> pGridMesh, shared_ptr<Scene2D> pScene, shared_ptr<Magnum::SceneGraph::Camera2D> pCamera);
			#pragma endregion

			#pragma region AccessFunctions
			FORCE_INLINE scalarColorScheme_t & getScalarColorScheme() {
				return m_scalarColorScheme;
			}
			#pragma endregion
			
			#pragma region Functionalities
			void update();
			void draw();
			void drawMenu();
			#pragma endregion

			#pragma region ColorFunctionalities
			/** CPU-implemented shaders */
			static Vector3 viridisColorMap(Scalar scalarValue, Scalar minValue, Scalar maxValue);
			static Vector3 jetColorMap(Scalar scalarValue, Scalar minValue, Scalar maxValue);
			static Vector3 grayScaleColorMap(Scalar scalarValue, Scalar minValue, Scalar maxValue);
			#pragma endregion
			
		protected:
			#pragma region ClassMembers
			scalarColorScheme_t m_scalarColorScheme;
			shared_ptr<QuadGridMesh<GridMeshType>> m_pGridMesh;
			unique_ptr<DrawableMesh2D<GridMeshType>> m_pDrawableGrid;
			shared_ptr<Scene2D> m_pScene;
			shared_ptr<Magnum::SceneGraph::Camera2D> m_pCamera;
			vector<string> m_vertexScalarFieldNames;
			shared_ptr<Magnum::SceneGraph::DrawableGroup2D> m_pDrawableGroup;
			
			shared_ptr<Object2D> m_pObject2D;
			shared_ptr<unordered_map<uint, shared_ptr<Cell<VectorType>>>> m_pVertexIDToCellMap;

			string m_scalarFieldRendererName;
			bool m_visible = false;
			uint m_selectedField = 0;
			uint m_oldSelectedField = 0;
			pair<Scalar, Scalar> m_minMax;
			#pragma endregion
		};

	}
}
#endif
