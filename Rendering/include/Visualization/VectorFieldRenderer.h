//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.


#ifndef _CHIMERA_VECTOR_FIELD_RENDERER_H_
#define _CHIMERA_VECTOR_FIELD_RENDERER_H_
#pragma once


#include "ChimeraCore.h"
#include "ChimeraGrids.h"
//#include "ChimeraResources.h"
//Windows cross reference
//#include "Windows/BaseWindow.h"
#include "Magnum/SceneGraph/Scene.h"
#include "Magnum/SceneGraph/Camera.h"
#include "Magnum/SceneGraph/Drawable.h"
#include "Magnum/SceneGraph/MatrixTransformation2D.h"
#include "Magnum/Math/Matrix3.h"
#include "Magnum/Shaders/Flat.h"
#include "Magnum/GL/Mesh.h"
#include "Magnum/MeshTools/Compile.h"
#include "Magnum/Trade/MeshData.h"
#include "Magnum/Primitives/Line.h"
#include "Magnum/ImGuiIntegration/Context.hpp"

#include "DrawableObjects/DrawableVectors2D.h"

namespace Chimera {

	namespace Rendering {
		
		//using namespace IO;
		using namespace Grids;
		typedef Magnum::SceneGraph::Scene<Magnum::SceneGraph::MatrixTransformation2D> Scene2D;
		using Scene3D = Magnum::SceneGraph::Scene<Magnum::SceneGraph::MatrixTransformation3D>;
		typedef Magnum::SceneGraph::Object<Magnum::SceneGraph::MatrixTransformation2D> Object2D;
		typedef Magnum::SceneGraph::Object<Magnum::SceneGraph::MatrixTransformation3D> Object3D;

		template<typename VectorType>
		class VectorFieldGroup {
			public:
				VectorFieldGroup() {};
				vector<string> m_idToName;
				unordered_map<string, uint> m_nameToID;
				//vector<unique_ptr<VectorFieldElements<VectorType>>> m_vectorFieldElements;
				vector<unique_ptr<DrawableVectors2D<VectorType>>> m_vectorFieldElements;
				vector<shared_ptr<Magnum::SceneGraph::DrawableGroup2D>> m_drawableGroups;
				uint m_offset;
				uint m_size;
		};

		/** Flow Variables renderer base class */
		template <typename GridMeshType>
		class VectorFieldRenderer {
		protected:
			using VectorType = typename GridMeshType::VectorType;

		public:
			#pragma region Constructors
			VectorFieldRenderer(const string& vectorFieldRendererName, shared_ptr<GridMesh<GridMeshType>> pGridMesh, shared_ptr<Scene2D> pScene, shared_ptr<Magnum::SceneGraph::Camera2D> pCamera);
			#pragma endregion
			
			#pragma region AccessFunctions
			#pragma endregion
			

			#pragma region Functionalities
			/** This has to be called whenever the internal rendering attributes have changed */
			void update();
			void draw();
			void drawMenu();
			#pragma endregion
			

		protected:
			#pragma region ClassMembers
			
			/** Grid that will be rendered*/
			shared_ptr<GridMesh<GridMeshType>> m_pGridMesh;
			shared_ptr<Scene2D> m_pScene;
			shared_ptr<Magnum::SceneGraph::Camera2D> m_pCamera;

			VectorFieldGroup<VectorType> m_nodalVectorFields;
			VectorFieldGroup<VectorType> m_edgeVectorFields;
			VectorFieldGroup<VectorType> m_faceVectorFields;
			VectorFieldGroup<VectorType> m_voxelVectorFields;
			shared_ptr<Magnum::Object2D> m_pObject2D;

			shared_ptr<Magnum::Shaders::Flat2D> m_pFlatShader;
			
			string m_vectorFieldRendererName;
			bool m_visible = false;
			vector<string> m_vectorFieldNames;
			Scalar m_vectorScale = 0.1;
			int m_selectedField = 0;
			Scalar m_time = 0.0;
			Magnum::Color3 m_vectorColor;

			#pragma region Initialization
			void initializeVectors();
			void initializeVertexVectorFields();
			void initializeEdgeVectorFields();
			void initializeFaceVectorFields();
			void initializeVolumeVectorFields();
			#pragma endregion

			#pragma region Drawing
			/** Velocity field */
			//void drawCellCenteredVelocityField(bool auxVelocity = false, dimensions_t kthSlices = dimensions_t(-1, -1, -1)) const;
			//void drawStaggeredVelocityField() const;
			//void drawGridArrows(int kthSlice = -1) const;
			#pragma endregion 
			
			#pragma region Updating
			
			//void updateVelocity(bool auxiliaryVel = false) const ;
			//void updateVelocityArrows() const;
			//void synchronizeVelocities(const string& velocityName);
			//void synchronizeCellCenters();
			#pragma endregion 
		};

	}
}
#endif
