//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef _CHIMERA_RENDERING_UTILS_
#define _CHIMERA_RENDERING_UTILS_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "Windows/BaseWindow.h"
#include "Primitives/ColorShader.h"

namespace Chimera {
	using namespace Core;
	using namespace Windows;

	namespace Rendering {

		class RenderingUtils : public Singleton<RenderingUtils> {

		public:
			#pragma region Constructors
			RenderingUtils() :	m_viridisColorShader(viridis), 
													m_jetColorShader(jet), 
													m_grayScaleColorShader(grayscale) {

			}
			#pragma endregion
			#pragma region Drawing
			void drawVectorArrow(const Vector2 &vectorArrow, Scalar arrowSize);
			void drawVectorArrow(const Vector3 &vectorArrow, Scalar arrowSize);
			template<class VectorType>
			void drawVectorT(const VectorType &startingPoint, const VectorType &endingPoint, isVector2False, Scalar arrowLenght = 0.02);
			template<class VectorType>
			void drawVectorT(const VectorType &startingPoint, const VectorType &endingPoint, isVector2True, Scalar arrowLenght = 0.02);

			template<class VectorType>
			void drawVector(const VectorType &startingPoint, const VectorType &endingPoint, Scalar arrowLenght = 0.02) {
				drawVectorT(startingPoint, endingPoint, isVector2<VectorType>(), arrowLenght);
			}
			#pragma endregion

			#pragma region Functionalities
			void applyColorShader(Scalar minValue, Scalar maxValue, Scalar avgValue, scalarColorScheme_t colorScheme) {
				switch (colorScheme) {
					case viridis:
						m_viridisColorShader.applyShader(minValue, maxValue, avgValue);
					break;

					case jet:
						m_jetColorShader.applyShader(minValue, maxValue, avgValue);
					break;

					case grayscale:
						m_grayScaleColorShader.applyShader(minValue, maxValue, avgValue);
					break;
				}
			}

			void removeColorShader(scalarColorScheme_t colorScheme) {
				switch (colorScheme) {
				case viridis:
					m_viridisColorShader.removeShader();
					break;

				case jet:
					m_jetColorShader.removeShader();
					break;

				case grayscale:
					m_grayScaleColorShader.removeShader();
					break;
				}
			}
			#pragma endregion

		protected:
			#pragma region InternalClasses
			struct vectorParams_t {
				Scalar	arrowSize;
				Scalar	lineWidth;
				Scalar	scale;
				Color	color;

				vectorParams_t() {
					arrowSize = 0.1f;
					lineWidth = 1.0f;
					scale = 1.0f;
					color = Color(0, 0, 0);
				}
			};
			#pragma endregion

			#pragma region ClassMembers
			/** Shaders data*/
			ColorShader m_viridisColorShader;
			ColorShader m_jetColorShader;
			ColorShader m_grayScaleColorShader;
			#pragma endregion
		};
	}

}
#endif
