//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef _RENDERING_FLOW_SOLVER_RENDERER_H
#define _RENDERING_FLOW_SOLVER_RENDERER_H

#pragma once
#include "ChimeraCore.h"
#include "ChimeraSolvers.h"
#include "ChimeraGrids.h"

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif


#include "Visualization/GridRenderer.h"
#include "Visualization/VectorFieldRenderer.h"
#include "Visualization/ScalarFieldRenderer.h"
#include "Visualization/VerticesRenderer.h"
#include "Visualization/EdgesRenderer.h"

#include "DrawableObjects/SelectableDrawableMesh2D.h"
#include "Visualization/MeshesRenderer.h"

#include <Magnum/ImGuiIntegration/Context.hpp>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/GL/Buffer.h>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/Math/Color.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/MeshTools/Interleave.h>
#include <Magnum/MeshTools/CompressIndices.h>
#include <Magnum/MeshTools/GenerateIndices.h>
#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/Primitives/Cube.h>
#include <Magnum/Shaders/Phong.h>
#include <Magnum/Trade/MeshData.h>

#include <Magnum/ImGuiIntegration/Context.hpp>
#ifdef CORRADE_TARGET_ANDROID
#include <Magnum/Platform/AndroidApplication.h>
#elif defined(CORRADE_TARGET_EMSCRIPTEN)
#include <Magnum/Platform/EmscriptenApplication.h>
#else
#include <Magnum/Platform/Sdl2Application.h>
#endif

using namespace std;

namespace Chimera {

	namespace Rendering {

		template <typename SolverType>
		class FlowSolverRenderer : public Magnum::Platform::Application {
		protected:
			using VectorType = typename SolverType::VectorType;
			using EmbeddedMeshType = typename SolverType::EmbeddedMeshType;
			using GridMeshType = typename SolverType::GridMeshType;

		public:
			#pragma region Constructors
			explicit FlowSolverRenderer(shared_ptr<FlowSolver<SolverType>> pFlowSolver, const Magnum::Platform::Application::Arguments& arguments);
			#pragma endregion

			#pragma region Events
			virtual void drawEvent();
			void drawMenu();
			void update();

			void viewportEvent(ViewportEvent& event) override;

			void keyPressEvent(KeyEvent& event) override;
			void keyReleaseEvent(KeyEvent& event) override;

			void mousePressEvent(MouseEvent& event) override;
			void mouseReleaseEvent(MouseEvent& event) override;
			void mouseMoveEvent(MouseMoveEvent& event) override;
			void mouseScrollEvent(MouseScrollEvent& event) override;
			void textInputEvent(TextInputEvent& event) override;

			void addMeshRenderer(const vector<shared_ptr<Mesh<typename SolverType::EmbeddedMeshType>>>& meshes) {
				m_pMeshesRenderer = make_shared<MeshesRenderer<typename SolverType::EmbeddedMeshType>>("meshesRenderer", meshes, m_pScene, m_pObjCamera, m_pCamera);
			}
			#pragma endregion

			
		protected:
			#pragma region ClassMembers
			Magnum::ImGuiIntegration::Context m_imgui{ Magnum::NoCreate };
			Magnum::Color4 m_clearColor;

			/** 2-D Scene Configuration*/
			shared_ptr<Magnum::Scene2D> m_pScene;
			shared_ptr<Magnum::Object2D> m_pObjCamera;
			shared_ptr<Magnum::SceneGraph::Camera2D> m_pCamera;

			/** Input configuration */
			Magnum::Float m_mouseMaxSensivity = 0.01;
			Magnum::Float m_mouseMinSensivity = 0.00001;
			Magnum::Float m_mouseSensitivity;
			Magnum::Float m_zDistance;
			Magnum::Float m_minZDistance;
			Magnum::Float m_maxZDistance;
			Magnum::Vector2 gridCentroid;

			/** Flow Solver */
			shared_ptr<FlowSolver<SolverType>> m_pFlowSolver;

			/** Grid Renderer */
			map<string, shared_ptr<GridRenderer<GridMeshType>>> m_gridRenderers;

			/** Vector field renderer */
			map<string, shared_ptr<VectorFieldRenderer<GridMeshType>>> m_vectorFieldRenderers;

			/** Scalar field renderer */
			map<string, shared_ptr<ScalarFieldRenderer<GridMeshType>>> m_scalarFieldRenderers;

			/** Vertex field renderer */
			map<string, shared_ptr<VerticesRenderer<GridMeshType>>> m_verticesRenderers;

			/** Vertex field renderer */
			map<string, shared_ptr<EdgesRenderer<GridMeshType>>> m_edgesRenderers;
			
			/** Mesh renderer */
			shared_ptr<MeshesRenderer<typename SolverType::EmbeddedMeshType>> m_pMeshesRenderer;

			shared_ptr<QuadGridMesh<GridMeshType>> m_pQuadGridMesh;

			shared_ptr<Magnum::Object2D> m_pObject;
			shared_ptr<Magnum::SceneGraph::DrawableGroup2D> m_pDrawableGroup;

			Magnum::Shaders::Flat2D m_flatShader{ Magnum::NoCreate };
			Magnum::GL::Mesh m_testMesh{ Magnum::NoCreate };

			bool m_runSimulation;

			Scalar m_timeStep;

			#pragma endregion

			#pragma region Initialization
			virtual void initGL() { };
			#pragma endregion
		};
	}
}

#endif
