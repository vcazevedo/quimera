//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef _RENDERING_BASE_RENDERER_H_
#define _RENDERING_BASE_RENDERER_H_

#pragma once

#include "ChimeraCore.h"

#include "Primitives/Camera.h"

#include "imgui/imgui.h"
#include "imgui/examples/imgui_impl_glut.h"
#include "imgui/examples/imgui_impl_glfw.h"
#include "imgui/examples/imgui_impl_opengl3.h"

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

#include "Visualization/LineMeshRenderer.h"
#include "Visualization/ScalarFieldRenderer.h"
#include "Visualization/PolygonMeshRenderer.h"


using namespace std;

namespace Chimera {

	using namespace Windows;
	namespace Rendering { 
		
		class BaseGLRenderer : public UpdateInterface {
		
		public:

			#pragma region Constructors
			explicit BaseGLRenderer() {
				m_windowWidth = m_windowHeight = 0;

			}

			//Destructor
			virtual ~BaseGLRenderer() {

			}
			#pragma endregion
		
			#pragma region Callbacks
			/**A keyboard callback, that is util when assigning keyboard entries for
			renderization parameters modifications.
			@param key: the pushed key*/
			virtual void keyboardCallback(unsigned char key, int x, int y) {
				//if (!TwEventKeyboardGLUT(key, x, y)) 
					m_pCamera->keyboardCallback(key, x, y);
			}

			/**A mouse callback, that is util when assigning mouse callbacks for
			renderization parameters modifications.
			@param button: mouse button ID
			@param state: mouse button state
			@param x: mouse poisition according to the x axis.
			@param y: mouse poisition according to the x axis. */
			virtual void mouseCallback(int button, int state, int x, int y) {
				//if(!TwEventMouseButtonGLUT(button, state, x, y))
					m_pCamera->mouseCallback(button, state, x, y);
			}

			/**A mouse motion callback, that is util when assigning mouse callbacks for
			renderization parameters modifications.
			@param x: mouse poisition according to the x axis.
			@param y: mouse poisition according to the x axis. */
			virtual void motionCallback(int x, int y) {
				//if(!TwEventMouseMotionGLUT(x, y))
					m_pCamera->motionCallback(x, y);
			}

			/** Reshape callback*/
			void reshapeCallback(int width, int height) {
				m_windowWidth = width;
				m_windowHeight = height;
				m_pCamera->reshapeCallback(width, height);
				glViewport(0, 0, width, height);
			}
			#pragma endregion
			
			#pragma region AccessFunctions
			FORCE_INLINE int getScreenWidth() const {
				return m_screenWidth;
			}
			
			FORCE_INLINE int getScreenHeight() const {
				return m_screenHeight;
			}

			FORCE_INLINE shared_ptr<Camera> getCamera() const {
				return m_pCamera;
			}
			#pragma endregion

			#pragma region Functionalities
			virtual void renderLoop() = 0;
			#pragma endregion
		
		protected:
			
			#pragma region ClassMembers
			/** The openGL window width */
			Scalar	m_windowWidth;
			/** The openGL window height */
			Scalar	m_windowHeight;

			/** Current full screen width and size */
			int m_screenWidth;
			int m_screenHeight;

			shared_ptr<Camera> m_pCamera;
			#pragma endregion

			#pragma region Initialization
			virtual void initGL() { };
			#pragma endregion	
		};
	}
}

#endif
