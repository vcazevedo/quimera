#ifndef _CHIMERA_DRAWABLE_TEXT_H
#define _CHIMERA_DRAWABLE_TEXT_H
#pragma once

#include "ChimeraCore.h"
#include "ChimeraIO.h"

#include <Corrade/Containers/Pointer.h>
#include <Corrade/PluginManager/Manager.h>

#include <Magnum/Math/Color.h>

#include <Magnum/GL/Mesh.h>

#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/SceneGraph/MatrixTransformation2D.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/Camera.h>

#include <Magnum/Shaders/DistanceFieldVector.h>

#include <Magnum/Text/AbstractFont.h>
#include <Magnum/Text/DistanceFieldGlyphCache.h>
#include <Magnum/Text/Renderer.h>


namespace Magnum {
	using Object2D = SceneGraph::Object<SceneGraph::MatrixTransformation2D>;
	using Scene2D = SceneGraph::Scene<SceneGraph::MatrixTransformation2D>;
}

namespace Chimera {

	using namespace Core;
	namespace Rendering {

		using namespace Magnum::Math::Literals;
		using namespace IO;

		class DrawableText2D : public Magnum::Object2D, Magnum::SceneGraph::Drawable2D {
			public:
				#pragma region Constructor
				explicit DrawableText2D(const string& text, const Magnum::Vector2& position,
																	shared_ptr<Magnum::Object2D> pParent,
																	shared_ptr<Magnum::SceneGraph::DrawableGroup2D> pDrawableGroup,
																	const string &fontFilepath = "Resources/SourceSansPro-Regular.ttf",
																	const Magnum::Color3 &baseColor = 0xd1d1d1_rgbf);
				#pragma endregion
				
				#pragma region Functionalities
				void draw(const Magnum::Matrix3& transformation, Magnum::SceneGraph::Camera2D& camera) override;
				#pragma endregion
				

				#pragma region AccessFunctions
				
				#pragma endregion
				
				
			private:
				
				#pragma region ClassMembers
				shared_ptr<Font> m_pFont;
				shared_ptr<Magnum::Text::DistanceFieldGlyphCache> m_cache;
				Magnum::Color3 m_baseColor;

				Magnum::GL::Mesh m_textMesh{ Magnum::NoCreate };
				Magnum::GL::Buffer m_vertices, m_indices;
				Magnum::Shaders::DistanceFieldVector2D m_shader;

				string m_text;
				#pragma endregion
		};
	}
}

#endif
