#ifndef _CHIMERA_DRAWABLE_MESH_H
#define _CHIMERA_DRAWABLE_MESH_H
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"

//Magnum includes
#include <Corrade/Containers/Pointer.h>

#include <Magnum/Magnum.h>
#include <Magnum/Mesh.h>
#include <Magnum/Math/Color.h>

#include <Magnum/GL/Buffer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/Renderer.h>

#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/SceneGraph/MatrixTransformation2D.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/Camera.h>

#include <Magnum/Shaders/VertexColor.h>

namespace Magnum {
	using Object2D = SceneGraph::Object<SceneGraph::MatrixTransformation2D>;
	using Scene2D = SceneGraph::Scene<SceneGraph::MatrixTransformation2D>;
}

namespace Chimera {

	using namespace Core;
	namespace Rendering {

		/** Different modes that can mutually activated for rendering the mesh */
		enum class drawingMeshMode_t : uint {
			dontDraw = 0 << 0,
			drawVertices = 1 << 0,
			drawEdges = 1 << 1,
			drawCells = 1 << 2
		};
		typedef Corrade::Containers::EnumSet<drawingMeshMode_t> drawingMeshModes_t;

		using namespace Magnum::Math::Literals;

		template <typename MeshType>
		class DrawableMesh2D : public Magnum::SceneGraph::Drawable2D {
			using VectorType = typename MeshType::VectorType;

			public:
				#pragma region Constructor
				/** Standard constructor, a mesh is passed and the internal magnum meshes are initialized */
				explicit DrawableMesh2D(shared_ptr<Mesh<MeshType>> pMesh, shared_ptr<Magnum::Object2D> pObjTransform, shared_ptr<Magnum::SceneGraph::DrawableGroup2D> pDrawableGroup,
																	const Magnum::Color3 &baseColor = 0xd1d1d1_rgbf);
				#pragma endregion
				
				#pragma region Functionalities
				void draw(const Magnum::Matrix3& transformation, Magnum::SceneGraph::Camera2D& camera) override;
				#pragma endregion
				
				#pragma region AccessFunctions
				void setDrawingMode(const drawingMeshModes_t &drawingMeshMode) {
					m_drawingMeshMode = drawingMeshMode;
				}

				void addDrawingMode(const drawingMeshModes_t& drawingMeshMode) {
					m_drawingMeshMode |= drawingMeshMode;
				}

				void removeDrawingMode(const drawingMeshModes_t& drawingMeshMode) {
					m_drawingMeshMode &= ~drawingMeshMode;
				}

				vector<pair<Magnum::Vector2, Magnum::Color3>>& getVertexData() {
					return m_vertexData;
				}

				shared_ptr<Mesh<MeshType>> getMesh() {
					return m_pMesh;
				}

				Scalar & getVertexSize() {
					return m_vertexSize;
				}
				
				Scalar & getEdgeSize() {
					return m_edgeSize;
				}
				#pragma endregion
								
			protected:

				#pragma region Initialization
				void initializeMagnumMeshes();
				#pragma endregion

				#pragma region ClassMembers
				shared_ptr<Mesh<MeshType>> m_pMesh;
				drawingMeshModes_t m_drawingMeshMode;
				
				Magnum::Color3 m_baseColor;

				/** Per vertex color shader*/
				Magnum::Shaders::VertexColor2D m_vertexColorShader;

				shared_ptr<Magnum::GL::Mesh> m_pMagnumMeshVertices;
				shared_ptr<Magnum::GL::Mesh> m_pMagnumMeshEdges;
				shared_ptr<Magnum::GL::Mesh> m_pMagnumMeshCells;
				
				vector<pair<Magnum::Vector2, Magnum::Color3>> m_vertexData;
				Magnum::GL::Buffer m_vertexBuffer;
				Magnum::GL::Buffer m_indexBuffer;

				Scalar m_vertexSize;
				Scalar m_edgeSize;

				/** Mapping from Chimera's vertices ID to internal mesh ID*/
				map<uint, uint> m_verticesIDMap;
				#pragma endregion
		};
	}
}

#endif
