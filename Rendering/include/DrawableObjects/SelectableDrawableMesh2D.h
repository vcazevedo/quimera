#ifndef _CHIMERA_SELECTABLE_DRAWABLE_MESH_H
#define _CHIMERA_SELECTABLE_DRAWABLE_MESH_H
#pragma once

#include <Corrade/Containers/Pointer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/SceneGraph/MatrixTransformation2D.h>
#include <Magnum/Shaders/Flat.h>

#include "ChimeraCore.h"
#include "ChimeraGrids.h"

#include "DrawableObjects/DrawableMesh2D.h"

namespace Magnum {
	using Object2D = SceneGraph::Object<SceneGraph::MatrixTransformation2D>;
	using Scene2D = SceneGraph::Scene<SceneGraph::MatrixTransformation2D>;
}

namespace Chimera {

	using namespace Core;
	namespace Rendering {

		/** Different modes that can mutually activated for rendering the mesh */
		enum class selectedDrawingMode_t : uint {
			dontDraw = 0 << 0,
			drawSelectedVertex = 1 << 0,
			drawSelectedEdge = 1 << 1,
			drawSelectedCell = 1 << 2
		};

		typedef Corrade::Containers::EnumSet<selectedDrawingMode_t> selectedDrawingModes_t;

		using namespace Magnum::Math::Literals;

		template <typename MeshType>
		class SelectableDrawableMesh2D : public DrawableMesh2D<MeshType> {
			using VectorType = typename MeshType::VectorType;
			using ParentClass = DrawableMesh2D<MeshType>;

			public:
				#pragma region Constructor

				/** Standard constructor, a mesh is passed and the internal magnum meshes are initialized */
				explicit SelectableDrawableMesh2D(shared_ptr<Mesh<MeshType>> pMesh, shared_ptr<Magnum::Object2D> pObjTransform, shared_ptr<Magnum::SceneGraph::DrawableGroup2D> pDrawableGroup,
																					const Magnum::Color3 &baseColor = 0xd1d1d1_rgbf, const Magnum::Color3 &selectedColor = 0xff484a_rgbf);
				#pragma endregion
				
				#pragma region Functionalities
				void draw(const Magnum::Matrix3& transformation, Magnum::SceneGraph::Camera2D& camera) override;
				#pragma endregion
				

				#pragma region AccessFunctions
				void setDrawingMode(const selectedDrawingModes_t&drawingMeshMode) {
					m_selectedDrawingMode = drawingMeshMode;
				}

				void addDrawingMode(const selectedDrawingModes_t&drawingMeshMode) {
					m_selectedDrawingMode |= drawingMeshMode;
				}

				void removeDrawingMode(const selectedDrawingModes_t &drawingMeshMode) {
					m_selectedDrawingMode &= ~drawingMeshMode;
				}

				/** Selection functions */
				void setSelectedVertex(shared_ptr<Vertex<VectorType>> pVertex);

				void setSelectedEdge(shared_ptr<Edge<VectorType>> pEdge);

				void setSelectedCell(shared_ptr<Cell<VectorType>> pCell);
				#pragma endregion
				
				
			private:
				
				#pragma region Initialization
				void initializeSelectedVertex();
				void initializeSelectedEdge();
				void initializeSelectedCell();
				#pragma endregion

				#pragma region ClassMembers
				selectedDrawingModes_t m_selectedDrawingMode;
				Magnum::Color3 m_selectedColor;

				struct selectedMeshElement_t {
					Magnum::GL::Buffer vertexBuffer;
					Magnum::GL::Buffer indexBuffer;

					shared_ptr<Magnum::GL::Mesh> pMagnumMesh;
					vector<pair<Magnum::Vector2, Magnum::Color3>> vertexData;
					vector<Magnum::UnsignedInt> indexData;

					selectedMeshElement_t() : pMagnumMesh(nullptr) {
					}
				};

				selectedMeshElement_t m_selectedVertex;
				selectedMeshElement_t m_selectedEdge;
				selectedMeshElement_t m_selectedCell;
				#pragma endregion
		};
	}
}

#endif
