#ifndef _CHIMERA_DRAWABLE_VECTORS2D_H
#define _CHIMERA_DRAWABLE_VECTORS2D_H
#pragma once

#include "ChimeraCore.h"
#include "ChimeraGrids.h"
#include "Magnum/Math/Color.h"
#include "Magnum/SceneGraph/Scene.h"
#include "Magnum/SceneGraph/Camera.h"
#include "Magnum/SceneGraph/Drawable.h"
#include "Magnum/SceneGraph/MatrixTransformation2D.h"
#include "Magnum/Math/Matrix3.h"
#include "Magnum/Shaders/Flat.h"
#include "Magnum/GL/Mesh.h"
#include "Magnum/MeshTools/Compile.h"
#include "Magnum/Trade/MeshData.h"
#include "Magnum/Primitives/Line.h"
#include "Magnum/ImGuiIntegration/Context.hpp"

namespace Magnum {
	using Object2D = SceneGraph::Object<SceneGraph::MatrixTransformation2D>;
	using Scene2D = SceneGraph::Scene<SceneGraph::MatrixTransformation2D>;
}

namespace Chimera {
	using namespace Core;
	namespace Rendering {
		template <typename VectorType>
		class DrawableVectors2D : public Magnum::SceneGraph::Drawable2D, public Magnum::Object2D {
			public:
				explicit DrawableVectors2D(
						vector<VectorType> &positions,
						vector<VectorType> &vectorsAtPosition,
						Scalar scale,
						shared_ptr<Magnum::Scene2D> pScene,
						shared_ptr<Magnum::Shaders::Flat2D> pShader,
						shared_ptr<Magnum::Object2D> pObject,
						Magnum::Color3 color,
						shared_ptr<Magnum::SceneGraph::DrawableGroup2D> pDrawableGroup);
				void updateVectors(vector<VectorType> &vectorsAtPosition);
				void updateScale(Scalar newScale);
				void draw(const Magnum::Matrix3& transformation, Magnum::SceneGraph::Camera2D& camera) override;
			private:
				void makeArrow(VectorType start, VectorType end);

				Scalar m_scale;
				vector<VectorType> m_vectors;
				vector<Magnum::Float> m_vertexPositionsFloat;
				vector<Magnum::UnsignedInt> m_vertexIndices;
				vector<Magnum::Float> m_arrowPositionsFloat;
				vector<Magnum::UnsignedInt> m_arrowIndices;
				Magnum::GL::Buffer m_vertexBuffer, m_indexBuffer, m_arrowVertexBuffer, m_arrowIndexBuffer;
				Magnum::GL::Mesh m_vectorMesh, m_arrowMesh;
				//Magnum::Object2D m_vectorObject, m_arrowObject;
				Magnum::Color3 m_color;
				shared_ptr<Magnum::Shaders::Flat2D> m_pShader;
				//unique_ptr<Magnum::FlatShadeObject2D> m_flatVectors, m_flatArrows;
		};
	}
}

#endif
