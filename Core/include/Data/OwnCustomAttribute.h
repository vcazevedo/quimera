//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_CUSTOM_ATTRIBUTE_H__
#define __CHIMERA_CUSTOM_ATTRIBUTE_H__

#include "Config/ChimeraConfig.h"
#include "Data/ChimeraStructures.h"
#include "Data/Array2D.h"
#include "Data/Array3D.h"

#pragma once

namespace Chimera {
	using namespace std;
	namespace Core {

		/** Classes can inherit this class for having custom attribute interfaces and members functionalities added to 
			them. Multiple different attribute types are added through multiple inheritance.*/
		template <class AttributeClassT>
		class OwnCustomAttribute {

		public:
			#pragma region Constructors
			OwnCustomAttribute() { }


			OwnCustomAttribute(const string &attributesName, const AttributeClassT &attribute) {
				setAttribute(attributesName, attribute);
			}

			OwnCustomAttribute(const vector<string> &attributesNames, const vector<AttributeClassT> &attributes) {
				for (uint i = 0; i < attributesNames.size(); i++) {
					setAttribute(attributesNames[i], attributes[i]);
				}
			}

			#pragma endregion

			#pragma region AccessFunctions
			
			/** Adds an attributes based an empty constructor */
			void addAttributes(const vector<string> &attributeNames) {
				for (auto attributeName : attributeNames) {
					string lowerCase(attributeName);
					transform(lowerCase.begin(), lowerCase.end(), lowerCase.begin(), ::tolower);
					m_attributes[lowerCase] = AttributeClassT();
				}		
			}

			/** Returns attribute by name. Does not error treat, in favor of efficiency */
			AttributeClassT & getAttribute(const string &attributeName) {
				string lowerCase(attributeName);
				transform(lowerCase.begin(), lowerCase.end(), lowerCase.begin(), ::tolower);
				return m_attributes[lowerCase];
			}

			/** Returns attribute by name. Does not error treat, in favor of efficiency */
			const AttributeClassT & getAttribute(const string & attributeName) const {
				string lowerCase(attributeName);
				transform(lowerCase.begin(), lowerCase.end(), lowerCase.begin(), ::tolower);
				return m_attributes.at(lowerCase);
			}

			/** Sets attribute by name. It converts strings to lower case, to reduce mismatching. 
					Does not error treat, in favor of efficiency*/
			void setAttribute(const string & attributeName, const AttributeClassT &attribute) {
				string lowerCase(attributeName);
				transform(lowerCase.begin(), lowerCase.end(), lowerCase.begin(), ::tolower);
				m_attributes[lowerCase] = attribute;
			}

			void incrementAttribute(const string& attributeName, const AttributeClassT& attribute) {
				string lowerCase(attributeName);
				transform(lowerCase.begin(), lowerCase.end(), lowerCase.begin(), ::tolower);
				m_attributes[lowerCase] += attribute;
			}

			virtual bool hasAttribute(const string & attributeName) {
				string lowerCase(attributeName);
				transform(lowerCase.begin(), lowerCase.end(), lowerCase.begin(), ::tolower);
				if (m_attributes.find(lowerCase) == m_attributes.end())
					return false;
				return true;
			}

			/** Clears attributes */
			virtual void clearAttributes() {
				m_attributes.clear();
			}

			const map<string, AttributeClassT> & getAttributesMap() const {
				return m_attributes;
			}
			
			map<string, AttributeClassT> & getAttributesMap() {
				return m_attributes;
			}
			
			#pragma endregion

			#pragma region Functionalities
			template <class ParentClassT>
			static OwnCustomAttribute<AttributeClassT> * get(shared_ptr<ParentClassT> pParent) {
				return dynamic_cast<OwnCustomAttribute<AttributeClassT> *>(pParent.get());
			}
			template <class ParentClassT>
			static OwnCustomAttribute<AttributeClassT>* get(ParentClassT * pParent) {
				return dynamic_cast<OwnCustomAttribute<AttributeClassT>*>(pParent);
			}
			#pragma endregion
		protected:
			#pragma region ClassMembers
			/* Custom attributes map*/
			map<string, AttributeClassT> m_attributes;
			#pragma endregion
		};

		template <class AttributeClassT>
		using OwnCustomVectorAttribute = OwnCustomAttribute<vector<AttributeClassT>>;

		template <class AttributeClassT>
		using OwnCustomArray2DAttribute = OwnCustomAttribute<Array2D<AttributeClassT>>;

		template <class AttributeClassT>
		using OwnCustomArray3DAttribute = OwnCustomAttribute<Array3D<AttributeClassT>>;
	}
}
#endif
