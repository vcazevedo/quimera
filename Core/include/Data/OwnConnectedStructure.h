//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_CUSTOM_CONNECTED_STRUCTURE_H__
#define __CHIMERA_CUSTOM_CONNECTED_STRUCTURE_H__

#include "Config/ChimeraConfig.h"
#include "Data/ChimeraStructures.h"
#include "Data/Array2D.h"
#include "Data/Array3D.h"

#pragma once

namespace Chimera {
	using namespace std;
	namespace Core {

		/** Classes can inherit this class for having custom connected structure interfaces and members 
				functionalities added to  them. Multiple different attribute types are added through multiple 
				inheritance.*/
		template <class StructureClassT>
		class OwnConnectedStructure {

		public:
			#pragma region Constructors
			OwnConnectedStructure() { }
			#pragma endregion

			#pragma region AccessFunctions
			void addConnectedStructure(uint structureID, weak_ptr<StructureClassT> pStructure) {
				m_connectedStructures[structureID] = pStructure;
			}

			vector<shared_ptr<StructureClassT>> getConnectedStructures() {
				vector<shared_ptr<StructureClassT>> connectedStructuresVec;

				for (auto iter = m_connectedStructures.begin(); iter != m_connectedStructures.end(); iter++) {
					connectedStructuresVec.push_back(iter->second.lock());
				}

				return connectedStructuresVec;
			}

			void clearConnectedStructures() {
				m_connectedStructures.clear();
			}
			#pragma endregion

			#pragma region Functionalities
			template <class ParentClassT>
			static OwnConnectedStructure<StructureClassT> * get(shared_ptr<ParentClassT> pParent) {
				return dynamic_cast<OwnConnectedStructure<StructureClassT> *>(pParent.get());
			}
			template <class ParentClassT>
			static OwnConnectedStructure<StructureClassT>* get(ParentClassT * pParent) {
				return dynamic_cast<OwnConnectedStructure<StructureClassT>*>(pParent);
			}
			#pragma endregion

		protected:
			#pragma region ClassMembers
			/* Custom attributes map*/
			map<uint, weak_ptr<StructureClassT>> m_connectedStructures;
			#pragma endregion
		};
	}
}
#endif
