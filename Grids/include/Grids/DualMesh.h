#ifndef __CHIMERA_DUALMESH__
#define __CHIMERA_DUALMESH__
#pragma once


#include "ChimeraCore.h"
#include "ChimeraMesh.h"
#include "Grids/QuadGridMesh.h"

using namespace std;
namespace Chimera {

	using namespace Core;
	using namespace Meshes;

	namespace Interpolation {

		/** Forward declaration of the interpolant */
		template <class valueType, typename MeshType>
		class Interpolant;
	}

	namespace Grids {

		//
		template<typename GridMeshType>
		class DualMesh : public LineMesh<typename GridMeshType::VectorType> {
		
			protected:
			using VectorType = typename GridMeshType::VectorType;
			using ElementPrimitiveType = typename GridMeshType::ElementPrimitiveType;

		public:

			#pragma region Constructor
			DualMesh(shared_ptr<QuadGridMesh<GridMeshType>> pQuadGridMesh);
			DualMesh(shared_ptr<QuadGridMesh<GridMeshType>> pQuadGridMesh, solverType_t solver);
			#pragma endregion

			#pragma region Functionalities
			void addGhostEdge(shared_ptr<Vertex<VectorType>> pDualVertex, shared_ptr<Edge<VectorType>> pPrimalEdge);

			vector<shared_ptr<Cell<VectorType>>> getCells() {
				return m_cells;
			}
			#pragma endregion
		
		protected:
			#pragma region ClassMembers
			vector<shared_ptr<Cell<VectorType>>> m_cells;
			#pragma endregion

			#pragma PrivateFunctionalities
			void visitConnectedEdges(shared_ptr<Vertex<VectorType>> pInitialVertex, shared_ptr<Edge<VectorType>> pCurrEdge, vector<shared_ptr<Edge<VectorType>>> connectedEdges, Scalar gridSpacing = 0, solverType_t solverType = voxelizedSolver);
			#pragma endregion
		};
	}
}

#endif
