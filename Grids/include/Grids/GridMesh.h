//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_GRIDMESH__
#define __CHIMERA_GRIDMESH__
#pragma once


#include "ChimeraCore.h"
#include "ChimeraMesh.h"

using namespace std;
namespace Chimera {

	using namespace Core;
	using namespace Meshes;

	namespace Interpolation {

		/** Forward declaration of the interpolant */
		template <class valueType, typename MeshType>
		class Interpolant;
	}

	namespace Grids {


		template<typename MeshType>
		class GridMesh : public Mesh<MeshType>,
			public enable_shared_from_this<GridMesh<MeshType>> {
		protected:
			typedef typename MeshType::VectorType VectorType;
			typedef typename MeshType::ElementPrimitiveType ElementPrimitiveType;
			using ParentClass = Mesh<MeshType>;

		public:

#pragma region Constructors
			/** Create a regular grid with pre-specified dimensions. The grid is generated according with initial and final
			 ** points. This type of grid is always non-periodic. */
			GridMesh(const VectorType& initialPoint, const VectorType& finalPoint, Scalar gridSpacing) {
				ParentClass::m_bounds.first = initialPoint;
				ParentClass::m_bounds.second = finalPoint;

				m_gridSpacing = gridSpacing;

				VectorType boundariesLength(finalPoint - initialPoint);

				m_gridDimensions.x = static_cast<int>(boundariesLength.x / gridSpacing);
				m_gridDimensions.y = static_cast<int>(boundariesLength.y / gridSpacing);
				if constexpr (isVector3<VectorType>::value) {
					m_gridDimensions.z = static_cast<int>(boundariesLength.z / gridSpacing);
				}
				else {
					m_gridDimensions.z = 0;
				}

				m_gridCentroid = boundariesLength * 0.5 + initialPoint;
			}

			GridMesh(Scalar gridSpacing, const dimensions_t& gridDimensions) {
				m_gridSpacing = gridSpacing;
				m_gridDimensions = gridDimensions;
			}
#pragma endregion

#pragma region IO
			virtual void exportToFile(const string& gridExportname) { };
#pragma endregion

#pragma AccessFunctions
			Scalar getGridSpacing() const {
				return m_gridSpacing;
			}

			dimensions_t getGridDimensions() {
				return m_gridDimensions;
			}

			void setGridDimensions(const dimensions_t& gridDimensions) {
				m_gridDimensions = gridDimensions;
			}

			const VectorType& getGridCentroid() const {
				return m_gridCentroid;
			}

			/** Calculates the grid space position, useful for interpolation */
			VectorType getGridSpacePosition(const VectorType &worldPosition) {
				return ((worldPosition - ParentClass::m_bounds.first) / m_gridSpacing);
			}
#pragma endregion

#pragma region CustomAttributes
			template<class AttributeClassT, class InterpolantClassT>
			shared_ptr<InterpolantClassT> addVerticesAttribute(const string& attributeName, const AttributeClassT& initialValue) {
				for (auto pVertex : ParentClass::m_vertices) {
					OwnCustomAttribute<AttributeClassT>::get(pVertex)->setAttribute(attributeName, initialValue);
				}
				/** Then we initialize the interpolation */
				/** We currently only support vector and scalar attributes per element*/
				/** The interpolant type (vector or scalar) is selected depending on the first argument of the interpolant function.
						This happens since staggered interpolants, for example, store scalar attributes on elements but the interpolation
						function returns a vector component from it. So the interpolant first template guides the way that this attribute
						is reconstructed continuously. */
				if constexpr (isVector<firstTemplateType<InterpolantClassT>>::value) {
					auto pInterpolant = make_shared<InterpolantClassT>(this->shared_from_this(), attributeName);
					addVectorFieldInterpolantVertices(pInterpolant);
					return pInterpolant;
				}
				else {
					auto pInterpolant = make_shared<InterpolantClassT>(this->shared_from_this(), attributeName);
					addScalarFieldInterpolantVertices(pInterpolant);
					return pInterpolant;
				}
			}

			/** Attribute adding functions: everytime an attribute is added, an interpolant for continuously sampling it in
					space has to be also defined. The interpolants are defined separately depending on the structure on which
					the attribute lies on */
			template<class AttributeClassT, class InterpolantClassT>
			shared_ptr<InterpolantClassT> addEdgesAttribute(const string& attributeName, const AttributeClassT& initialValue) {
				for (auto pEdge : m_edges) {
					OwnCustomAttribute<AttributeClassT>::get(pEdge)->setAttribute(attributeName, initialValue);
				}

				/** Edges require vector interpolation because they are structures with a direction */
				auto pInterpolant = make_shared<InterpolantClassT>(this->shared_from_this(), attributeName);
				addVectorFieldInterpolantEdges(pInterpolant);
				return pInterpolant;
			}
			template<class AttributeClassT, class InterpolantClassT>
			shared_ptr<InterpolantClassT> addCellsAttribute(const string& attributeName, const AttributeClassT& initialValue) {
				for (auto pCell : m_cells) {
					OwnCustomAttribute<AttributeClassT>::get(pCell)->setAttribute(attributeName, initialValue);
				}

				/** Then we initialize the interpolation */
				/** We currently only support vector and scalar attributes per element*/
				/** The interpolant type (vector or scalar) is selected depending on the first argument of the interpolant function.
						This happens since staggered interpolants, for example, store scalar attributes on elements but the interpolation
						function returns a vector component from it. So the interpolant first template guides the way that this attribute
						is reconstructed continuously. */
				if constexpr (isVector<firstTemplateType<InterpolantClassT>>::value) {
					auto pInterpolant = make_shared<InterpolantClassT>(this->shared_from_this(), attributeName);
					addVectorFieldInterpolantCells(pInterpolant);
					return pInterpolant;
				}
				else {
					auto pInterpolant = make_shared<InterpolantClassT>(this->shared_from_this(), attributeName);
					addScalarFieldInterpolantCells(pInterpolant);
					return pInterpolant;
				}
			}

			template<class AttributeClassT, class InterpolantClassT>
			shared_ptr<InterpolantClassT> addVolumesAttribute(const string& attributeName, const AttributeClassT& initialValue) {
				for (auto pCell : m_cells) {
					OwnCustomAttribute<AttributeClassT>::get(pCell)->setAttribute(attributeName, initialValue);
				}

				/** Then we initialize the interpolation */
				/** We currently only support vector and scalar attributes per element*/
				/** The interpolant type (vector or scalar) is selected depending on the first argument of the interpolant function.
						This happens since staggered interpolants, for example, store scalar attributes on elements but the interpolation
						function returns a vector component from it. So the interpolant first template guides the way that this attribute
						is reconstructed continuously. */
				if constexpr (isVector<firstTemplateType<InterpolantClassT>>::value) {
					auto pInterpolant = make_shared<InterpolantClassT>(this->shared_from_this(), attributeName);
					addVectorFieldInterpolantVolumes(pInterpolant);
					return pInterpolant;
				}
				else {
					auto pInterpolant = make_shared<InterpolantClassT>(this->shared_from_this(), attributeName);
					addScalarFieldInterpolantVolumes(pInterpolant);
					return pInterpolant;
				}
			}

			/** Getters use directly the attribute name, the location is on the method name */
			FORCE_INLINE shared_ptr<Interpolation::Interpolant<VectorType, MeshType>> getVectorFieldInterpolantVertices(const string& interpolantName) {
				const string fullInterpolantName("vertex" + interpolantName);
				return m_vectorFieldInterpolants[fullInterpolantName];
			}
			FORCE_INLINE shared_ptr<Interpolation::Interpolant<VectorType, MeshType>> getVectorFieldInterpolantCells(const string& interpolantName) {
				const string fullInterpolantName("cell" + interpolantName);
				return m_vectorFieldInterpolants[fullInterpolantName];
			}
			FORCE_INLINE shared_ptr<Interpolation::Interpolant<VectorType, MeshType>> getVectorFieldInterpolantEdges(const string& interpolantName) {
				const string fullInterpolantName("edge" + interpolantName);
				return m_vectorFieldInterpolants[fullInterpolantName];
			}
			FORCE_INLINE shared_ptr<Interpolation::Interpolant<VectorType, MeshType>> getVectorFieldInterpolantVolumes(const string& interpolantName) {
				const string fullInterpolantName("volume" + interpolantName);
				return m_vectorFieldInterpolants[fullInterpolantName];
			}

			/** Getters use directly the attribute name, the location is on the method name */
			FORCE_INLINE shared_ptr<Interpolation::Interpolant<Scalar, MeshType>> getScalarFieldInterpolantVertices(const string& interpolantName) {
				const string fullInterpolantName("vertex" + interpolantName);
				return m_scalarFieldInterpolants[fullInterpolantName];
			}
			FORCE_INLINE shared_ptr<Interpolation::Interpolant<Scalar, MeshType>> getScalarFieldInterpolantCells(const string& interpolantName) {
				const string fullInterpolantName("cell" + interpolantName);
				return m_scalarFieldInterpolants[fullInterpolantName];
			}
			FORCE_INLINE shared_ptr<Interpolation::Interpolant<Scalar, MeshType>> getScalarFieldInterpolantEdges(const string& interpolantName) {
				const string fullInterpolantName("edge" + interpolantName);
				return m_scalarFieldInterpolants[fullInterpolantName];
			}
			FORCE_INLINE shared_ptr<Interpolation::Interpolant<Scalar, MeshType>> getScalarFieldInterpolantVolumes(const string& interpolantName) {
				const string fullInterpolantName("volume" + interpolantName);
				return m_scalarFieldInterpolants[fullInterpolantName];
			}
#pragma endregion

			virtual vector<shared_ptr<Edge<VectorType>>>& getEdges() {
				return m_edges;
			}

			virtual const vector<shared_ptr<Edge<VectorType>>>& getEdges() const {
				return m_edges;
			}

			virtual vector<shared_ptr<Cell<VectorType>>>& getCells() {
				return m_cells;
			}
			virtual const vector<shared_ptr<Cell<VectorType>>>& getCells() const {
				return m_cells;
			}
			virtual shared_ptr<typename MeshType::ElementPrimitiveType> getGridCell(const dimensions_t& location) {
				return nullptr;
			};

			//TODO: implement this
			virtual shared_ptr<Vertex<VectorType>> getVertex(const dimensions_t& location) { return nullptr; }

			#pragma endregion


			
		protected:
			#pragma region ClassMembers
			/** Incorporated grid attributes */
			DoubleScalar m_gridSpacing;
			dimensions_t m_gridDimensions;

			VectorType m_gridCentroid;

			/** All edges, cells and volumes of this structure (vertices are already given by the mesh structure) */
			vector<shared_ptr<Edge<VectorType>>> m_edges;
			vector<shared_ptr<Cell<VectorType>>> m_cells;
			vector<shared_ptr<Volume<VectorType>>> m_volumes;

			/** Vector fields interpolants*/
			map<string, shared_ptr<Interpolation::Interpolant<VectorType, MeshType>>> m_vectorFieldInterpolants;

			/** Scalar fields interpolants*/
			map<string, shared_ptr<Interpolation::Interpolant<Scalar, MeshType>>> m_scalarFieldInterpolants;

			#pragma endregion


			#pragma region CustomInterpolants
			/** Interpolants that are used to continuously retrieve custom added GridMesh attributes. The interpolant
					name is preceded by the type of mesh element in which the attribute is stored. */
			FORCE_INLINE void addVectorFieldInterpolantVertices(shared_ptr<Interpolation::Interpolant<VectorType, MeshType>> pVectorInterpolant) {
				const string interpolantName("vertex" + pVectorInterpolant->getInterpolatedAttributeName());
				m_vectorFieldInterpolants[interpolantName] = pVectorInterpolant;
			}
			FORCE_INLINE void addVectorFieldInterpolantCells(shared_ptr<Interpolation::Interpolant<VectorType, MeshType>> pVectorInterpolant) {
				const string interpolantName("cell" + pVectorInterpolant->getInterpolatedAttributeName());
				m_vectorFieldInterpolants[interpolantName] = pVectorInterpolant;
			}
			FORCE_INLINE void addVectorFieldInterpolantEdges(shared_ptr<Interpolation::Interpolant<VectorType, MeshType>> pVectorInterpolant) {
				const string interpolantName("edge" + pVectorInterpolant->getInterpolatedAttributeName());
				m_vectorFieldInterpolants[interpolantName] = pVectorInterpolant;
			}
			FORCE_INLINE void addVectorFieldInterpolantVolumes(shared_ptr<Interpolation::Interpolant<VectorType, MeshType>> pVectorInterpolant) {
				const string interpolantName("volume" + pVectorInterpolant->getInterpolatedAttributeName());
				m_vectorFieldInterpolants[interpolantName] = pVectorInterpolant;
			}

			/** Interpolants that are used to continuously retrieve custom added GridMesh attributes. The interpolant
					name is preceded by the type of mesh element in which the attribute is stored. */
			FORCE_INLINE void addScalarFieldInterpolantVertices(shared_ptr<Interpolation::Interpolant<Scalar, MeshType>> pScalarInterpolant) {
				const string interpolantName("vertex" + pScalarInterpolant->getInterpolatedAttributeName());
				m_scalarFieldInterpolants[interpolantName] = pScalarInterpolant;
			}
			FORCE_INLINE void addScalarFieldInterpolantCells(shared_ptr<Interpolation::Interpolant<Scalar, MeshType>> pScalarInterpolant) {
				const string interpolantName("cell" + pScalarInterpolant->getInterpolatedAttributeName());
				m_scalarFieldInterpolants[interpolantName] = pScalarInterpolant;
			}
			FORCE_INLINE void addScalarFieldInterpolantEdges(shared_ptr<Interpolation::Interpolant<Scalar, MeshType>> pScalarInterpolant) {
				const string interpolantName("edge" + pScalarInterpolant->getInterpolatedAttributeName());
				m_scalarFieldInterpolants[interpolantName] = pScalarInterpolant;
			}
			FORCE_INLINE void addScalarFieldInterpolantVolumes(shared_ptr<Interpolation::Interpolant<Scalar, MeshType>> pScalarInterpolant) {
				const string interpolantName("volume" + pScalarInterpolant->getInterpolatedAttributeName());
				m_scalarFieldInterpolants[interpolantName] = pScalarInterpolant;
			}
			#pragma endregion

		
		};

	}
}

#endif
