//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_HEXA_GRID_MESH_3D_H__
#define __CHIMERA_HEXA_GRID_MESH_3D_H__

#pragma once

#include "ChimeraCore.h"
#include "ChimeraMesh.h"
#include "Grids/QuadGridMesh.h"


namespace Chimera {

	using namespace Meshes;

	namespace Grids {


		class QuadGridType3D {
		public:
			using VectorType = Vector3;
			using ElementPrimitiveType = Meshes::QuadCell<VectorType>;
			//template <class ArrayClass>
			using ArrayType = Array2D<VectorType>;
		};

		class QuadGridType3DDouble {
		public:
			using VectorType = Vector3D;
			using ElementPrimitiveType = Meshes::QuadCell<VectorType>;
			//template <class ArrayClass>
			using ArrayType = Array2D<VectorType>;
		};

		class HexaGridType {
		public:
			using VectorType = Vector3;
			using ElementPrimitiveType = Meshes::HexaVolume<VectorType>;
			using SubGridMeshType = Grids::QuadGridType3D;
			//template <class ArrayClass>
			using ArrayType = Array3D<VectorType>;
		};

		class HexaGridTypeDouble {
		public:
			using VectorType = Vector3D;
			using ElementPrimitiveType = Meshes::HexaVolume<VectorType>;
			using SubGridMeshType = Grids::QuadGridType3DDouble;
			//template <class ArrayClass>
			using ArrayType = Array3D<VectorType>;
		};


		template <typename T>
		struct isHexaGridType : false_type
		{
			static const bool value = false;
		};

		template <>
		struct isHexaGridType<HexaGridType> : true_type
		{
			static const bool value = true;
		};

		template <>
		struct isHexaGridType<HexaGridTypeDouble> : true_type
		{
			static const bool value = true;
		};

		template <typename HexaGridMeshType>
		class HexaGridMesh : public GridMesh<HexaGridMeshType> {

		protected:

			//Useful definitions
			typedef typename HexaGridMeshType::VectorType VectorType;
			typedef typename HexaGridMeshType::SubGridMeshType SubGridMeshType;
			using ParentClass = GridMesh<HexaGridMeshType>;
			using GrandParentClass = Mesh<HexaGridMeshType>;

			/** Shared_ptr vector of a Custom type shared_ptr */
			template<template <class> class CustomType>
			using VectorOffPtr = shared_ptr<vector<shared_ptr<CustomType<VectorType>>>>;

		public:

			#pragma region Constructors
			/** Create a regular grid with pre-specified dimensions. The grid is generated according with initial and final
			 ** points. This type of grid is always non-periodic. */
			HexaGridMesh(const VectorType& initialPoint, const VectorType& finalPoint, Scalar gridSpacing);

			/** This constructor builds a planar mesh with multiple initialized line meshes. */
			HexaGridMesh(const vector<shared_ptr<IntersectedPolyMesh<VectorType>>> &polygonalMeshes, const VectorType& initialPoint, const VectorType& finalPoint, Scalar gridSpacing);
			#pragma endregion

			#pragma region AccessFunctions
			/** Cut-Cell access functions */
			bool isCutVoxel(dimensions_t dimensions) {
				//Checks if the number of cells of this QuadCell is bigger than 1
				return (*m_pGridVolumes)(dimensions)->getVolumes().size() > 1;
			};
			bool isCutVoxel(int i, int j, int k) {
				//Checks if the number of cells of this QuadCell is bigger than 1
				return (*m_pGridVolumes)(i, j, k)->getVolumes().size() > 1;
			}
			/** Given a grid position, checks if it is a cut-cell. Position is in GRID SPACE */
			bool isCutVoxel(const VectorType &position) {
				return (*m_pGridVolumes)(floor(position.x), floor(position.y), floor(position.z))->getVolumes().size() > 1;
			}

			shared_ptr<HexaVolume<VectorType>> getHexaVolume(uint i, uint j, uint k) {
				return (*m_pGridVolumes)(i, j, k);
			}


			/** Cut-cells getters */
			uint getNumberGeometricCutVoxels() const {
				return m_geometricCutVolumes.size();
			}

			const Volume<VectorType>& getGeometricCutVoxel(uint cellIndex) const {
				return *m_geometricCutVolumes[cellIndex];
			}

			Volume<VectorType>& getGeometricCutVoxel(uint cellIndex) {
				return *m_geometricCutVolumes[cellIndex];
			}

			const Array3D<std::shared_ptr<Vertex<VectorType>>> & getNodalVerticesArray() const {
				return m_nodeVertices;
			}

			shared_ptr<Vertex<VectorType>> getVertex(uint i, uint j, uint k) {
				return m_nodeVertices(i, j, k);
			}

			shared_ptr<Vertex<VectorType>> getVertex(const dimensions_t& location) {
				return  m_nodeVertices(location.x, location.y, location.z);
			}

			shared_ptr<typename HexaGridMeshType::ElementPrimitiveType> getGridCell(const dimensions_t& gridCellIndex) override {
				return (*m_pGridVolumes)(gridCellIndex);
			}

			const map<uint, vector<shared_ptr<IntersectedLineMesh<VectorType>>>> & getXYLineMeshes() const {
				return m_XYLineMeshes;
			}

			const map<uint, vector<shared_ptr<IntersectedLineMesh<VectorType>>>> & getXZLineMeshes() const {
				return m_XZLineMeshes;
			}

			const map<uint, vector<shared_ptr<IntersectedLineMesh<VectorType>>>> & getYZLineMeshes() const {
				return m_YZLineMeshes;
			}


			const std::map<uint, std::unique_ptr<QuadGridMesh<SubGridMeshType>>> & getXYQuadGridMesh() const {
				return m_XYQuadGridMeshes;
			}

			const std::map<uint, std::unique_ptr<QuadGridMesh<SubGridMeshType>>> & getXZQuadGridMesh() const {
				return m_XZQuadGridMeshes;
			}

			const std::map<uint, std::unique_ptr<QuadGridMesh<SubGridMeshType>>> & getYZQuadGridMesh() const {
				return m_YZQuadGridMeshes;
			}

			/** Edges access functions */
			const vector<shared_ptr<Cell<VectorType>>>& getCells(dimensions_t cellIndex, cellLocation_t cellLocation) const {
				switch (cellLocation) {
					case XYPlane:
						return m_XYQuadGridMeshes.at(cellIndex.z)->getCells(cellIndex.x, cellIndex.y);
					break;
					case XZPlane:
						return m_XZQuadGridMeshes.at(cellIndex.y)->getCells(cellIndex.x, cellIndex.z);
					break;
					case YZPlane:
						return m_YZQuadGridMeshes.at(cellIndex.x)->getCells(cellIndex.z, cellIndex.y);
					break;

					default:
						throw(std::logic_error("HexaGridMesh::getCells invalid cellLocation option"));
				}
			}

			shared_ptr<Vertex<VectorType>> getNodalVertex(uint i, uint j, uint k) {
				return m_nodeVertices(i, j, k);
			}

			const vector<shared_ptr<Volume<VectorType>>>& getVolumes(uint i, uint j, uint k) {
				return (*m_pGridVolumes)(i, j, k)->getVolumes();
			}

			/** Line meshes */
			const vector<shared_ptr<IntersectedPolyMesh<VectorType>>> & getPolygonalMeshes() const {
				return m_polyMeshes;
			}

			template<class AttributeClassT>
			void addCellsAttribute(const string& attributeName, const AttributeClassT& initialValue) {
				for (uint i = 0; i <= ParentClass::m_gridDimensions.x; i++) {
					for (uint j = 0; j <= ParentClass::m_gridDimensions.y; j++) {
						for (uint k = 0; k <= ParentClass::m_gridDimensions.z; k++) {
							for (auto pCell : getCells(dimensions_t(i, j, k), YZPlane)) {
								OwnCustomAttribute<AttributeClassT>::get(pCell)->setAttribute(attributeName, initialValue);
							}

							for (auto pCell : getCells(dimensions_t(i, j, k), XZPlane)) {
								OwnCustomAttribute<AttributeClassT>::get(pCell)->setAttribute(attributeName, initialValue);
							}

							for (auto pCell : getCells(dimensions_t(i, j, k), XYPlane)) {
								OwnCustomAttribute<AttributeClassT>::get(pCell)->setAttribute(attributeName, initialValue);
							}
						}
					}
				}
			}
			#pragma endregion

			#pragma region Functionalities
			/** Reinitilize internal structures without deleting the pointer to this class*/
			void reinitialize(const vector<shared_ptr<IntersectedPolyMesh<VectorType>>> &polygonalMeshes);
			#pragma endregion
		protected:
			#pragma region ClassMembers
			/** HexaVolumes are non-manifold non-oriented face structures. In order to initialize their volumes, that
				correspond to cut-voxels on a grid, one has to individually split these half volumes
				This is done locally by each volume. */
			shared_ptr<Array3D<shared_ptr<HexaVolume<VectorType>>>> m_pGridVolumes;

			/** Pointers to volumes that are in contact with polygonal cells. Useful for solvers */
			vector<shared_ptr<Volume<VectorType>>> m_geometricCutVolumes;

			/** Polygonal meshes */
			vector<shared_ptr<IntersectedPolyMesh<VectorType>>> m_polyMeshes;

			/** Patches indices, accessible by the array structure. First index of the pair indicates the PolygonalMesh index,
				second is a vector containing all faces indices from a polygonal mesh that are contained inside the cell. */
			Array3D<vector<pair<uint, vector<uint>>>> m_polyPatches;

			/** Line meshes organized by slice index. Since multiple poly meshes are possible, we have to store the ones
			  * that are on the same slice on the same structure, so cut-cells can be properly initialized */
			map<uint, vector<shared_ptr<IntersectedLineMesh<VectorType>>>> m_XYLineMeshes;
			map<uint, vector<shared_ptr<IntersectedLineMesh<VectorType>>>> m_YZLineMeshes;
			map<uint, vector<shared_ptr<IntersectedLineMesh<VectorType>>>> m_XZLineMeshes;

			/** QuadGridMeshes: the first index indicates the index on the orthogonal dimension to the plane*/
			map<uint, unique_ptr<QuadGridMesh<SubGridMeshType>>> m_XYQuadGridMeshes;
			map<uint, unique_ptr<QuadGridMesh<SubGridMeshType>>> m_YZQuadGridMeshes;
			map<uint, unique_ptr<QuadGridMesh<SubGridMeshType>>> m_XZQuadGridMeshes;

			/** Edges: keeping horizontal and vertical names to match the 2-D version */
			/** X oriented edges */
			Array3D<VectorOffPtr<Edge>> m_horizontalEdges;
			/** Y oriented edges */
			Array3D<VectorOffPtr<Edge>> m_verticalEdges;
			/** Z oriented edges */
			Array3D<VectorOffPtr<Edge>> m_transversalEdges;
			/** Nodal vertices */
			Array3D<shared_ptr<Vertex<VectorType>>> m_nodeVertices;
			#pragma endregion

			#pragma region InitializationFunctions
			/** Initializes line patches inside each regular grid cell . Each pair added to polypatches stores the index
				of the current polymesh; the second vector stores the faces indices relative to that specific polymesh */
			void buildPolyPatches();

			/** Creates a single node vertex */
			FORCE_INLINE void createNodeVertex(const dimensions_t &cellIndex) {
				VectorType vertexPosition(cellIndex.x* ParentClass::m_gridSpacing,
																	cellIndex.y* ParentClass::m_gridSpacing,
																	cellIndex.z* ParentClass::m_gridSpacing);
				m_nodeVertices(cellIndex) = this->constructVertex(vertexPosition, gridVertex);
			}

			/** Creates vertices that are on top of grid nodes */
			void buildVertices();

			/** Verifies if faces on the face patch have vertices that are on top of grid nodes */
			void buildOnNodeVerticesFacesPatch(const dimensions_t &cellIndex);

			/** Organize all cut-cells lines into a single vector ordered by the slice index*/
			void buildLineMeshes();

			/** Creates cut-slices from line meshes computed from intersections of polyMeshes with the grid*/
			void buildQuadGridMeshes();

			/** Creates all grid faces, which will be subdivided in a later step. */
			void buildVolumes();
			#pragma endregion
		};
	}

}
#endif
