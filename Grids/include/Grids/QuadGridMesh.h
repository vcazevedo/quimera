//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_QUADGRIDMESH__
#define __CHIMERA_QUADGRIDMESH__
#pragma once


#include "ChimeraCore.h"
#include "ChimeraMesh.h"

#include "Grids/GridMesh.h"

#include <iostream>
#include <tuple>

using namespace std;
namespace Chimera {

	using namespace Core;
	using namespace Meshes;

	namespace Grids {

		class QuadGridType {
		public:
			using VectorType = Vector2;
			//This mesh element, the building block of QuadGrids
			using ElementPrimitiveType = Meshes::QuadCell<VectorType>;

			//template <class ArrayClass>
			using ArrayType = Array2D<VectorType>;
		};

		class QuadGridTypeDouble {
		public:
			using VectorType = Vector2D;
			using ElementPrimitiveType = Meshes::QuadCell<VectorType>;

			//template <class ArrayClass>
			using ArrayType = Array2D<VectorType>;
		};

		template <typename T>
		struct isQuadGridType : false_type
		{
			static const bool value = false;
		};

		template <>
		struct isQuadGridType<QuadGridType> : true_type
		{
			static const bool value = true;
		};

		template <>
		struct isQuadGridType<QuadGridTypeDouble> : true_type
		{
			static const bool value = true;
		};


		template <typename QuadGridMeshType>
		class QuadGridMesh :	public GridMesh<QuadGridMeshType> {
			protected:
			//Useful definitions
			using VectorType = typename QuadGridMeshType::VectorType;
			using ElementPrimitiveType = typename QuadGridMeshType::ElementPrimitiveType;
			using ParentClass = GridMesh<QuadGridMeshType>;
			using GrandParentClass = Mesh<QuadGridMeshType>;


			/** Shared_ptr vector of a Custom type shared_ptr */
			template<template <class> class CustomType>
			using VectorOffPtr = shared_ptr<vector<shared_ptr<CustomType<VectorType>>>>;

			/** Shared_ptr of Array2D of a Custom type shared_ptr */
			template <template <class> class CustomType>
			using Array2DOffPtr = shared_ptr<Array2D<shared_ptr<CustomType<VectorType>>>>;

			/** Shared_ptr of Array2D of a shared_ptr vector of a Custom type shared_ptr */
			template <template <class> class CustomType>
			using Array2DVectorOffPtr = shared_ptr<Array2D<VectorOffPtr<CustomType>>>;

			public:

			#pragma region External Structures
			// Encodes all necessary pointers and structures needed for class construction
			struct extStructures_t {

				/** Edges arrays */
				Array2DVectorOffPtr<Edge> pVerticalEdges;
				Array2DVectorOffPtr<Edge> pHorizontalEdges;
				Array2DOffPtr<Vertex> pNodeVertices;
				/* Min-max bounds of the grid */
				pair<VectorType, VectorType> bounds;

				extStructures_t() {
					pVerticalEdges = nullptr;
					pHorizontalEdges = nullptr;
					pNodeVertices = nullptr;
				}
			};
			#pragma endregion

			#pragma region Constructors
			/** Create a regular grid with pre-specified dimensions. The grid is generated according with initial and final
			 ** points. This type of grid is always non-periodic. */
			QuadGridMesh(const VectorType&initialPoint, const VectorType&finalPoint, Scalar gridSpacing, cellLocation_t faceLocation = XYPlane,
										const vector<shared_ptr<IntersectedLineMesh<VectorType>>> &lineMeshes = vector<shared_ptr<IntersectedLineMesh<VectorType>>>());

			/** External structures are passed so this class can properly initialize its quad-meshes using
					previously defined edge and nodal data. */
			QuadGridMesh(const extStructures_t& extStructures, Scalar gridSpacing, const dimensions_t& gridDimensions, cellLocation_t faceLocation = XYPlane,
										const vector<shared_ptr<IntersectedLineMesh<VectorType>>>& lineMeshes = vector<shared_ptr<IntersectedLineMesh<VectorType>>>());
			#pragma endregion

			#pragma region Functionalities
			/** Reinitilize internal structures without deleting the pointer to this class*/
			void reinitialize(const vector<shared_ptr<IntersectedLineMesh<VectorType>>> &lineMeshes);
			#pragma endregion

			#pragma AccessFunctions
			/** Cut-Cell access functions */
			virtual bool isCutCell(const dimensions_t& dimensions) {
				//Checks if the number of cells of this QuadCell is bigger than 1
				return (*m_pGridCells)(dimensions)->getCells().size() > 1;
			};

			bool isCutCell(int i, int j) const {
				//Checks if the number of cells of this QuadCell is bigger than 1
				return (*m_pGridCells)(i, j)->getCells().size() > 1;
			}

			const vector<shared_ptr<Cell<VectorType>>>& getCells(const dimensions_t &dimensions) {
				return (*m_pGridCells)(dimensions)->getCells();
			}

			const vector<shared_ptr<Cell<VectorType>>>& getCells(uint i, uint j) {
				return (*m_pGridCells)(i, j)->getCells();
			}

			shared_ptr<QuadCell<VectorType>> getQuadCell(uint i, uint j) {
				return (*m_pGridCells)(i, j);
			}

			//Name wrapping for better syntax
			const vector<shared_ptr<QuadCell<VectorType>>> & getQuadCells() const {
				return this->getElements();
			}
			//Name wrapping for better syntax
			vector<shared_ptr<QuadCell<VectorType>>> & getQuadCells() {
				return this->getElements();
			}

			/** Cut-cells getters */
			uint getNumberGeometricCutCells() const {
				return m_geometricCutCells.size();
			}

			const Cell<VectorType>& getGeometricCutCell(uint cellIndex) const {
				return *m_geometricCutCells[cellIndex];
			}

			Cell<VectorType>& getGeometricCutCell(uint cellIndex) {
				return *m_geometricCutCells[cellIndex];
			}

			const shared_ptr<Array2D<shared_ptr<Vertex<VectorType>>>> getNodalVerticesPtr() const {
				return m_pNodeVertices;
			}

			shared_ptr<Vertex<VectorType>> getVertex(uint i, uint j) {
				return (*m_pNodeVertices)(i, j);
			}

			shared_ptr<Vertex<VectorType>> getVertex(const dimensions_t &location) {
				return (*m_pNodeVertices)(location.x, location.y);
			}

			shared_ptr<typename QuadGridMeshType::ElementPrimitiveType> getQuadGridCell(const dimensions_t & gridCellIndex)  {
				return (*m_pGridCells)(gridCellIndex);
			}

			shared_ptr<typename QuadGridMeshType::ElementPrimitiveType> getQuadGridCell(uint i, uint j) {
				return (*m_pGridCells)(i, j);
			}

			/** Edges access functions */
			virtual const vector<shared_ptr<Edge<VectorType>>>& getEdges(dimensions_t edgeIndex, edgeType_t edgeType) const {
				if (edgeType == xAlignedEdge) {
					return *(*m_pHorizontalEdges)(edgeIndex);
				}
				else if (edgeType == yAlignedEdge) {
					return *(*m_pVerticalEdges)(edgeIndex);
				}
				return vector<shared_ptr<Edge<VectorType>>>();
			}

			virtual vector<shared_ptr<Edge<VectorType>>> & getEdges(dimensions_t edgeIndex, edgeType_t edgeType) {
				if (edgeType == xAlignedEdge) {
					return *(*m_pHorizontalEdges)(edgeIndex);
				}
				else if (edgeType == yAlignedEdge) {
					return *(*m_pVerticalEdges)(edgeIndex);
				}
				static vector<shared_ptr<Edge<VectorType>>> empty{};
				return empty;
			}

			virtual vector<shared_ptr<Edge<VectorType>>>& getEdges() override{
				return GridMesh<QuadGridMeshType>::getEdges();
			}

			virtual vector<shared_ptr<Vertex<VectorType>>>& getVertices() override {
				return Mesh<QuadGridMeshType>::getVertices();
			}

			void setDualQuadGridMesh(shared_ptr<QuadGridMesh<QuadGridMeshType>> pDualMesh) {
				m_pDualGridMesh = pDualMesh;
			}

			shared_ptr<QuadGridMesh<QuadGridMeshType>> getDualQuadGridMesh() {
				return m_pDualGridMesh;
			}
			#pragma endregion


			#pragma region StaticFunctionalities
			template <class ParentClassT>
			static shared_ptr<QuadGridMesh<QuadGridMeshType>> get(shared_ptr<ParentClassT> pParent) {
				return shared_ptr<QuadGridMesh<QuadGridMeshType>>(dynamic_cast<QuadGridMesh<QuadGridMeshType> *>(pParent.get()));
			}
			template <class ParentClassT>
			static QuadGridMesh<QuadGridMeshType>* get(ParentClassT * pParent) {
				return dynamic_cast<QuadGridMesh<QuadGridMeshType>*>(pParent);
			}

			static void depthFirstSearch(shared_ptr<Cell<VectorType>> pCurrentCell, typename QuadGridMesh<QuadGridMeshType>::extStructures_t &externalStructures);
			
			static shared_ptr<QuadGridMesh<QuadGridMeshType>> createDualGrid(
				shared_ptr<QuadGridMesh<QuadGridMeshType>> pQuadGridMesh,
				shared_ptr<unordered_map<uint, shared_ptr<Cell<VectorType>>>> pVertexIDToCellMap = nullptr);

			static void findCycle(
				shared_ptr<Vertex<VectorType>> pInitialVertex,
				shared_ptr<Edge<VectorType>> pCurrEdge,
				vector<shared_ptr<Edge<VectorType>>> connectedEdges,
				vector<shared_ptr<Edge<VectorType>>> &dualEdges,
				typename QuadGridMesh<QuadGridMeshType>::extStructures_t& externalStructures,
				shared_ptr<unordered_map<uint, shared_ptr<Cell<VectorType>>>> pVertexIDToCellMap = nullptr);

			

			static void matchDualGrids(shared_ptr<QuadGridMesh<QuadGridMeshType>> pQuadGridMesh,
				shared_ptr<QuadGridMesh<QuadGridMeshType>> pDualQuadGridMesh);
			#pragma endregion

		protected:
			#pragma region ClassMembers
			/** Quad Cells array structure */
			shared_ptr<Array2D<shared_ptr<QuadCell<VectorType>>>> m_pGridCells;

			/** Pointers to cells that are in contact with line meshes. Useful for solvers */
			vector<shared_ptr<Cell<VectorType>>> m_geometricCutCells;

			/** Line-meshes are the 2-D objects or 3-D planar intersection of a 3-D object and the grid. */
			vector<shared_ptr<IntersectedLineMesh<VectorType>>> m_lineMeshes;

			/** Patches indices, accessible by the array structure. First index of the pair indicates the LineMesh index,
			second is a vector containing all indices from that LineMesh that are contained inside the cell. This is
			always a 2-D structure, since it can be local to each patch index*/
			shared_ptr<Array2D<vector<pair<uint, vector<uint>>>>> m_pLinePatches;

			/** Slices attributes */
			uint m_sliceIndex;
			cellLocation_t m_cellsLocation;

			/** Dual mesh */
			shared_ptr<QuadGridMesh<QuadGridMeshType>> m_pDualGridMesh;
			#pragma endregion

			#pragma region SharedStructures
			/** Possibly shared structures in 3-D*/
			/** Vertical and horizontal undirected edges */
			Array2DVectorOffPtr<Edge> m_pVerticalEdges;
			Array2DVectorOffPtr<Edge> m_pHorizontalEdges;


			/** Nodal vertices */
			Array2DOffPtr<Vertex> m_pNodeVertices;

			/** Indicates if dangling faces (border edges or holes) are present on cut-cells. Default is false. **/
			bool m_hasDanglingFaces;
			#pragma endregion

			#pragma region InitializationFunctions
			shared_ptr<Array2D<vector<pair<uint, vector<uint>>>>> createLinePatches(cellLocation_t cellLocation);

			/** Creates a grid nodal vertex on a specified location*/
			shared_ptr<Vertex<VectorType>> createNodalVertex(uint i, uint j);

			/** Initializes line patches inside each regular grid cell */
			void buildLinePatches();

			/** Creates vertices that are on top of grid nodes and add vertices on line meshes to be accessible by mesh vertices*/
			void buildVertices();

			/** Creates all necessary grid edges. */
			void buildEdges();

			/** Creates all grid faces, which will be subdivided in a later step. */
			void buildCells();

			void initializeBorderEdges();
			void initializeBorderVertices();
			#pragma endregion

			#pragma region AuxiliaryHelperFunctions
			template <template <class> class CustomType , class ... Args>
			static Array2DOffPtr<CustomType> constructArray2DOffPtr(Args && ... args) {
				return shared_ptr<Array2D<shared_ptr<CustomType<VectorType>>>>(new Array2D<shared_ptr<CustomType<VectorType>>>(std::forward<Args>(args)...));
			}

			template <template <class> class CustomType, class ... Args>
			static Array2DVectorOffPtr<CustomType> constructArray2DVectorOffPtr(Args && ... args) {
				auto pArray = shared_ptr<Array2D<VectorOffPtr<CustomType>>>(new Array2D<VectorOffPtr<CustomType>>(std::forward<Args>(args)...));

				for (VectorOffPtr<CustomType> &pElement : pArray->getRawData()) {
					pElement = make_shared<vector<shared_ptr<CustomType<VectorType>>>>();
				}
				return pArray;
			}

			/** Check if a potential edge is aligned with geometry edges inside a cell. If it is, the fluid edge will not be added
				to the list of edges of a certain face. */
			FORCE_INLINE bool hasAlignedEdges(uint vertex1, uint vertex2, dimensions_t linePatchesDim, halfEdgeLocation_t edgeType) {
				if (vertex1 == vertex2) {
					return true;
				}
				dimensions_t nextDim(linePatchesDim);
				if (hasAlignedEdges(vertex1, vertex2, linePatchesDim)) {
					return true;
				}
				if (edgeType == bottomHalfEdge) {
					nextDim.y -= 1;
				} else if (edgeType == topHalfEdge) {
					nextDim.y += 1;
				} else if (edgeType == leftHalfEdge) {
					nextDim.x -= 1;
				} else if (edgeType == rightHalfEdge) {
					nextDim.x += 1;
				}
				return hasAlignedEdges(vertex1, vertex2, nextDim);
			}

			bool hasAlignedEdges(uint vertex1, uint vertex2, dimensions_t linePatchesDim);

			FORCE_INLINE bool validVertex(uint i, uint j) {
				if ((*m_pNodeVertices)(i, j) == nullptr || (*m_pNodeVertices)(i, j)->getID() == UINT_MAX) {
					return false;
				}
				return true;
			}
			/** Passes the half-edge location, which only makes sense in 2-D. In this way the create edge function must
				realize where the location is relative to*/
			virtual shared_ptr<Edge<VectorType>> createGridEdge(shared_ptr<Vertex<VectorType>> pV1, shared_ptr<Vertex<VectorType>> pV2, halfEdgeLocation_t halfEdgeLocation);

			/** Sorts the vertices on edges accordingly with their positions on space */
			virtual void sortVertices(vector<shared_ptr<Vertex<VectorType>>>& bottomVertices, vector<shared_ptr<Vertex<VectorType>>>& leftVertices, vector<shared_ptr<Vertex<VectorType>>>& topVertices, vector<shared_ptr<Vertex<VectorType>>>& rightVertices);

			/** Classifies vertices on 2-D or 3-D */
			virtual void classifyVertex(const dimensions_t& gridDim, shared_ptr<Vertex<VectorType>>pVertex, vector<shared_ptr<Vertex<VectorType>>>& bottomVertices, vector<shared_ptr<Vertex<VectorType>>>& leftVertices,
																	vector<shared_ptr<Vertex<VectorType>>>& topVertices, vector<shared_ptr<Vertex<VectorType>>>& rightVertices);
			#pragma endregion

		};

	}
}

#endif
