//  Copyright (c) 2017, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_LEVEL_SET_GRID_MESH_H_
#define __CHIMERA_LEVEL_SET_GRID_MESH_H_

#pragma once

#include "ChimeraCore.h"
#include "ChimeraInterpolation.h"
#include "ChimeraGrids.h"

namespace Chimera {

	using namespace Interpolation;
	using namespace Grids;
	using namespace Meshes;
	
	namespace Grids {

		template <class EmbeddedMeshClass, typename GridMeshType>
		class LevelSetGridMesh {
		protected:
			//Useful definitions
			using VectorType = typename GridMeshType::VectorType;

		public:
		#pragma region Constructors
			/** Constructor is protected since this class cannot be directly instantiated */
			LevelSetGridMesh(shared_ptr<GridMesh<GridMeshType>> pSimulationGrid, uint numSubdivisions = 0);
			#pragma endregion

			#pragma region Functionalities
			/*Update meshes representation to FLIP particles position update. It also updates the level-set values
				on the coarse simulation grid. */
			void update();

			void initializeSphere(const VectorType& position, Scalar radius);

			vector<shared_ptr<IntersectedLineMesh<VectorType>>> extractIntersectedLineMeshes();
			#pragma endregion


			#pragma region AccessFunctions
			const vector<shared_ptr<EmbeddedMeshClass>> & getImmersedMeshes() const {
				return m_immersedMeshes;
			}

			void addImmersedMesh(shared_ptr<EmbeddedMeshClass> pImmersedMesh) {
				m_immersedMeshes.push_back(pImmersedMesh);
			}

			shared_ptr<GridMesh<GridMeshType>> getGrid() {
				return m_pLevelSetGrid;
			}

			shared_ptr<GridMesh<GridMeshType>> getSimulationGrid() {
				return m_pSimulationGrid;
			}

			void reset() {
				for (auto pVertex : m_pLevelSetGrid->getVertices()) {
					OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("levelSet", 0);
				}
			}
			#pragma endregion
		protected:
			#pragma region ClassMembers
			/** The original grid used for simulations */
			shared_ptr<GridMesh<GridMeshType>> m_pSimulationGrid;

			/** How much the levelsetGrid was subdvided (n = pow(2, numSubdivis)) in each direction */
			uint m_numberOfSubdivisions;

			/** Possibly higher-resolution hexa or quad mesh created for tracking immersed objects */
			shared_ptr<GridMesh<GridMeshType>> m_pLevelSetGrid;

			/** Objects inside this levelset grid representation*/
			vector<shared_ptr<EmbeddedMeshClass>> m_immersedMeshes;

			/** Level set value interpolant */
			shared_ptr<Interpolant<Scalar, GridMeshType>> m_pLevelSetInterpolant;
			#pragma endregion

			#pragma region PrivateFunctionalities
			void initializeBorderVertices();
			void constructCutCell();
			#pragma endregion
		};





	}
}

#endif
