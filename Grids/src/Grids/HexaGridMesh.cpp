#include "Grids/HexaGridMesh.h"

namespace Chimera {
	namespace Grids {
		#pragma region Constructors
		template <typename HexaGridMeshType>
		HexaGridMesh<HexaGridMeshType>::HexaGridMesh(const VectorType& initialPoint, const VectorType& finalPoint, Scalar gridSpacing)
			: ParentClass(initialPoint, finalPoint, gridSpacing), m_polyPatches(dimensions_t(0, 0, 0)),
				m_nodeVertices(dimensions_t(ParentClass::m_gridDimensions.x + 1, ParentClass::m_gridDimensions.y + 1, ParentClass::m_gridDimensions.z + 1)),
				m_horizontalEdges(ParentClass::m_gridDimensions),
				m_verticalEdges(ParentClass::m_gridDimensions),
				m_transversalEdges(ParentClass::m_gridDimensions) {

			m_nodeVertices.assign(nullptr);

			buildVertices();
			buildQuadGridMeshes();
			buildVolumes();
		}

		template <typename HexaGridMeshType>
		HexaGridMesh<HexaGridMeshType>::HexaGridMesh(const vector<shared_ptr<IntersectedPolyMesh<VectorType>>> &polygonalMeshes, const VectorType& initialPoint, const VectorType& finalPoint, Scalar gridSpacing)
			: ParentClass(initialPoint, finalPoint, gridSpacing), m_polyPatches(ParentClass::m_gridDimensions),
			m_nodeVertices(dimensions_t(ParentClass::m_gridDimensions.x + 1, ParentClass::m_gridDimensions.y + 1, ParentClass::m_gridDimensions.z + 1)),
			m_horizontalEdges(ParentClass::m_gridDimensions, true),
			m_verticalEdges(ParentClass::m_gridDimensions, true),
			m_transversalEdges(ParentClass::m_gridDimensions, true) {

			m_nodeVertices.assign(nullptr);
			m_polyMeshes = polygonalMeshes;

			buildPolyPatches();
			buildLineMeshes();
			buildVertices();
			buildQuadGridMeshes();
			buildVolumes();
		}
		#pragma endregion

		#pragma region InitializationFunctions
		template <typename HexaGridMeshType>
		void HexaGridMesh<HexaGridMeshType>::buildPolyPatches() {
			for (int i = 0; i < ParentClass::m_gridDimensions.x; i++) {
				for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
					for (int k = 0; k < ParentClass::m_gridDimensions.z; k++) {

						for (int l = 0; l < m_polyMeshes.size(); l++) {
							if (!m_polyMeshes[l]->getPatchesIndices(i, j, k).empty()) {
								m_polyPatches(i, j, k).push_back(pair<uint, vector<uint>>(l, m_polyMeshes[l]->getPatchesIndices(i, j, k)));
							}
						}
					}
				}
			}
		}

		template <typename HexaGridMeshType>
		void HexaGridMesh<HexaGridMeshType>::buildVertices() {
			/** First build node vertices */
			for (int i = 0; i < ParentClass::m_gridDimensions.x; i++) {
				for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
					for (int k = 0; k < ParentClass::m_gridDimensions.z; k++) {
						if (!m_polyPatches(i, j, k).empty()) {
							buildOnNodeVerticesFacesPatch(dimensions_t(i, j, k));

							if (m_nodeVertices(i, j, k) == nullptr) {
								createNodeVertex(dimensions_t(i, j, k));
							}

							if (m_nodeVertices(i + 1, j, k) == nullptr && m_polyPatches(i + 1, j, k).empty()) {
								createNodeVertex(dimensions_t(i + 1, j, k));
							}

							if (m_nodeVertices(i + 1, j + 1, k) == nullptr && m_polyPatches(i + 1, j + 1, k).empty()) {
								createNodeVertex(dimensions_t(i + 1, j + 1, k));
							}

							if (m_nodeVertices(i, j + 1, k) == nullptr && m_polyPatches(i, j + 1, k).empty()) {
								createNodeVertex(dimensions_t(i, j + 1, k));
							}

							// k + 1
							if (m_nodeVertices(i, j, k + 1) == nullptr && m_polyPatches(i, j, k + 1).empty()) {
								createNodeVertex(dimensions_t(i, j, k + 1));

							}
							if (m_nodeVertices(i + 1, j, k + 1) == nullptr && m_polyPatches(i + 1, j, k + 1).empty()) {
								createNodeVertex(dimensions_t(i + 1, j, k + 1));
							}

							if (m_nodeVertices(i + 1, j + 1, k + 1) == nullptr && m_polyPatches(i + 1, j + 1, k + 1).empty()) {
								createNodeVertex(dimensions_t(i + 1, j + 1, k + 1));
							}

							if (m_nodeVertices(i, j + 1, k + 1) == nullptr && m_polyPatches(i, j + 1, k + 1).empty()) {
								createNodeVertex(dimensions_t(i, j + 1, k + 1));
							}
						}
						else {
							if (m_nodeVertices(i, j, k) == nullptr) {
								createNodeVertex(dimensions_t(i, j, k));
							}
						}
					}
				}
			}

			/** Adding outer band of vertices */
			for (int i = 0; i <= ParentClass::m_gridDimensions.x; i++) {
				for (int j = 0; j <= ParentClass::m_gridDimensions.y; j++) {
					if (m_nodeVertices(i, j, ParentClass::m_gridDimensions.z) == nullptr) {
						createNodeVertex(dimensions_t(i, j, ParentClass::m_gridDimensions.z));
					}
				}
			}

			for (int i = 0; i <= ParentClass::m_gridDimensions.x; i++) {
				for (int k = 0; k <= ParentClass::m_gridDimensions.z; k++) {
					if (m_nodeVertices(i, ParentClass::m_gridDimensions.y, k) == nullptr) {
						createNodeVertex(dimensions_t(i, ParentClass::m_gridDimensions.y, k));
					}
				}
			}

			for (int j = 0; j <= ParentClass::m_gridDimensions.y; j++) {
				for (int k = 0; k <= ParentClass::m_gridDimensions.z; k++) {
					if (m_nodeVertices(ParentClass::m_gridDimensions.x, j, k) == nullptr) {
						createNodeVertex(dimensions_t(ParentClass::m_gridDimensions.x, j, k));
					}
				}
			}

			/** Then add mesh vertices to m_vertices as well */
			map<uint, bool> addedMeshVertices;
			for (auto pPolyMesh : m_polyMeshes) {
				for (auto pVertex : pPolyMesh->getVertices()) {
					if (addedMeshVertices.find(pVertex->getID()) == addedMeshVertices.end()) {
						addedMeshVertices[pVertex->getID()] = true;
						this->m_vertices.push_back(pVertex);
					}
				}
			}
		}

		template <typename HexaGridMeshType>
		void HexaGridMesh<HexaGridMeshType>::buildOnNodeVerticesFacesPatch(const dimensions_t &cellIndex) {
			//Check if any of the vertices of the line mesh are on top of a grid nodes
			for (int i = 0; i < m_polyPatches(cellIndex).size(); i++) {
				uint polyMeshIndex = m_polyPatches(cellIndex)[i].first;
				auto polyMeshPatches = m_polyPatches(cellIndex)[i].second;

				/** Verifying if there's geometry nodes on top of grid nodes */
				/*for (int j = 0; j < m_polyPatches.size(); j++) {
					shared_ptr<Face<VectorType>>pFace = m_polyMeshes[polyMeshIndex]->getElements()[polyMeshPatches[j]];
					shared_ptr<HalfFace<VectorType>>pHalfFface = pFace->getHalfFaces().front();
					for (int k = 0; k < pHalfFface->getHalfEdges().size(); k++) {
						if(pHalfFface->getVertices().first->isOnGridNode()) {
							const VectorType &position = pHalfFface->getVertices().first->getPosition();
							dimensions_t nodeVertexDim(position.x / m_gridSpacing, position.y / m_gridSpacing, position.z / m_gridSpacing);
							m_nodeVertices(nodeVertexDim) = pHalfFface->getVertices().first;
						}
					}
				}*/
			}
		}

		template <typename HexaGridMeshType>
		void HexaGridMesh<HexaGridMeshType>::buildLineMeshes() {
			/** Mesh slicer is responsible for this now*/
			for (int i = 0; i < m_polyMeshes.size(); i++) {
				auto currXYLineMeshes = m_polyMeshes[i]->template getLineMeshes<Meshes::XYPlane>();
				for (auto iter = currXYLineMeshes.begin(); iter != currXYLineMeshes.end(); iter++) {
					m_XYLineMeshes[iter->first].insert(m_XYLineMeshes[iter->first].end(), iter->second.begin(), iter->second.end());
				}

				auto currXZLineMeshes = m_polyMeshes[i]->template getLineMeshes<Meshes::XZPlane>();
				for (auto iter = currXZLineMeshes.begin(); iter != currXZLineMeshes.end(); iter++) {
					m_XZLineMeshes[iter->first].insert(m_XZLineMeshes[iter->first].end(), iter->second.begin(), iter->second.end());
				}

				auto currYZLineMeshes = m_polyMeshes[i]->template getLineMeshes<Meshes::YZPlane>();
				for (auto iter = currYZLineMeshes.begin(); iter != currYZLineMeshes.end(); iter++) {
					m_YZLineMeshes[iter->first].insert(m_YZLineMeshes[iter->first].end(), iter->second.begin(), iter->second.end());
				}
			}
		}

		template <typename HexaGridMeshType>
		void HexaGridMesh<HexaGridMeshType>::buildQuadGridMeshes() {
			/** Try to respect XY, XZ, YZ order */

			/** Building XYCutCells */
			dimensions_t xyGridDimensions(ParentClass::m_gridDimensions.x, ParentClass::m_gridDimensions.y);
			for (auto iter = m_XYLineMeshes.begin(); iter != m_XYLineMeshes.end(); iter++) {
				typename QuadGridMesh<SubGridMeshType>::extStructures_t extStructures;
				extStructures.pHorizontalEdges = Array3D<VectorOffPtr<Edge>>::createArraySlicePtr(m_horizontalEdges, iter->first, Array3D<VectorOffPtr<Edge>>::XYSlice);
				extStructures.pVerticalEdges = Array3D<VectorOffPtr<Edge>>::createArraySlicePtr(m_verticalEdges, iter->first, Array3D<VectorOffPtr<Edge>>::XYSlice);
				extStructures.pNodeVertices = Array3D<shared_ptr<Vertex<VectorType>>>::createArraySlicePtr(m_nodeVertices, iter->first, Array3D<shared_ptr<Vertex<VectorType>>>::XYSlice);
				m_XYQuadGridMeshes[iter->first] = std::make_unique<QuadGridMesh<SubGridMeshType>>(extStructures, ParentClass::m_gridSpacing, xyGridDimensions, XYPlane, iter->second);
			}

			/** Building YZCutCells */
			dimensions_t yzGridDimensions(ParentClass::m_gridDimensions.z, ParentClass::m_gridDimensions.y);
			for (auto iter = m_YZLineMeshes.begin(); iter != m_YZLineMeshes.end(); iter++) {
				typename QuadGridMesh<SubGridMeshType>::extStructures_t extStructures;
				extStructures.pHorizontalEdges = Array3D<VectorOffPtr<Edge>>::createArraySlicePtr(m_transversalEdges, iter->first, Array3D<VectorOffPtr<Edge>>::YZSlice);
				extStructures.pVerticalEdges = Array3D<VectorOffPtr<Edge>>::createArraySlicePtr(m_verticalEdges, iter->first, Array3D<VectorOffPtr<Edge>>::YZSlice);
				extStructures.pNodeVertices = Array3D<shared_ptr<Vertex<VectorType>>>::createArraySlicePtr(m_nodeVertices, iter->first, Array3D<shared_ptr<Vertex<VectorType>>>::YZSlice);
				m_YZQuadGridMeshes[iter->first] = std::make_unique<QuadGridMesh<SubGridMeshType>>(extStructures, ParentClass::m_gridSpacing, yzGridDimensions, YZPlane, iter->second);
			}

			/** Building XZCutCells */
			dimensions_t xzGridDimensions(ParentClass::m_gridDimensions.x, ParentClass::m_gridDimensions.z);
			for (auto iter = m_XZLineMeshes.begin(); iter != m_XZLineMeshes.end(); iter++) {
				typename QuadGridMesh<SubGridMeshType>::extStructures_t extStructures;
				extStructures.pHorizontalEdges = Array3D<VectorOffPtr<Edge>>::createArraySlicePtr(m_horizontalEdges, iter->first, Array3D<VectorOffPtr<Edge>>::XZSlice);
				extStructures.pVerticalEdges = Array3D<VectorOffPtr<Edge>>::createArraySlicePtr(m_transversalEdges, iter->first, Array3D<VectorOffPtr<Edge>>::XZSlice);
				extStructures.pNodeVertices = Array3D<shared_ptr<Vertex<VectorType>>>::createArraySlicePtr(m_nodeVertices, iter->first, Array3D<shared_ptr<Vertex<VectorType>>>::XZSlice);
				m_XZQuadGridMeshes[iter->first] = std::make_unique<QuadGridMesh<SubGridMeshType>>(extStructures, ParentClass::m_gridSpacing, xzGridDimensions, XZPlane, iter->second);
			}

			/** Initialize regular faces stored as QuadGridMeshes slices */
			for (int k = 0; k <= ParentClass::m_gridDimensions.z; k++) {
				VectorType initialPoint(0, 0, k* ParentClass::m_gridSpacing);
				VectorType finalPoint(xyGridDimensions.x* ParentClass::m_gridSpacing, xyGridDimensions.y* ParentClass::m_gridSpacing,	k* ParentClass::m_gridSpacing);
				m_XYQuadGridMeshes[k] = std::make_unique<QuadGridMesh<SubGridMeshType>>(initialPoint, finalPoint, ParentClass::m_gridSpacing, XYPlane);
			}

			for (int i = 0; i <= ParentClass::m_gridDimensions.x; i++) {
				VectorType initialPoint(i * ParentClass::m_gridSpacing, 0, 0);
				VectorType finalPoint(i * ParentClass::m_gridSpacing, yzGridDimensions.y * ParentClass::m_gridSpacing, yzGridDimensions.x * ParentClass::m_gridSpacing);
				m_YZQuadGridMeshes[i] = std::make_unique<QuadGridMesh<SubGridMeshType>>(initialPoint, finalPoint, ParentClass::m_gridSpacing, YZPlane);
			}

			for (int j = 0; j <= ParentClass::m_gridDimensions.y; j++) {
				VectorType initialPoint(0, j * ParentClass::m_gridSpacing, 0);
				VectorType finalPoint(xzGridDimensions.x * ParentClass::m_gridSpacing, j * ParentClass::m_gridSpacing, xzGridDimensions.y * ParentClass::m_gridSpacing);
				m_XZQuadGridMeshes[j] = std::make_unique<QuadGridMesh<SubGridMeshType>>(initialPoint, finalPoint, ParentClass::m_gridSpacing, XZPlane);
			}
		}

		template<typename HexaGridMeshType>
		void HexaGridMesh<HexaGridMeshType>::buildVolumes() {
			for (int i = 0; i < ParentClass::m_gridDimensions.x; i++) {
				for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
					for (int k = 0; k < ParentClass::m_gridDimensions.z; k++) {
						vector<shared_ptr<Cell<VectorType>>> volumeFaces;
						//Back, front, bottom, top, left right
						volumeFaces.insert(volumeFaces.end(), m_XYQuadGridMeshes[k]->getQuadCell(i, j)->getCells().begin(), m_XYQuadGridMeshes[k]->getQuadCell(i, j)->getCells().end());
						volumeFaces.insert(volumeFaces.end(), m_XYQuadGridMeshes[k + 1]->getQuadCell(i, j)->getCells().begin(), m_XYQuadGridMeshes[k + 1]->getQuadCell(i, j)->getCells().end());

						volumeFaces.insert(volumeFaces.end(), m_YZQuadGridMeshes[i]->getQuadCell(k, j)->getCells().begin(), m_YZQuadGridMeshes[i]->getQuadCell(k, j)->getCells().end());
						volumeFaces.insert(volumeFaces.end(), m_YZQuadGridMeshes[i + 1]->getQuadCell(k, j)->getCells().begin(), m_YZQuadGridMeshes[i + 1]->getQuadCell(k, j)->getCells().end());

						volumeFaces.insert(volumeFaces.end(), m_XZQuadGridMeshes[j]->getQuadCell(i, k)->getCells().begin(), m_XZQuadGridMeshes[j]->getQuadCell(i, k)->getCells().end());
						volumeFaces.insert(volumeFaces.end(), m_XZQuadGridMeshes[j + 1]->getQuadCell(i, k)->getCells().begin(), m_XZQuadGridMeshes[j + 1]->getQuadCell(i, k)->getCells().end());

						/** Adding geometric faces */
						for (int l = 0; l < m_polyPatches(i, j, k).size(); l++) {
							uint faceMeshIndex = m_polyPatches(i, j, k)[l].first;
							auto facesFromPatch = m_polyPatches(i, j, k)[l].second;
							for (int m = 0; m < facesFromPatch.size(); m++) {
								volumeFaces.push_back(m_polyMeshes[faceMeshIndex]->getElements()[facesFromPatch[m]]);
							}
						}
						(*m_pGridVolumes)(i, j, k) = this->constructElement(volumeFaces, dimensions_t(i, j, k), ParentClass::m_gridSpacing);
					}
				}
			}
		}
		#pragma endregion

		template class HexaGridMesh<HexaGridType>;
		template class HexaGridMesh<HexaGridTypeDouble>;
	}
}
