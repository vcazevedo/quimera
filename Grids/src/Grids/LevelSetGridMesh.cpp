#include "Grids/LevelSetGridMesh.h"

namespace Chimera {

	namespace Grids {

		#pragma region 
		template <class EmbeddedMeshClass, typename GridMeshType>
		LevelSetGridMesh<EmbeddedMeshClass, GridMeshType>::LevelSetGridMesh(shared_ptr<GridMesh<GridMeshType>> pSimulationGrid, uint numSubdivisions) :
			m_pSimulationGrid(pSimulationGrid), m_numberOfSubdivisions(numSubdivisions) {

			/** Creates the level set grid mesh based on the original simulated grid subdivided in each dimension */
			Scalar fineGridDx = m_pSimulationGrid->getGridSpacing() / (powf(2, static_cast<Scalar>(m_numberOfSubdivisions)));

			if constexpr (isVector3<VectorType>::value) {

			}
			else {
				m_pLevelSetGrid = make_shared<QuadGridMesh<GridMeshType>>(m_pSimulationGrid->getBounds().first, m_pSimulationGrid->getBounds().second, fineGridDx);
				m_pLevelSetGrid->template addVerticesAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, GridMeshType>>("levelSet", 0.0f);
				//Needed because it might be a higher resolution grid
				initializeBorderVertices();
			}
		}
		#pragma endregion
		#pragma region Functionalities
		template <typename EmbeddedMeshType, typename GridMeshType>
		void LevelSetGridMesh<EmbeddedMeshType, GridMeshType>::update() {
			/** Cycle through all vertices of the grid */
			for (auto pVertex : m_pLevelSetGrid->getVertices()) {
				/** For each rigid body mesh inside this level set*/
				DoubleScalar minDistance = FLT_MAX;
				for (auto pRigidBodyMesh : m_immersedMeshes) {
					DoubleScalar currDistance = pRigidBodyMesh->getSignedDistance(pVertex->getPosition());
					if (abs(currDistance) < abs(minDistance)) {
						minDistance = currDistance;
					}
				}
				OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("levelSet", minDistance);
			}

			/** Copy the level-set grid values back to the original simulation grid.
					Useful for checking if there are any obstacles */
			for (auto pVertex : m_pSimulationGrid->getVertices()) {
				uint scaleFactor = pow(2, m_numberOfSubdivisions);
				dimensions_t levelSetGridLoc(pVertex->getPosition().x * scaleFactor, pVertex->getPosition().y * scaleFactor, 0);
				if constexpr (isVector3<VectorType>::value) {
					levelSetGridLoc.z = pVertex->getPosition()[2] * scaleFactor;
				}
				auto pVertexLevelSet = m_pLevelSetGrid->getVertex(levelSetGridLoc);
				Scalar levelSetValue = OwnCustomAttribute<Scalar>::get(pVertexLevelSet)->getAttribute("levelSet");
				OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("levelSet", levelSetValue);
			}
		}

		template <typename EmbeddedMeshType, typename GridMeshType>
		void LevelSetGridMesh<EmbeddedMeshType, GridMeshType>::initializeSphere(const VectorType& position, Scalar radius) {
			/** Cycle through all vertices of the grid */
			for (auto pVertex : m_pLevelSetGrid->getVertices()) {
				Scalar distance = (pVertex->getPosition() - position).length();
				if (distance < radius) {
					OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("levelSet", -distance);
				}
				else {
					OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute("levelSet", distance);
				}
			}
		}


		template <typename EmbeddedMeshType, typename GridMeshType>
		vector<shared_ptr<IntersectedLineMesh<typename GridMeshType::VectorType>>> LevelSetGridMesh<EmbeddedMeshType, GridMeshType>::extractIntersectedLineMeshes() {
			vector<shared_ptr<IntersectedLineMesh<typename GridMeshType::VectorType>>> intersectedLineMeshes;

			return intersectedLineMeshes;
		}
		#pragma endregion
		#pragma region Private Functionalities

		template <typename EmbeddedMeshType, typename GridMeshType>
		void LevelSetGridMesh<EmbeddedMeshType, GridMeshType>::constructCutCell() {

			for (int i = 0; i < m_pLevelSetGrid->getGridDimensions().x; i++) {
				for (int j = 0; j < m_pLevelSetGrid->getGridDimensions().y; j++) {

					auto current_vertex = m_pLevelSetGrid->getVertices()[i, j];
					auto left_vertex = m_pLevelSetGrid->getVertices()[i-1, j];
					auto right_vertex = m_pLevelSetGrid->getVertices()[i+1, j];
					auto bottom_vertex = m_pLevelSetGrid->getVertices()[i, j-1];
					auto top_vertex = m_pLevelSetGrid->getVertices()[i, j+1];

					auto neighbor_vertices = {left_vertex, right_vertex, bottom_vertex, top_vertex};
					Scalar current_value = OwnCustomAttribute<Scalar>::get(current_vertex)->getAttribute("levelSet");

					for (auto neighbor : neighbor_vertices) {
						Scalar neighbor_value = OwnCustomAttribute<Scalar>::get(current_vertex)->getAttribute("levelSet");

						if ( ( current_value <= 0 && neighbor_value > 0) || ( current_value > 0 && neighbor_value <= 0) ) {
							// find fraction where crossing happens on edge, and store it to vector? (marching squares)
							// connect those edges while looping over vertices (think about how to do this)
						}
					}
				}
			}
		}

		template <typename EmbeddedMeshType, typename GridMeshType>
		void LevelSetGridMesh<EmbeddedMeshType, GridMeshType>::initializeBorderVertices() {
			int numberSubdivisOffset = pow(2, m_numberOfSubdivisions);
			QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<GridMeshType>*>(m_pLevelSetGrid.get());

			for (int i = 0; i < numberSubdivisOffset; i++) {
				for (int j = 0; j < m_pLevelSetGrid->getGridDimensions().y; j++) {
					//left
					{
						auto vertices = pQuadGridMesh->getQuadGridCell(i, j)->getVertices();
						for (auto pVertex : vertices) {
							pVertex->setBorder(true);
						}
					}

					//right
					{
						auto vertices = pQuadGridMesh->getQuadGridCell(m_pLevelSetGrid->getGridDimensions().x - (i + 1), j)->getVertices();
						for (auto pVertex : vertices) {
							pVertex->setBorder(true);
						}
					}
				}
			}

			for (int j = 0; j < numberSubdivisOffset; j++) {
				for (int i = 0; i < m_pLevelSetGrid->getGridDimensions().x; i++) {
					//bottom
					{
						auto vertices = pQuadGridMesh->getQuadGridCell(i, j)->getVertices();
						for (auto pVertex : vertices) {
							pVertex->setBorder(true);
						}
					}

					//top
					{
						auto vertices = pQuadGridMesh->getQuadGridCell(i, m_pLevelSetGrid->getGridDimensions().y - (j + 1))->getVertices();
						for (auto pVertex : vertices) {
							pVertex->setBorder(true);
						}
					}
				}
			}
		}
		#pragma endregion
		
		
		template class LevelSetGridMesh<IntersectedLineMesh<typename QuadGridType::VectorType>, QuadGridType>;
		template class LevelSetGridMesh<IntersectedLineMesh<typename QuadGridTypeDouble::VectorType>, QuadGridTypeDouble>;


		//template class LevelSetGridMesh<PolygonalMeshType<typename HexaGridType::VectorType>, HexaGridType>;
		//template class LevelSetGridMesh<PolygonalMeshType<typename HexaGridTypeDouble::VectorType>, HexaGridTypeDouble>;

	}


}
