//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#include "Grids/DualMesh.h"

namespace Chimera {
	namespace Grids {
		#pragma region Constructor
		template<typename GridMeshType>
		DualMesh<GridMeshType>::DualMesh(shared_ptr<QuadGridMesh<GridMeshType>> pQuadGridMesh) {
			//Iterate over the vertices of the primal grid and construct the "basic" dual
			for (auto pVertex : pQuadGridMesh->getVertices()) {
				vector<shared_ptr<Edge<VectorType>>> connectedEdges = OwnConnectedStructure<Edge<VectorType>>::get(pVertex)->getConnectedStructures();
				for (auto pEdge : connectedEdges) {
					pEdge->setVisited(false);
				}

				if (connectedEdges.size() > 3) { //If vertex has > 3 connected edges, change for checking boundaries
					visitConnectedEdges(pVertex, connectedEdges[0], connectedEdges); //it construct the vertices and the edges of the dual mesh
					if (pVertex->getDualElement2D() == nullptr) {
						vector<shared_ptr<HalfEdge<VectorType>>> dualHalfEdges;
						for (auto pEdge : connectedEdges) {
							if (pEdge->getDualElement2D() != nullptr) {
								dualHalfEdges.push_back(pEdge->getDualElement2D()->getHalfEdges().first);
							}
						}
						if (dualHalfEdges.size() == 4 && !pVertex->isSolid()) { //Works only for voxelized approach
							auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
							dualCell->setDualElement2D(pVertex);
							m_cells.push_back(dualCell);
							pVertex->setDualElement2D(dualCell);
						}
					}
				}
			}
		}

		template<typename GridMeshType>
		DualMesh<GridMeshType>::DualMesh(shared_ptr<QuadGridMesh<GridMeshType>> pQuadGridMesh, solverType_t solver){
			//Iterate over the vertices of the primal grid and construct the "basic" dual
			for (auto pVertex : pQuadGridMesh->getVertices()) {
				vector<shared_ptr<Edge<VectorType>>> connectedEdges = OwnConnectedStructure<Edge<VectorType>>::get(pVertex)->getConnectedStructures();
				for (auto pEdge : connectedEdges) {
					pEdge->setVisited(false);
				}

				if (connectedEdges.size() > 3) { //If vertex has > 3 connected edges, change for checking boundaries
					visitConnectedEdges(pVertex, connectedEdges[0], connectedEdges, pQuadGridMesh->getGridSpacing(), solver); //it construct the vertices and the edges of the dual mesh
					if (pVertex->getDualElement2D() == nullptr) {
						vector<shared_ptr<HalfEdge<VectorType>>> dualHalfEdges;
						for (auto pEdge : connectedEdges) {
							if (pEdge->getDualElement2D() != nullptr) {
								dualHalfEdges.push_back(pEdge->getDualElement2D()->getHalfEdges().first);
							}
						}
						if (dualHalfEdges.size() == 4 && !pVertex->isSolid()) { //Works only for voxelized approach
							auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
							dualCell->setDualElement2D(pVertex);
							m_cells.push_back(dualCell);
							pVertex->setDualElement2D(dualCell);
						}
					}
				}
			}

			//Contruct further vertices/edges/cells if needed.
			if (solver == voxelizedDECSolver) {
				//Adding the "external edges" (edges that go out of the domain) needed for outflow condition (Dirichlet for pressure)
				Vector2 gridSpacingInY(0., pQuadGridMesh->getGridSpacing());
				int j1 = 0;
				int j2 = pQuadGridMesh->getGridDimensions().y - 1;
				for (int i = 0; i < pQuadGridMesh->getGridDimensions().x; ++i) {
					//bottom
					auto cell = pQuadGridMesh->getCells(i, j1).front(); //works only for voxelized approach
					for (auto edge : cell->getEdges()) {
						if (edge->isBorder() && edge->getBoundaryCondition() == outflow && edge->getType() == xAlignedEdge) {
							auto dualVertex1 = cell->getDualElement2D();
							auto dualVertex2 = this->constructVertex(dualVertex1->getPosition() - gridSpacingInY, gridVertex);
							dualVertex2->setBorder(true);

							auto dualEdge = this->constructElement(dualVertex2, dualVertex1, yAlignedEdge);
							dualEdge->setDualElement2D(edge);
							if (edge->getDualElement2D() == nullptr) edge->setDualElement2D(dualEdge);
						}
					}

					//top
					cell = pQuadGridMesh->getCells(i, j2).front(); //works only for voxelized approach
					for (auto edge : cell->getEdges()) {
						if (edge->isBorder() && edge->getBoundaryCondition() == outflow && edge->getType() == xAlignedEdge) {
							auto dualVertex1 = cell->getDualElement2D();
							auto dualVertex2 = this->constructVertex(dualVertex1->getPosition() + gridSpacingInY, gridVertex);
							dualVertex2->setBorder(true);

							auto dualEdge = this->constructElement(dualVertex1, dualVertex2, yAlignedEdge);
							dualEdge->setDualElement2D(edge);
							if (edge->getDualElement2D() == nullptr) edge->setDualElement2D(dualEdge);
						}
					}
				}

				Vector2 gridSpacingInX(pQuadGridMesh->getGridSpacing(), 0.);
				int i1 = 0;
				int i2 = pQuadGridMesh->getGridDimensions().x - 1;
				for (int j = 0; j < pQuadGridMesh->getGridDimensions().y; ++j) {
					//left
					auto cell = pQuadGridMesh->getCells(i1, j).front(); //works only for voxelized approach
					for (auto edge : cell->getEdges()) {
						if (edge->isBorder() && edge->getBoundaryCondition() == outflow && edge->getType() == yAlignedEdge) {
							auto dualVertex1 = cell->getDualElement2D();
							auto dualVertex2 = this->constructVertex(dualVertex1->getPosition() - gridSpacingInX, gridVertex);
							dualVertex2->setBorder(true);

							auto dualEdge = this->constructElement(dualVertex2, dualVertex1, xAlignedEdge);
							dualEdge->setDualElement2D(edge);
							if (edge->getDualElement2D() == nullptr) edge->setDualElement2D(dualEdge);
						}
					}

					//right
					cell = pQuadGridMesh->getCells(i2, j).front(); //works only for voxelized approach
					for (auto edge : cell->getEdges()) {
						if (edge->isBorder() && edge->getBoundaryCondition() == outflow && edge->getType() == yAlignedEdge) {
							auto dualVertex1 = cell->getDualElement2D();
							auto dualVertex2 = this->constructVertex(dualVertex1->getPosition() + gridSpacingInX, gridVertex);
							dualVertex2->setBorder(true);

							auto dualEdge = this->constructElement(dualVertex1, dualVertex2, xAlignedEdge);
							dualEdge->setDualElement2D(edge);
							if (edge->getDualElement2D() == nullptr) edge->setDualElement2D(dualEdge);
						}
					}
				}
			}
			else if(solver == streamfunctionDECSolver){
				//Adding the "external cells" (edges that go out of the domain) 
				Vector2 gridSpacingInY(0., pQuadGridMesh->getGridSpacing());
				int j1 = 0;
				int j2 = pQuadGridMesh->getGridDimensions().y - 1;
				for (int i = 0; i < pQuadGridMesh->getGridDimensions().x; ++i) {
					//bottom
					auto cell = pQuadGridMesh->getCells(i, j1).front(); //works only for voxelized approach
					for (auto edge : cell->getEdges()) {
						if (edge->isBorder() /*&& edge->getBoundaryCondition() == neumann*/ && edge->getType() == xAlignedEdge) {
							auto dualVertex1 = cell->getDualElement2D();
							auto dualVertex2 = this->constructVertex(dualVertex1->getPosition() - gridSpacingInY, gridVertex);
							dualVertex2->setBorder(true);
							dualVertex2->setBoundaryCondition(dirichlet);

							auto dualEdge = this->constructElement(dualVertex2, dualVertex1, yAlignedEdge);
							dualEdge->setDualElement2D(edge);
							if (edge->getDualElement2D() == nullptr) edge->setDualElement2D(dualEdge);
						}
					}

					//top
					cell = pQuadGridMesh->getCells(i, j2).front(); //works only for voxelized approach
					for (auto edge : cell->getEdges()) {
						if (edge->isBorder() /*&& edge->getBoundaryCondition() == neumann*/ && edge->getType() == xAlignedEdge) {
							auto dualVertex1 = cell->getDualElement2D();
							auto dualVertex2 = this->constructVertex(dualVertex1->getPosition() + gridSpacingInY, gridVertex);
							dualVertex2->setBorder(true);
							dualVertex2->setBoundaryCondition(dirichlet);

							auto dualEdge = this->constructElement(dualVertex1, dualVertex2, yAlignedEdge);
							dualEdge->setDualElement2D(edge);
							if (edge->getDualElement2D() == nullptr) edge->setDualElement2D(dualEdge);
						}
					}
				}

				Vector2 gridSpacingInX(pQuadGridMesh->getGridSpacing(), 0.);
				int i1 = 0;
				int i2 = pQuadGridMesh->getGridDimensions().x - 1;
				for (int j = 0; j < pQuadGridMesh->getGridDimensions().y; ++j) {
					//left
					auto cell = pQuadGridMesh->getCells(i1, j).front(); //works only for voxelized approach
					for (auto edge : cell->getEdges()) {
						if (edge->isBorder() /*&& edge->getBoundaryCondition() == neumann*/ && edge->getType() == yAlignedEdge) {
							auto dualVertex1 = cell->getDualElement2D();
							auto dualVertex2 = this->constructVertex(dualVertex1->getPosition() - gridSpacingInX, gridVertex);
							dualVertex2->setBorder(true);
							dualVertex2->setBoundaryCondition(dirichlet);

							auto dualEdge = this->constructElement(dualVertex2, dualVertex1, xAlignedEdge);
							dualEdge->setDualElement2D(edge);
							if (edge->getDualElement2D() == nullptr) edge->setDualElement2D(dualEdge);
						}
					}

					//right
					cell = pQuadGridMesh->getCells(i2, j).front(); //works only for voxelized approach
					for (auto edge : cell->getEdges()) {
						if (edge->isBorder() /*&& edge->getBoundaryCondition() == neumann*/ && edge->getType() == yAlignedEdge) {
							auto dualVertex1 = cell->getDualElement2D();
							auto dualVertex2 = this->constructVertex(dualVertex1->getPosition() + gridSpacingInX, gridVertex);
							dualVertex2->setBorder(true);
							dualVertex2->setBoundaryCondition(dirichlet);

							auto dualEdge = this->constructElement(dualVertex1, dualVertex2, xAlignedEdge);
							dualEdge->setDualElement2D(edge);
							if (edge->getDualElement2D() == nullptr) edge->setDualElement2D(dualEdge);
						}
					}
				}

				for (auto pVertex : pQuadGridMesh->getVertices()) {
					if (pVertex->isBorder()) {
						vector<shared_ptr<Edge<VectorType>>> connectedEdges = OwnConnectedStructure<Edge<VectorType>>::get(pVertex)->getConnectedStructures();
						vector<shared_ptr<HalfEdge<VectorType>>> dualHalfEdges;
						if (connectedEdges.size() == 3) {
							if (pVertex->getPosition().x == pQuadGridMesh->getBounds().first[0]) { //left
								auto dualVertex1 = connectedEdges[0]->getDualElement2D()->getVertex1();
								auto dualVertex2 = connectedEdges[2]->getDualElement2D()->getVertex1();
								auto dualEdge = this->constructElement(dualVertex1, dualVertex2, yAlignedEdge);

								dualHalfEdges.push_back(connectedEdges[0]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[1]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[2]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(dualEdge->getHalfEdges().first);

								auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
								dualCell->setDualElement2D(pVertex);
								m_cells.push_back(dualCell);
								pVertex->setDualElement2D(dualCell);
							}
							else if (pVertex->getPosition().x == pQuadGridMesh->getBounds().second[0]) { //right
								auto dualVertex1 = connectedEdges[0]->getDualElement2D()->getVertex2();
								auto dualVertex2 = connectedEdges[2]->getDualElement2D()->getVertex2();
								auto dualEdge = this->constructElement(dualVertex1, dualVertex2, yAlignedEdge);

								dualHalfEdges.push_back(connectedEdges[0]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(dualEdge->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[2]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[1]->getDualElement2D()->getHalfEdges().first);

								auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
								dualCell->setDualElement2D(pVertex);
								m_cells.push_back(dualCell);
								pVertex->setDualElement2D(dualCell);
							}

							if (pVertex->getPosition().y == pQuadGridMesh->getBounds().first[1]) { //bottom
								auto dualVertex1 = connectedEdges[0]->getDualElement2D()->getVertex1();
								auto dualVertex2 = connectedEdges[1]->getDualElement2D()->getVertex1();
								auto dualEdge = this->constructElement(dualVertex1, dualVertex2, xAlignedEdge);

								dualHalfEdges.push_back(dualEdge->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[1]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[2]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[0]->getDualElement2D()->getHalfEdges().first);

								auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
								dualCell->setDualElement2D(pVertex);
								m_cells.push_back(dualCell);
								pVertex->setDualElement2D(dualCell);
							}
							else if (pVertex->getPosition().y == pQuadGridMesh->getBounds().second[1]) { //top
								auto dualVertex1 = connectedEdges[0]->getDualElement2D()->getVertex2();
								auto dualVertex2 = connectedEdges[2]->getDualElement2D()->getVertex2();
								auto dualEdge = this->constructElement(dualVertex1, dualVertex2, xAlignedEdge);

								dualHalfEdges.push_back(connectedEdges[1]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[2]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(dualEdge->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[0]->getDualElement2D()->getHalfEdges().first);

								auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
								dualCell->setDualElement2D(pVertex);
								m_cells.push_back(dualCell);
								pVertex->setDualElement2D(dualCell);
							}
						}
						else if (connectedEdges.size() == 2) {
							if (pVertex->getPosition() == pQuadGridMesh->getBounds().first) { //Lower left corner
								auto dualVertex1 = this->constructVertex(pVertex->getPosition() - 0.5*gridSpacingInX - 0.5*gridSpacingInY, gridVertex);
								auto dualVertex2 = connectedEdges[0]->getDualElement2D()->getVertex1();
								auto dualEdge1 = this->constructElement(dualVertex1, dualVertex2, xAlignedEdge);

								auto dualVertex3 = connectedEdges[1]->getDualElement2D()->getVertex1();
								auto dualEdge2 = this->constructElement(dualVertex1, dualVertex3, yAlignedEdge);

								dualHalfEdges.push_back(dualEdge1->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[0]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[1]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(dualEdge2->getHalfEdges().first);

								auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
								dualCell->setDualElement2D(pVertex);
								m_cells.push_back(dualCell);
								pVertex->setDualElement2D(dualCell);								
							}
							else if (pVertex->getPosition().x == pQuadGridMesh->getBounds().first[0] && pVertex->getPosition().y == pQuadGridMesh->getBounds().second[1]) { //Upper left corner
								auto dualVertex1 = connectedEdges[0]->getDualElement2D()->getVertex1();
								auto dualVertex2 = this->constructVertex(pVertex->getPosition() - 0.5 * gridSpacingInX + 0.5 * gridSpacingInY, gridVertex);
								auto dualEdge1 = this->constructElement(dualVertex1, dualVertex2, yAlignedEdge);

								auto dualVertex3 = connectedEdges[1]->getDualElement2D()->getVertex2();
								auto dualEdge2 = this->constructElement(dualVertex2, dualVertex3, xAlignedEdge);

								dualHalfEdges.push_back(connectedEdges[0]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[1]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(dualEdge2->getHalfEdges().first);
								dualHalfEdges.push_back(dualEdge1->getHalfEdges().first);

								auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
								dualCell->setDualElement2D(pVertex);
								m_cells.push_back(dualCell);
								pVertex->setDualElement2D(dualCell);
							}
							else if (pVertex->getPosition() == pQuadGridMesh->getBounds().second) { //Upper right corner
								auto dualVertex1 = connectedEdges[0]->getDualElement2D()->getVertex2();
								auto dualVertex2 = this->constructVertex(pVertex->getPosition() + 0.5 * gridSpacingInX + 0.5 * gridSpacingInY, gridVertex);
								auto dualEdge1 = this->constructElement(dualVertex1, dualVertex2, yAlignedEdge);

								auto dualVertex3 = connectedEdges[1]->getDualElement2D()->getVertex2();
								auto dualEdge2 = this->constructElement(dualVertex3, dualVertex2, xAlignedEdge);

								dualHalfEdges.push_back(connectedEdges[0]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(dualEdge1->getHalfEdges().first);
								dualHalfEdges.push_back(dualEdge2->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[1]->getDualElement2D()->getHalfEdges().first);

								auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
								dualCell->setDualElement2D(pVertex);
								m_cells.push_back(dualCell);
								pVertex->setDualElement2D(dualCell);
							}
							else if (pVertex->getPosition().x == pQuadGridMesh->getBounds().second[0] && pVertex->getPosition().y == pQuadGridMesh->getBounds().first[1]) { //Lower right corner
								auto dualVertex1 = connectedEdges[0]->getDualElement2D()->getVertex1();
								auto dualVertex2 = this->constructVertex(pVertex->getPosition() + 0.5 * gridSpacingInX - 0.5 * gridSpacingInY, gridVertex);
								auto dualEdge1 = this->constructElement(dualVertex1, dualVertex2, xAlignedEdge);

								auto dualVertex3 = connectedEdges[1]->getDualElement2D()->getVertex2();
								auto dualEdge2 = this->constructElement(dualVertex2, dualVertex3, yAlignedEdge);

								dualHalfEdges.push_back(dualEdge1->getHalfEdges().first);
								dualHalfEdges.push_back(dualEdge2->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[1]->getDualElement2D()->getHalfEdges().first);
								dualHalfEdges.push_back(connectedEdges[0]->getDualElement2D()->getHalfEdges().first);

								auto dualCell = Cell<VectorType>::create(dualHalfEdges, XYPlane);
								dualCell->setDualElement2D(pVertex);
								m_cells.push_back(dualCell);
								pVertex->setDualElement2D(dualCell);
							}
						}
					}
				}
			}
		}
		#pragma endregion

		#pragma region Functionalities
		template<typename GridMeshType>
		void DualMesh<GridMeshType>::addGhostEdge(shared_ptr<Vertex<VectorType>> pDualVertex, shared_ptr<Edge<VectorType>> pPrimalEdge) {
			//Creates the ghost vertex outside the grid 
			auto pGhostVertex = this->constructVertex(pPrimalEdge->getCentroid(), gridVertex);
			
			auto dualEdge = this->constructElement(pDualVertex, pGhostVertex, geometricEdge);
			dualEdge->setDualElement2D(pPrimalEdge);
			pPrimalEdge->setDualElement2D(dualEdge);
		}
		#pragma endregion
	
		template<typename GridMeshType>
		void DualMesh<GridMeshType>::visitConnectedEdges(shared_ptr<Vertex<VectorType>> pInitialVertex, shared_ptr<Edge<VectorType>> pCurrEdge, vector<shared_ptr<Edge<VectorType>>> connectedEdges, Scalar gridSpacing, solverType_t solverType) {
				pCurrEdge->setVisited(true);
				shared_ptr<Cell<VectorType>> pCurrCell, pNextCell;
				{
					//Use the half-edge that points to this vertex to get consistent orientations
					shared_ptr<HalfEdge<VectorType>> pPointingToHalfEdge = pCurrEdge->getHalfEdges().first->getVertices().first == pInitialVertex ? pCurrEdge->getHalfEdges().first : pCurrEdge->getHalfEdges().second;
					auto connectedHalfFaces1 = OwnConnectedStructure<HalfCell<VectorType>>::get(pPointingToHalfEdge)->getConnectedStructures();
					assert(connectedHalfFaces1.size() == 1); //Should hold for 2-D;
					pCurrCell = connectedHalfFaces1.front()->getCell();

					//Use the half-edge that points to this vertex to get consistent orientations
					shared_ptr<HalfEdge<VectorType>> pPointingFromHalfEdge = pCurrEdge->getHalfEdges().first->getVertices().second == pInitialVertex ? pCurrEdge->getHalfEdges().first : pCurrEdge->getHalfEdges().second;
					auto connectedHalfFaces2 = OwnConnectedStructure<HalfCell<VectorType>>::get(pPointingFromHalfEdge)->getConnectedStructures();
					assert(connectedHalfFaces2.size() == 1); //Should hold for 2-D;
					pNextCell = connectedHalfFaces2.front()->getCell();

					//If the curr and next cells don't have a dual element, construct the dual vertices
					shared_ptr<Vertex<VectorType>> dualVertex1 = nullptr;
					shared_ptr<Vertex<VectorType>> dualVertex2 = nullptr;

					
					if (!pCurrCell->isSolid()) {
						if (pCurrCell->getDualElement2D() == nullptr) {
							dualVertex1 = this->constructVertex(pCurrCell->getCentroid(), gridVertex);
							dualVertex1->setDualElement2D(pCurrCell);
							pCurrCell->setDualElement2D(dualVertex1);
						}
						else {
							dualVertex1 = pCurrCell->getDualElement2D();
						}
					}
					else {
						if(solverType != streamfunctionDECSolver)
							dualVertex1 = nullptr;
						else {
							//Check that there is at least one internalBorderVertex
							int count = 0;
							auto pVertices = pCurrCell->getQuadCell()->getVertices();
							for (auto pVertex : pVertices)
								if (pVertex->isInternalBorder()) ++count;
							
							if (count > 0) {
								if (pCurrCell->getDualElement2D() == nullptr) {
									dualVertex1 = this->constructVertex(pCurrCell->getCentroid(), gridVertex);
									dualVertex1->setDualElement2D(pCurrCell);
									pCurrCell->setDualElement2D(dualVertex1);
								}
								else {
									dualVertex1 = pCurrCell->getDualElement2D();
								}
							}
						}
					}

					if (!pNextCell->isSolid()) {
						if (pNextCell->getDualElement2D() == nullptr) {
							dualVertex2 = this->constructVertex(pNextCell->getCentroid(), gridVertex);
							dualVertex2->setDualElement2D(pNextCell);
							pNextCell->setDualElement2D(dualVertex2);
						}
						else {
							dualVertex2 = pNextCell->getDualElement2D();
						}
					}
					else {
						if (solverType != streamfunctionDECSolver)
							dualVertex2 = nullptr;
						else {
							//Check that there is at least one internalBorderVertex
							int count = 0;
							auto pVertices = pNextCell->getQuadCell()->getVertices();
							for (auto pVertex : pVertices)
								if (pVertex->isInternalBorder()) ++count;

							if (count > 0) {
								if (pNextCell->getDualElement2D() == nullptr) {
									dualVertex2 = this->constructVertex(pNextCell->getCentroid(), gridVertex);
									dualVertex2->setDualElement2D(pNextCell);
									pNextCell->setDualElement2D(dualVertex2);
								}
								else {
									dualVertex2 = pNextCell->getDualElement2D();
								}
							}
						}
					}

					//If the current edge doesn't have a dual element, construct the dual edge
					shared_ptr<Edge<VectorType>> dualEdge;
					if (pCurrEdge->getDualElement2D() == nullptr && dualVertex1 != nullptr && dualVertex2 != nullptr && !(pCurrEdge->getVertex1()->isSolid() && pCurrEdge->getVertex2()->isSolid())) {
						//yAlignedEdge
						if (dualVertex1->getPosition().x == dualVertex2->getPosition().x) {
							if (dualVertex1->getPosition().y < dualVertex2->getPosition().y)
								dualEdge = this->constructElement(dualVertex1, dualVertex2, yAlignedEdge);
							else
								dualEdge = this->constructElement(dualVertex2, dualVertex1, yAlignedEdge);
						}
						//xAlignedEdge
						else if (dualVertex1->getPosition().y == dualVertex2->getPosition().y) {
							if (dualVertex1->getPosition().x < dualVertex2->getPosition().x)
								dualEdge = this->constructElement(dualVertex1, dualVertex2, xAlignedEdge);
							else
								dualEdge = this->constructElement(dualVertex2, dualVertex1, xAlignedEdge);
						}
						dualEdge->setDualElement2D(pCurrEdge);
						pCurrEdge->setDualElement2D(dualEdge);
					}

					
					/** Find the next edge from the list of possible edges*/
					for (auto pTestEdge : connectedEdges) {
						if (pTestEdge->getID() != pCurrEdge->getID() && !pTestEdge->isVisited()) {
							vector<shared_ptr<Cell<VectorType>>> connectedFaces = OwnConnectedStructure<Cell<VectorType>>::get(pTestEdge)->getConnectedStructures();
							if (connectedFaces.size() != 2) {
								throw(std::logic_error("QuadGridMesh::findCycle: Invalid number of connected faces"));
							}
							else {
								if (connectedFaces[0]->getID() == pNextCell->getID() || connectedFaces[1]->getID() == pNextCell->getID()) {
									visitConnectedEdges(pInitialVertex, pTestEdge, connectedEdges, gridSpacing, solverType);
								}
							}
						}
					}
				}
		}


		template class DualMesh<QuadGridType>;
		//template class DualMesh<QuadGridType, LineMesh<Vector2D>>;
	}
}
