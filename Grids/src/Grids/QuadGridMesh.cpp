//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#include "Grids/QuadGridMesh.h"

namespace Chimera {
	namespace Grids {

		template<typename QuadGridMeshType>
		QuadGridMesh<QuadGridMeshType>::QuadGridMesh(const VectorType& initialPoint, const VectorType& finalPoint, Scalar gridSpacing, cellLocation_t cellLocation /*= XYPlane*/, const vector<shared_ptr<IntersectedLineMesh<VectorType>>>& lineMeshes)
			: ParentClass(initialPoint, finalPoint, gridSpacing), m_cellsLocation(cellLocation), m_lineMeshes(lineMeshes) {
			dimensions_t gridDimensions = ParentClass::m_gridDimensions;
			m_pGridCells = shared_ptr<Array2D<shared_ptr<QuadCell<VectorType>>>>(new Array2D<shared_ptr<QuadCell<VectorType>>>(gridDimensions));
			m_pVerticalEdges = constructArray2DVectorOffPtr<Edge>(dimensions_t(gridDimensions.x + 1, gridDimensions.y), true);
			m_pHorizontalEdges = constructArray2DVectorOffPtr<Edge>(dimensions_t(gridDimensions.x, gridDimensions.y + 1), true);
			m_pNodeVertices = constructArray2DOffPtr<Vertex>(dimensions_t(gridDimensions.x + 1, gridDimensions.y + 1));

			m_sliceIndex = 0;
			m_hasDanglingFaces = false;

			m_pLinePatches = shared_ptr<Array2D<vector<pair<uint, vector<uint>>>>>(new Array2D<vector<pair<uint, vector<uint>>>>(dimensions_t(gridDimensions.x, gridDimensions.y)));

			if (lineMeshes.size() > 0) {
				buildLinePatches();
			}

			buildVertices();
			buildEdges();
			buildCells();

			initializeBorderEdges();
			initializeBorderVertices();

		}

		template <typename QuadGridMeshType>
		QuadGridMesh<QuadGridMeshType>::QuadGridMesh(const extStructures_t& extStructures, Scalar gridSpacing, const dimensions_t& gridDimensions, cellLocation_t cellLocation /*= XYPlane*/,
																								const vector<shared_ptr<IntersectedLineMesh<VectorType>>>& lineMeshes)
			: ParentClass(gridSpacing, gridDimensions),
			m_lineMeshes(lineMeshes),
			m_cellsLocation(cellLocation),
			m_pGridCells(make_shared<Array2D<shared_ptr<QuadCell<VectorType>>>>(gridDimensions)),
			m_pVerticalEdges(extStructures.pVerticalEdges),
			m_pHorizontalEdges(extStructures.pHorizontalEdges),
			m_pNodeVertices(extStructures.pNodeVertices) {

			dimensions_t lGridDimensions = ParentClass::m_gridDimensions;

			GrandParentClass::m_bounds = extStructures.bounds;
			m_sliceIndex = 0;
			m_hasDanglingFaces = false;

			m_pLinePatches = shared_ptr<Array2D<vector<pair<uint, vector<uint>>>>>(new Array2D<vector<pair<uint, vector<uint>>>>(lGridDimensions));
			if (lineMeshes.size() > 0) {
				buildLinePatches();
			}

			//Adding external structures to parent class
			for (uint i = 0; i < extStructures.pNodeVertices->getDimensions().x; i++) {
				for (uint j = 0; j < extStructures.pNodeVertices->getDimensions().y; j++) {
					ParentClass::m_vertices.push_back((*extStructures.pNodeVertices)(i, j));
				}
			}

			//Adding external structures to parent class
			for (uint i = 0; i < extStructures.pVerticalEdges->getDimensions().x; i++) {
				for (uint j = 0; j < extStructures.pVerticalEdges->getDimensions().y; j++) {
					ParentClass::m_edges.insert(ParentClass::m_edges.end(), (*extStructures.pVerticalEdges)(i, j)->begin(), (*extStructures.pVerticalEdges)(i, j)->end());
				}
			}

			//Adding external structures to parent class
			for (uint i = 0; i < extStructures.pHorizontalEdges->getDimensions().x; i++) {
				for (uint j = 0; j < extStructures.pHorizontalEdges->getDimensions().y; j++) {
					ParentClass::m_edges.insert(ParentClass::m_edges.end(), (*extStructures.pHorizontalEdges)(i, j)->begin(), (*extStructures.pHorizontalEdges)(i, j)->end());
				}
			}

			buildVertices();
			buildEdges();
			buildCells();

			initializeBorderEdges();
			initializeBorderVertices();
		}


		#pragma region InitializationFunctions
		template<typename QuadGridMeshType>
		shared_ptr<Vertex<typename QuadGridMeshType::VectorType>> QuadGridMesh<QuadGridMeshType>::createNodalVertex(uint i, uint j) {
			isVector2<VectorType> isVector2D;
			if (m_cellsLocation == XYPlane) {
				VectorType vertexPosition;
				vertexPosition[0] = i * ParentClass::m_gridSpacing;
				vertexPosition[1] = j * ParentClass::m_gridSpacing;
				if (!isVector2D.value) {
					vertexPosition[2] = m_sliceIndex * ParentClass::m_gridSpacing;
				}
				return this->constructVertex(vertexPosition + ParentClass::m_bounds.first, gridVertex);
			}
			else if (m_cellsLocation == XZPlane) {
				if (!isVector2D.value) {
					throw std::logic_error("Invalid call to createNodalVertex: 2-D vertices cannot be on XZPlane");
				}
				VectorType vertexPosition;
				vertexPosition[0] = i * ParentClass::m_gridSpacing;
				vertexPosition[1] = m_sliceIndex * ParentClass::m_gridSpacing;
				vertexPosition[2] = j * ParentClass::m_gridSpacing;

				return this->constructVertex(vertexPosition + ParentClass::m_bounds.first, gridVertex);
			}
			else if (m_cellsLocation == YZPlane) {
				if (!isVector2D.value) {
					throw std::logic_error("Invalid call to createNodalVertex: 2-D vertices cannot be on YZPlane");
				}
				VectorType vertexPosition;
				vertexPosition[0] = m_sliceIndex * ParentClass::m_gridSpacing;
				vertexPosition[1] = j * ParentClass::m_gridSpacing;
				vertexPosition[2] = i * ParentClass::m_gridSpacing;

				return this->constructVertex(vertexPosition + ParentClass::m_bounds.first, gridVertex);
			}

			return nullptr;
		}

		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::buildLinePatches() {
			for (int i = 0; i < ParentClass::m_gridDimensions.x; i++) {
				for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
					for (int k = 0; k < m_lineMeshes.size(); k++) {
						if (m_lineMeshes[k]->getPatchesIndices(i, j).size() > 0) {
							(*m_pLinePatches)(i, j).push_back(pair<uint, vector<uint>>(k, m_lineMeshes[k]->getPatchesIndices(i, j)));
						}
					}
				}
			}

			/** Treatment of vertices that are on top of grid nodes. Adopted solution is to add empty geometry patches
				to all adjacent cells to grid nodes geometry vertices. */
			vector<dimensions_t> cornerCases;
			for (int k = 0; k < m_lineMeshes.size(); k++) {
				const vector<shared_ptr<Edge<VectorType>>>& elements = m_lineMeshes[k]->getElements();
				for (uint j = 0; j < elements.size(); j++) {
					shared_ptr<Edge<VectorType>>pCurrEdge = elements[j];
					if (pCurrEdge->getVertex2()->isOnGridNode()) {
						dimensions_t vertexDim;
						vertexDim.x = floor(pCurrEdge->getVertex2()->getPosition().x / ParentClass::m_gridSpacing);
						vertexDim.y = floor(pCurrEdge->getVertex2()->getPosition().y / ParentClass::m_gridSpacing);
						cornerCases.emplace_back(vertexDim);
					}

					if (!m_lineMeshes[k]->isClosedMesh() && j == 0) {
						if (pCurrEdge->getVertex1()->isOnGridNode()) {
							dimensions_t vertexDim;
							vertexDim.x = floor(pCurrEdge->getVertex1()->getPosition().x / ParentClass::m_gridSpacing);
							vertexDim.y = floor(pCurrEdge->getVertex1()->getPosition().y / ParentClass::m_gridSpacing);
							cornerCases.emplace_back(vertexDim);
						}
					}
				}
			}

			/** Check corner cases and add adjacent cells */
			for (uint j = 0; j < cornerCases.size(); j++) {
				if ((*m_pLinePatches)(cornerCases[j]).empty())
					(*m_pLinePatches)(cornerCases[j]).emplace_back();

				if ((*m_pLinePatches)(cornerCases[j].x - 1, cornerCases[j].y).empty())
					(*m_pLinePatches)(cornerCases[j].x - 1, cornerCases[j].y).emplace_back();

				if ((*m_pLinePatches)(cornerCases[j].x, cornerCases[j].y - 1).empty())
					(*m_pLinePatches)(cornerCases[j].x, cornerCases[j].y - 1).emplace_back();

				if ((*m_pLinePatches)(cornerCases[j].x - 1, cornerCases[j].y - 1).empty())
					(*m_pLinePatches)(cornerCases[j].x - 1, cornerCases[j].y - 1).emplace_back();
			}
		}

		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::buildVertices() {
			for (int i = 0; i < ParentClass::m_gridDimensions.x; i++) {
				for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
					//Check if any of the vertices of the line mesh are on top of a grid nodes
					for (int k = 0; k < (*m_pLinePatches)(i, j).size(); k++) {
						uint lineMeshIndex = (*m_pLinePatches)(i, j)[k].first;
						auto lineMeshPatches = (*m_pLinePatches)(i, j)[k].second;

						for (int l = 0; l < lineMeshPatches.size(); l++) {
							shared_ptr<Edge<VectorType>>pEdge = m_lineMeshes[lineMeshIndex]->getElements()[lineMeshPatches[l]];

							if (pEdge->getVertex1()->isOnGridNode()) { //If it is, initialize the node vertex to be this edge vertex
								const VectorType& position = pEdge->getVertex1()->getPosition();
								dimensions_t nodeVertexDim(position.x / ParentClass::m_gridSpacing, position.y / ParentClass::m_gridSpacing);
								(*m_pNodeVertices)(nodeVertexDim) = pEdge->getVertex1();
							}
							if (pEdge->getVertex2()->isOnGridNode()) {
								const VectorType& position = pEdge->getVertex2()->getPosition();
								dimensions_t nodeVertexDim(position.x / ParentClass::m_gridSpacing, position.y / ParentClass::m_gridSpacing);
								(*m_pNodeVertices)(nodeVertexDim) = pEdge->getVertex2();
							}
						}
					}

					/** If no line meshes vertices are on top, create a gridVertex */
					if (!validVertex(i, j)) {
						(*m_pNodeVertices)(i, j) = createNodalVertex(i, j);
					}
				}
			}

			/** Adding outer band of vertices */
			for (int i = 0; i <= ParentClass::m_gridDimensions.x; i++) {
				if (!validVertex(i, ParentClass::m_gridDimensions.y)) {
					(*m_pNodeVertices)(i, ParentClass::m_gridDimensions.y) = createNodalVertex(i, ParentClass::m_gridDimensions.y);
				}
			}
			for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
				if (!validVertex(ParentClass::m_gridDimensions.x, j)) {
					(*m_pNodeVertices)(ParentClass::m_gridDimensions.x, j) = createNodalVertex(ParentClass::m_gridDimensions.x, j);
				}
			}

			//Adding all edge vertices to be accessible from the m_vertices vector
			for (int i = 0; i < m_lineMeshes.size(); i++) {
				for (int j = 0; j < m_lineMeshes[i]->getVertices().size(); j++) {
					this->m_vertices.push_back(m_lineMeshes[i]->getVertices()[j]);
				}
			}
		}

		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::buildEdges() {
			/** Initialize grid edges*/
			for (int i = 0; i < ParentClass::m_gridDimensions.x; i++) {
				for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
					
					if ((*m_pLinePatches)(i, j).size() > 0) {
						vector <shared_ptr<Vertex<VectorType>>> bottomVertices, leftVertices, topVertices, rightVertices;
						//Add all vertices from all line patches
						for (int k = 0; k < (*m_pLinePatches)(i, j).size(); k++) {
							uint lineMeshIndex = (*m_pLinePatches)(i, j)[k].first;
							auto lineMeshpatch = (*m_pLinePatches)(i, j)[k];
							auto lineMeshPatches = (*m_pLinePatches)(i, j)[k].second;

							for (int l = 0; l < lineMeshPatches.size(); l++) {
								shared_ptr<Edge<VectorType>> pEdge = m_lineMeshes[lineMeshIndex]->getElements()[lineMeshPatches[l]];
								OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute("relativeFraction", pEdge->getLength() / ParentClass::m_gridSpacing);
								if (pEdge->getVertex1()->getType() == edgeVertex && !pEdge->getVertex1()->isOnGridNode()) {
									classifyVertex(dimensions_t(i, j), pEdge->getVertex1(), bottomVertices, leftVertices, topVertices, rightVertices);
								}
								if (pEdge->getVertex2()->getType() == edgeVertex && !pEdge->getVertex2()->isOnGridNode()) {
									classifyVertex(dimensions_t(i, j), pEdge->getVertex2(), bottomVertices, leftVertices, topVertices, rightVertices);
								}
							}
						}
						//Sort crossings
						sortVertices(bottomVertices, leftVertices, topVertices, rightVertices);

						/** Bottom-Top edges initialization */
						shared_ptr<Vertex<VectorType>>pIniVertex = (*m_pNodeVertices)(i, j);
						shared_ptr<Vertex<VectorType>>pFinalVertex = (*m_pNodeVertices)(i + 1, j);
						dimensions_t currDimension(i, j);
						if ((*m_pHorizontalEdges)(i, j)->size() == 0) { //Only initialize if another cut-slice didn't initialized this edge vector (valid for 3-D only)
							if (bottomVertices.size() == 0) { //Create full grid edge
								if (!hasAlignedEdges(pIniVertex->getID(), pFinalVertex->getID(), currDimension, bottomHalfEdge)) { //Only add if this edge is not aligned with geometry edges
									(*m_pHorizontalEdges)(i, j)->push_back(createGridEdge(pIniVertex, pFinalVertex, bottomHalfEdge));
								}
							}
							else {
								shared_ptr<Vertex<VectorType>>pCurrVertex = pIniVertex;
								for (int k = 0; k < bottomVertices.size(); k++) {
									shared_ptr<Vertex<VectorType>>pEdgeVertex = bottomVertices[k];
									if (!hasAlignedEdges(pCurrVertex->getID(), pEdgeVertex->getID(), currDimension, bottomHalfEdge)) { //Only add if this edge is not aligned with geometry edges
										(*m_pHorizontalEdges)(i, j)->push_back(createGridEdge(pCurrVertex, pEdgeVertex, bottomHalfEdge));
									}
									pCurrVertex = pEdgeVertex;
								}
								if (!hasAlignedEdges(pCurrVertex->getID(), pFinalVertex->getID(), currDimension, bottomHalfEdge)) { //Only add if this edge is not aligned with geometry edges
									(*m_pHorizontalEdges)(i, j)->push_back(createGridEdge(pCurrVertex, pFinalVertex, bottomHalfEdge));
								}
							}
						}
						else {

						}

						/** Left edges initialization */
						pFinalVertex = (*m_pNodeVertices)(i, j + 1);
						if ((*m_pVerticalEdges)(i, j)->size() == 0) {//Only initialize if another cut-slice didn't initialized this edge vector (valid for 3-D only)
							if (leftVertices.size() == 0) { //Create full grid edge
								if (!hasAlignedEdges(pIniVertex->getID(), pFinalVertex->getID(), currDimension, leftHalfEdge)) { //Only add if this edge is not aligned with geometry edges
									(*m_pVerticalEdges)(i, j)->push_back(createGridEdge(pIniVertex, pFinalVertex, leftHalfEdge));
								}
							}
							else {
								shared_ptr<Vertex<VectorType>>pCurrVertex = pIniVertex;
								for (int k = 0; k < leftVertices.size(); k++) {
									shared_ptr<Vertex<VectorType>>pEdgeVertex = leftVertices[k];
									if (!hasAlignedEdges(pCurrVertex->getID(), pEdgeVertex->getID(), currDimension, leftHalfEdge)) { //Only add if this edge is not aligned with geometry edges
										(*m_pVerticalEdges)(i, j)->push_back(createGridEdge(pCurrVertex, pEdgeVertex, leftHalfEdge));
									}
									pCurrVertex = pEdgeVertex;
								}
								if (!hasAlignedEdges(pCurrVertex->getID(), pFinalVertex->getID(), currDimension, leftHalfEdge)) { //Only add if this edge is not aligned with geometry edges
									(*m_pVerticalEdges)(i, j)->push_back(createGridEdge(pCurrVertex, pFinalVertex, leftHalfEdge));
								}
							}
						}


						pIniVertex = (*m_pNodeVertices)(i, j + 1);
						pFinalVertex = (*m_pNodeVertices)(i + 1, j + 1);
						if ((*m_pHorizontalEdges)(i, j + 1)->size() == 0 && topVertices.size() == 0 && (*m_pLinePatches)(i, j + 1).size() == 0) {
							if (!hasAlignedEdges(pIniVertex->getID(), pFinalVertex->getID(), currDimension, topHalfEdge)) { //Only add if this edge is not aligned with geometry edges
								(*m_pHorizontalEdges)(i, j + 1)->push_back(createGridEdge(pIniVertex, pFinalVertex, topHalfEdge));
							}
						}

						pIniVertex = (*m_pNodeVertices)(i + 1, j);
						pFinalVertex = (*m_pNodeVertices)(i + 1, j + 1);
						if ((*m_pVerticalEdges)(i + 1, j)->size() == 0 && rightVertices.size() == 0 && (*m_pLinePatches)(i + 1, j).size() == 0) {
							if (!hasAlignedEdges(pIniVertex->getID(), pFinalVertex->getID(), currDimension, rightHalfEdge)) { //Only add if this edge is not aligned with geometry edges
								(*m_pVerticalEdges)(i + 1, j)->push_back(createGridEdge(pIniVertex, pFinalVertex, rightHalfEdge));
								OwnCustomAttribute<Scalar>::get((*m_pVerticalEdges)(i + 1, j)->back())->setAttribute("relativeFraction", 1.0f);
							}
						}
					} else { /** Initialize regular grid edges */
						if ((*m_pHorizontalEdges)(i, j)->size() == 0) { //Only initialize if another cut-slice didn't initialized this edge vector
							(*m_pHorizontalEdges)(i, j)->push_back(createGridEdge((*m_pNodeVertices)(i, j), (*m_pNodeVertices)(i + 1, j), bottomHalfEdge));
						}
						if ((*m_pVerticalEdges)(i, j)->size() == 0) {//Only initialize if another cut-slice didn't initialized this edge vector
							(*m_pVerticalEdges)(i, j)->push_back(createGridEdge((*m_pNodeVertices)(i, j), (*m_pNodeVertices)(i, j + 1), leftHalfEdge));
						}

						if (i == ParentClass::m_gridDimensions.x - 1) {
							if ((*m_pVerticalEdges)(i + 1, j)->size() == 0) {//Only initialize if another cut-slice didn't initialized this edge vector
								(*m_pVerticalEdges)(i + 1, j)->push_back(createGridEdge((*m_pNodeVertices)(i + 1, j), (*m_pNodeVertices)(i + 1, j + 1), rightHalfEdge));
							}
						}
						if (j == ParentClass::m_gridDimensions.y - 1) {
							if ((*m_pHorizontalEdges)(i, j + 1)->size() == 0) {//Only initialize if another cut-slice didn't initialized this edge vector
								(*m_pHorizontalEdges)(i, j + 1)->push_back(createGridEdge((*m_pNodeVertices)(i, j + 1), (*m_pNodeVertices)(i + 1, j + 1), topHalfEdge));
							}
						}
					}
				}
			}


			/** Add other line mesh edges to the QuadGridMesh */
			for (auto pLineMesh : m_lineMeshes) {
				for (auto pEdge : pLineMesh->getElements()) {
					ParentClass::m_edges.push_back(pEdge);
				}
			}
		


		}

		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::buildCells() {
			for (int i = 0; i < ParentClass::m_gridDimensions.x; i++) {
				for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
					vector<shared_ptr<Edge<VectorType>>> faceEdges;
					//Circle around Bottom, right, top, left edges adding those to quadCell vector structure
					faceEdges.insert(faceEdges.end(), (*m_pHorizontalEdges)(i, j)->begin(), (*m_pHorizontalEdges)(i, j)->end());
					faceEdges.insert(faceEdges.end(), (*m_pVerticalEdges)(i + 1, j)->begin(), (*m_pVerticalEdges)(i + 1, j)->end());
					faceEdges.insert(faceEdges.end(), (*m_pHorizontalEdges)(i, j + 1)->begin(), (*m_pHorizontalEdges)(i, j + 1)->end());
					faceEdges.insert(faceEdges.end(), (*m_pVerticalEdges)(i, j)->begin(), (*m_pVerticalEdges)(i, j)->end());

					for (int k = 0; k < (*m_pLinePatches)(i, j).size(); k++) {
						uint lineMeshIndex = (*m_pLinePatches)(i, j)[k].first;
						auto lineMeshPatches = (*m_pLinePatches)(i, j)[k].second;

						//LineMesh vertices
						for (int l = 0; l < lineMeshPatches.size(); l++) {
							faceEdges.push_back(m_lineMeshes[lineMeshIndex]->getElements()[lineMeshPatches[l]]);
						}
					}

					this->constructElement(faceEdges, dimensions_t(i, j), ParentClass::m_gridSpacing, m_cellsLocation);
					(*m_pGridCells)(i, j) = this->getElements().back();

					ParentClass::m_cells.insert(ParentClass::m_cells.end(), (*m_pGridCells)(i, j)->getCells().begin(), (*m_pGridCells)(i, j)->getCells().end());
				}
			}
		}

		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::initializeBorderEdges() {
			//Only the edges that lies on the outer boundary of the domain are borderEdges
			
			for (int i = 0; i < ParentClass::m_gridDimensions.x; i++) {
				//bottom
				{
					auto edges = getQuadGridCell(i, 0)->getEdges();
					for (auto pEdge : edges) {
						if (pEdge->getCentroid().y == ParentClass::getBounds().first[1])
							pEdge->setBorder(true);
					}
				}

				//top
				{
					auto edges = getQuadGridCell(i, ParentClass::m_gridDimensions.y - 1)->getEdges();
					for (auto pEdge : edges) {
						if (pEdge->getCentroid().y == ParentClass::getBounds().second[1])
							pEdge->setBorder(true);
					}
				}
			}

			for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
				//left
				{
					auto edges = getQuadGridCell(0, j)->getEdges();
					for (auto pEdge : edges) {
						if (pEdge->getCentroid().x == ParentClass::getBounds().first[0])
							pEdge->setBorder(true);
					}
				}

				//right
				{
					auto edges = getQuadGridCell(ParentClass::m_gridDimensions.x - 1, j)->getEdges();
					for (auto pEdge : edges) {
						if (pEdge->getCentroid().x == ParentClass::getBounds().second[0])
							pEdge->setBorder(true);
					}
				}
			}
		}

		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::initializeBorderVertices() {
			//Only the vertices that lies on the outer boundary of the domain are borderVertices

			for (int i = 0; i < ParentClass::m_gridDimensions.x; i++) {
				//bottom
				{
					auto vertices = getQuadGridCell(i, 0)->getVertices();
					for (auto pVertex : vertices) {
						if (pVertex->getPosition().y == ParentClass::getBounds().first[1]) {
							pVertex->setBorder(true);
						}
					}
				}

				//top
				{
					auto vertices = getQuadGridCell(i, ParentClass::m_gridDimensions.y - 1)->getVertices();
					for (auto pVertex : vertices) {
						if (pVertex->getPosition().y == ParentClass::getBounds().second[1]) {
							pVertex->setBorder(true);
						}
					}
				}
			}

			for (int j = 0; j < ParentClass::m_gridDimensions.y; j++) {
				//left
				{
					auto vertices = getQuadGridCell(0, j)->getVertices();
					for (auto pVertex : vertices) {
						if (pVertex->getPosition().x == ParentClass::getBounds().first[0]) {
							pVertex->setBorder(true);
						}
					}
				}

				//right
				{
					auto vertices = getQuadGridCell(ParentClass::m_gridDimensions.x - 1, j)->getVertices();
					for (auto pVertex : vertices) {
						if (pVertex->getPosition().x == ParentClass::getBounds().second[0]) {
							pVertex->setBorder(true);
						}
					}
				}
			}
		}

		#pragma endregion

		#pragma region AuxiliaryHelperFunctions
		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::classifyVertex(const dimensions_t& gridDim, shared_ptr<Vertex<VectorType>>pVertex, vector<shared_ptr<Vertex<VectorType>>>& bottomVertices, vector<shared_ptr<Vertex<VectorType>>>& leftVertices,
																									vector<shared_ptr<Vertex<VectorType>>>& topVertices, vector<shared_ptr<Vertex<VectorType>>>& rightVertices) {
			if (pVertex->getType() != edgeVertex)
				return;

			isVector2<VectorType> isVector2D;
			if (isVector2D.value && (m_cellsLocation == XZPlane || m_cellsLocation == YZPlane))
				throw std::logic_error("QuadGridMesh::classifyVertex(): cannot handle XZ or YZ planes with Vector2 classes");

			VectorType gridSpaceVertex = pVertex->getPosition() / this->ParentClass::m_gridSpacing;
			Scalar vx, vy;
			switch (m_cellsLocation) {
				case XYPlane:
					vx = gridSpaceVertex[0] - gridDim[0];
					vy = gridSpaceVertex[1] - gridDim[1];
				break;
			case XZPlane:
				vx = gridSpaceVertex[0] - gridDim[0];
				//grimDim[1] acts like z here
				vy = gridSpaceVertex[2] - gridDim[1];
				break;
			case YZPlane:
				//grimDim[0] acts like z here
				vx = gridSpaceVertex[2] - gridDim[0];
				vy = gridSpaceVertex[1] - gridDim[1];
				break;
			}

			if (vx == 0) { //Left-edge
				leftVertices.push_back(pVertex);
			}
			else if (vy == 0) { //Bottom-edge
				bottomVertices.push_back(pVertex);
			}
			else if (vx == 1) {
				rightVertices.push_back(pVertex);
			}
			else if (vy == 1) {
				topVertices.push_back(pVertex);
			}
		}

		template<typename QuadGridMeshType>
		shared_ptr<Edge<typename QuadGridMeshType::VectorType>> QuadGridMesh<QuadGridMeshType>::createGridEdge(	shared_ptr<Vertex<VectorType>> pV1,
																																																						shared_ptr<Vertex<VectorType>> pV2,
																																																						halfEdgeLocation_t halfEdgeLocation) {
			isVector2<VectorType> isVector2D;
			if (isVector2D.value && (m_cellsLocation == XZPlane || m_cellsLocation == YZPlane))
				throw std::logic_error("QuadGridMesh::createGridEdge(): cannot handle XZ or YZ planes with Vector2 classes");

			shared_ptr<Edge<VectorType>> pEdge;

			switch (m_cellsLocation) {
				case XYPlane:
					if (halfEdgeLocation == bottomHalfEdge || halfEdgeLocation == topHalfEdge) {
						pEdge = Edge<VectorType>::create(pV1, pV2, xAlignedEdge);
					}
					else if (halfEdgeLocation == leftHalfEdge || halfEdgeLocation == rightHalfEdge) {
						pEdge = Edge<VectorType>::create(pV1, pV2, yAlignedEdge);
					}
					else {
						throw(std::logic_error("QuadGridMesh createGridEdge: invalid edge location"));
					}
				break;
				case XZPlane:
					if (halfEdgeLocation == bottomHalfEdge || halfEdgeLocation == topHalfEdge) {
						pEdge = Edge<VectorType>::create(pV1, pV2, xAlignedEdge);
					}
					else if (halfEdgeLocation == leftHalfEdge || halfEdgeLocation == rightHalfEdge) {
						pEdge = Edge<VectorType>::create(pV1, pV2, zAlignedEdge);
					}
					else {
						throw(std::logic_error("QuadGridMesh createGridEdge: invalid edge location"));
					}
				break;
				case YZPlane:
					if (halfEdgeLocation == bottomHalfEdge || halfEdgeLocation == topHalfEdge) {
						pEdge = Edge<VectorType>::create(pV1, pV2, zAlignedEdge);
					}
					else if (halfEdgeLocation == leftHalfEdge || halfEdgeLocation == rightHalfEdge) {
						pEdge = Edge<VectorType>::create(pV1, pV2, yAlignedEdge);
					}
					else {
						throw(std::logic_error("QuadGridMesh createGridEdge: invalid edge location"));
					}
				break;
			}

			/** Adding edges to parent class*/
			ParentClass::m_edges.push_back(pEdge);
			OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute("relativeFraction", pEdge->getLength() / ParentClass::m_gridSpacing);

			return pEdge;
		}

		template <class VectorType>
		bool compareVerticesHorizontal(shared_ptr<Vertex<VectorType>> pV1, shared_ptr<Vertex<VectorType>> pV2) {
			return pV1->getPosition().x < pV2->getPosition().x;
		}

		template <class VectorType>
		bool compareVerticesVertical(shared_ptr<Vertex<VectorType>> pV1, shared_ptr<Vertex<VectorType>> pV2) {
			return pV1->getPosition().y < pV2->getPosition().y;
		}

		template <class VectorType>
		bool compareVerticesTransversal(shared_ptr<Vertex<VectorType>> pV1, shared_ptr<Vertex<VectorType>> pV2) {
			return pV1->getPosition()[2] < pV2->getPosition()[2];
		}

		template<typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::sortVertices(vector<shared_ptr<Vertex<VectorType>>>& bottomVertices, vector<shared_ptr<Vertex<VectorType>>>& leftVertices,
																								vector<shared_ptr<Vertex<VectorType>>>& topVertices, vector<shared_ptr<Vertex<VectorType>>>& rightVertices) {
			isVector2<VectorType> isVector2D;
			if (isVector2D.value && (m_cellsLocation == XZPlane || m_cellsLocation == YZPlane))
				throw std::logic_error("QuadGridMesh::classifyVertex(): cannot handle XZ or YZ planes with Vector2 classes");

			switch (m_cellsLocation) {
			case XYPlane:
				sort(bottomVertices.begin(), bottomVertices.end(), compareVerticesHorizontal<VectorType>);
				sort(leftVertices.begin(), leftVertices.end(), compareVerticesVertical<VectorType>);
				sort(topVertices.begin(), topVertices.end(), compareVerticesHorizontal<VectorType>);
				sort(rightVertices.begin(), rightVertices.end(), compareVerticesVertical<VectorType>);
				break;
			case XZPlane:
				sort(bottomVertices.begin(), bottomVertices.end(), compareVerticesHorizontal<VectorType>);
				sort(leftVertices.begin(), leftVertices.end(), compareVerticesTransversal<VectorType>);
				sort(topVertices.begin(), topVertices.end(), compareVerticesHorizontal<VectorType>);
				sort(rightVertices.begin(), rightVertices.end(), compareVerticesTransversal<VectorType>);
				break;
			case YZPlane:
				sort(bottomVertices.begin(), bottomVertices.end(), compareVerticesTransversal<VectorType>);
				sort(leftVertices.begin(), leftVertices.end(), compareVerticesVertical<VectorType>);
				sort(topVertices.begin(), topVertices.end(), compareVerticesTransversal<VectorType>);
				sort(rightVertices.begin(), rightVertices.end(), compareVerticesVertical<VectorType>);
				break;
			}
		}
		#pragma endregion

		#pragma region AuxiliaryHelperFunctions
		template<typename QuadGridMeshType>
		bool QuadGridMesh<QuadGridMeshType>::hasAlignedEdges(uint vertex1, uint vertex2, dimensions_t linePatchesDim) {
			if(linePatchesDim.x < 0 || linePatchesDim.y < 0 || linePatchesDim.z < 0) return false;
			for (int k = 0; k < (*m_pLinePatches)(linePatchesDim).size(); k++) {
				uint lineMeshIndex = (*m_pLinePatches)(linePatchesDim)[k].first;
				auto lineMeshPatches = (*m_pLinePatches)(linePatchesDim)[k].second;

				for (int l = 0; l < lineMeshPatches.size(); l++) {
					shared_ptr<Edge<VectorType>>pEdge = m_lineMeshes[lineMeshIndex]->getElements()[lineMeshPatches[l]];
					if ((pEdge->getVertex1()->getID() == vertex1 && pEdge->getVertex2()->getID() == vertex2) ||
						(pEdge->getVertex2()->getID() == vertex1 && pEdge->getVertex1()->getID() == vertex2)) {
						return true;
					}
				}
			}
			return false;
		}
		#pragma endregion


		#pragma region StaticFunctionalities
		template <typename QuadGridMeshType>
		shared_ptr<QuadGridMesh<QuadGridMeshType>> QuadGridMesh<QuadGridMeshType>::createDualGrid(
				shared_ptr<QuadGridMesh<QuadGridMeshType>> pQuadGridMesh,
				shared_ptr<unordered_map<uint, shared_ptr<Cell<VectorType>>>> pVertexIDToCellMap) {
			QuadGridMesh<QuadGridMeshType>::extStructures_t externalStructures;

			//Grid starts at centroid of the first cell
			VectorType gridOrigin = pQuadGridMesh->getCells(dimensions_t(0, 0))[0]->getCentroid();
			VectorType gridEnd = pQuadGridMesh->getCells(dimensions_t(pQuadGridMesh->getGridDimensions().x - 1, pQuadGridMesh->getGridDimensions().y - 1))[0]->getCentroid();
			externalStructures.bounds.first = gridOrigin;
			externalStructures.bounds.second = gridEnd;


			//The dual grid dimensions will span a space that is a bit smaller than the original one
			dimensions_t newGridDimensions = pQuadGridMesh->getGridDimensions();

			externalStructures.pNodeVertices = constructArray2DOffPtr<Vertex>(newGridDimensions);
			externalStructures.pHorizontalEdges = constructArray2DVectorOffPtr<Edge>(newGridDimensions);
			externalStructures.pVerticalEdges = constructArray2DVectorOffPtr<Edge>(newGridDimensions);

			//Algorithm
			for (auto pVertex : pQuadGridMesh->getVertices()) {
				vector<shared_ptr<Edge<VectorType>>> connectedEdges = OwnConnectedStructure<Edge<VectorType>>::get(pVertex)->getConnectedStructures();
				for (auto pEdge : connectedEdges) {
					pEdge->setVisited(false);
				}

				if (connectedEdges.size() > 3) { //If vertex has > 3 connected edges, change for checking boundaries
					vector<shared_ptr<Edge<VectorType>>> dualEdges;
					findCycle(pVertex, connectedEdges[0], connectedEdges, dualEdges, externalStructures, pVertexIDToCellMap);
				}
			}

			newGridDimensions.x -= 1; newGridDimensions.y -= 1;
			auto dualQuadGridMesh = make_shared<QuadGridMesh<QuadGridMeshType>>(externalStructures, pQuadGridMesh->getGridSpacing(), newGridDimensions);
			matchDualGrids(pQuadGridMesh, dualQuadGridMesh);
			return dualQuadGridMesh;
		}

		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::findCycle(
			shared_ptr<Vertex<VectorType>> pInitialVertex,
			shared_ptr<Edge<VectorType>> pCurrEdge,
			vector<shared_ptr<Edge<VectorType>>> connectedEdges,
			vector<shared_ptr<Edge<VectorType>>> &dualEdges,
			typename QuadGridMesh<QuadGridMeshType>::extStructures_t& externalStructures,
			shared_ptr<unordered_map<uint, shared_ptr<Cell<VectorType>>>> pVertexIDToCellMap) {

			pCurrEdge->setVisited(true);
			shared_ptr<Cell<VectorType>> pCurrCell, pNextCell;
			{
				//Use the half-edge that points to this vertex to get consistent orientations
				shared_ptr<HalfEdge<VectorType>> pPointingToHalfEdge = pCurrEdge->getHalfEdges().first->getVertices().first == pInitialVertex ? pCurrEdge->getHalfEdges().first : pCurrEdge->getHalfEdges().second;
				auto connectedHalfFaces1 = OwnConnectedStructure<HalfCell<VectorType>>::get(pPointingToHalfEdge)->getConnectedStructures();
				assert(connectedHalfFaces1.size() == 1); //Should hold for 2-D;
				pCurrCell = connectedHalfFaces1.front()->getCell();

				//Use the half-edge that points to this vertex to get consistent orientations
				shared_ptr<HalfEdge<VectorType>> pPointingFromHalfEdge = pCurrEdge->getHalfEdges().first->getVertices().second == pInitialVertex ? pCurrEdge->getHalfEdges().first : pCurrEdge->getHalfEdges().second;
				auto connectedHalfFaces2 = OwnConnectedStructure<HalfCell<VectorType>>::get(pPointingFromHalfEdge)->getConnectedStructures();
				assert(connectedHalfFaces2.size() == 1); //Should hold for 2-D;
				pNextCell = connectedHalfFaces2.front()->getCell();
			}

			shared_ptr<Edge<VectorType>> pDualEdge;
			//Create the edge and vertices linked to the dual of this Edge
			{
				Scalar gridSpacing = pCurrCell->getQuadCell()->getGridSpacing();
		
				shared_ptr<Vertex<VectorType>> pV1 = pCurrCell->getDualElement2D();
				if (pV1 == nullptr) {
					pV1 = Vertex<VectorType>::create(pCurrCell->getCentroid(), gridVertex);
					pV1->setDualElement2D(pCurrCell);
				}
				if(pVertexIDToCellMap && !pVertexIDToCellMap->count(pV1->getID())) {
					pVertexIDToCellMap->insert({pV1->getID(), pCurrCell});
				}

				shared_ptr<Vertex<VectorType>> pV2 = pNextCell->getDualElement2D();
				if (pV2 == nullptr) {
					pV2 = Vertex<VectorType>::create(pNextCell->getCentroid(), gridVertex);
					pV2->setDualElement2D(pNextCell);
				}
				if(pVertexIDToCellMap && !pVertexIDToCellMap->count(pV2->getID())) {
					pVertexIDToCellMap->insert({pV2->getID(), pNextCell});
				}

				//Create the edge
				if (pCurrEdge->getType() == xAlignedEdge) {
					VectorType edgeCentroid = (pV1->getPosition() + pV2->getPosition()) * 0.5;
					dimensions_t edgeLocation(floor((edgeCentroid.x - externalStructures.bounds.first.x)/gridSpacing),
																		floor((edgeCentroid.y - externalStructures.bounds.first.y)/gridSpacing));

					if ((*externalStructures.pVerticalEdges)(edgeLocation)->size() == 0) {
						pDualEdge = Edge<VectorType>::create(pV1, pV2, yAlignedEdge);
						pDualEdge->setDualElement2D(pCurrEdge);
						(*externalStructures.pVerticalEdges)(edgeLocation)->push_back(pDualEdge);
					}
					else {
						pDualEdge = (*externalStructures.pVerticalEdges)(edgeLocation)->front();
					}
				}
				else if(pCurrEdge->getType() == yAlignedEdge) {
					VectorType edgeCentroid = (pV1->getPosition() + pV2->getPosition()) * 0.5;
					dimensions_t edgeLocation(floor((edgeCentroid.x - externalStructures.bounds.first.x)/gridSpacing),
																		floor((edgeCentroid.y - externalStructures.bounds.first.y)/gridSpacing));


					if ((*externalStructures.pHorizontalEdges)(edgeLocation)->size() == 0) {
						pDualEdge = Edge<VectorType>::create(pV1, pV2, xAlignedEdge);
						pDualEdge->setDualElement2D(pCurrEdge);
						(*externalStructures.pHorizontalEdges)(edgeLocation)->push_back(pDualEdge);
					}
					else {
						pDualEdge = (*externalStructures.pHorizontalEdges)(edgeLocation)->front();
					}
				}

				dualEdges.push_back(pDualEdge);
			}

			/** Find the next edge from the list of possible edges*/
			for (auto pTestEdge : connectedEdges) {
				if (pTestEdge->getID() != pCurrEdge->getID() && !pTestEdge->isVisited()) {
					vector<shared_ptr<Cell<VectorType>>> connectedFaces = OwnConnectedStructure<Cell<VectorType>>::get(pTestEdge)->getConnectedStructures();
					if (connectedFaces.size() != 2) {
						throw(std::logic_error("QuadGridMesh::findCycle: Invalid number of connected faces"));
					}
					else {
						if (connectedFaces[0]->getID() == pNextCell->getID() || connectedFaces[1]->getID() == pNextCell->getID()) {
							findCycle(pInitialVertex, pTestEdge, connectedEdges, dualEdges, externalStructures, pVertexIDToCellMap);
						}
					}
				}

			}
		}



		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::matchDualGrids(shared_ptr<QuadGridMesh<QuadGridMeshType>> pQuadGridMesh,
			shared_ptr<QuadGridMesh<QuadGridMeshType>> pDualQuadGridMesh) {

			//Match Edges-Edges
			auto dualEdges = pDualQuadGridMesh->getEdges();
			for (auto dualEdge : dualEdges) {
				auto primalEdge = dualEdge->getDualElement2D();
				primalEdge->setDualElement2D(primalEdge);
			}

			//Match Vertices-Cells
			auto dualVertices = pDualQuadGridMesh->getVertices();
			for (auto dualVertex : dualVertices) {
				auto primalCell = dualVertex->getDualElement2D();
				primalCell->setDualElement2D(dualVertex);
			}

			//Match Cells-Vertices
			dimensions_t dualDimensions = pDualQuadGridMesh->getGridDimensions();
			for (int j = 0; j < dualDimensions.y; ++j) {
				for (int i = 0; i < dualDimensions.x; ++i) {
					auto dualCells = pDualQuadGridMesh->getCells(i, j);
					for (auto dualCell : dualCells) {
						auto location = dualCell->getCentroid();
						auto primalVertex = pQuadGridMesh->getVertex(dimensions_t(pQuadGridMesh->getGridSpacePosition(location).x, pQuadGridMesh->getGridSpacePosition(location).y));
						dualCell->setDualElement2D(primalVertex);
						primalVertex->setDualElement2D(dualCell);
					}
				}
			}
		}
		
		template <typename QuadGridMeshType>
		void QuadGridMesh<QuadGridMeshType>::depthFirstSearch(shared_ptr<Cell<VectorType>> pCurrentCell, typename QuadGridMesh<QuadGridMeshType>::extStructures_t &externalStructures) {
			if (pCurrentCell->isVisited()) {
				return;
			}
			pCurrentCell->setVisited(true);

			//for (auto pEdge : pCurrentCell->getEdges()) {
			//	if (!pEdge->isVisited()) {
			//		/** Each edge has to be connected to only two cells*/
			//		vector<shared_ptr<Cell<VectorType>>> connectedCells = OwnConnectedStructure<Cell<VectorType>>::get(pEdge)->getConnectedStructures();
			//		if (connectedCells.size() > 2) {
			//			throw std::logic_error("createDualGrid: Each edge cannot be connected to more than 2 cells");
			//		}
			//		else if (connectedCells.size() == 2) {
			//			dimensions_t vertexLocation(	floor(pCurrentCell->getCentroid().x - externalStructures.bounds.first.x),
			//																		floor(pCurrentCell->getCentroid().y - externalStructures.bounds.first.y));
			//			if ((*externalStructures.pNodeVertices)(vertexLocation) == nullptr) {
			//				(*externalStructures.pNodeVertices)(vertexLocation) = Vertex<VectorType>::create(pCurrentCell->getCentroid(), gridVertex);
			//			}

			//			auto pNextCell = connectedCells[0]->getID() == pCurrentCell->getID() ? connectedCells[1] : connectedCells[0];

			//			dimensions_t nextVertexLocation(	floor(pNextCell->getCentroid().x - externalStructures.bounds.first.x),
			//																				floor(pNextCell->getCentroid().y - externalStructures.bounds.first.y));
			//
			//

			//
			//			//auto pV1 = Vertex<VectorType>::create(pCurrentCell->getCentroid(), gridVertex);
			//			//auto pV1 = pNewMesh->constructVertex(pCurrentCell->getCentroid(), gridVertex);
			//			//auto pV2 = pNewMesh->constructVertex(pNextCell->getCentroid(), gridVertex);

			//			//halfEdgeLocation_t edgeLocation;
			//			//if (pEdge->getType() == xAlignedEdge) { //The edge is opposite than the original orientation
			//			//	edgeLocation = leftHalfEdge;
			//			//}
			//			//else if (pEdge->getType() == yAlignedEdge) {
			//			//	edgeLocation = bottomHalfEdge;
			//			//}

			//			//pNewMesh->createGridEdge(pV1, pV2, edgeLocation);
			//			//pEdge->setVisited(true);

			//			////Go to next cell
			//			//depthFirstSearch(pNextCell, pNewMesh);
			//		}
			//	}
			//}

		}
		#pragma endregion

		template class QuadGridMesh<QuadGridType>;
		template class QuadGridMesh<QuadGridTypeDouble>;
	}
}
