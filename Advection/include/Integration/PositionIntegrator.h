//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef _MATH_PARTICLE_POSITION_INTEGRATOR_H_
#define _MATH_PARTICLE_POSITION_INTEGRATOR_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraInterpolation.h"
#include "ChimeraMesh.h"

namespace Chimera {

	using namespace Core;
	using namespace Interpolation;

	namespace Advection {

		/** Integrates particles position from a vector of VectorType */
		template <typename GridMeshType>
		class PositionIntegrator {
		
		protected:
			using VectorType = typename GridMeshType::VectorType;

		public:
			
			#pragma region Constructors
			PositionIntegrator(shared_ptr<GridMesh<GridMeshType>> pGridMesh, shared_ptr<Interpolant<VectorType, GridMeshType>> pInterpolant) {
				m_pInterpolant = pInterpolant;
				m_pGridMesh = pGridMesh;
			}
			#pragma endregion

			#pragma region Functionalities
			/** Integrate a single position with an initial velocity */
			virtual VectorType integrate(const VectorType &position, const VectorType &velocity, Scalar dt, shared_ptr<Interpolant<VectorType, GridMeshType>> pCustomInterpolant = nullptr) = 0;

			/** Checks if a collision happened with the embedded mesh */
			bool checkCollision(const VectorType& p1, const VectorType& p2, VectorType& collisionPoint);

			#pragma endregion

			#pragma region AccessFunctions
			shared_ptr<Interpolant<VectorType, GridMeshType>> getInterpolant() {
				return m_pInterpolant;
			}
			#pragma endregion

		protected:
			#pragma region PrivateFunctionalities
			void clampPosition(VectorType& position) {
				position[0] = clamp(position[0], m_pGridMesh->getBounds().first[0], m_pGridMesh->getBounds().second[0]);
				position[1] = clamp(position[1], m_pGridMesh->getBounds().first[1], m_pGridMesh->getBounds().second[1]);
				if(isVector3<VectorType>().value)
					position[2] = clamp(position[2], m_pGridMesh->getBounds().first[2], m_pGridMesh->getBounds().second[2]);
				
			}
 
			#pragma region ClassMembers
			shared_ptr<Interpolant<VectorType, GridMeshType>> m_pInterpolant;
			shared_ptr<GridMesh<GridMeshType>> m_pGridMesh;
			#pragma endregion
		
		};
	}
}
#endif
