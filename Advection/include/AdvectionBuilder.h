//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef __CHIMERA_ADVECTION_BUILDER_H_
#define __CHIMERA_ADVECTION_BUILDER_H_

#pragma once

#include "ChimeraCore.h"
#include "ChimeraParticles.h"
#include "ChimeraInterpolation.h"
//
#include "Integration/PositionIntegrator.h"
#include "AdvectionBase.h"

namespace Chimera {
	using namespace Interpolation;
	namespace Advection {

		template <class VectorType, template <class> class VoxelType>
		class AdvectionBuilder : public Singleton<AdvectionBuilder<VectorType, VoxelType> {
		public:

			#pragma region Constructors
			AdvectionBuilder() {
			
			}
			#pragma endregion

			#pragma region Functionalities
			/*template <template <class> class ElementType>
			shared_ptr<ParticleBasedAdvection<VectorType, ElementType, VoxelType>> createParticleBasedAdvection()*/
			
			#pragma endregion
		};


		template <class VectorType, template <class> class VoxelType>
		class SemiLagrangianAdvectionT : public SemiLagrangianAdvectionBase<VectorType, VoxelType> {
		
			public:
			#pragma Constructors 
			template <class ... Args>
			SemiLagrangianAdvectionT(Args&& ... args) : SemiLagrangianAdvectionBase(std::forward<Args>(args)...) {
				
			}
			#pragma endregion


			#pragma region UpdateFunctioons
			virtual void advect(const string& velocityDst, const string& velocityTarget, shared_ptr<Interpolant<VectorType, GridMeshType>> pVelocityInterpolant, Scalar dt) { 
			
			};

			virtual void advect(Scalar dt) override { 
				advect("velocity", "auxVelocity", SemiLagrangianAdvectionBase<VectorType, VoxelType>::m_pVelocityInterpolant, dt);
			};

			/* Optional post projection update. Particle based advection schemes will use this*/
			virtual void postProjectionUpdate(Scalar dt) override { 
				for (auto pInterpolant : SemiLagrangianAdvectionBase<VectorType, VoxelType>::m_scalarFieldsInterpolants) {
					advectDensityField(pInterpolant, dt);
				}
			};

			virtual void advectDensityField(shared_ptr<Interpolant<Scalar, GridMeshType>> pInterpolant, Scalar dt) { };
			#pragma endregion
		};

		template <class VectorType>
		class SemiLagrangianAdvectionT<VectorType, QuadCell> : public SemiLagrangianAdvectionBase<VectorType, QuadCell> {
		
			public:
		
			#pragma region UpdateFunctioons
			virtual void advect(const string& velocitySource, const string& velocityTarget, shared_ptr<Interpolant<VectorType, QuadCell, VectorType>> pVelocityInterpolant, Scalar dt);

			/** Advects a density field with a given interpolant. Density field name is the same as the interpolant attribute field */
			virtual void advectDensityField(shared_ptr<Interpolant<Scalar, QuadCell, VectorType>> pInterpolant, Scalar dt);
			#pragma endregion
		};

		template <class VectorType>
		class SemiLagrangianAdvectionT<VectorType, HexaVolume> : public SemiLagrangianAdvectionBase<VectorType, HexaVolume> {
		
			public:
		
			#pragma region UpdateFunctioons
			virtual void advect(const string& velocitySource, const string& velocityTarget, shared_ptr<Interpolant<VectorType, HexaVolume, VectorType>> pVelocityInterpolant, Scalar dt);

			/** Advects a density field with a given interpolant. Density field name is the same as the interpolant attribute field */
			virtual void advectDensityField(shared_ptr<Interpolant<Scalar, HexaVolume, VectorType>> pInterpolant, Scalar dt);
			#pragma endregion
		};

		template <class VectorType, template <class> class VoxelType>
		using SemiLagrangianAdvection = SemiLagrangianAdvectionT<VectorType, VoxelType>;
	}
}

#endif
