//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef _ADVECTION_SEMI_LAGRANGIAN_H_
#define _ADVECTION_SEMI_LAGRANGIAN_H_
#pragma once

#include "ChimeraCore.h"
#include "ChimeraInterpolation.h"
//
#include "Integration/PositionIntegrator.h"
#include "AdvectionBase.h"

namespace Chimera {
	using namespace Interpolation;
	namespace Advection {

		template <typename GridMeshType>
		class SemiLagrangianAdvection : public AdvectionBase {
		protected:
			typedef typename GridMeshType::VectorType VectorType;
			
		public:

			#pragma region Constructors
			SemiLagrangianAdvection(const baseParams_t & baseParams, shared_ptr<GridMesh<GridMeshType>> pMesh,
																	shared_ptr<PositionIntegrator<GridMeshType>> pPositionIntegrator,
																	shared_ptr<Interpolant<VectorType, GridMeshType>> pVelocityInterpolation) 
																	: AdvectionBase(baseParams) {
				m_pGridMesh = pMesh;
				m_pPositionIntegrator = pPositionIntegrator;
				m_pVelocityInterpolant = pVelocityInterpolation;
			}
			#pragma endregion

			#pragma region AccessFunctions
			shared_ptr<Interpolant<VectorType, GridMeshType>> getVelocityInterpolant() {
				return m_pVelocityInterpolant;
			}
			
			void setVelocityInterpolant(shared_ptr <Interpolant<VectorType, GridMeshType>> pVelocityInterpolant) {
				m_pVelocityInterpolant = pVelocityInterpolant;
			}

			const vector<shared_ptr<Interpolant<Scalar, GridMeshType>>> & getScalarFieldInterpolants() const {
				return m_scalarFieldsInterpolants;
			}

			virtual void addScalarFieldInterpolant(shared_ptr<Interpolant<Scalar, GridMeshType>> pInterpolant) {
				m_scalarFieldsInterpolants.push_back(pInterpolant);
			}
			#pragma endregion

			#pragma region Functionalities
			virtual void advect(Scalar dt) override {
				if constexpr (isQuadGridType<GridMeshType>::value) {
					advectVectorFieldQuadGrid("velocity", "auxVelocity", m_pVelocityInterpolant, dt);
				}
				else if constexpr (isHexaGridType<GridMeshType>::value) {
					advectVectorFieldHexaGrid("velocity", "auxVelocity", m_pVelocityInterpolant, dt);
				}
			};

			/* Optional post projection update. Particle based advection schemes will use this*/
			virtual void postProjectionUpdate(Scalar dt) override {
				if constexpr (isQuadGridType<GridMeshType>::value) {
					for (auto pInterpolant : m_scalarFieldsInterpolants) {
						advectScalarFieldQuadGrid(pInterpolant, dt);
					}
				} else if constexpr (isHexaGridType<GridMeshType>::value) {
					for (auto pInterpolant : m_scalarFieldsInterpolants) {
						advectScalarFieldHexaGrid(pInterpolant, dt);
					}
				}
			};
			#pragma endregion

		protected:

			#pragma region ClassMembers
			/** Mesh */
			shared_ptr<GridMesh<GridMeshType>> m_pGridMesh;

			/** Interpolants */
			shared_ptr<Interpolant<VectorType, GridMeshType>> m_pVelocityInterpolant;
			vector<shared_ptr<Interpolant<Scalar, GridMeshType>>> m_scalarFieldsInterpolants;
			
			/** Integrator */
			shared_ptr<PositionIntegrator<GridMeshType>> m_pPositionIntegrator;
			#pragma endregion


			#pragma region PrivateFunctionalities
			//QuadGrid Advection
			virtual void advectScalarFieldQuadGrid(shared_ptr<Interpolant<Scalar, GridMeshType>> pInterpolant, Scalar dt);
			virtual void advectVectorFieldQuadGrid(const string& velocityDst, const string& velocityTarget, shared_ptr<Interpolant<VectorType, GridMeshType>> pVelocityInterpolant, Scalar dt);

			//HexaGrid Advection
			virtual void advectScalarFieldHexaGrid(shared_ptr<Interpolant<Scalar, GridMeshType>> pInterpolant, Scalar dt);
			virtual void advectVectorFieldHexaGrid(const string& velocityDst, const string& velocityTarget, shared_ptr<Interpolant<VectorType, GridMeshType>> pVelocityInterpolant, Scalar dt);
			#pragma endregion

		};

	}
}

#endif

