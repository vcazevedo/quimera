//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met:
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies,
//	either expressed or implied, of the FreeBSD Project.

#ifndef _ADVECTION_MAC_CORMACK_H_
#define _ADVECTION_MAC_CORMACK_H_
#pragma once

#include "GridBased/SemiLagrangianAdvection.h"

namespace Chimera {

	namespace Advection {

		template <typename GridMeshType>
		class MacCormackAdvection : public SemiLagrangianAdvection<GridMeshType> {

		protected:
			//Useful definitions
			using VectorType = typename GridMeshType::VectorType;
			using ParentClass = SemiLagrangianAdvection<GridMeshType>;

		public:

			#pragma region Constructors
			template <class ... Args>
			MacCormackAdvection(Args&& ... args) : SemiLagrangianAdvection<GridMeshType>(std::forward<Args>(args)...) {
				if constexpr (isQuadGridType<GridMeshType>::value) {
					QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<GridMeshType>*>(ParentClass::m_pGridMesh.get());

					pQuadGridMesh->template addVerticesAttribute<VectorType, BilinearNodalInterpolant2D<Scalar, GridMeshType>>("velocityMinLimiter", 0.0f);
					pQuadGridMesh->template addVerticesAttribute<VectorType, BilinearNodalInterpolant2D<Scalar, GridMeshType>>("velocityMaxLimiter", 0.0f);
					pQuadGridMesh->template addEdgesAttribute<Scalar, BilinearStaggeredInterpolant2D<VectorType, GridMeshType>>("auxVelocityMacCormack", 0.0f);

					m_minLimiterInterpolant = pQuadGridMesh->getVectorFieldInterpolantVertices("velocityMinLimiter");
					m_maxLimiterInterpolant = pQuadGridMesh->getVectorFieldInterpolantVertices("velocityMaxLimiter");

				} else if constexpr (isHexaGridType<GridMeshType>::value) {
					HexaGridMesh<GridMeshType>* pHexaGridMesh = dynamic_cast<HexaGridMesh<GridMeshType>*>(ParentClass::m_pGridMesh.get());

					//pHexaGridMesh->template addVerticesAtribute<VectorType>("velocityMinLimiter", VectorType());
					//pHexaGridMesh->template addVerticesAtribute<VectorType>("velocityMaxLimiter", VectorType());
					//pHexaGridMesh->template addCellsAttribute<VectorType>("auxVelocityMacCormack", VectorType());

					//m_minLimiterInterpolant = make_unique<TrilinearNodalInterpolant3D>(pHexaGridMesh, "velocityMinLimiter");
					//m_maxLimiterInterpolant = make_unique<TrilinearNodalInterpolant3D>(pHexaGridMesh, "velocityMaxLimiter");
				}
			}
			#pragma endregion

			#pragma region AccessFunctions
			virtual void addScalarFieldInterpolant(shared_ptr<Interpolant<Scalar, GridMeshType>> pInterpolant) override {
				ParentClass::m_scalarFieldsInterpolants.push_back(pInterpolant);

				if constexpr (isQuadGridType<GridMeshType>::value) {
					QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<GridMeshType>*>(ParentClass::m_pGridMesh.get());

					/** Adding limiter tracking for each scalar field advected attribute*/
					pQuadGridMesh->template addVerticesAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, GridMeshType>>(pInterpolant->getInterpolatedAttributeName() + "minLimiter", 0.0f);
					pQuadGridMesh->template addVerticesAttribute<Scalar, BilinearNodalInterpolant2D<Scalar, GridMeshType>>(pInterpolant->getInterpolatedAttributeName() + "maxLimiter", 0.0f);
				}
			}
			#pragma endregion

			#pragma region UpdateFunctions
			virtual void advect(Scalar dt) override {
				/** Standard Semi-Lagrangian step first */
				ParentClass::advect(dt);

				if constexpr (isQuadGridType<GridMeshType>::value) {
					/** Do the Semi-Lagrangian step, but this time go forward (-dt) and store the result on a custom auxVelocity2 field */
				ParentClass::advectVectorFieldQuadGrid("auxVelocity", "auxVelocityMacCormack",
																								ParentClass::m_pVelocityInterpolant->getSibilingInterpolant(),
																								-dt);
					applyCorrectionsQuadGrid(dt);
					updateVelocityLimitersQuadGrid();
					applyVelocityLimitersQuadGrid(dt);

				}
				else if constexpr (isHexaGridType<GridMeshType>::value) {
					/** Do the Semi-Lagrangian step, but this time go forward (-dt) and store the result on a custom auxVelocity2 field */
				ParentClass::advectVectorFieldHexaGrid("auxVelocity", "auxVelocityMacCormack",
																								ParentClass::m_pVelocityInterpolant->getSibilingInterpolant(),
																								-dt);
					applyCorrectionsHexaGrid(dt);
					updateVelocityLimitersHexaGrid();
					applyVelocityLimitersHexaGrid(dt);
				}
			};

			/* Optional post projection update. Particle based advection schemes will use this*/
			virtual void postProjectionUpdate(Scalar dt) override { };
			#pragma endregion

		protected:

			#pragma PrivateFunctionalities
			virtual void applyCorrectionsQuadGrid(Scalar dt);
			virtual void updateVelocityLimitersQuadGrid();
			virtual void applyVelocityLimitersQuadGrid(Scalar dt);

			virtual void applyCorrectionsHexaGrid(Scalar dt);
			virtual void updateVelocityLimitersHexaGrid();
			virtual void applyVelocityLimitersHexaGrid(Scalar dt);
			#pragma endregion

			#pragma ClassMembers
			shared_ptr<Interpolant<VectorType, GridMeshType>> m_minLimiterInterpolant;
			shared_ptr<Interpolant<VectorType, GridMeshType>> m_maxLimiterInterpolant;
			#pragma endregion
		};
	}
}

#endif
