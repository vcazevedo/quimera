#include "Integration/RungeKutta2Integrator.h"

namespace Chimera {


	
	namespace Advection {

		#pragma region Functionalities
		template<typename GridMeshType>
		typename GridMeshType::VectorType RungeKutta2Integrator<GridMeshType>::integrate(const VectorType &position, const VectorType &velocity,
																			Scalar dt, shared_ptr<Interpolant<VectorType, GridMeshType>> pCustomInterpolant) {
			/** Midpoint step */
			VectorType tempPos = position + velocity*dt*0.5;

			PositionIntegrator<GridMeshType>::clampPosition(tempPos);

			VectorType collisionPoint;
			if (PositionIntegrator<GridMeshType>::checkCollision(position, tempPos, collisionPoint)) {
				return position;
			} 

			VectorType interpVel;
			if (pCustomInterpolant) {
				interpVel = pCustomInterpolant->interpolate(tempPos);
				tempPos = position + interpVel*dt;
			}
			else {
				interpVel = this->m_pInterpolant->interpolate(tempPos);
				tempPos = position + interpVel*dt;
			}

			PositionIntegrator<GridMeshType>::clampPosition(tempPos);

			if (PositionIntegrator<GridMeshType>::checkCollision(position, tempPos, collisionPoint)) {
				return position;
			}

			return tempPos;
		}
		#pragma endregion

		/** Template linker trickerino for templated classes in CPP*/
		template class RungeKutta2Integrator<QuadGridType>;
		template class RungeKutta2Integrator<QuadGridTypeDouble>;

		template class RungeKutta2Integrator<HexaGridType>;
		template class RungeKutta2Integrator<HexaGridTypeDouble>;
	}

	
}
