#include "Integration/PositionIntegrator.h"


namespace Chimera {

	namespace Advection {

		template<typename GridMeshType>
		bool PositionIntegrator<GridMeshType>::checkCollision(const VectorType& p1, const VectorType& p2, VectorType& collisionPoint) {
			typedef typename GridMeshType::VectorType VectorType;
			if constexpr (isQuadGridType<GridMeshType>::value) { //Checks collisions for QuadGridMeshes
				QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<GridMeshType>*>(m_pGridMesh.get());

				if (pQuadGridMesh) {
					dimensions_t gridPosition1(p1[0] / pQuadGridMesh->getGridSpacing(), p1[1] / pQuadGridMesh->getGridSpacing());
					gridPosition1.x = clamp(gridPosition1.x, 0, m_pGridMesh->getGridDimensions().x - 1);
					gridPosition1.y = clamp(gridPosition1.y, 0, m_pGridMesh->getGridDimensions().y - 1);
					auto pCell1 = pQuadGridMesh->getQuadCell(gridPosition1.x, gridPosition1.y)->getCell(p1);
					if (pCell1->crossedThroughGeometry(p1, p2, collisionPoint)) {
						return true;
					}

					dimensions_t gridPosition2(p2[0] / pQuadGridMesh->getGridSpacing(), p2[1] / pQuadGridMesh->getGridSpacing());
					gridPosition2.x = clamp(gridPosition2.x, 0, m_pGridMesh->getGridDimensions().x - 1);
					gridPosition2.y = clamp(gridPosition2.y, 0, m_pGridMesh->getGridDimensions().y - 1);
					auto pCell2 = pQuadGridMesh->getQuadCell(gridPosition2.x, gridPosition2.y)->getCell(p2);
					if (pCell2->crossedThroughGeometry(p1, p2, collisionPoint)) {
						return true;
					}
				}
				else {
					throw std::logic_error("PositionIntegrator::checkCollision: invalid GridMeshType");
				}
				return false;
			}
			else { //Checks collisions for HexaGridMeshes
				HexaGridMesh<GridMeshType>* pHexaGridMesh = dynamic_cast<HexaGridMesh<GridMeshType>*>(m_pGridMesh.get());

				if (pHexaGridMesh) {
					Scalar dx = pHexaGridMesh->getGridSpacing();
					dimensions_t gridPosition1(p1[0] / dx, p1[1] / dx, p1[2] / dx);
					auto pVolume1 = pHexaGridMesh->getHexaVolume(gridPosition1.x, gridPosition1.y, gridPosition1.z)->getVolume(p1);
					if (pVolume1->crossedThroughGeometry(p1, p2, collisionPoint)) {
						return true;
					}

					dimensions_t gridPosition2(p2[0] / dx, p2[1] / dx, p2[2] / dx);
					auto pVolume2 = pHexaGridMesh->getHexaVolume(gridPosition2.x, gridPosition2.y, gridPosition2.z)->getVolume(p2);
					if (pVolume2->crossedThroughGeometry(p1, p2, collisionPoint)) {
						return true;
					}
				}
				else {
					throw std::logic_error("PositionIntegrator::checkCollision: invalid GridMeshType");
				}
				return false;
			}
		}


		template class PositionIntegrator<QuadGridType>;
		template class PositionIntegrator<QuadGridTypeDouble>;
		template class PositionIntegrator<HexaGridType>;
		template class PositionIntegrator<HexaGridTypeDouble>;
	}


}
