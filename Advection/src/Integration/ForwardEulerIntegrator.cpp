#include "Integration/ForwardEulerIntegrator.h"

namespace Chimera {
	
	namespace Advection {

		#pragma region Functionalities
		template<typename GridMeshType>
		typename GridMeshType::VectorType ForwardEulerIntegrator<GridMeshType>::integrate(const VectorType &position, const VectorType &velocity, Scalar dt, shared_ptr<Interpolant<VectorType, GridMeshType>> pCustomInterpolant/* = nullptr*/) {
			VectorType integratedPosition = position + (velocity)*dt;

			PositionIntegrator<GridMeshType>::clampPosition(integratedPosition);
			VectorType collisionPoint;

			if (PositionIntegrator<GridMeshType>::checkCollision(position, integratedPosition, collisionPoint))
				return collisionPoint;

			return integratedPosition;
		}
		#pragma endregion

		/** Template linker trickerino for templated classes in CPP*/
		template class ForwardEulerIntegrator<QuadGridType>;
		template class ForwardEulerIntegrator<QuadGridTypeDouble>;
		//template class ForwardEulerIntegrator<HexaGridType>;
		//template class ForwardEulerIntegrator<HexaGridTypeDouble>;
	}


	
}
