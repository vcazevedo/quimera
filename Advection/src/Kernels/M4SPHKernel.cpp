#include "Kernels/M4SPHKernel.h"

namespace Chimera {

	namespace Advection {
		
		template <class VectorType>
		Scalar M4SPHKernel<VectorType>::calculateKernel(const VectorType &position, const VectorType & destPosition) {
			Scalar r = (position - destPosition).length();
			if (r > 2) {
				return 0;
			} else if (r >= 1 && r <= 2) {
				return 0.5*(2 - r)*(2 - r)*(1 - r);
			}
			else { //r >= 0, r <= 1
				return 1 - 2.5*r*r + 1.5*r*r*r;
			}
		}

		template class M4SPHKernel<Vector2>;
		template class M4SPHKernel<Vector2D>;
		template class M4SPHKernel<Vector3>;
		template class M4SPHKernel<Vector3D>;
	}
}
