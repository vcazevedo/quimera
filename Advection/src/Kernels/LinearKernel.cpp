#include "Kernels/LinearKernel.h"

namespace Chimera {
	
	namespace Advection {
		template <class VectorType>
		Scalar LinearKernelT<VectorType, true>::calculateKernel(const VectorType &position, const VectorType &destPosition) {
			Vector2 relativeFractions;
			relativeFractions.x = roundClamp<Scalar>(position.x - destPosition.x, 0.f, 1.f);
			relativeFractions.y = roundClamp<Scalar>(position.y - destPosition.y, 0.f, 1.f);
			return ((1 - relativeFractions.x)*(1 - relativeFractions.y));
		}

		template <class VectorType>
		Scalar LinearKernelT<VectorType, false>::calculateKernel(const VectorType &position, const VectorType &destPosition) {
			Vector3 relativeFractions;
			relativeFractions.x = roundClamp<Scalar>(position.x - destPosition.x, 0.f, 1.f);
			relativeFractions.y = roundClamp<Scalar>(position.y - destPosition.y, 0.f, 1.f);
			relativeFractions.z = roundClamp<Scalar>(position.z - destPosition.z, 0.f, 1.f);
			return (1 - relativeFractions.x)*(1 - relativeFractions.y)*(1 - relativeFractions.z);
		}

		template class LinearKernelT<Vector2, isVector2<Vector2>::value>;
		template class LinearKernelT<Vector2D, isVector2<Vector2D>::value>;

		template class LinearKernelT<Vector3, isVector2<Vector3>::value>;
		template class LinearKernelT<Vector3D, isVector2<Vector3D>::value>;
	}
	
}
