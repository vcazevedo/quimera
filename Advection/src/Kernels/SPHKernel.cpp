#include "Kernels/SPHKernel.h"

namespace Chimera {

	namespace Advection {
		
		template <class VectorType>
		Scalar SPHKernel<VectorType>::calculateKernel(const VectorType &position, const VectorType & destPosition) {
			Scalar r = (position - destPosition).length();
			return (15.0f / (PI * pow(this->m_kernelSize, 6)))*pow(this->m_kernelSize*this->m_kernelSize - r*r, 3);
		}

		template class SPHKernel<Vector2>;
		template class SPHKernel<Vector2D>;
		template class SPHKernel<Vector3>;
		template class SPHKernel<Vector3D>;
	}
}
