#include "GridBased/SemiLagrangianAdvection.h"


namespace Chimera {

	namespace Advection {
		
		#pragma region UpdateFunctions
		template <typename GridMeshType>
		void SemiLagrangianAdvection<GridMeshType>::advectVectorFieldQuadGrid(const string& velocitySource, const string &velocityTarget,
																																					shared_ptr<Interpolant<VectorType, GridMeshType>> pVelocityInterpolant, 
																																					Scalar dt) {
			
			if constexpr (isQuadGridType<GridMeshType>::value) { //Only if GridMeshType represents a QuadGrid
				QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<GridMeshType>*>(pVelocityInterpolant->getGridMesh().get());
				
				Scalar dx = pQuadGridMesh->getGridSpacing();

				for (int i = 1; i < pQuadGridMesh->getGridDimensions().x - 1; i++) {
					for (int j = 1; j < pQuadGridMesh->getGridDimensions().y - 1; j++) {
						//X velocity components first
						for (auto pEdge : pQuadGridMesh->getEdges(dimensions_t(i, j), yAlignedEdge)) {
							//Calculate virtual particle initial velocity
							VectorType position = pEdge->getCentroid();
							VectorType velocity = pVelocityInterpolant->interpolate(position);
							velocity.x = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute(velocitySource);
							
							/** Use negative time-step to go backwards in time */
							position = m_pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);
							velocity = pVelocityInterpolant->interpolate(position);
							OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute(velocityTarget, velocity.x);
						}

						//Y Velocity components 
						for (auto pEdge : pQuadGridMesh->getEdges(dimensions_t(i, j), xAlignedEdge)) {
							//Calculate virtual particle initial velocity
							VectorType position = pEdge->getCentroid();
							VectorType velocity = pVelocityInterpolant->interpolate(position);
							velocity.y = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute(velocitySource);
							
							/** Use negative time-step to go backwards in time */
							position = m_pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);
							velocity = pVelocityInterpolant->interpolate(position);
							OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute(velocityTarget, velocity.y);
						}
					}
				}
			}
		}

		template <typename GridMeshType>
		void SemiLagrangianAdvection<GridMeshType>::advectScalarFieldQuadGrid(shared_ptr<Interpolant<Scalar, GridMeshType>> pInterpolant, Scalar dt) {
			if constexpr (isQuadGridType<GridMeshType>::value) { //Only if GridMeshType represents a QuadGrid
				QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<GridMeshType>*>(pInterpolant->getGridMesh().get());

				/** Aliasing for smaller ref names */
				Scalar dx = pQuadGridMesh->getGridSpacing();

				for (auto pVertex : pQuadGridMesh->getVertices()) {
					if (!pVertex->isBorder()) {
						VectorType position = pVertex->getPosition();
						VectorType velocity = m_pVelocityInterpolant->interpolate(position);

						/** Use negative time-step to go backwards in time */
						position = m_pPositionIntegrator->integrate(position, velocity, -dt);
						OwnCustomAttribute<Scalar>::get(pVertex)->setAttribute(pInterpolant->getInterpolatedAttributeName(), pInterpolant->interpolate(position));
					}
					
				}
			}
			
		}

		template <typename GridMeshType>
		void SemiLagrangianAdvection<GridMeshType>::advectVectorFieldHexaGrid(const string& velocitySource, const string &velocityTarget,
																																					shared_ptr<Interpolant<VectorType, GridMeshType>> pVelocityInterpolant,
																																					Scalar dt) {
			if constexpr (isHexaGridType<GridMeshType>::value) { //Only if GridMeshType represents a HexaGrid

				HexaGridMesh<GridMeshType>* pHexaGridMesh = dynamic_cast<HexaGridMesh<GridMeshType>*>(pVelocityInterpolant->getGridMesh().get());

				Scalar dx = pHexaGridMesh->getGridSpacing();

				for (int i = 1; i < pHexaGridMesh->getGridDimensions().x - 1; i++) {
					for (int j = 1; j < pHexaGridMesh->getGridDimensions().y - 1; j++) {
						for (int k = 1; k < pHexaGridMesh->getGridDimensions().z - 1; k++) {

							//X velocity
							for (auto pCell : pHexaGridMesh->getCells(dimensions_t(i, j, k), YZPlane)) {
								VectorType position = pCell->getCentroid();
								VectorType velocity = pVelocityInterpolant->interpolate(position);
								velocity.x = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute(velocitySource).x;

								/** Use negative time-step to go backwards in time */
								position = m_pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);
								velocity = pVelocityInterpolant->interpolate(position);
								OwnCustomAttribute<VectorType>::get(pCell)->setAttribute(velocityTarget, velocity);
							}

							//Y velocity
							for (auto pCell : pHexaGridMesh->getCells(dimensions_t(i, j, k), XZPlane)) {
								VectorType position = pCell->getCentroid();
								VectorType velocity = pVelocityInterpolant->interpolate(position);
								velocity.y = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute(velocitySource).y;

								/** Use negative time-step to go backwards in time */
								position = m_pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);
								velocity = pVelocityInterpolant->interpolate(position);
								OwnCustomAttribute<VectorType>::get(pCell)->setAttribute(velocityTarget, velocity);
							}

							//Z velocity
							for (auto pCell : pHexaGridMesh->getCells(dimensions_t(i, j, k), XYPlane)) {
								VectorType position = pCell->getCentroid();
								VectorType velocity = pVelocityInterpolant->interpolate(position);
								velocity.z = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute(velocitySource).z;

								/** Use negative time-step to go backwards in time */
								position = m_pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);
								velocity = pVelocityInterpolant->interpolate(position);
								OwnCustomAttribute<VectorType>::get(pCell)->setAttribute(velocityTarget, velocity);
							}
						}
					}
				}
			}
		}

		template <typename GridMeshType>
		void SemiLagrangianAdvection<GridMeshType>::advectScalarFieldHexaGrid(shared_ptr<Interpolant<Scalar, GridMeshType>> pInterpolant, Scalar dt) {
			if constexpr (isHexaGridType<GridMeshType>::value) { //Only if GridMeshType represents a HexaGrid
				HexaGridMesh<GridMeshType>* pHexaGridMesh = dynamic_cast<HexaGridMesh<GridMeshType>*>(pInterpolant->getGridMesh().get());

				Scalar dx = pHexaGridMesh->getGridSpacing();

				for (int i = 1; i < pHexaGridMesh->getGridDimensions().x - 1; i++) {
					for (int j = 1; j < pHexaGridMesh->getGridDimensions().y - 1; j++) {
						for (int k = 1; k < pHexaGridMesh->getGridDimensions().z - 1; k++) {
							for (auto pVolume : pHexaGridMesh->getHexaVolume(i, j, k)->getVolumes()) {
								VectorType position = pVolume->getCentroid();
								VectorType velocity = m_pVelocityInterpolant->interpolate(position);

								/** Use negative time-step to go backwards in time */
								position = m_pPositionIntegrator->integrate(position, velocity, -dt);

								OwnCustomAttribute<Scalar>::get(pVolume)->setAttribute(pInterpolant->getInterpolatedAttributeName(), pInterpolant->interpolate(position));
							}
						}
					}
				}
			}
		}
		#pragma endregion

		template class SemiLagrangianAdvection<QuadGridType>;
		template class SemiLagrangianAdvection<QuadGridTypeDouble>;

		template class SemiLagrangianAdvection<HexaGridType>;
		template class SemiLagrangianAdvection<HexaGridTypeDouble>;
	}
}
