#include "GridBased/MacCormackAdvection.h"


namespace Chimera {

	namespace Advection {
		
		#pragma region PrivateFunctionalities
		template <typename GridMeshType>
		void MacCormackAdvection<GridMeshType>::applyCorrectionsQuadGrid(Scalar dt) {
			if constexpr (isQuadGridType<GridMeshType>::value) {
				QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<GridMeshType>*>(ParentClass::m_pGridMesh.get());

				/** Apply corrections */
				for (uint i = 2; i < pQuadGridMesh->getGridDimensions().x - 2; i++) {
					for (uint j = 2; j < pQuadGridMesh->getGridDimensions().y - 2; j++) {

						for (auto pEdge : pQuadGridMesh->getEdges(dimensions_t(i, j), yAlignedEdge)) {
							Scalar correction = (OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity") -
																				OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("auxVelocityMacCormack")) * 0.5f;

							Scalar auxVelocity = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("auxVelocity") + correction;
							OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute("auxVelocity", auxVelocity);
						}

						for (auto pEdge : pQuadGridMesh->getEdges(dimensions_t(i, j), xAlignedEdge)) {
							Scalar correction = (OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity") -
																				OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("auxVelocityMacCormack")) * 0.5f;

							Scalar auxVelocity = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("auxVelocity") + correction;
							OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute("auxVelocity", auxVelocity);
						}
					}
				}
			}			
		}

		
		template <typename GridMeshType>
		void MacCormackAdvection<GridMeshType>::updateVelocityLimitersQuadGrid() {
			if constexpr (isQuadGridType<GridMeshType>::value) {
				QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<GridMeshType>*>(ParentClass::m_pGridMesh.get());

				for (uint i = 0; i <= pQuadGridMesh->getGridDimensions().x; i++) {
					for (uint j = 0; j <= pQuadGridMesh->getGridDimensions().y; j++) {
						auto pVertex = pQuadGridMesh->getVertex(i, j);
						Scalar minLimiterX = FLT_MAX, minLimiterY = FLT_MAX, maxLimiterX = -FLT_MAX, maxLimiterY = -FLT_MAX;

						for (auto pEdge : OwnConnectedStructure<Edge<VectorType>>::get(pVertex)->getConnectedStructures()) {
							if (pEdge->getType() == yAlignedEdge) {
								minLimiterX = std::min<Scalar>(minLimiterX, OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity"));
								maxLimiterX = std::max<Scalar>(maxLimiterX, OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity"));
							}
							else if (pEdge->getType() == xAlignedEdge) {
								minLimiterY = std::min<Scalar>(minLimiterY, OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity"));
								maxLimiterY = std::max<Scalar>(maxLimiterY, OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity"));
							}
						}
						OwnCustomAttribute<VectorType>::get(pVertex)->setAttribute("velocityMinLimiter", VectorType(minLimiterX, minLimiterY));
						OwnCustomAttribute<VectorType>::get(pVertex)->setAttribute("velocityMaxLimiter", VectorType(maxLimiterX, maxLimiterY));
					}
				}
			}
		}

		template <typename GridMeshType>
		void MacCormackAdvection<GridMeshType>::applyVelocityLimitersQuadGrid(Scalar dt) {
			if constexpr (isQuadGridType<GridMeshType>::value) {
				QuadGridMesh<GridMeshType>* pQuadGridMesh = dynamic_cast<QuadGridMesh<GridMeshType>*>(ParentClass::m_pGridMesh.get());
				auto pPositionIntegrator = ParentClass::m_pPositionIntegrator;
				auto pVelocityInterpolant = ParentClass::m_pVelocityInterpolant;
				for (uint i = 0; i <= pQuadGridMesh->getGridDimensions().x; i++) {
					for (uint j = 0; j <= pQuadGridMesh->getGridDimensions().y; j++) {
						for (auto pEdge : pQuadGridMesh->getEdges(dimensions_t(i, j), yAlignedEdge)) {
							VectorType position = pEdge->getCentroid();
							VectorType velocity = pVelocityInterpolant->interpolate(position);
							velocity.x = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity");

							/** Use negative time-step to go backwards in time */
							position = pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);

							VectorType interpolatedMinVelocity = m_minLimiterInterpolant->interpolate(position);
							VectorType interpolatedMaxVelocity = m_maxLimiterInterpolant->interpolate(position);
							Scalar auxVelocity = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("auxVelocity");
							if (auxVelocity < interpolatedMinVelocity.x) {
								auxVelocity = interpolatedMinVelocity.x;
								OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute("auxVelocity", auxVelocity);
							}
							else if (auxVelocity > interpolatedMaxVelocity.x) {
								auxVelocity = interpolatedMaxVelocity.x;
								OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute("auxVelocity", auxVelocity);
							}
						}

						for (auto pEdge : pQuadGridMesh->getEdges(dimensions_t(i, j), xAlignedEdge)) {
							VectorType position = pEdge->getCentroid();
							VectorType velocity = pVelocityInterpolant->interpolate(position);
							velocity.y = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("velocity");

							/** Use negative time-step to go backwards in time */
							position = pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);

							VectorType interpolatedMinVelocity = m_minLimiterInterpolant->interpolate(position);
							VectorType interpolatedMaxVelocity = m_maxLimiterInterpolant->interpolate(position);
							Scalar auxVelocity = OwnCustomAttribute<Scalar>::get(pEdge)->getAttribute("auxVelocity");
							if (auxVelocity < interpolatedMinVelocity.y) {
								auxVelocity = interpolatedMinVelocity.y;
								OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute("auxVelocity", auxVelocity);
							}
							else if (auxVelocity > interpolatedMaxVelocity.y) {
								auxVelocity = interpolatedMaxVelocity.y;
								OwnCustomAttribute<Scalar>::get(pEdge)->setAttribute("auxVelocity", auxVelocity);
							}
						}
					}
				}
			}
			
		}

		
		template <typename GridMeshType>
		void MacCormackAdvection<GridMeshType>::applyCorrectionsHexaGrid(Scalar dt) {
			if constexpr (isHexaGridType<GridMeshType>::value) {
				HexaGridMesh<GridMeshType>* pHexaGridMesh = dynamic_cast<HexaGridMesh<GridMeshType>*>(ParentClass::m_pGridMesh.get());
				/** Apply corrections */
				for (uint i = 2; i < pHexaGridMesh->getGridDimensions().x - 2; i++) {
					for (uint j = 2; j < pHexaGridMesh->getGridDimensions().y - 2; j++) {
						for (uint k = 2; k < pHexaGridMesh->getGridDimensions().z - 2; k++) {
							//X velocity
							for (auto pCell : pHexaGridMesh->getCells(dimensions_t(i, j, k), YZPlane)) {
								VectorType correction = (OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity") -
									OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("auxVelocityMacCormack")) * 0.5f;

								VectorType auxVelocity = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("auxVelocity") + correction;
								OwnCustomAttribute<VectorType>::get(pCell)->setAttribute("auxVelocity", auxVelocity);
							}

							//Y velocity
							for (auto pCell : pHexaGridMesh->getCells(dimensions_t(i, j, k), XZPlane)) {
								VectorType correction = (OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity") -
									OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("auxVelocityMacCormack")) * 0.5f;

								VectorType auxVelocity = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("auxVelocity") + correction;
								OwnCustomAttribute<VectorType>::get(pCell)->setAttribute("auxVelocity", auxVelocity);
							}

							//Z velocity
							for (auto pCell : pHexaGridMesh->getCells(dimensions_t(i, j, k), XYPlane)) {
								VectorType correction = (OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity") -
									OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("auxVelocityMacCormack")) * 0.5f;

								VectorType auxVelocity = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("auxVelocity") + correction;
								OwnCustomAttribute<VectorType>::get(pCell)->setAttribute("auxVelocity", auxVelocity);
							}
						}
					}
				}
			}
		}

		template <typename GridMeshType>
		void MacCormackAdvection<GridMeshType>::updateVelocityLimitersHexaGrid() {
			if constexpr (isHexaGridType<GridMeshType>::value) {
				HexaGridMesh<GridMeshType>* pHexaGridMesh = dynamic_cast<HexaGridMesh<GridMeshType>*>(ParentClass::m_pGridMesh.get());

				for (uint i = 0; i <= pHexaGridMesh->getGridDimensions().x; i++) {
					for (uint j = 0; j <= pHexaGridMesh->getGridDimensions().y; j++) {
						for (uint k = 0; k <= pHexaGridMesh->getGridDimensions().z; k++) {
							auto pVertex = pHexaGridMesh->getVertex(i, j, k);
							Scalar	minLimiterX = FLT_MAX, minLimiterY = FLT_MAX, minLimiterZ = FLT_MAX,
								maxLimiterX = -FLT_MAX, maxLimiterY = -FLT_MAX, maxLimiterZ = -FLT_MAX;

							for (auto pCell : OwnConnectedStructure<Cell<VectorType>>::get(pVertex)->getConnectedStructures()) {
								if (pCell->getLocation() == YZPlane) {
									minLimiterX = std::min<Scalar>(minLimiterX, OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity").x);
									maxLimiterX = std::max<Scalar>(maxLimiterX, OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity").x);
								}
								else if (pCell->getLocation() == XZPlane) {
									minLimiterY = std::min<Scalar>(minLimiterY, OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity").y);
									maxLimiterY = std::max<Scalar>(maxLimiterY, OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity").y);
								}
								else if (pCell->getLocation() == XYPlane) {
									minLimiterZ = std::min<Scalar>(minLimiterZ, OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity").z);
									maxLimiterZ = std::max<Scalar>(maxLimiterZ, OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity").z);
								}
							}
							OwnCustomAttribute<VectorType>::get(pVertex)->setAttribute("velocityMinLimiter", VectorType(minLimiterX, minLimiterY, minLimiterZ));
							OwnCustomAttribute<VectorType>::get(pVertex)->setAttribute("velocityMaxLimiter", VectorType(maxLimiterX, maxLimiterY, maxLimiterZ));
						}
					}
				}
			}
		}

		template <typename GridMeshType>
		void MacCormackAdvection<GridMeshType>::applyVelocityLimitersHexaGrid(Scalar dt) {
			if constexpr (isHexaGridType<GridMeshType>::value) {
				HexaGridMesh<GridMeshType>* pHexaGridMesh = dynamic_cast<HexaGridMesh<GridMeshType>*>(ParentClass::m_pGridMesh.get());

				auto pPositionIntegrator = ParentClass::m_pPositionIntegrator;
				auto pVelocityInterpolant = ParentClass::m_pVelocityInterpolant;

				for (uint i = 0; i <= pHexaGridMesh->getGridDimensions().x; i++) {
					for (uint j = 0; j <= pHexaGridMesh->getGridDimensions().y; j++) {
						for (uint k = 0; k <= pHexaGridMesh->getGridDimensions().z; k++) {

							//X velocity
							for (auto pCell : pHexaGridMesh->getCells(dimensions_t(i, j, k), YZPlane)) {
								VectorType position = pCell->getCentroid();
								VectorType velocity = pVelocityInterpolant->interpolate(position);
								velocity.x = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity").x;

								/** Use negative time-step to go backwards in time */
								position = pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);

								VectorType interpolatedMinVelocity = m_minLimiterInterpolant->interpolate(position);
								VectorType interpolatedMaxVelocity = m_maxLimiterInterpolant->interpolate(position);
								VectorType auxVelocity = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("auxVelocity");

								if (auxVelocity.x < interpolatedMinVelocity.x) {
									auxVelocity.x = interpolatedMinVelocity.x;
									OwnCustomAttribute<VectorType>::get(pCell)->setAttribute("auxVelocity", auxVelocity);
								}
								else if (auxVelocity.x > interpolatedMaxVelocity.x) {
									auxVelocity.x = interpolatedMaxVelocity.x;
									OwnCustomAttribute<VectorType>::get(pCell)->setAttribute("auxVelocity", auxVelocity);
								}
							}

							//Y velocity
							for (auto pCell : pHexaGridMesh->getCells(dimensions_t(i, j, k), XZPlane)) {
								VectorType position = pCell->getCentroid();
								VectorType velocity = pVelocityInterpolant->interpolate(position);
								velocity.y = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity").y;

								/** Use negative time-step to go backwards in time */
								position = pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);

								VectorType interpolatedMinVelocity = m_minLimiterInterpolant->interpolate(position);
								VectorType interpolatedMaxVelocity = m_maxLimiterInterpolant->interpolate(position);
								VectorType auxVelocity = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("auxVelocity");

								if (auxVelocity.y < interpolatedMinVelocity.x) {
									auxVelocity.y = interpolatedMinVelocity.y;
									OwnCustomAttribute<VectorType>::get(pCell)->setAttribute("auxVelocity", auxVelocity);
								}
								else if (auxVelocity.y > interpolatedMaxVelocity.y) {
									auxVelocity.y = interpolatedMaxVelocity.y;
									OwnCustomAttribute<VectorType>::get(pCell)->setAttribute("auxVelocity", auxVelocity);
								}
							}

							//Z velocity
							for (auto pCell : pHexaGridMesh->getCells(dimensions_t(i, j, k), XYPlane)) {
								VectorType position = pCell->getCentroid();
								VectorType velocity = pVelocityInterpolant->interpolate(position);
								velocity.z = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("velocity").z;

								/** Use negative time-step to go backwards in time */
								position = pPositionIntegrator->integrate(position, velocity, -dt, pVelocityInterpolant);

								VectorType interpolatedMinVelocity = m_minLimiterInterpolant->interpolate(position);
								VectorType interpolatedMaxVelocity = m_maxLimiterInterpolant->interpolate(position);
								VectorType auxVelocity = OwnCustomAttribute<VectorType>::get(pCell)->getAttribute("auxVelocity");

								if (auxVelocity.z < interpolatedMinVelocity.z) {
									auxVelocity.z = interpolatedMinVelocity.z;
									OwnCustomAttribute<VectorType>::get(pCell)->setAttribute("auxVelocity", auxVelocity);
								}
								else if (auxVelocity.z > interpolatedMaxVelocity.z) {
									auxVelocity.z = interpolatedMaxVelocity.z;
									OwnCustomAttribute<VectorType>::get(pCell)->setAttribute("auxVelocity", auxVelocity);
								}
							}
						}
					}
				}
			}
		}
		


#pragma endregion
		
		template class MacCormackAdvection<QuadGridType>;
		template class MacCormackAdvection<QuadGridTypeDouble>;

		template class MacCormackAdvection<HexaGridType>;
		template class MacCormackAdvection<HexaGridTypeDouble>;
	}
}
